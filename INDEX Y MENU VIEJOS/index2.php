<? session_start();?>
<? include("scripts/funciones.php");?>
<? include("scripts/nombres.php"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<? /* if (isset($_SESSION['logPerm'])){
	echo '<script type="text/javascript" language="JavaScript1.2" src="scripts/menu_'.$_SESSION["logPerm"].' /stmenu.js"></script>';
 }else{
 		echo '<script type="text/javascript" language="JavaScript1.2" src="scripts/menu_default/stmenu.js"></script>';
 } */?>
 <script type="text/javascript" src="js/jquery-1.2.3.js"></script>
<script type="text/javascript" src="js/jquery.blockUI.js"></script>
 <script src="scripts/auto_completa.js"></script>
<script language="javascript" src="scripts/Calendar.js"></script>
<script src="scripts/desbloqueo.js"></script>
<script src="scripts/validaciones.js"></script>
<script src="scripts/formatos.js"></script>
<script src="scripts/funcionesjs.js"></script>
<script src="scripts/func_calculo.js"></script>
<script src="scripts/campoVacio.js"></script>
<script type="text/javascript" src="js/prototype-1.5.0.js"></script>
<script src="scripts/funcionesajax.js"></script>
<script src="scripts/javascript/func_ajax.js"></script>
<script src="scripts/javascript/func_estudiantes.js"></script>
<script src="scripts/javascript/func_carrera.js"></script>
<script src="scripts/javascript/func_casosdev.js"></script>
<script src="scripts/javascript/func_cartaaval.js"></script>
<script src="scripts/javascript/func_resumensocio.js"></script>
<script src="scripts/javascript/func_relacion.js"></script>
<script src="scripts/javascript/func_maestro.js"></script>
<script src="scripts/javascript/func_patologias.js"></script>
<script src="scripts/javascript/func_actual_casos.js"></script>
<script src="scripts/javascript/func_universidad.js"></script>
<script src="scripts/javascript/func_clinicas.js"></script>
<script src="scripts/javascript/func_resumedico.js"></script>
<script src="scripts/javascript/func_estado.js"></script>
<!-- CALENDARIO DINAMICO -->
<link type="text/css" rel="stylesheet" href="includes/fechas/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></LINK>
<SCRIPT type="text/javascript" src="includes/fechas/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
<!-- FIN CALENDARIO DINAMICO -->

<link href="scripts/estilos.css" rel="stylesheet" type="text/css">
<link href="scripts/estilos2.css" rel="stylesheet" type="text/css">
<link type="text/css" rel="stylesheet" href="includes/fechas/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></LINK>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />


</head>

<body>
	<script>		
		var globalCallbacks = 
		{
                alert('Sirve');
				onCreate: function(){
                        //document.parent.getElementById('cargando').style.visibility:visible;
						 $('cargando').show();


                },
                onComplete: function() {
                        if(Ajax.activeRequestCount == 0){
                                $('cargando').hide();
                        }

                }

        };
		//Ajax.Responders.register( globalCallbacks );
	</script>



	<div id='cargando' style="position:absolute;top:50%;left:50%;float:left">
    		<div align="center" id="preloader"><img  name="load" src="imagenes/loader.gif" /></div>
  	</div>
    
	<script type="text/javascript"> $('cargando').hide();</script>



<table>
	<!-- <a href="scripts/patologia_raiz.php">patologia raiz</a> -->
	
	<tr>
		 
		<td valign="top" height="100%" colspan="3" width="800">
			<? 
			/*@funcion(); es para ignorar cualquier error que genere eso*/
 			$id = @$_GET["id"];  
			
				switch($id){
					
					case "deslogueo":
						session_unset();						
						echo "<script>//document.location.href = '?id=home';
						 parent.location.href='?id=home';</script>";
						break;	
					
					case "":
						include("includes/inicio.php");
						break;
						
					case "home":
						include("includes/inicio.php");
						break;	
							
					case "estudiantes":
						include("includes/estudiantes.php");
						break;					
					case "estudiantes2":
						include("includes/estudiantes2.php");						
						break;
						
					case "universidad":
						include("includes/universidad.php");
						break;						
					case "universidad2":
						include("includes/universidad2.php");
						break;
						
					case "resumensocio":
						include("includes/resumensocio.php");
						break;
					case "resumensocio2":
						include("includes/resumensocio2.php");
						break;	
					
					case "carrera":
						include("includes/carrera.php");
						break;
					case "carrera2":
						include("includes/carrera2.php");
						break;
					
					case "clinicareport":
						include("includes/clinicareport.php");
						break;
						
					case "carrerareport":
						include("includes/carrerareport.php");
						break;
						
					case "totalgeneralreport":
						include("includes/totalgeneralreport.php");
						break;
						
					case "totalpagosreport":
						include("includes/totalpagosreport.php");
						break;
						
					case "totalusuarioreport":
						include("includes/totalusuarioreport.php");
						break;
						
					case "totalmedicoreport":
						include("includes/totalmedicoreport.php");
						break;
						
					case "totalestadoreport":
						include("includes/totalestadoreport.php");
						break;
					
					case "maternidadporestado":
						include("includes/maternidadporestado.php");
						break;
						
					case "grafpatologia10":
						include("includes/grafpatologia10.php");
						break;
						
						case "resumenanual":
						include("includes/resumenanual.php");
						break;
					
					case "maternidadreport":
						include("includes/maternidadreport.php");
						break;
						
					case "autorizaciones":
						include("includes/autorizaciones.php");
						break;
						
					case "patologiareport":
						include("includes/patologiareport.php");
						break;
						
					case "universidadreport":
						include("includes/universidadreport.php");
						break;
						
					case "asignacion":
						include("includes/cheque.php");
						break;						
					case "asignacion2":
						include("includes/cheque2.php");
						break;
						
					case "finiquitos":
						include("includes/finiquito.php");
						break;
					case "finiquitos2":
						include("includes/finiquito2.php");
						break;
						
					case "carta":
						include("includes/cartaaval.php");
						break;						
					case "carta2":
						include("includes/cartaaval2.php");
						break;
						
					case "casosdevA":
						include("includes/casosdevA.php");
						break;	
					case "casosdevP":
						include("includes/casosdevP.php");
						break;	
					case "casosdev2":
						include("includes/casosdev2.php");
						break;
						
					case "sintesis":
						include("includes/sintesis.php");
						break;
					case "sintesis2":
						include("includes/sintesis2.php");
						break;	
											
					case "patologia":
						include("includes/patologias.php");
						break;					
					case "patologia2":
						include("includes/patologias2.php");
						break;
					
					case "resumedico":
					     include("includes/resumedico.php");
						 break;
						 
					case "resumedico2":
					     include("includes/resumedico2.php");
						 break;
						 
					case "relacion":
						include("includes/relacion.php");
						break;
					case "relacion2":
						include("includes/relacion2.php");
						break;						
					case "maestro":
						include("includes/maestro.php");
						break;
						
					case "maestro_reimpresion":
						include("includes/maestro_reimpresion.php");
						break;
					
					case "calculo":
						include("includes/calculo.php");
						break;						
						
					case "localidad":
						include("includes/localidad.php");
						break;
					case "estado":
						include("includes/estado.php");
						break;	
					case "ciudad":
						include("includes/ciudad.php");
						break;				
					case "municipio":
						include("includes/municipio.php");
						break;										
					
					case "clinicas":
						include("includes/clinicas.php");
						break;
					case "clinicas2":
						include("includes/clinicas2.php");
						break;	
						
					case "devueltoreport":
						include("includes/devueltoreport.php");
						break;	
					
					case "baremo":
						include("includes/baremo.php");
						break;
					case "baremo2":
						include("includes/baremo2.php");
						break;	
						
				   case "actual_casos":
						include("includes/actual_casos.php");
						break;	
				   case "actual_casos2":
						include("includes/actual_casos2.php");
						break;
					case "actual_casos3":
						include("includes/actual_casos3.php");
						break;
						
					case "reimpresion":
						include("includes/reimpresion.php");
						break;	
					case "reimpresion2":
						include("includes/reimpresion2.php");
						break;
						
						case "reimpresion_relacion":
						include("includes/reimpresion_relacion.php");
						break;
						
						case "reimpresion2_relacion":
						include("includes/reimpresion2_relacion.php");
						break;
						
					case "estadodecuentas":
						include("includes/estadosdecuentaclinicas.php");
						break;
					case "estadosdecuentaclinica2":
						include("includes/estadosdecuentaclinicas2.php");
						break;
					case "estadodecuentasest":
						include("includes/estadosdecuentaestudiantes.php");
						break;
					case "estadosdecuentaestudiantes2":
						include("includes/estadosdecuentaestudiantes2.php");
						break;
					case "estadosdecuentaestudiantes3":
						include("includes/estadosdecuentaestudiante3.php");
						break;
					case "reportcheq":
						include("includes/reportcheq.php");
						break;	
						
					case "usuarios":
						include("includes/usuarios.php");
						break;
					case "usuarios2":
						include("includes/usuarios2.php");
						break;	
					case "permisos":
						include("includes/permisos.php");
						break;
					case "permisos2":
						include("includes/permisos2.php");
						break;							
						
					case "reverso":
						include("includes/reverso.php");
						break;
						
					case "bitacora":
						include("includes/bitacora.php");
						break;
										
				/************************EXPEDIENTES******************************/		
					case "registroCarpetas":
						include("includes/expedientes/registroCarpetas.php");
						break;	
						
					case "construccion":
						include("includes/construccion.php");
						break;		
						
					case "registroCarpetasCons":
						include("includes/expedientes/registroCarpetasCons.php");
						break;
						
					case "pendienteNormalizar":
						include("reportespdf/pendienteNormalizar.php");
						break;
							
					case "depurador":
						include("includes/expedientes/depurador.php");
						break;	
						
					case "depuradorCons":
						include("includes/expedientes/depuradorCons.php");
						break;	
					
					case "pers":
						include("includes/expedientes/persona.php");
						break;	
						
					case "persCons":
						include("includes/expedientes/personaCons.php");
						break;	
					
					case "rtepers":
						include("includes/expedientes/rtepersona.php");
						break;	
						
					case "registroExpedientes":
						include("includes/expedientes/registroExpedientes.php");
						break;	
				/*************************************************************/		
				/************************REPORTES******************************/		

					case "cabeCons":
						include("includes/cabecera/cabeceraCons.php");
						break;	
				/*************************************************************/	
					case "pagi":
						include("includes/pagi.php");
						break;		
						
					case "pagiFran":
						include("includes/pagiFran.php");
						break;		

		/*****************************ASIGNACION****************************************/
 				  case "analistaAsig":
						include("includes/asignacion/analistaAsig.php");
						break;	
                  case "consultaCons":
						include("includes/asignacion/consultaCons.php");
						break;	
                  case "contador":
						include("includes/asignacion/contador.php");
						break;	
/**************************************GENERICA**************************************/

 					case "movimientos":
						include("includes/generico.php");
						break;	
					
					case "movimientosCons":
						include("includes/genericoCons.php");
						break;	
						
				   case "cargos":
						include("includes/generico.php");
						break;	
					
					case "cargosCons":
						include("includes/genericoCons.php");
						break;	
						
					case "pagos":
						include("includes/generico.php");
						break;	
					
					case "pagosCons":
						include("includes/genericoCons.php");
						break;	
						
					 case "conceptos":
						include("includes/generico.php");
						break;	
					
					case "conceptoCons":
						include("includes/genericoCons.php");
						break;		
/**********************CARPETAS**********************************************/
                   case "carpeta":
						include("includes/carpeta/carpeta.php");
						break;			
                    case "agre":
						include("includes/carpeta/certifica.php");
						break;																																				
				

/**********************CERTIFICACION**********************************************/
                   case "certi":
						include("includes/certificacion/certiDigitalizacion.php");
						break;			
                    case "certiExpe":
						include("includes/certificacion/certificarExpe.php");
						break;	
                    case "modiHoja":
						include("includes/certificacion/modificarExpediente.php");
						break;	
                    case "migra":
						include("includes/certificacion/migracion.php");
						break;		
                    case "agreTsap":
						include("includes/certificacion/agreTsap.php");
						break;		
                  case "darSaliCarp":
						include("includes/certificacion/darSalida.php");
						break;	
                  case "modiFecha":
						include("includes/certificacion/modiFechaCarpetas.php");
						break;	
                 case "prod":
						include("includes/certificacion/produccion.php");
						break;																																				
				}
			?>		</td>
	</tr>

</table>

</body></html>
