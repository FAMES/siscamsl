<? 	include('../scripts/funciones.php');

	$rif = $_POST['rif'];
	
	//Arreglo de los datos seleccionados con los checkbox
	$array = $_POST['rel'];
	$valores = split(",",$array);
	$cont=count($valores);
	
	$usuario = strtoupper(@$_POST["usuario"]);
	$nombreusu = strtoupper(@$_POST["nombreusu"]);	
	
	if($array!=NULL){	
		$total=0;
		$montoti = 0;
		$montote = 0;
		$hora = ve_date('h:i A'); 
		$fecha=date('d-m-Y');
		$fecha2=date('Y-m-d');
		$fechacheque = '1900-01-01';
		$x=substr(mktime(date("Y:m:d:H:i:s")),6);
		$relacion=date("Ymd").$x;
		for ($i=0;$i<$cont;$i++) { 
			$consulta = "select * from siscam.vst_aut_fac where siscam.vst_aut_fac.aut_cartaaval = $valores[$i]";
			//echo $consulta;
			$query = pg_query($consulta);
			$row = pg_fetch_object($query);			
			if($row->cli_rif != '')			
			{			
			$mod=$row->aut_modalidad;
			$impuestoislr=$row->cli_impuestoislr;
			$impuestoltf=$row->cli_impuestoltf;				
			$centrouni=$row->cli_centrouni;
			$respso=$row->cli_respso;
			if ($row->cli_montoacum!="" or $row->cli_montoacum!=NULL) { $montoacum = $row->cli_montoacum; } else { $montoacum = 0; }		
			$estado=$row->esta_estado;
			$montout=$row->esta_montout;
			$estariam=$row->esta_riam;
			$montoi=$row->fac_total_factura;
			$montoe=$row->fac_monto_exo;
			$moniva=$row->moniva;
			$arr_moniva=$arr_moniva+$moniva;
			$retiva=$row->retiva;
			$arr_retiva=$arr_retiva+$retiva;
			$montoti=$montoti+$montoi;
			$montote=$montote+$montoe;
			$estatus=$row->aut_estatus;
			$ppto=0;
			}
			if ($estatus=='PAGOPAR') {
				$estatus2='FINPAR';
			} else {
				$estatus2='FIN';
			}
			$consulta3="update siscam.autorizaciones set re_id=$relacion,aut_estatus='$estatus2' where siscam.autorizaciones.aut_cartaaval= $valores[$i]"; 
			$query3=pg_query($consulta3);			
			//BITACORA
			$consultabit="update siscam.bitacora set
				bit_relfecha='$fecha2',
				bit_relhora ='$hora',
				usu_rel='$nombreusu',
				bit_relestatus='$estatus2',
				bit_relacion=$relacion
				where aut_cartaaval = $valores[$i]"; 
			$query2=pg_query($consultabit);
		}
		
		//SUMO EL MONTO TOTAL DE IMPUESTO (montoti) MAS EL MONTO TOTAL EXONERADO (montote) PARA APLICAR IMPUESTO DE PAGO A TIMBRE FISCAL(RIAM)
		$montoBruto = $montoti+$montote;
		$montob=($montoti+$montote)-$arr_moniva;
		$montoNeto=$montoti-$arr_moniva; //aplica para el monto Bruto de factura menos el IVA		
		$r=substr($rif,0,1); 		
			
		//CONSULTA DE LA TABLA CALCULO
		$consulta4="select * from siscam.calculo"; 
		$query4=pg_query($consulta4);
		$row = pg_fetch_object($query4);
		$sustraendo=$row->cal_sustraendo;
		$islrn=$row->cal_islrn;
		$islrj=$row->cal_islrj;
		$islrm=$row->cal_islrm;
			
		//SI LA CLINICA AMPLICA IMPUESTO ISLR////////////////////////////////////////////////
		if ($impuestoislr=='SI') {			
			//SI ES RIF NATURAL
			if ($r=='V' or $r=='E') {				
				if ($montoNeto>=$islrm) {					
					//COMPARO SI SE GENERO UNA RELACION APLICANDO EL SUTRAENDO EN EL MISMO MES 
					$feccom=date('m');
					$consulta5="select * from siscam.relacion where siscam.relacion.cli_rif like '$rif'";
					$query5=pg_query($consulta5);
					while($row = pg_fetch_object($query5)){ 					
						$fecharel=$row->re_fecha;
						$fecrel=substr($fecharel,3,2);
						$sus=$row->re_sustraendo;
						if ($fecrel==$feccom and $sus=='SI') {
							$apply='SA'; //SI APLIC� SUSTRAENDO
							break;									
						}							
					}
					//DEPENDIENDO DE LA COMPRACI�N APLICO O NO EL SUSTRAENDO
					if ($apply!='SA') {
						$islr=(($montoNeto*$islrn)/100)-($sustraendo);
						$i=$islrn;
						$ap='SI';
					} else {
						if ($apply=='SA') {
							$islr=(($montoNeto*$islrn)/100);
							$i=$islrn;								
							$ap='NO';
						}
					}///////////////////////////////////////////////////		
				} else {
					$islr=0;
					$i=0;
				}						
			//SI ES RIF JURIDICO O GOBIERNO			
			} else {
				if ($r=='J' or $r=='G') {
					$islr=($montoNeto*$islrj)/100;
					$i=$islrj;
				} else {
					$islr=0;
					$i=0;
				}
			} 	
		//SI NO APLICA IMPUESTO ISLR
		} else { 
			$i=0;
			$islr=0;				
		}//////////////////////////FIN AMPLICA IMPUESTO ISLR////////////////////////////
			
		//SI LA CLINICA AMPLICA IMPUESTO DE LEY DE TIMBRE FISCAL (RIAM)
		if ($impuestoltf=='SI') {			
				//CALCULO DEL IMPUESTO LTF (RIAM)
				if ($montob>=$montout) {
					
					$riam=($montob * $estariam);
						
				} else {
					if ($montob<$montout) { $riam=0; }
				}			
		} else { //SI NO APLICA IMPUESTO LTF (RIAM)
			$riam=0;	
		}////////////////////////FIN AMPLICA IMPUESTO LTF///////////////////////////////
				
		//CALCULO DE RESPONSABILIDAD SOCIAL (2%)/////////////////////////////////////////
		if ($respso=='SI' /*&& $r!='G'*/ && $convenio!='NO') {
			$totalrespso=(($montob*3)/100);
		} else {
			$totalrespso=0;
		}//////////////////////////FIN CALCULO DE RESPONSABILIDAD SOCIAL/////////////////	
		
		//TOTAL A CANCELAR = MONTO BRUTO - ISLR - LTF (RIAM) - IMPUESTO DE RESPONSABILIDAD SOCIAL (2%)
		$totalcan = ((round($montoBruto,2) - round($islr,2)) - round($riam,2)) - round($totalrespso,2) - round($arr_retiva,2);
		//echo $totalcan;
		$totalcanfor= number_format($totalcan,2,',','.');
		//echo $totalcanfor;
		
		//SUMA TOTAL DE PAGO A CLINICA
		$totalacumcli=$montoacum+$totalcan;
		
		$consulta2="insert into siscam.relacion values ($relacion,'$rif',$montoBruto,$islr,$riam,$totalcan,$usuario,'$fecha2','NO',0,'$fechacheque','$ap',0,$totalrespso,$arr_moniva,$arr_retiva,$ppto)";
		//echo $consulta2;
		$query2=pg_query($consulta2);
		//UPDATE EN CLINICAS CON EL ACUMULADO DEL CALCULO DE RESPONSABILIDAD SOCIAL
		$consultacli="update siscam.clinicas set cli_montoacum=$totalacumcli where siscam.clinicas.cli_rif like '$rif'"; 
		$querycli=pg_query($consultacli);
	} //fin del condicional ?>
<form id="frmrelacion" name="frmrelacion" method="post" action="reports/reportRE.php" target="_blank" >
<input type="hidden" name="hdnrelacion" id="hdnrelacion" value="<?=$relacion;?>" />
<input type="hidden" name="hdnimpuesto" id="hdnimpuesto" value="<?=$i;?>" />
</form>
<form id="frmrel" name="frmrel" method="post">
<table cellspacing="0" width="950">
    <tr><td colspan="6" class="titulo2">Casos en Proceso de Pago</td></tr>
    <tr class="TR1">
        <td width="7%"><div align="left">Selecci&oacute;n</div></td>
        <td width="9%"><div align="left">Carta Aval</div></td>
        <td width="9%"><div align="left">N&deg; Control</div></td>
        <td width="16%"><div align="left">N&deg; Factura</div></td>
        <td width="8%"><div align="left">C&eacute;dula</div></td>
        <td width="26%"><div align="left">Nombre y Apellido</div></td>
        <td width="14%"><div align="left">Fecha Autorizaci&oacute;n</div></td>
        <td width="11%"><div align="left">Monto a Pagar</div></td>            
    </tr>		
    <? $consulta = Consultar("select * from siscam.vst_aut_fac where siscam.vst_aut_fac.cli_rif = '$rif' and
							 	siscam.vst_aut_fac.aut_estatus = 'PAGO'
		or siscam.vst_aut_fac.cli_rif = '$rif' and siscam.vst_aut_fac.aut_estatus = 'PAGOPAR' order by siscam.vst_aut_fac.est_id");
    if($consulta!=false){ 		
		$cont=pg_num_rows($consulta);
		$num = 1; 
		$i=1;
		$data=array();
		while($row = pg_fetch_object($consulta)){ ?>
        <input type="hidden" id="valor_oculto" name="valor_oculto" value="<?= pg_num_rows($consulta); ?>" />
        <tr <? if($num%2==0){?> class="TR2"<? }else{ ?>class="TR3"<? } ?>>
        <td><input type="checkbox" name="check<?=$i;?>" id="check<?=$i;?>" value="<?=$row->aut_cartaaval?>" onclick="suma_total_relacion(this.id);"/></td>
        <td width="9%" id="carta<?= $i; ?>" ><? if ($row->fac_cartaaso <> ''){ echo $row->fac_cartaaso; }else{ echo 'No definido'; }?></td>
        <td width="9%" ><? if ($row->aut_ncontrol <> ''){ echo $row->aut_ncontrol; }else{ echo 'No definido'; }?></td>
        <td width="16%" ><? if ($row->fac_numero <> ''){ echo substr($row->fac_numero,0,20); }else{ echo 'No definido'; }?></td>
        <td width="8%" ><? if ($row->est_id <> ''){ echo $row->est_id; }else{ echo 'No definido'; }?></td>
        <td width="26%" ><? if ($row->est_pnombre <> ''){ echo $row->est_pnombre; }else{ echo 'No definido'; }?>&nbsp;<? if ($row->est_papellido <> ''){ echo $row->est_papellido; }else{ echo 'No definido'; }?></td>
        <td width="14%" ><? if ($row->aut_fechaa <> ''){ echo $row->aut_fechaa; }else{ echo 'No definido'; }?></td>
        <td width="11%" align="center" id="monto<?=$i;?>" ><? if ($row->fac_monto <> 0){ echo $row->fac_monto; }else{ echo $row->fac_pres_monto; }?></td>
        <? $totalapagar = $totalapagar + $row->fac_monto; ?>
        <? $montodecimal = number_format($totalapagar,2,',','.');?>
        </tr>
        <?  $i++; $num++; } 
    }else{ //end if consulta
        echo '<table cellspacing="0"><tr><td class="titulo3">NO HAY INFORMACI&oacute;N PARA SELECCIONAR</td></tr></table>';	
    } //end if consulta ?>     	
</table>
<table align="right" width="318">
    <tr>
    <td align="left" colspan="9" class="titulo3"><hr class="linea"></td>
    <td width="3%" align="rigth" class="titulo3"></td>
    </tr>
    <tr>
    <td width="71%" align="left" class="titulo3">Monto Total Seleccionado : </td>
    <td width="19%" align="left" id="monto_total_sel" class="TR3">0.00</td>    
    </tr>
</table>
<table align="left">
	<tr><td>
    <tr><td>
    <tr><td>
    <tr align="left">
    <td class="titulo3">Monto Total de Clinica Bs.F:</td>
    <td class="titulo3"><? echo  $montodecimal; ?></td>
    </tr>
</table>
</form>