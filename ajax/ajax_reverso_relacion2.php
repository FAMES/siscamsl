<? 	include('../scripts/funciones.php');

	$rif = $_POST['rif'];
	
	//Arreglo de los datos seleccionados con los checkbox
	$array = $_POST['relsel'];
	$valores = split(",",$array);
	$cont=count($valores);	
	
	//arreglo de los datos que no fueron seleccionados los checkbox
	$array2 = $_POST['relnosel'];
	$valores2 = split(",",$array2);	
	$cont2=count($valores2);
	
	
	$opcion = $_POST['opcion'];
	$accion = "REVERSO";
	$usuario = strtoupper(@$_POST["usuario"]);
	$nombreusu = strtoupper(@$_POST["nombreusu"]);	
	
	if($array!=NULL){	
		$total=0;
		$montoti = 0;
		$montote = 0;
		$hora = ve_date('h:i A'); 
		$fecha=date('d-m-Y');
		$fecha2=date('Y-m-d');
		$fechacheque = '1900-01-01';
		$x=substr(mktime(date("Y:m:d:H:i:s")),6);
		$relacion = $_POST['relacion'];	
		$consulta = "select * from siscam.relacion where siscam.relacion.re_id = $relacion";
		$consulta_relacion = pg_query($consulta);
		$consulta_monto_acum = pg_result($consulta_relacion,0,"re_totalcan");		
		
		for ($i=0;$i<$cont2;$i++) {
			if($array2!=NULL) 
			{				
			$consulta = "select * from siscam.vst_aut_fac where siscam.vst_aut_fac.aut_cartaaval = $valores2[$i]";					
			$query = pg_query($consulta);
			$row = pg_fetch_object($query);
			$rif=$row->cli_rif;
			$mod=$row->aut_modalidad;
			$impuestoislr=$row->cli_impuestoislr;
			$impuestoltf=$row->cli_impuestoltf;				
			$centrouni=$row->cli_centrouni;
			$respso=$row->cli_respso;
			if ($row->cli_montoacum!="" or $row->cli_montoacum!=NULL) { $montoacum = $row->cli_montoacum; } else { $montoacum = 0; }		
			$estado=$row->esta_estado;
			$montout=$row->esta_montout;
			$estariam=$row->esta_riam;
			$montoi=$row->fac_total_factura;
			$montoe=$row->fac_monto_exo;
			$montoti=$montoti+$montoi;
			$montote=$montote+$montoe;
			$estatus=$row->aut_estatus;			
			
			//BITACORA
			$consultabit="update siscam.bitacora set
				bit_relfecha='$fecha2',
				bit_relhora ='$hora',
				usu_rel='$nombreusu',
				bit_relestatus='$estatus2',
				bit_relacion=$relacion
				where aut_cartaaval = $valores2[$i]"; 
			$query2=pg_query($consultabit);
			}
		}
		//Cambio estatus a 'PAGO' y relacion = null de las Cartas que fueron seleccionadas en tabla autorizaciones
		for($i=0; $i<$cont; $i++)
		{
			//EXTRAER EL ESTATUS ANTERIOR PARA REVERSAR A 'PAGOPAR' SI ES 'FINPAR' O 'PAGO' SI ES 'FIN'
			$consEstatusAnt = "select siscam.autorizaciones.aut_estatus as estatus,
									  siscam.autorizaciones.aut_ncontrol as control,
									  siscam.autorizaciones.aut_modalidad as modalidad,
									  siscam.autorizaciones.aut_tipo as tipo
								from 
								siscam.autorizaciones 
								where 
								siscam.autorizaciones.aut_cartaaval = $valores[$i]";
			$resEstatusAnt = @pg_query($consEstatusAnt);
			$res = @pg_fetch_object($resEstatusAnt,0);			
				if($res->estatus == 'FIN')
				{				
					$estatus = 'PAGO';		
				}
				elseif($res->estatus == 'FINPAR')
				{
					$estatus = 'PAGOPAR';
				}
			//ACTUALIZACION DE ESTATUS EN DE LA(S) CARTA(S) SELECCIONADAS(S) 			
			$actEstatus="update siscam.autorizaciones 
								set re_id=null,aut_estatus='$estatus' 
								where siscam.autorizaciones.aut_cartaaval= $valores[$i]";
						
			//REGISTRO EN BITACORA DE REVERSOS			
			$regBit = "select siscam.func_bit_reverso($valores[$i],
													 $res->control,
													'$res->modalidad',
													'$res->tipo',
													 $usuario,
													'$fecha2',
													'$hora',
													'$res->estatus',
													'$estatus',
													 0,													
													'$accion',
													'REVERSO DE RELACION Nro $relacion',
													'I')";
			//echo $regBit;
			
			$query3 = pg_query($actEstatus);	
			$aux = pg_query($regBit);
		}
		
		//SUMO EL MONTO TOTAL DE IMPUESTO (montoti) MAS EL MONTO TOTAL EXONERADO (montote) PARA APLICAR IMPUESTO DE PAGO A TIMBRE FISCAL(RIAM)
		$montob=$montoti+$montote; 	
		$r=substr($rif,0,1); 		
			
		//CONSULTA DE LA TABLA CALCULO
		$consulta4="select * from siscam.calculo"; 
		$query4=pg_query($consulta4);
		$row = pg_fetch_object($query4);
		$sustraendo=$row->cal_sustraendo;
		$islrn=$row->cal_islrn;
		$islrj=$row->cal_islrj;
		$islrm=$row->cal_islrm;
			
		//SI LA CLINICA AMPLICA IMPUESTO ISLR////////////////////////////////////////////////
		if ($impuestoislr=='SI') {			
			//SI ES RIF NATURAL
			if ($r=='V') {				
				if ($montoti>=$islrm) {					
					//COMPARO SI SE GENERO UNA RELACION APLICANDO EL SUTRAENDO EN EL MISMO MES 
					$feccom=date('m');
					$consulta5="select * from siscam.relacion where siscam.relacion.cli_rif like '$rif'";
					$query5=pg_query($consulta5);
					while($row = pg_fetch_object($query5)){ 					
						$fecharel=$row->re_fecha;
						$fecrel=substr($fecharel,3,2);
						$sus=$row->re_sustraendo;
						if ($fecrel==$feccom and $sus=='SI') {
							$apply='SA'; //SI APLIC� SUSTRAENDO
							break;									
						}							
					}
					//DEPENDIENDO DE LA COMPRACI�N APLICO O NO EL SUSTRAENDO
					if ($apply!='SA') {
						$islr=(($montoti*$islrn)/100)-($sustraendo);
						$i=$islrn;
						$ap='SI';
					} else {
						if ($apply=='SA') {
							$islr=(($montoti*$islrn)/100);
							$i=$islrn;								
							$ap='NO';
						}
					}///////////////////////////////////////////////////		
				} else {
					$islr=0;
					$i=0;
				}						
			//SI ES RIF JURIDICO O GOBIERNO			
			} else {
				if ($r=='J' or $r=='G') {
					$islr=($montoti*$islrj)/100;
					$i=$islrj;
				} else {
					$islr=0;
					$i=0;
				}
			} 	
		//SI NO APLICA IMPUESTO ISLR
		} else { 
			$i=0;
			$islr=0;				
		}//////////////////////////FIN AMPLICA IMPUESTO ISLR////////////////////////////
			
		//SI LA CLINICA AMPLICA IMPUESTO DE LEY DE TIMBRE FISCAL (RIAM)
		if ($impuestoltf=='SI') {			
				//CALCULO DEL IMPUESTO LTF (RIAM)
				if ($montob>=$montout) 
				{					
					$riam=($montob * $estariam);
				} 
				else 
				{
					if ($montob<$montout) { $riam=0; }
				}			
		} else { //SI NO APLICA IMPUESTO LTF (RIAM)
			$riam=0;	
		}////////////////////////FIN AMPLICA IMPUESTO LTF///////////////////////////////
				
		//CALCULO DE RESPONSABILIDAD SOCIAL (2%)/////////////////////////////////////////
		if ($respso=='SI' && $r!='G' && $mod <>'ESPECIAL') {
			$totalrespso=(($montob*2)/100);
		} else {
			$totalrespso=0;
		}//////////////////////////FIN CALCULO DE RESPONSABILIDAD SOCIAL/////////////////	
		
		//TOTAL A CANCELAR = MONTO BRUTO - ISLR - LTF (RIAM) - IMPUESTO DE RESPONSABILIDAD SOCIAL (2%)
		$totalcan = ((round($montob,2) - round($islr,2)) - round($riam,2)) - round($totalrespso,2);
		//echo $totalcan;
		$totalcanfor= number_format($totalcan,2,',','.');
		//echo $totalcanfor;
		
		//ACTUALIZAR ACUMULADO DE LA CLINICA
		$totalacumcli=$consulta_monto_acum-$totalcan;
		//echo "fdfsdfsdfs" . $totalacumcli;
		//echo $totalcan;
		if($array2!=NULL) 
		{			
		$consulta2="update siscam.relacion set re_id=$relacion,
												cli_rif='$rif',
												re_total=$montob,
												re_islr=$islr,
												re_riam=$riam,
												re_totalcan=$totalcan,
												usu_cedul=$usuario,
												re_fecha='$fecha2',
												re_reporte='NO',
												re_nrocheq=0,
												re_fechapagocheq='$fechacheque',
												re_sustraendo='$ap',
												re_banco=0,
												re_respso=$totalrespso
					where re_id = $relacion";					
		}
		else 
		{			
		$consulta2="delete from siscam.relacion where re_id = '$relacion'";				
		}
		$query2=pg_query($consulta2);
		
		//ACTUALIZACION DE MONTO ACUMULADO DE LA CLINICA RESPECTIVA
		//EXTRAIGO EL MONTO ACUMULADO ACTUAL DE LA CLINICA
		$consulta = "select * from siscam.clinicas where siscam.clinicas.cli_rif = '$rif'";
		$consulta_acum=pg_query($consulta);
		$acumulado = pg_result($consulta_acum,0,"cli_montoacum");
		//CALCULO EL MONTO ACUMULADO MENOS EL MONTO DE LAS CARTAS Q SIGUEN EN LA RELACION
		$actual_acum = $acumulado - $totalacumcli;		
		//ACTUALIZO EL MONTO ACUMULADO DE LA CLINICA
		$consulta = "update siscam.clinicas set cli_montoacum = $actual_acum where clinicas.cli_rif = '$rif'";
		$actualiza_acum = pg_query($consulta);		
		
	} //fin del condicional ?>

