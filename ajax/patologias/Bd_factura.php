<?php 
include('../../scripts/funciones.php'); 
//Datos Recibidos para tabla factura 
  $id = $_POST["fac_num"];
  $fecha = CamFormFechBD($_POST["fecha"]);  //Se le aplica funcion 'CamFormFechBD' para formato YYYY-MM-DD
  $fechaing = CamFormFechBD($_POST["fechaing"]);  //Se le aplica funcion 'CamFormFechBD' para formato YYYY-MM-DD
  $monto = $_POST["monto"];
  $monto_bruto = $_POST["monto_bruto"];
  $montoexo = $_POST['montoexo'];
  $montonoexo = $_POST['montonoexo'] - $_POST['montoexo'];
  $monto_iva = $_POST['monto_iva'];
  $re_iva = $_POST['re_iva'];
  $usu_cedula = $_POST["usucedula"];   
  $nombreusu = $_POST["nombreUsu"];
  $accion = $_POST["accion"];  
  $modalidad = $_POST["modalidad"];  
  $fac_id = $_POST["fac_id"];
  $montop = $_POST["montop"];
//Datos Recibidos para actualizacion de patologia
  $cartaaval = $_POST["ncarta"];  
  $ncontrol = $_POST["ncontrol"];
  $cedula = $_POST["cedula"];
  $bar_anio = $_POST["bar_anio"];
  $bar_id = $_POST["bar_id"];
  $bar_desc = $_POST["bar_desc"];
  $bar_baremo = $_POST["bar_baremo"];
  $monto_ajuste = $_POST["monto_ajuste"];
  $pat_id = $_POST["pat_id"];
  $hora = ve_date('h:i A'); 
  $fecha_bit = date('Y-m-d');  
 
//coloco el monto exonerado a 0

//Divido el array de numero de cartas avales 
$array_cartas = split("/",$cartaaval);	
//Cuento los valores del arreglo creado
$cont = count($array_cartas);	
//Tomo la primera carta del array com carta principal
$cartap = $array_cartas[0];
//Suplanto de la variable $cartaaval todos los '/' por ','
$cartaaso = str_replace("/",",",$cartaaval); 

  //Verifico la Accion a Tomar
if($accion == 'FACTURA') 
{ 
  if($id != '' and $fecha != '' and $monto != '')
  {       
//Estatus de las cartas avales luego de registradas las facturas de pago, 'umed' si es AYUDA SOLIDARIA, sino es 'pago'
		if($modalidad=='AYUDA SOLIDARIA' or $modalidad=='AYUDA SOLIDARIA ESPECIAL') { $estatus = 'UMED'; $fechaing = '1900-01-01'; } else { $estatus = 'PAGO'; }
				if($fac_id == '') 
				{
					//Antes de Incluir la Factura verifico que esta no exista en la tabla					
					$consulta = "SELECT * FROM siscam.factura WHERE siscam.factura.fac_cartaaso like '$cartaaval'";
						//echo $query_fac2;
						$queryfac = pg_query($consulta);
						$rowr = pg_fetch_object($queryfac); 
						$nrofactura =$rowr->fac_numero; 						
						if($nrofactura == $id)
						{
							{
								echo 'ESTE CASO FUE REGISTRADO ATERIORMENTE, PUEDE PASAR A GENERAR LA RELACION. ';
							}							
						}
						else
						{
								$var = "I"; 														
								$query = "SELECT siscam.func_factura(0,'$id','$fecha',$monto,'0','1900-01-01',0,$montoexo,$montonoexo,'NO',$usu_cedula,'$nombreusu',$cartap, '$cartaaso',$monto_bruto, $monto_iva, $re_iva,'$fechaing','$var')";
								//echo $query;
								$aux = pg_query($query);								
								$uvar="UCP";					
								registrobit($cedula, $cartaaval, $ncontrol, $fecha_bit, $hora, $nombreusu, $estatus, $uvar);
						}
				} 
				else 
				{ 										
					//ACTUALIZA EL CASO CON LA FACTURA Y CIERRA EL CASO CON ESTATUS "PDO"													
					$var = "U";
					$estatus = "PDO";
					//VERIFICAR QUE EL VALOR DE LA FACTURA NO SEA MENOR QUE EL `PRESUPUESTO DE LO CONTRARIO HABRA REINTEGRO
					if($monto < $montop){ $reint = 'SI'; } else { $reint = 'NO'; }
					
					$query = "SELECT siscam.func_factura($fac_id,'$id','$fecha',$monto,'0','1900-01-01',0,$montoexo,$montonoexo,'$reint',$usu_cedula,'$nombreusu',$cartap,'$cartaaso',$monto_bruto,$monto_iva,$re_iva,'$fechaing','$var')";
					echo $query;
					$aux = pg_query($query);					
				}
		for($i = 0; $i < $cont; $i++)
      	{
		$var2 = "U";
		$query2 = "SELECT siscam.func_fac_estatus($ncontrol,$array_cartas[$i],'$estatus','$var2')";		
		$aux2 = pg_query($query2);
		$uvar="UCP";
		//registrobit($cedula, $cartaaval, $ncontrol, $fecha, $hora, $nombreusu, $estatus, $uvar);
		$consulta = "SELECT siscam.pa_bitacora($cedula, $array_cartas[$i],$ncontrol,'$fecha_bit', '$hora','$nombreusu','$estatus','$uvar')";
		$con=pg_query($consulta);
	  	}
		if($aux2 == true and $aux == true)
		{		
		echo '<table width="100%">
				<tr><td style="display:none;">AGREGADO</td></tr>			
			</table>';
		}
		else
		{
		echo '<table width="100%">
				<tr><td style="display:none;">NO_AGREGAR</td></tr>			
			</table>';
		}
  	}	
}
  		elseif($accion == 'PRESUPUESTO')
  		{
		 	if($id != '' and $fecha != '' and $monto != '')
  			{ 
       			for($i = 0; $i < $cont; $i++)
      			{
//Estatus de las cartas avales luego de registradas las facturas de pago
				$estatus = 'PAGOPAR';
				$fechaing = '1900-01-01';
	    		$var = "I";
				$var2 = "U";
				$query = "SELECT siscam.func_factura(0,'0','1111-11-11',0,'$id','$fecha',$monto,$montoexo,$montonoexo,'NO',$usu_cedula,'$nombreusu',$cartap,'$cartaaso',$monto_bruto,$monto_iva,$re_iva,'$fechaing','$var')";
				//echo  $query;
				$query2 = "SELECT siscam.func_fac_estatus($ncontrol,$array_cartas[$i],'$estatus','$var2')";
				$aux = pg_query($query);
				$aux2 = pg_query($query2);
				$uvar="UCP";
				//registrobit($cedula,$array_cartas[$i], $ncontrol, $fecha, $hora, $nombreusu, $estatus, $uvar);
				$consulta = "SELECT siscam.pa_bitacora($cedula, $array_cartas[$i],$ncontrol,'$fecha_bit', '$hora','$nombreusu','$estatus','$uvar')";
				$con=pg_query($consulta);
	  			}
		if($aux == true)
		{		
		echo '<table width="100%">
				<tr><td style="display:none;">AGREGADO</td></tr>			
			</table>';
		}
		else
		{
		echo '<table width="100%">
				<tr><td style="display:none;">NO_AGREGAR</td></tr>			
			</table>';
		}
  }  
  }
  			else if($accion == 'Ajustar')
			{
				$var = 'U';
				$query = "SELECT siscam.pa_patologia($pat_id, $cartaaval, $ncontrol, $cedula, $bar_anio, $bar_id, '$bar_desc', $bar_baremo, $monto_ajuste, $usu_cedula, '$nombreusu', '$var')";
				$aux = pg_query($query);
				
				//ACTUALIZAR MONTO AUTORIZADO DE CARTA AVAL EN TABLA AUTORIZACIONES AGG 18/02/2014
				$conPat = "select sum(patologia.pat_majust) as ajuste from siscam.patologia where patologia.aut_cartaaval = '$cartaaval'";
				$exeConPat = pg_query($conPat);
				while($valor = pg_fetch_object($exeConPat))
				{
					$actAut = "update siscam.autorizaciones 
							   set aut_montoa = ".$valor->ajuste." 
							   where autorizaciones.aut_cartaaval = '$cartaaval'";					
					$exeActAut = pg_query($actAut);
				}
				if($aux)
				{
					echo '<table width="100%">
				<tr><td class="TRAMARILLO" colspan="6" style="display:none;">PATOLOGIA_ACTUALIZADA</td></tr>			
			</table>';
				}
				else
				{
					echo '<table width="100%">
					  <tr><td class="TRAMARILLO" colspan="6" style="display:none;">NO_ACTUA_PAT</td></tr>			
					  </table>';
				}
			}		
?>




