<?php
require("class.phpmailer.php");

$mail = new PHPMailer();

$mail->IsSMTP();                                      
$mail->Host = "mail.cantv.net";
$mail->SMTPAuth = false;
$mail->From = "fames@fames.gob.ve";
$mail->FromName = "Hector";
$mail->AddAddress("hguerrero@fames.gob.ve", "Hector Guerrero");
$mail->AddAddress("fames@fames.gob.ve");                  // name is optional
$mail->AddReplyTo("fames@fames.gob.ve", "Information");

$mail->WordWrap = 50;                                 // set word wrap to 50 characters
#$mail->AddAttachment("/var/tmp/file.tar.gz");         // add attachments
#$mail->AddAttachment("/tmp/image.jpg", "new.jpg");    // optional name
$mail->IsHTML(true);                                  // set email format to HTML

$mail->Subject = "Prueba";
$mail->Body    = "This is the HTML message body <b>in bold!</b>";
$mail->AltBody = "This is the body in plain text for non-HTML mail clients";

if(!$mail->Send())
{
   echo "Message could not be sent. <p>";
   echo "Mailer Error: " . $mail->ErrorInfo;
   exit;
}

echo "Message has been sent";
?>
