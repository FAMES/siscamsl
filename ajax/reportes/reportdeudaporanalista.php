<? 	include('../../scripts/funciones.php');
//RECIBO LAS VARIABLES
	$fechadesde = $_REQUEST['fechadesde'];
	//echo "fechadesde : ". $fechadesde;
	$fechahasta = $_REQUEST['fechahasta'];
	//echo "fechadesde : ". $fechahasta;
	$rifClinica = $_REQUEST['rif_clinica'];
	//echo "rif_clinica: ". $rif_clinica;
	$analista = $_REQUEST['analista'];	
	//echo "anlaista : ". $analista;
	$anio = $_REQUEST['anio'];
	//echo "anio : ". $anio;
?>


<table cellspacing="0" width="950">
    <tr><td colspan="8" class="titulo2">Listado de Casos</td></tr>    
    <tr class="TR1">       
        <td width="12%"><div align="center">Estado</div></td>
        <td width="30%"><div align="center">Clinica</div></td>
        <td width="12%"><div align="center">Cedula</div></td>
        <td width="13%"><div align="center">Estudiante</div></td>
        <td width="7%"><div align="center">Fecha Ingreso</div></td>
        <td width="10%"><div align="center">Nro Factura</div></td> 
        <td width="10%"><div align="center">Fec. Emisión</div></td>
        <td width="7%"><div align="center">Monto Factura</div></td>           
    </tr>
    <? 	
    	//Consulta por defecto
		if($fechadesde != NULL and $fechahasta != NULL) 
		{ 
			if($analista != 'TODOS')
			{
				$consulta = "SELECT B.esta_estado, 
									C.cli_nombre, 
									C.cli_rif,
									A.est_id, 
									G.est_pnombre, 
									G.est_papellido, 
									G.est_id, 
									D.fechaing as ingreso, 
									D.fac_numero, 
									D.fac_fecha as emision, 
									D.fac_monto 
							 FROM 
							 		siscam.autorizaciones A, 
									siscam.estado B, 
									siscam.clinicas C, 
									siscam.factura D,									 
									siscam.estudiantes G
							 WHERE 
							 		A.cli_rif = C.cli_rif and
		      						C.esta_id = B.esta_id and
		      						A.est_id = G.est_id   and		      						
		      						A.aut_cartaaval = D.aut_cartaaval and
									A.aut_estatus <> 'PDO' and
									A.aut_modalidad <> 'ESPECIAL' and
		      						D.fechaing >= '$fechadesde' and D.fechaing <= '$fechahasta' and 
		      						B.esta_regionp = '$analista' order by C.cli_rif";	
			}			
			else
			{
				$consulta = "SELECT B.esta_estado, 
									C.cli_nombre, 
									C.cli_rif, 
									A.est_id, 
									G.est_pnombre, 
									G.est_papellido, 
									G.est_id, 
									D.fechaing as ingreso,  
									D.fac_numero, 
									D.fac_fecha as emision, 
									D.fac_monto 
							 FROM 
							 		siscam.autorizaciones A, 
									siscam.estado B, 
									siscam.clinicas C, 
									siscam.factura D,									
									siscam.estudiantes G
							 WHERE 
							 		A.cli_rif = C.cli_rif and
		      						C.esta_id = B.esta_id and
		      						A.est_id = G.est_id   and		      						
		      						A.aut_cartaaval = D.aut_cartaaval and
									A.aut_estatus <> 'PDO' and
									A.aut_modalidad <> 'ESPECIAL' and
		      						D.fechaing >= '$fechadesde' and D.fechaing <= '$fechahasta' 
									order by C.cli_rif";							
			}
		}elseif($anio != '0')
		{
			if($analista != 'TODOS') { $aux = " and D.esta_regionp = '$analista' " ; } 
			$consulta = "SELECT A.aut_cartaaval, 
								A.fac_numero,
								D.esta_estado, 
								C.cli_nombre,
								B.est_id,   
								E.est_pnombre, 
								E.est_papellido, 
								A.fechaing as ingreso, 
								A.fac_fecha as emision,
								A.fac_monto															
						 FROM siscam.factura A, 
						 	  siscam.autorizaciones B, 
							  siscam.clinicas C, 
							  siscam.estado D, 
							  siscam.estudiantes E 
						 WHERE  
						(A.fechaing >= '2014-01-01' or (A.fechaing < '2014-01-01' and A.fechaing <> '1900-01-01' and A.aut_cartaaval >= 201400000))
						and A.aut_cartaaval = B.aut_cartaaval
						and B.cli_rif = C.cli_rif
						and C.esta_id = D.esta_id
						and B.est_id = E.est_id
						and B.aut_modalidad <> 'ESPECIAL'												
						and B.aut_Estatus <> 'PDO'		
						$aux									
						order by D.esta_estado, A.fechaing";
		}		
    	$query = @Consultar($consulta);	
    		if($query != false)
			{
				$cont=pg_num_rows($query); 
				$num = 1; 
				$i=1;
				$data=array();
    				while($row = pg_fetch_object($query))
					{ ?>
    	<input type="hidden" id="valor_oculto" name="valor_oculto" value="<?=pg_num_rows($query); ?>" />
    <tr <? if($num%2==0){?> class="TR2"<? }else{ ?>class="TR3"<? } ?>>              
        <td width="12%" align="center" ><? if ($row->esta_estado <> ''){  echo $row->esta_estado; } else { echo 'NO DEFINIDO'; }?></td>
        <td width="30%"><? if ($row->cli_nombre <> '') {  echo $row->cli_nombre;  } else { echo 'NO DEFINIDO'; }?></td>
        <td width="12%" align="center"><? if ($row->est_id <> '') {  echo $row->est_id;  } else { echo 'NO DEFINIDO'; }?></td>
        <td width="12%"><? if ($row->est_pnombre <> ''){  echo $row->est_pnombre.' '.$row->est_papellido; } else { echo 'NO DEFINIDO'; }?></td>  
        <td width="10%" align="center" ><? if ($row->ingreso <> '')    {  echo CamFormFech($row->ingreso);   } else { echo 'NO DEFINIDO'; }?></td>   
        <td width="10%" align="center" ><? if ($row->fac_numero <> '') {  echo $row->fac_numero;  } else { echo 'NO DEFINIDO'; }?></td>
        <td width="10%" align="center" ><? if ($row->emision <> '')    {  echo CamFormFech($row->emision);     } else { echo 'NO DEFINIDO'; }?></td> 
        <td width="10%" align="center" ><? if ($row->fac_monto <> '')  {  echo $row->fac_monto;   } else { echo 'NO DEFINIDO'; }?></td>    
    </tr>
    <?  $i++; $num++; $totalDeuda+=$row->fac_monto; 
					} ?>
	<tr class="TOTAL">
    	<td colspan="8" width="100%">Total : <?= number_format($totalDeuda,2,',','.'); ?> Bs.</td>
    </tr>	
     <?
    }else{//end if consulta
    	echo '<table cellspacing="0"><tr><td class="titulo3">NO HAY INFORMACI&oacute;N PARA SELECCIONAR</td></tr></table>';
    }//end if consulta ?>				
</table>