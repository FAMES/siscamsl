<? 	include('../../scripts/funciones.php');
//RECIBO LAS VARIABLES
	$antig = $_REQUEST['antig'];	
	$anioActual = date('Y')."-01-01";
?>


<table cellspacing="0" width="950">
    <tr><td colspan="8" class="titulo2">Listado de Casos</td></tr>    
    <tr class="TR1">       
        <td width="30%"><div align="center">Clinica</div></td>
        <td width="10%"><div align="center">Telefono</div></td>
        <td width="10%"><div align="center">Banco</div></td>                  
        <td width="10%"><div align="center">Cuenta</div></td>
<? if($antig == 'TODOS' or $antig == 1)  { ?>
        <td width="8%"><div align="center">Menor a 30 Dias</div></td> 
<? } ?>
<? if($antig == 'TODOS' or $antig == 2)  { ?>
        <td width="8%"><div align="center">Menor a 60 Dias</div></td> 
<? } ?>
<? if($antig == 'TODOS' or $antig == 3)  { ?>
        <td width="8%"><div align="center">Menor a 90 Dias</div></td> 
<? } ?>
<? if($antig == 'TODOS' or $antig == 4)  { ?>
        <td width="8%"><div align="center">Mayor a 90 Dias</div></td> 
<? } ?>
        <td width="8%"><div align="center">Totales</div></td>                  
    </tr>
<? 	
  	//Consulta por defecto
		if($antig == 'TODOS')
		{								
			$consulta = "SELECT distinct(B.cli_rif) as rif
						 FROM  
							siscam.bitacora A, siscam.clinicas B, siscam.Estado C, siscam.Factura D, siscam.Autorizaciones E									
						 WHERE 
								D.aut_cartaaval = E.aut_cartaaval and
								E.cli_rif = B.cli_rif and
								C.esta_id = B.esta_id and
								A.aut_cartaaval = E.aut_cartaaval and
								E.aut_modalidad <> 'ESPECIAL' and	
								E.aut_Estatus <> 'PDO' and								
								D.fechaing > '".$anioActual."'
						 ORDER BY 
								B.cli_rif";																				
		}elseif($antig == 1) 
		{ 
			    $consulta = "SELECT distinct(B.cli_rif) as rif
						 FROM  
							siscam.bitacora A, siscam.clinicas B, siscam.Estado C, siscam.Factura D, siscam.Autorizaciones E									
						 WHERE 
								D.aut_cartaaval = E.aut_cartaaval and
								E.cli_rif = B.cli_rif and
								C.esta_id = B.esta_id and
								A.aut_cartaaval = E.aut_cartaaval and
								E.aut_modalidad <> 'ESPECIAL' and	
								E.aut_Estatus <> 'PDO' and
								(date('now') - A.bit_recfecha) <= 30 and								
								D.fechaing > '".$anioActual."'
						 ORDER BY 
								B.cli_rif";
				$aux = " (date('now') - A.bit_recfecha) <= 30 and ";
		}elseif($antig == 2) 
		{ 
				$consulta = "SELECT distinct(B.cli_rif) as rif
						 FROM  
							siscam.bitacora A, siscam.clinicas B, siscam.Estado C, siscam.Factura D, siscam.Autorizaciones E									
						 WHERE 
								D.aut_cartaaval = E.aut_cartaaval and
								E.cli_rif = B.cli_rif and
								C.esta_id = B.esta_id and
								A.aut_cartaaval = E.aut_cartaaval and
								E.aut_modalidad <> 'ESPECIAL' and	
								E.aut_Estatus <> 'PDO' and
								(date('now') - A.bit_recfecha) > 30 and (date('now') - A.bit_recfecha) <= 60 and
								D.fechaing > '".$anioActual."'
						 ORDER BY 
								B.cli_rif"; 
				$aux = " (date('now') - A.bit_recfecha) > 30 and (date('now') - A.bit_recfecha) <= 60 and ";
		}elseif($antig == 3) 
		{ 
				$consulta = "SELECT distinct(B.cli_rif) as rif
						 FROM  
							siscam.bitacora A, siscam.clinicas B, siscam.Estado C, siscam.Factura D, siscam.Autorizaciones E									
						 WHERE 
								D.aut_cartaaval = E.aut_cartaaval and
								E.cli_rif = B.cli_rif and
								C.esta_id = B.esta_id and
								A.aut_cartaaval = E.aut_cartaaval and
								E.aut_modalidad <> 'ESPECIAL' and
								E.aut_Estatus <> 'PDO' and	
								(date('now') - A.bit_recfecha) > 60 and (date('now') - A.bit_recfecha) <= 90 and
								D.fechaing > '".$anioActual."'
						 ORDER BY 
								B.cli_rif"; 
				$aux = " (date('now') - A.bit_recfecha) > 60 and (date('now') - A.bit_recfecha) <= 90 and ";
		}	
		elseif($antig == 4) 
		{ 
				$consulta = "SELECT distinct(B.cli_rif) as rif
						 FROM  
							siscam.bitacora A, siscam.clinicas B, siscam.Estado C, siscam.Factura D, siscam.Autorizaciones E									
						 WHERE 
								D.aut_cartaaval = E.aut_cartaaval and
								E.cli_rif = B.cli_rif and
								C.esta_id = B.esta_id and
								A.aut_cartaaval = E.aut_cartaaval and
								E.aut_modalidad <> 'ESPECIAL' and	
								E.aut_Estatus <> 'PDO' and
								(date('now') - A.bit_recfecha) > 90 and
								D.fechaing > '".$anioActual."'
						 ORDER BY 
								B.cli_rif";
				$aux = " (date('now') - A.bit_recfecha) > 90 and "; 		
		}							
    		$query = @Consultar($consulta);	
    			if($query != false)
				{
					$cont=pg_num_rows($query); 
					$num = 1; 
					$i=1;
					$data=array();
					$total = 0;
    					while($row = pg_fetch_object($query))
						{
							$monto1=0;
							$monto2=0;
							$monto3=0;
							$monto4=0;
							$totalAntig=0;														
							$rif = $row->rif; 							
							$consAntig = "SELECT B.cli_rif, 
												 B.cli_nombre, 
												 B.cli_telefono, 
												 B.cli_banco, 
												 B.cli_cuentaban, 
											 	 B.cli_convenio, 
												 B.cli_estatus, 
												 C.esta_estado, 
												 C.esta_regionp, 
												 D.fac_monto as monto,
												 (date('now') - A.bit_recfecha) as dias
										  FROM siscam.bitacora A, siscam.clinicas B, siscam.Estado C, siscam.Factura D, siscam.Autorizaciones E									
									 	  WHERE 
												D.aut_cartaaval = E.aut_cartaaval and
												E.cli_rif = B.cli_rif and
												C.esta_id = B.esta_id and
												A.aut_cartaaval = E.aut_cartaaval and
												E.aut_modalidad <> 'ESPECIAL' and
												E.aut_Estatus <> 'PDO' and
												B.cli_rif = '$rif' and
												$aux								
												  D.fechaing > '".$anioActual."'				
										ORDER BY 
												B.cli_rif, dias";
							  $queryAntig = pg_query($consAntig);
							  	while($fila = pg_fetch_object($queryAntig))
								{
									$nombre = $fila->cli_nombre;
									$telefono = $fila->cli_telefono;
									$banco = $fila->cli_banco;
									$cuentaban = $fila->cli_cuentaban;	
									if($antig == 'TODOS')
									{																	 
										if($fila->dias <= 30) { $monto1 += $fila->monto; }	
										if($fila->dias > 30 and $fila->dias <=60) { $monto2 += $fila->monto; }
										if($fila->dias > 60 and $fila->dias <=90) { $monto3 += $fila->monto; }
										if($fila->dias > 90) { $monto4 += $fila->monto; }		
									}elseif($antig == 1)
									{
										 $monto1 += $fila->monto;
									}elseif($antig == 2)
									{
										 $monto2 += $fila->monto;	
									}elseif($antig == 3)
									{
										 $monto3 += $fila->monto;	
									}elseif($antig == 4)
									{
										 $monto4 += $fila->monto;	
									}
									
									$totalAntig = $monto1+$monto2+$monto3+$monto4;							
								}
								?>
    	<input type="hidden" id="valor_oculto" name="valor_oculto" value="<?=pg_num_rows($query); ?>" />
    <tr <? if($num%2==0){?> class="TR2"<? }else{ ?>class="TR3"<? } ?>>              
        <td width="30%"  ><? if ($nombre <> ''){  echo $nombre; } else { echo 'NO DEFINIDO'; }?></td>  
        <td width="10%" align="center" ><? if ($telefono <> ''){  echo $telefono; } else { echo 'NO DEFINIDO'; }?></td> 
        <td width="10%" align="center" ><? if ($banco <> ''){  echo $banco; } else { echo 'NO DEFINIDO'; }?></td>
        <td width="10%" align="center" ><? if ($cuentaban <> ''){  echo $cuentaban; } else { echo 'NO DEFINIDO'; }?></td>        
<? if($antig == 'TODOS' or $antig == 1)  { ?>
        <td width="8%" align="center" ><? if ($monto1 <> ''){  echo number_format($monto1,2,',','.'); } else { echo '0.00'; }?></td> 
<? }if($antig == 'TODOS' or $antig == 2) { ?>
        <td width="8%" align="center" ><? if ($monto2 <> ''){  echo number_format($monto2,2,',','.'); } else { echo '0.00'; }?></td> 
<? }if($antig == 'TODOS' or $antig == 3) { ?>
        <td width="8%" align="center" ><? if ($monto3 <> ''){  echo number_format($monto3,2,',','.'); } else { echo '0.00'; }?></td> 
<? }if($antig == 'TODOS' or $antig == 4) { ?>
        <td width="8%" align="center" ><? if ($monto4 <> ''){  echo number_format($monto4,2,',','.'); } else { echo '0.00'; }?></td> 
<? } ?>
        <td width="8%" align="center" ><? if ($totalAntig <> ''){  echo number_format($totalAntig,2,',','.'); } else { echo '0.00'; }?></td> 
   </tr>
    <?  $i++; $num++; $total+=$totalAntig; 
					} ?>
	<tr class="TOTAL">
    	<td colspan="9" width="100%">Total : <?= number_format($total,2,',','.'); ?> Bs.</td>
    </tr>	
     <?
    }else{//end if consulta
    	echo '<table cellspacing="0"><tr><td class="titulo3">NO HAY INFORMACI&oacute;N PARA SELECCIONAR</td></tr></table>';
    }//end if consulta ?>				
</table>