<? 	include('../../scripts/funciones.php');
//RECIBO LAS VARIABLES
	$fechadesde = $_REQUEST['fechadesde'];
	//echo "fechadesde : ". $fechadesde;
	$fechahasta = $_REQUEST['fechahasta'];
	//echo "fechadesde : ". $fechahasta;
	$rifClinica = $_REQUEST['clinica'];
	//echo "riflinica: ". $rifClinica;	
	$anio = $_REQUEST['anio'];
	//echo "anio: ". $anio;	
?>


<table cellspacing="0" width="950">
    <tr><td colspan="8" class="titulo2">Listado de Casos</td></tr>    
    <tr class="TR1">       
        <td width="9%"><div align="center">Rif.</div></td>
        <td width="30%"><div align="center">Clinica</div></td>
        <td width="13%"><div align="center">Estado</div></td>
        <td width="7%"><div align="center">Región</div></td>        
        <td width="7%"><div align="center">Total Deuda</div></td>           
    </tr>
    <? 	
    	//Consulta por defecto
		if($fechadesde != NULL and $fechahasta != NULL) 
		{ 
			if($rifClinica != 'TODOS')
			{
				$consulta = "SELECT	C.cli_rif, 
									C.cli_nombre,
									C.cli_direccion,									 
									B.esta_estado,
									B.esta_regionp,
									sum(D.fac_monto) as monto									
							 FROM 
							 		siscam.autorizaciones A, 
									siscam.estado B, 
									siscam.clinicas C, 
									siscam.factura D,								
									siscam.estudiantes G
							 WHERE 
							 		A.cli_rif = C.cli_rif and
		      						C.esta_id = B.esta_id and
		      						A.est_id = G.est_id   and		      						
		      						A.aut_cartaaval = D.aut_cartaaval and
									A.aut_estatus <> 'PDO' and
									A.aut_modalidad <> 'ESPECIAL' and
		      						D.fechaing >= '$fechadesde' and 
									D.fechaing <= '$fechahasta' and 
		      						C.cli_rif = '$rifClinica' 
							 GROUP BY C.cli_rif, B.esta_estado,c.cli_nombre, c.cli_direccion, B.esta_regionp 
							 ORDER BY B.esta_regionp";
								
			}			
			else
			{
				$consulta = "SELECT C.cli_rif, 
									C.cli_nombre,
									C.cli_direccion,									 
									B.esta_estado,
									B.esta_regionp,
									sum(D.fac_monto) as monto									
							 FROM 
							 		siscam.autorizaciones A, 
									siscam.estado B, 
									siscam.clinicas C, 
									siscam.factura D,								
									siscam.estudiantes G
							 WHERE 
							 		A.cli_rif = C.cli_rif and
		      						C.esta_id = B.esta_id and
		      						A.est_id = G.est_id   and		      						
		      						A.aut_cartaaval = D.aut_cartaaval and
									A.aut_estatus <> 'PDO' and
									A.aut_modalidad <> 'ESPECIAL' and
		      						D.fechaing >= '$fechadesde' and 
									D.fechaing <= '$fechahasta'		      						
							 GROUP BY C.cli_rif, c.cli_nombre, B.esta_estado, c.cli_direccion, B.esta_regionp 
							 ORDER BY B.esta_regionp";												
			}
		}elseif($anio != 0)
		{
			$fecini = $anio.'-01-01';
			$fechas = $anio.'-12-31';
			$cartaini = $anio.'00000';
			$cartafin = $anio.'99999';
			if($rifClinica != 'TODOS') { $aux = " and C.cli_rif = '$rifClinica' " ; }
			$consulta = "SELECT	C.cli_rif, 
									C.cli_nombre,
									C.cli_direccion,									 
									D.esta_estado,
									D.esta_regionp,
									sum(A.fac_monto) as monto									
							 FROM 
							 		siscam.factura A, 
									siscam.autorizaciones B, 
									siscam.clinicas C, 
									siscam.estado D, 
									siscam.estudiantes E
							 WHERE 
							 		(A.fechaing >= '$fecini' and A.fechaing <= '$fechas' and A.aut_cartaaval <= $cartafin 
									or (A.fechaing <= '$fechas' and A.fechaing <> '1900-01-01' and A.aut_cartaaval >= $cartaini 
									and A.aut_cartaaval <= $cartafin))
									and A.aut_cartaaval = B.aut_cartaaval
									and B.cli_rif = C.cli_rif
									and C.esta_id = D.esta_id
									and B.est_id = E.est_id
									and B.aut_modalidad <> 'ESPECIAL'						
									and B.aut_Estatus <> 'PDO'
									$aux
							 GROUP BY C.cli_rif, D.esta_estado,c.cli_nombre, c.cli_direccion, D.esta_regionp 
							 ORDER BY D.esta_regionp, D.esta_estado, c.cli_nombre";	
		}
		
    	$query = @Consultar($consulta);	
    		if($query != false)
			{
				$cont=pg_num_rows($query); 
				$num = 1; 
				$i=1;
				$data=array();
    				while($row = pg_fetch_object($query))
					{ ?>
    	<input type="hidden" id="valor_oculto" name="valor_oculto" value="<?=pg_num_rows($query); ?>" />
    <tr <? if($num%2==0){?> class="TR2"<? }else{ ?>class="TR3"<? } ?>>              
        <td width="9%" align="center"><? if ($row->cli_rif <> '') { ?><a href="?id=deudaxclinica2&rif=<?= $row->cli_rif ?>&fechadesde=<?= $fechadesde ?>&fechahasta=<?= $fechahasta ?>&anio=<?= $anio ?>"><?= $row->cli_rif; } else { echo 'NO DEFINIDO'; } ?></a></td>
        <td width="30%"><? if ($row->cli_nombre <> '') {  echo $row->cli_nombre;  } else { echo 'NO DEFINIDO'; }?></td>
        <td width="12%" align="center" ><? if ($row->esta_estado <> ''){  echo $row->esta_estado; } else { echo 'NO DEFINIDO'; }?></td>         
        <td width="10%" align="center" ><? if ($row->esta_regionp <> '') {  echo substr($row->esta_regionp,8,1);  } else { echo 'NO DEFINIDO'; }?></td>
        <td width="10%" align="center" ><? if ($row->monto <> '')  {  echo $row->monto;   } else { echo 'NO DEFINIDO'; }?></td>    
    </tr>
    <?  $i++; $num++; $totalDeuda+=$row->monto; 
					} ?>
	<tr class="TOTAL">
    	<td colspan="7" width="100%">Total : <?= number_format($totalDeuda,2,',','.'); ?> Bs.</td>
    </tr>	
     <?
    }else{//end if consulta
    	echo '<table cellspacing="0"><tr><td class="titulo3">NO HAY INFORMACI&oacute;N PARA SELECCIONAR</td></tr></table>';
    }//end if consulta ?>				
</table>