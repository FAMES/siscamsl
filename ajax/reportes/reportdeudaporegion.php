<? 	include('../../scripts/funciones.php');
//RECIBO LAS VARIABLES
	$fechadesde = $_REQUEST['fechadesde'];
	//echo "fechadesde : ". $fechadesde;
	$fechahasta = $_REQUEST['fechahasta'];
	//echo "fechadesde : ". $fechahasta;
	$region = $_REQUEST['region'];
	//echo "region: ". $region;	
	$anio = $_REQUEST['anio'];
?>


<table cellspacing="0" width="950">
    <tr><td colspan="8" class="titulo2">Listado de Casos</td></tr>    
    <tr class="TR1">       
        <td width="9%"><div align="center">Estado</div></td>
        <td width="15%"><div align="center">Region</div></td>
        <td width="30%"><div align="center">Deuda</div></td>                  
    </tr>
    <? 	
    	//Consulta por defecto
		if($fechadesde != NULL and $fechahasta != NULL) 
		{ 
			if($region != 'TODOS')
			{
				$consulta = "SELECT
									B.esta_estado,
									B.esta_regionp,
									sum(D.fac_monto) as monto									
							 FROM 
							 		siscam.autorizaciones A, 
									siscam.estado B, 
									siscam.clinicas C, 
									siscam.factura D,
									siscam.bitacora F, 
									siscam.estudiantes G
							 WHERE 
							 		A.cli_rif = C.cli_rif and
		      						C.esta_id = B.esta_id and
		      						A.est_id = G.est_id   and
		      						A.aut_cartaaval = F.aut_cartaaval and
		      						A.aut_cartaaval = D.aut_cartaaval and
									A.aut_estatus <> 'PDO' and
									A.aut_modalidad <> 'ESPECIAL' and
		      						D.fechaing >= '$fechadesde' and 
									D.fechaing <= '$fechahasta' and 
		      						B.esta_regionp = '$region' 
							 GROUP BY B.esta_estado, B.esta_regionp
							 ORDER BY B.esta_estado";
								
			}			
			else
			{
				$consulta = "SELECT
									B.esta_estado,
									B.esta_regionp,
									sum(D.fac_monto) as monto									
							 FROM 
							 		siscam.autorizaciones A, 
									siscam.estado B, 
									siscam.clinicas C, 
									siscam.factura D,
									siscam.bitacora F, 
									siscam.estudiantes G
							 WHERE 
							 		A.cli_rif = C.cli_rif and
		      						C.esta_id = B.esta_id and
		      						A.est_id = G.est_id   and
		      						A.aut_cartaaval = F.aut_cartaaval and
		      						A.aut_cartaaval = D.aut_cartaaval and
									A.aut_estatus <> 'PDO' and
									A.aut_modalidad <> 'ESPECIAL' and
		      						D.fechaing >= '$fechadesde' and 
									D.fechaing <= '$fechahasta' 
							 GROUP BY B.esta_estado, B.esta_regionp
							 ORDER BY B.esta_regionp, B.esta_estado";												
			}
		}elseif($anio != '0')
		{
			$fecini = $anio.'-01-01';
			$fechas = $anio.'-12-31';
			$cartaini = $anio.'00000';
			$cartafin = $anio.'99999';
			if($region != 'TODOS') { $aux = " and D.esta_regionp = '$region' " ; }
			$consulta = "SELECT		D.esta_estado,
									D.esta_regionp,
									sum(A.fac_monto) as monto									
							 FROM 
							 		siscam.factura A, 
									siscam.autorizaciones B, 
									siscam.clinicas C, 
									siscam.estado D, 
									siscam.estudiantes E
							 WHERE 
							 		(A.fechaing >= '$fecini' and A.fechaing <= '$fechas' and A.aut_cartaaval <= $cartafin 
									or (A.fechaing <= '$fechas' and A.fechaing <> '1900-01-01' and A.aut_cartaaval >= $cartaini 
									and A.aut_cartaaval <= $cartafin))
									and A.aut_cartaaval = B.aut_cartaaval
									and B.cli_rif = C.cli_rif
									and C.esta_id = D.esta_id
									and B.est_id = E.est_id
									and B.aut_modalidad <> 'ESPECIAL'						
									and B.aut_Estatus <> 'PDO'
									$aux
							 GROUP BY D.esta_estado, D.esta_regionp 
							 ORDER BY D.esta_regionp, D.esta_estado";	
		}
		
    	$query = @Consultar($consulta);	
    		if($query != false)
			{
				$cont=pg_num_rows($query); 
				$num = 1; 
				$i=1;
				$data=array();
    				while($row = pg_fetch_object($query))
					{ ?>
    	<input type="hidden" id="valor_oculto" name="valor_oculto" value="<?=pg_num_rows($query); ?>" />
    <tr <? if($num%2==0){?> class="TR2"<? }else{ ?>class="TR3"<? } ?>>              
        <td width="9%" align="center" ><? if ($row->esta_estado <> ''){  echo $row->esta_estado; } else { echo 'NO DEFINIDO'; }?></td>
         <td width="15%" align="center" ><? if ($row->esta_regionp <> ''){  echo substr($row->esta_regionp,8,1); } else { echo 'NO DEFINIDO'; }?></td>
        <td width="30%" align="center"><? if ($row->monto <> '') {  echo $row->monto;  } else { echo 'NO DEFINIDO'; }?></td>          
    </tr>
    <?  $i++; $num++; $totalDeuda+=$row->monto; 
					} ?>
	<tr class="TOTAL">
    	<td colspan="7" width="100%">Total : <?= number_format($totalDeuda,2,',','.'); ?> Bs.</td>
    </tr>	
     <?
    }else{//end if consulta
    	echo '<table cellspacing="0"><tr><td class="titulo3">NO HAY INFORMACI&oacute;N PARA SELECCIONAR</td></tr></table>';
    }//end if consulta ?>				
</table>