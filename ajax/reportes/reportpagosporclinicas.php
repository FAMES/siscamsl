<? 	include('../../scripts/funciones.php');
//RECIBO LAS VARIABLES
	$fechadesde = $_REQUEST['fechadesde'];
	//echo "fechadesde : ". $fechadesde;
	$fechahasta = $_REQUEST['fechahasta'];
	//echo "fechadesde : ". $fechahasta;
	$rif_clinica = $_REQUEST['rif_clinica'];
	//echo "rif_clinica: ". $rif_clinica;
?>


<table cellspacing="0" width="950">
    <tr><td colspan="8" class="titulo2">Listado de Clinicas</td></tr>    
    <tr class="TR1">       
        <td width="13%"><div align="left">RIF.</div></td>
        <td width="39%"><div align="left">Cl&iacute;nica</div></td>
        <td width="13%"><div align="left">Numero de Pagos</div></td>
        <td width="12%"><div align="left">Monto Cancelado</div></td>          
    </tr>
    <? 	
    	//Consulta por defecto
		if($fechadesde != NULL and $fechahasta != NULL) 
		{ 
			if($rif_clinica != NULL)
			{
				$consulta = "select count(distinct(r.re_id)) as cuenta, 
							 r.cli_rif, 
							 c.cli_nombre, 
							 sum(r.re_totalcan) as total 
					  from 
	 						siscam.relacion r, siscam.clinicas c, siscam.autorizaciones b
					  where 
					  		b.aut_estatus <> 'FIN' and
					  		r.re_fecha between '$fechadesde' and '$fechahasta' and  
							r.cli_rif = '$rif_clinica' and
							r.cli_rif = c.cli_rif and
							r.re_id = b.re_id														
					  group by 
					  		r.cli_rif, c.cli_nombre";	
			}
			else
			{
				$consulta = "select count(distinct(r.re_id)) as cuenta, 
							 r.cli_rif, 
							 c.cli_nombre, 
							 sum(r.re_totalcan) as total 
					  from 
	 						siscam.relacion r, siscam.clinicas c, siscam.autorizaciones b
					  where 
					  		b.aut_estatus <> 'FIN' and
					  		r.re_fecha between '$fechadesde' and '$fechahasta' and 
							r.cli_rif = c.cli_rif and
							r.re_id = b.re_id
					  group by 
					  		r.cli_rif, c.cli_nombre";							
			}
		}
		elseif($rif_clinica != NULL)
		{
			$fechadesde = '2011-01-01';
			$fechahasta = date('Y-m-d');
			$consulta = "select count(distinct(r.re_id)) as cuenta, 
							 r.cli_rif, 
							 c.cli_nombre, 
							 sum(r.re_totalcan) as total 
					  from 
	 						siscam.relacion r, siscam.clinicas c, siscam.autorizaciones b
					  where
					  		b.aut_estatus <> 'FIN' and 					  		
							r.cli_rif = '$rif_clinica' and
							r.cli_rif = c.cli_rif and
							r.re_fecha between '$fechadesde' and '$fechahasta' and
							r.re_id = b.re_id
					  group by 
					  		r.cli_rif, c.cli_nombre";					
		}
		else
		{ 
			$fechadesde = '2011-01-01';
			$fechahasta = date('Y-m-d');			
			$consulta = "select distinct(r.cli_rif), 
							r.cli_rif,
							c.cli_nombre, 
							sum(r.re_totalcan) as total,
							count(distinct(r.re_id)) as cuenta
					   from 
					 		siscam.relacion r, siscam.clinicas c, siscam.autorizaciones b
					   where
					   		b.aut_estatus <> 'FIN' and
							c.cli_rif = r.cli_rif and
							r.re_fecha between '$fechadesde' and '$fechahasta' and
							r.re_id = b.re_id
					   group by r.cli_rif, c.cli_nombre";
		}
		
		
		
		
    	$query = @Consultar($consulta);	
    		if($query != false)
			{
				$cont=pg_num_rows($query); 
				$num = 1; 
				$i=1;
				$totalPag = 0;
				$data=array();
    				while($row = pg_fetch_object($query)){ ?>
    	<input type="hidden" id="valor_oculto" name="valor_oculto" value="<?=pg_num_rows($query); ?>" />
    <tr <? if($num%2==0){?> class="TR2"<? }else{ ?>class="TR3"<? } ?>>       
        <td width="13%" ><a href="?id=pagosporclinicas2&cod=<?= $row->cli_rif; ?>&fechadesde=<?=$fechadesde; ?>&fechahasta=<?= $fechahasta; ?>"><?= $row->cli_rif; ?></a></td>
        <td width="39%" ><? if ($row->cli_nombre <> ''){ echo $row->cli_nombre; }else{ echo 'No definido'; }?></td>
        <td width="13%" ><? if ($row->cuenta <> ''){ echo substr($row->cuenta,0,50); }else{ echo 'No definido'; }?></td>
        <td width="12%" ><? if ($row->total <> ''){ echo $row->total; }else{ echo 'No definido'; }?></td>       
    </tr>
    <?  $i++; $num++; $totalPag += $row->total; }   ?>
    <tr class="TOTAL">
    	<td colspan="4" width="100%">Total : <?= number_format($totalPag,2,',','.'); ?> Bs.</td>
    </tr>		<?
    }else{//end if consulta
    	echo '<table cellspacing="0"><tr><td class="titulo3">NO HAY INFORMACI&oacute;N PARA SELECCIONAR</td></tr></table>';
    }//end if consulta ?>				
</table>