<?php
require('../fpdf.php');

	//Conexion al servidor
	$servidor = "190.190.1.3";
	$base = "intranet";
	$usuario = "postgres";
	$password = "postgres";   
	pg_pconnect("host=$servidor dbname=$base user=$usuario password=$password"); 
	 
	//Consulta SQL
	$consulta = "select * from siscam.clinicas order by siscam.clinicas.cli_rif";
	$query = pg_query($consulta);
	
class PDF extends FPDF
{
	//Cabecera de página
	function Header()
	{
		//fecha
		$this->SetY(18);
		$this->SetFont('Arial','I',8);
		$this->Cell(0,8,'Fecha: '.date('d/m/Y'),0,0,'L');
		//Logo
		$this->Image('encabezado_gris.jpg',13,8,180,10);
		//Arial bold 15
		$this->SetFont('Arial','B',15);
		//Movernos a la derecha
		$this->SetY(15);
		$this->Cell(80);
		//Título		
		$this->Cell(30,25,'Relacion de casos a Pagar',0,0,'C');
		//Salto de línea
		$this->Ln(20);
	}

	//Numero de Página
	function Footer()
	{
		//Posición: a la derecha
		$this->SetY(17);
		//Arial italic 8
		$this->SetFont('Arial','I',8);
		//Número de página
		$this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'R');
	}

function fecha($fecha)
{
	//fecha
	$this->SetY(18);
	$this->SetFont('Arial','I',8);
	$this->Cell(0,8,'Fecha: '.$fecha,0,0,'L');
}

//Tabla del Reporte
function CrearTabla($titulo,$query)
{
	$this->SetY(34);
	$this->SetFont('Times','',8);
    //Anchuras de las columnas
    $w=array(30,50,30);
	
    //Cabeceras
    for($i=0;$i<count($titulo);$i++)
        $this->Cell($w[$i],7,$titulo[$i],0,0,'L');
	$this->Line(10,39,190,39);
    $this->Ln();
	
    //Datos
    while($row = pg_fetch_object($query))  //AQUI SE CREA EL OBJETO CON LOS DATOS DE LA BD
    {
        $this->Cell($w[0],5,$row->cli_rif,0);
        $this->Cell($w[1],5,$row->cli_nombre,0);
        $this->Cell($w[2],5,$row->cli_telefono,0);
        $this->Ln();
    }
	
	//////////////////////////////////////////////////////
	//CODIGO EXTRA PARA GENERAR VARIAS PAGINAS DE PRUEBA
	for($i=1;$i<=40;$i++)
    $this->Cell(0,10,'Imprimiendo línea número '.$i,0,1);
    $this->Ln();
	///////////////////////////////////////////////////////
	
    //Línea de cierre
    //$this->Cell(array_sum($w),0,'',0);
}
}

//Creación del objeto de la clase heredada
$pdf=new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();

//Títulos de las columnas
$titulo=array('Rif','Clinica','Telefono');

//llamada a la funcion de crear tabla y fecha
$pdf->CrearTabla($titulo,$query);
$pdf->Output();

?>
