<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8">
		<link rel="shortcut icon" type="image/ico" href="http://www.sprymedia.co.uk/media/images/favicon.ico">
		
		<title>ColReorder example</title>
		
		<script type="text/javascript" charset="utf-8">
			$(document).ready( function () {
				var oTable = $('#example').dataTable( {
					"sDom": 'R<"H"lfr>t<"F"ip>',
					"bJQueryUI": true,
					"sPaginationType": "full_numbers",
					"oLanguage": { 
"oPaginate": { 
"sPrevious": "<", 
"sNext": ">", 
"sLast": "ULTIMA", 
"sFirst": "PRIMERA" 
}, 

"sLengthMenu": 'Mostrar <select>'+ 
'<option value="10">10</option>'+ 
'<option value="20">20</option>'+ 
'<option value="30">30</option>'+ 
'<option value="40">40</option>'+ 
'<option value="50">50</option>'+ 
'<option value="-1">Todos</option>'+ 
'</select> registros', 

"sInfo": "Mostrando del _START_ a _END_ (Total: _TOTAL_ resultados)", 

"sInfoFiltered": " - filtrados de _MAX_ registros", 

"sInfoEmpty": "No hay resultados de busqueda", 

"sZeroRecords": "No hay registros a mostrar", 

"sProcessing": "Espere, por favor...", 

"sSearch": "Buscar:", 

} 
					
				} );
			} );
		</script>
       
	</head>

<table width="950">
    <tr><td class="titulo1">Bitacora de Reversos </td></tr>
    <tr><td><hr class="linea"/></td></tr>	
   <?
$consulta = Consultar("select * from siscam.vst_aut_bit where 
						 siscam.vst_aut_bit.aut_cartaaval in 
						 	(select siscam.bit_reverso.aut_cartaaval from siscam.bit_reverso where siscam.bit_reverso.accion = 'REVERSO') 
						 order by siscam.vst_aut_bit.aut_cartaaval");			
			$nr=pg_num_rows($consulta);
?>


	

<? if($consulta!=false) { ?>
<body id="dt_example">
		<div id="container">
			
			<form name="frmbitacora" method="post">
		<div id="demo">
        
<table cellpadding="0" cellspacing="0" border="0" id="example" class="display" style="width:100%">
		<!--<tr><td colspan="6" class="titulo2">Atenciones de los Estudiantes</td></tr> -->
     
     
      <thead>
		<tr class="TR1">
		<td width="10%"><div align="left">Carta Aval</div></td>
		<td width="10%"><div align="left">N&ordm; Control </div></td>
		<td width="10%"><div align="left">Cedula</div></td>
		<td width="30%"><div align="left">Nombre</div></td>
		<td width="9%"><div align="left">Modalidad</div></td>
		<td width="13%"><div align="left">Estatus </div></td>
		</tr>
      </thead>
      <tfoot>
      	
      </tfoot>
      <tbody>				
		<? $num = 1;
		$num=$inicial+1;
		while($row = pg_fetch_object($consulta)){ ?>
		<tr <?php if($num % 2 == 0) { echo 'class="TR2"'; } else { echo 'class="TR3"'; } ?>>
            <td width="10%" ><a onClick="document.getElementById('carta').value='<?=$row->aut_cartaaval?>'; document.frmBitReverso.submit();" target="_blank"><?=$row->aut_cartaaval?></a></td>
            <td width="10%" ><?=$row->aut_ncontrol?></td>
            <td width="10%" ><?=$row->est_id ?></td>
            <td width="30%" ><? if ($row->est_pnombre <> ''){ echo $row->est_pnombre; echo " ".$row->est_papellido; }else{ echo 'No definido'; }?></td>
            <td width="9%" ><? if ($row->aut_modalidad <> ''){ echo $row->aut_modalidad; }else{ echo 'No definido'; }?></td>
            <td width="13%" ><? if ($row->aut_estatus =='UMED'){echo 'UNIDAD MEDICA';
                                }elseif ($row->aut_estatus =='ANALISIS'){echo 'ANALISIS';
                                }elseif ($row->aut_estatus =='ANULADO'){echo 'ANULADO';
                                }elseif ($row->aut_estatus =='TRAMITE'){echo 'TRAMITE';
								}elseif ($row->aut_estatus =='ATENDIDO'){echo 'ATENDIDO';
                                }elseif ($row->aut_estatus =='EST'){echo 'ESTUDIO';
                                }elseif ($row->aut_estatus =='AUT'){echo 'AUTORIZADO';
                                }elseif ($row->aut_estatus =='REC'){echo 'RECIBIDO';
                                }elseif ($row->aut_estatus =='PAGO'){echo 'PAGO';
                                }elseif ($row->aut_estatus =='FIN'){echo 'FINANZAS';
                                }elseif ($row->aut_estatus =='ARC'){echo 'ARCHIVO';
                                }elseif ($row->aut_estatus =='PDO'){echo 'PAGADO';
                                }elseif ($row->aut_estatus =='PDOPAR'){echo 'PAGADO PARCIAL';
                                }elseif ($row->aut_estatus =='FINPAR'){echo 'FINANZA PARCIAL';
                                }elseif ($row->aut_estatus =='RECPAR'){echo 'RECEPCION PARCIAL';
                                }elseif ($row->aut_estatus =='PAGOPAR'){echo 'PAGO PARCIAL'; 
								}elseif ($row->aut_estatus =='NOVER'){echo 'NO VERIFICADO'; 
                                }else { echo 'No definido'; }?> 
             </td>
		</tr>
		<? $num++; }?>				
		</table>
		<? }else{//end if consulta
		echo '<table cellspacing="0" width="100%">
			<tr><td class="TR1">Registro de Cartas Avales</td></tr>
			<tr><td class="titulo3">NO SE ENCONTRO LA INFORMACIÓN </td></tr>
			</table>';	
		}//end if consulta ?>
		
		</tr>
        
  </tbody>
  
 </table>
   
   
   <!-- <tr>
		<td>
			<div id="divcbit"></div>
		</td>
	</tr> -->
</table>

</div>
		</form>
			
	
			
			
		</div>
	
<form name="frmBitReverso" method="post" action="reports/reportBitReverso.php" target="_blank">
<input type="hidden" name="carta" id="carta">
</form>
</body>
</html>