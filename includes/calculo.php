<form name="frmcalculo" id="frmcalculo" method="post" onsubmit="return validarvacio(this);">
  <div id="calculo"></div>
  <table border="0" width="950">   
	<tr><td colspan="2" class="titulo1">C&aacute;lculo de Operaciones Financieras</td></tr>	
	<?  //CODIGO DE CONSULTA A LA BD	
		$consulta = "select * from siscam.calculo";
		$con= Consultar($consulta);
		if ($con!=false){	
			$nr=pg_num_rows($con);
			$row = pg_fetch_object($con,0); //AQUI SE CREA EL OBJETO CON LOS DATOS DE LA BD
			$id = $row->cal_id;	
			$ut= $row->cal_utributaria;
			$factor = $row->cal_factor;
			$islrn = $row->cal_islrn;
			$islrm = $row->cal_islrm;
			$sustraendo = $row->cal_sustraendo;
			$islrj = $row->cal_islrj;
		}else{
			$id = 0;	
			$ut= 0;
			$factor = 0;
			$islrn = 0;
			$islrm = 0;
			$sustraendo = 0;
			$islrj = 0;
		} 
		$usuario = $_SESSION['usu_cedul'];
	?>	
	<tr><td><hr class="linea" align="left"></td></tr>	
	<tr><td>
	  	<? if($_SESSION['peracceso'] ==1 or $_SESSION['peracceso'] ==2 or $_SESSION['peracceso'] ==3) { ?>
	  	<input type="button" name="btnModificar" value="Modificar" class="boton" onclick="document.frmcalculo.oculto.value='modificar';desbloquear(this.form);bloqueamonto(this.form);document.frmcalculo.btnEnviar.disabled=false;">				
		<? } ?>
    </td></tr>		
	<tr><td><hr class="linea" align="left"></td></tr> 
	<tr><td></td></tr>
	<tr><td><table width="954">		
			<tr class="TR2">
			  <td width="178" height="28" class="titulo3">Unidad Tributaria:</td>
			  <td class="titulo3" colspan="5">
		      <input name="txtut" id="txtut" type="text" size="10" maxlength="10" class="inputbox" disabled="disabled" onkeypress="return V_all(this,event)" value="<?=@$ut?>" title="Monto de la Unidad Tributaria" onblur="return utributaria(this.form);"/>Bs. &nbsp;*</td>
			</tr>	
			<tr class="TR3">
			  <td height="31" class="titulo3">Monto Factor:</td>
			  <td class="titulo3"><input name="txtfactor" id="txtfactor" type="text" size="10" maxlength="10" class="inputbox" disabled="disabled" onkeypress="return V_all(this,event)" value="<?=@$factor?>" title="Monto Factor" onchange="return montofactor(this.form);"/>*</td>
			  <td colspan="4" class="titulo3">&nbsp;</td>
		    </tr>	
			<tr class="TR2">            	
			  	<td class="titulo3">ISLR Persona Natural<br />(Honorarios Profesionales):</td>
			  	<td width="149" class="titulo3"><input name="txtislrn" id="txtislrn" type="text" size="5" maxlength="5" class="inputbox" disabled="disabled" onkeypress="return V_all(this,event)" value="<?=@$islrn?>" title="ISLR a colocar de Persona Natura"  onchange="return montoislrn(this.form);"/>% *</td>
                <td width="180" class="titulo3">Monto M&iacute;nimo a Imponer<br />Impuesto: </td>
              	<td width="132" class="titulo3"><input name="txtislrm" id="txtislrm" type="text" size="10" maxlength="10" class="inputbox" disabled="disabled" onkeypress="return V_all(this,event)" value="<?=@$islrm?>" title="Monto m&iacute;nimo a imponer el impuesto a una persona natural"/>
                Bs.*</td>
              	<td width="93" class="titulo3">Sustraendo: </td>
              	<td width="194" class="titulo3"><input name="txtsustraendo" id="txtsustraendo" type="text" size="10" maxlength="10" class="inputbox" disabled="disabled" onkeypress="return V_all(this,event)" value="<?=@$sustraendo?>" title="Sustraendo"/>
              	  *</td>              
          </tr>	
		  <tr>
			 <td height="29" class="titulo3">ISLR Persona Jur&iacute;dica:</td>
		    <td class="titulo3" colspan="5"><input name="txtislrj" id="txtislrj" type="text" size="5" maxlength="100" class="inputbox" disabled="disabled" onkeypress="return V_all(this,event)" value="<?=@$islrj?>" title="ISLR a colocar de Persona Jur�dica"/>% *</td>
		  </tr>
		  <tr class="TR3">
		    <td height="27" class="titulo3">&nbsp;</td>
		    <td class="titulo3">&nbsp;</td>
		    <td class="titulo3">&nbsp;</td>
		    <td colspan="3" class="titulo3">&nbsp;</td>
	      </tr>
		  <tr class="TR2">
		    <td class="titulo">Estados:</td>
		    <td class="titulo">Cantidad de Unidades  Tributarias:</td>
		    <td class="titulo">Monto M&iacute;nimo para Aplicar Impuesto:</td>
		    <td width="132" class="titulo">Impuesto de Pago a Timbre Fiscal (RIAM):</td>
		    <td colspan="2" class="titulo">&nbsp;</td>
	      </tr>      
          <? $consulta1 = Consultar("select * from siscam.estado order by siscam.estado.esta_estado asc");		  	
			 $nr=pg_num_rows($consulta1);		
		  	 $numi = 0; 
			 while($row = pg_fetch_object($consulta1)){ ?>
		  <tr <? if($numi%2==0){?> class="TR3"<? }else{ ?>class="TR2"<? } ?>>
		    <td class="titulo3"><?=$row->esta_estado?></td>
		    <td class="titulo3"><input name="ut<?=$numi?>" id="ut<?=$numi?>" type="text" class="inputbox" title="Cantidad de Unidades Tributarias para aplicar el Impuesto de Pago a Timbre Fiscal al estado" onkeypress="return V_real(this,event)" value="<?=$row->esta_ut?>" size="10" maxlength="10" disabled="disabled" onchange="montoimp(this);"/>*</td>
            <td class="titulo3"><input name="monto<?=$numi?>" type="text" class="inputbox" id="monto<?=$numi?>" title="Monto n&iacute;nimo para aplicar 1*1000 al estado" onkeypress="return V_real(this,event)" value="<?=$row->esta_montout?>" size="10" maxlength="10" disabled="disabled"/></td>
		    <td class="titulo3"><input name="riam<?=$numi?>" id="riam<?=$numi?>" type="text" size="5" maxlength="5" class="inputbox" disabled="disabled" onkeypress="return V_all(this,event)" value="<?=$row->esta_riam?>" title="Impuesto 1*1000 a colocar"/>% *</td>
		    <td colspan="2" class="titulo3">&nbsp;</td>   
	      </tr>		      
          <? $estaid.=$row->esta_id.',';
		  	 $numi++; } ?>		  		
	</table></td></tr>
    <tr><td>
    <input type="hidden" name="oculto" id="oculto" value="insertar"/>
    <input type="hidden" name="id" id="id" value="<?=@$id?>"/>
    <input type="hidden" name="estaid" id="estaid" value="<?=$estaid?>">
    <input type="hidden" name="estaut" id="estaut" value="<?=$estaut?>"> 
    <input type="hidden" name="estamonto" id="estamonto" value="<?=$estamonto?>">  
    <input type="hidden" name="estariam" id="estariam" value="<?=$estariam?>">       
    <input type="hidden" id="hdnusuario" name="hdnusuario" value="<?=@$usuario?>"/>
    </td></tr>    
    <tr>
      <td colspan="2" align="center"><input type="button" class="boton" value="Guardar" name="btnEnviar" id="btnEnviar" onclick="estado(this.form);desbloqueamonto(this.form);calculo_ajax();"/></td>
    </tr>
  </table>
</form>