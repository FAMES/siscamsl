<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8">
		<link rel="shortcut icon" type="image/ico" href="http://www.sprymedia.co.uk/media/images/favicon.ico">
		
		<title>ColReorder example</title>
		
		<script type="text/javascript" charset="utf-8">
			$(document).ready( function () {
				var oTable = $('#example').dataTable( {
					"sDom": 'R<"H"lfr>t<"F"ip>',
					"bJQueryUI": true,
					"sPaginationType": "full_numbers",
					"oLanguage": { 
"oPaginate": { 
"sPrevious": "<", 
"sNext": ">", 
"sLast": "ULTIMA", 
"sFirst": "PRIMERA" 
}, 

"sLengthMenu": 'Mostrar <select>'+ 
'<option value="10">10</option>'+ 
'<option value="20">20</option>'+ 
'<option value="30">30</option>'+ 
'<option value="40">40</option>'+ 
'<option value="50">50</option>'+ 
'<option value="-1">Todos</option>'+ 
'</select> registros', 

"sInfo": "Mostrando del _START_ a _END_ (Total: _TOTAL_ resultados)", 

"sInfoFiltered": " - filtrados de _MAX_ registros", 

"sInfoEmpty": "No hay resultados de busqueda", 

"sZeroRecords": "No hay registros a mostrar", 

"sProcessing": "Espere, por favor...", 

"sSearch": "Buscar:", 

} 
					
				} );
			} );
		</script>
	</head>
<table width="950">
	<tr>
	  <td class="titulo1">Centros de Salud</td>
	</tr>
	<tr><td><hr class="linea"/></td></tr>
    
<?

$consulta = Consultar("select * from siscam.clinicas order by siscam.clinicas.cli_nombre asc");
			$nr=pg_num_rows($consulta);
		
?>

<? if($consulta!=false){?>
	
	<form name="frmclinicas" method="post" onsubmit="return validar (this);">
	<tr><td>
	<? /*Abre una tabla*/?>			
	</td></tr>
	<tr><td>
		<? if($_SESSION['peracceso'] !=5 and $_SESSION['permisosid'] == 1 or $_SESSION['peracceso'] !=5 and $_SESSION['permisosid'] == 4 or $_SESSION['peracceso'] !=5 and $_SESSION['permisosid'] == 2 or $_SESSION['peracceso'] !=5 and $_SESSION['permisosid'] == 12 or $_SESSION['peracceso'] !=5) { ?>
    	<input type="button" name="btnNuevo" value="Agregar" class="boton" onclick="document.location.href='?id=clinicas2&mod=nuevo'">
        <? } ?>
	  <input type="button" name="btnListar" value="Ver todos" class="boton" onclick="document.location.href='?id=clinicas'"></td>
	</tr>
	</form>	
     <!-- Campos Ocultos -->    
    <input type="hidden" name="hdnpg" id="hdnpg" value="<?=$_GET['pg'];?>" />    
    <!-- Fin Campos Ocultos -->
	<tr><td><hr class="linea"/></td></tr>
</table>

<body id="dt_example">
		<div id="container">
			<div id="demo">	
   	<table cellpadding="0" cellspacing="0" border="0" id="example" class="display" style="width:100%">
			<thead>				
				<tr>
					<td><div align="left">RIF</div></td>
		            <td><div align="left">Clinica</div></td>
		            <td><div align="left">Direcci&oacuten</div></td>
		            <td><div align="left">Tel&eacutefono</div></td>
		            <td><div align="left">Estatus</div></td>
				</tr>
             </thead>
             <tfoot>
      	
      		  </tfoot>				
		<tbody>
								
			<? while($row = pg_fetch_object($consulta)){?>
				<tr <? if($num%2==0){?> class="TR2"<? }else{ ?>class="TR3"<? } ?>>
					<td width="15%" ><a href="?id=clinicas2&cod=<?=$row->cli_rif?>"><?=$row->cli_rif?></a></td>
				    <td width="42%" ><?=utf8_decode(substr("$row->cli_nombre",0,40))?></td>
					<td width="23%" ><?=substr("$row->cli_direccion",0,22)?></td>
				    <td width="14%" ><?=$row->cli_telefono?></td>
				    <td width="6%" ><?=$row->cli_estatus?></td>
				</tr>
				<? $num++;
				}?>				
			</table>
			<? }else{//end if consulta
				echo '<table cellspacing="0" width="100%">
				<tr>
					<td class="TR1">Universidades</td>
				</tr>
					<tr>
						<td class="titulo3">NO SE ENCONTRO LA INFORMACIÓN </td>
					</tr>
				</table>';	
		}//end if consulta
?>

</tbody>
  
 </table>
   
   
   <!-- <tr>
		<td>
			<div id="divcbit"></div>
		</td>
	</tr> -->
</table>

</div>
		
			
	
			
			
		</div>
   </body>
</html>
	
