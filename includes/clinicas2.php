<script type="text/javascript" src="js/jquery-1.2.3.js"></script>
<script type="text/javascript" src="js/jquery.blockUI.js"></script>
<script>
window.onload = function()
{
	inicializa_clinica();
}
</script>
<?php $mod = @$_GET["mod"];?>	
<form name="frmclinicas" id="frmclinicas" method="post" >
  <table border="0" width="950">   
	<tr>
	  <td colspan="2" class="titulo1"><? if($mod=='nuevo'){ echo 'Nueva '; } ?>Cl&iacute;nica</td>
    </tr>	
	<?  //CODIGO DE CONSULTA A LA BD	
		if($mod!='nuevo'){		
			$cod = @$_GET["cod"];		
			$consulta = "select * from siscam.clinicas where siscam.clinicas.cli_rif like '%$cod%';";
			$aux = Consultar($consulta);
								 	    				
			$row = pg_fetch_object($aux,0); //AQUI SE CREA EL OBJETO CON LOS DATOS DE LA BD
			$rif = $row->cli_rif;
			$clinica = utf8_decode($row->cli_nombre);
			$estado = $row->esta_id;
			$usuario = $row->usu_cedul;
			$direccion = $row->cli_direccion;
			$fechaing= $row->cli_fechaing;
			$usuarioing = $row->usu_nombre_cli;
			$usuariomod = $row-> cli_usu_mod;
			$codigo_area = substr($row->cli_telefono, 0,3);
			$telefono = $row->cli_telefono;
			$cel = $row->cli_celular;
			$correo = $row->cli_correo;
			$persona = $row->cli_contacto;
			$convenio = $row->cli_convenio;	
			$centrouni = $row->cli_centrouni;
			$ciudad = $row->ciu_id;
			$municipio = $row->mun_id;
			$banco = $row->cli_banco;
			$cuentaban = $row->cli_cuentaban;
			$cuentadebe = $row->cli_cuentadebe;
			$cuentahaber = $row->cli_cuentahaber;
			$cboestatus = $row->cli_estatus;
			$cboimpuestoislr = $row->cli_impuestoislr;
			$cboimpuestoltf = $row->cli_impuestoltf;
			$cborespso = $row->cli_respso; 
			if ($row->cli_montoacum!="" or $row->cli_montoacum!=NULL) { $montoacum = $row->cli_montoacum; } else { $montoacum = 0; }		
			$celular = substr($cel, 3); 
			$cod_cel = substr($cel, 0,3);
	?>	
	<tr>
	  <td>
	  		<? if($_SESSION['peracceso'] ==1 or $_SESSION['peracceso'] ==2) { ?>
	  		<input type="button" name="btnModificar" value="Modificar" class="boton" onclick="desbloquear_secundario(this.form);document.frmclinicas.Xtxtmontorespso.disabled=true; document.getElementById('hdnaccion').value='MODIFICAR';">
			<? } ?>
            <? if($_SESSION['peracceso'] ==1 or $_SESSION['peracceso'] ==2) { ?>
			<input type="button" name="btnModificarRif" value="Modificar Rif" class="boton" onclick="desbloquear_principal(this.form); document.getElementById('hdnaccion').value='MODIFICAR_RIF'">				
			<? } ?>
            <? if($_SESSION['peracceso'] ==1) { ?>
			<input name="btnEliminar" type="button" class="boton" value="Eliminar" src="../imagenes/i.p.delete.gif" onclick="if(confirm('�Desea eliminar el registro? Ser� eliminado todo el contenido relacionado')){ document.getElementById('hdnaccion').value = 'ELIMINAR'; envia_clinica(); }" >
            <? } ?>
            <input type="button" id="btnestado" name="btnestado" value="Estado de Cuenta" class="boton" onclick="document.location.href='?id=estadosdecuentaclinica2&cod=<? echo $cod; ?>'">						
			<a href="javascript://" onclick="javascript:window.print();">
			<input name="btnimprimir" type="button" class="boton" id="btnimprimir" value="Imprimir" src="../imagenes/i.p.delete.gif">
			</a>
      </td>
	</tr>		
	<? }// END IF DE !=NUEVO 
	if($mod=='nuevo'){	$montoacum = 0; } ?>		
	<tr>
    	<td><hr class="linea" align="left"></td>
    </tr>	
	<tr>
	  <td colspan="2" class="titulo2">Datos del Centro de Salud</td>
    </tr>
	<tr>
      <td colspan="2"><hr class="linea"></td>
    </tr> 
	<tr><td>&nbsp;</td></tr>
	<tr>
    <tr>
      <td colspan="2" align="center" class="titulo2"><center> Esta Clinica fue Ingresada en sistema por  <? echo $usuarioing; ?>, Su ultima modificaci&oacute;n fue el <? echo CamFormFech($fechaing); ?> por <? echo $usuariomod; ?>  </center>
      </td>
    </tr>
    	<td>
    <table>	
	  <tr>
		<td width="193" class="titulo3">Rif: </td>
		<td class="titulo3">
			  <select id="cbotipo_rif" name="Zcbotipo_rif" class="inputbox" title="Tipo de Rif">
			  <option value="">--</option>
			  <option value="V">V</option>
			  <option value="J">J</option>
              <option value="G">G</option>
               <option value="E">E</option>
			  </select>
			  - <input type="text" id="txtrif" name="Ztxtrif" size="12" maxlength="9" class="inputbox" onkeypress="return V_numero(this,event)" value="<?=@substr($rif,1); ?>" title="Rif de la Cl�nica a Incluir" onblur="validar_rif()" />
	   *</td>
		<td width="188" class="titulo3">Monto Pagado Acumulado:</td>
			<td width="171" class="titulo3"><input type="text" id="Xtxtmontorespso" name="Xtxtmontorespso" size="12" maxlength="9" class="inputbox" onkeypress="return V_numero(this,event)" value="<?=$montoacum;?>" title="Monto Acumulado de Responsabilidad Social" disabled="disabled"/></td>
			
	  </tr>			
	  <tr>
		<td class="titulo3">Nombre:</td>
		<td colspan="3" class="titulo3"><input id="txtclinica" name="txtclinica" type="text" size="100" maxlength="100" class="inputbox"  value="<?=@$clinica?>" title="Nombre de la Cl�nica a Incluir"/>*</td>
	  </tr>
	  <tr>
	    <td class="titulo3">Localidad:</td>
		<td colspan="3" class="titulo3">
		  <? //LLENADO DE LOS COMBO'S
					$consulta2="select esta_id, esta_estado from siscam.estado order by esta_estado";				
					$aux2 = Consultar($consulta2);
					$consulta3="select * from siscam.ciudad"; 				
					$aux3 = Consultar($consulta3);
					$consulta4="select mun_id,mun_municipio from siscam.municipio"; 				
					$aux4 = Consultar($consulta4);
		  ?>
		<!-- list/menu donde se manipulan los estados-->
           <select id="cboestado" name="cboestado" class="inputbox" 
             title="Nombre del Estado a Seleccionar" onchange="asignar_ciudad('cbociudad','cboestado')" >
		  <? if($mod=='nuevo'){ ?>
			   <option value="">=Seleccione=</option> 
		  <?php } 
				 $total=pg_num_rows($aux2); 
				   for ($i=0; $i < $total; $i++) { 
					if (pg_result($aux2,$i,"esta_id")==$estado) { ?>
			   <option value="<?= pg_result($aux2, $i, "esta_id"); ?>" selected="selected">
			 				  <?php echo pg_result($aux2, $i, "esta_estado"); continue; } ?></option>  	
			   <option value="<?= pg_result($aux2, $i, "esta_id"); ?>">
				 			  <?= pg_result($aux2, $i, "esta_estado"); ?></option>
							  <?php } ?>
  		  </select>
	<!-- list/menu donde se manipulan las ciudades cargados mediante AJAX-->
          <select id="cbociudad" name="cbociudad" class="inputbox" 
           title="Nombre de la Ciudad a Seleccionar" onchange="codigo_ajax();" >
		 <? if($mod=='nuevo'){ ?>
		  	  <option value="">=Seleccione=</option> 
		 <?php } 
				 for($i=0; $i< pg_num_rows($aux3) ;$i++) { 
				  if(pg_result($aux3, $i, "ciu_id") == $ciudad) { ?>
		  	  <option value="<?= pg_result($aux3, $i, "ciu_id"); ?>" selected="selected">
							 <?php echo pg_result($aux3, $i, "ciu_ciudad"); } } ?></option>
		  </select>
	<!-- list/menu donde se manipulan los municipios a traves de AJAX--> 
		 <select id="cbomunicipio" name="cbomunicipio" class="inputbox" 
		   title="Nombre del Municipio a Seleccionar">
		<? if($mod=='nuevo'){ ?>
		     <option value="">=Seleccione=</option> <?php } 
			 	for($i=0;$i < pg_num_rows($aux4); $i++) { 
			  	 if(pg_result($aux4, $i, "mun_id") == $municipio) { ?>
			 <option value="<?= pg_result($aux4, $i, "mun_id"); ?>" selected="selected">
						    <?php echo pg_result($aux4, $i, "mun_municipio"); } } ?></option>
		  </select> 		
	 </td>
	</tr>				
	<tr>
	  <td class="titulo3">Direcci&oacute;n:</td>
	  <td colspan="3" class="titulo3"><textarea name="txtdireccion" id="txtdireccion" class="inputbox" cols="80" rows="4" onkeypress="return V_all(this,event)" title="Direcci&oacute;n de la Cl�nica a Incluir"><?=@$direccion?></textarea>*</td>
    </tr>
    
	<tr>
	  <td class="titulo3">Tel&eacute;fono:</td>
	  <td colspan="3" class="titulo3">
	 <h id="divcod_area" class="inputbox"><b><?php if($codigo_area!='') { echo $codigo_area; } else { echo "---"; } ?></b></h>  - 
    <input id="txttelefono" name="txttelefono" type="text" size="10" maxlength="11" class="inputbox" onkeypress="return V_numero(this,event)" value="<?=substr(@$telefono,3)?>" title="N&uacute;mero Telef�nico de la Cl&iacute;nica a Incluir" onblur="validar_cantidad()"/>*			 	  </td>
	</tr>
     <tr>
       <td class="titulo3">Celular:</td>
       <td colspan="4">
         <select name="cbocodcel" type="text" id="cbocodcel" class="inputbox" maxlength="4" >
           <option value="412">412</option>
           <option value="414">414</option>
           <option value="424">424</option>
           <option value="416">416</option>
           <option value="426">426</option>
           </select>-
         <input name="Xtxtcel" id="txtcel" type="text" size="10" maxlength="7" class="inputbox" onkeypress="return V_numero(this,event)" value="<?=@$celular; ?>" title="Tel&eacute;fono Celular de la Clinica a Incluir" onblur="valida_celular()" />
         *</td>
     </tr>
     <tr>
		<td class="titulo3">Correo Electronico:</td>
		<td colspan="3" class="titulo3"><input id="txtcorreo" name="txtcorreo" type="text" size="40" maxlength="40" class="inputbox"  value="<?=@$correo?>" title="Correo Electronico a Incluir"/>*</td>
	  </tr>	
      <tr>
		<td class="titulo3">Persona de Contacto:</td>
		<td colspan="3" class="titulo3"><input id="txtpersona" name="txtpersona" type="text" size="50" maxlength="50" class="inputbox"  value="<?=@$persona?>" title="Persona de Contacto a Incluir"/>*</td>
	  </tr>
	    <tr>
	  <td class="titulo3">Convenio:</td>
	  <td colspan="3" class="titulo3">					
				<select name="cboconvenio" id="cboconvenio" class="inputbox" title="Forma de Convenio de la Cl�nica a Sleccionar">					
        			<? if ($convenio!='' or $convenio!=NULL ) { 					
						if ($convenio=='SI') { ?>
							<option value="">=Seleccione=</option>
							<option value="SI" selected="selected">SI</option> 
							<option value="NO">NO</option><? } else { ?>
							<option value="0">=Seleccione=</option>
							<option value="NO" selected="selected">NO</option> 
							<option value="SI" >SI</option><? } }
					else { ?>
						<option value="">=Seleccione=</option>
						<option value="SI">SI</option> 
						<option value="NO">NO</option><? } ?>								
				</select>*
             </td>
		  </tr>
      <tr>
	  <td class="titulo3">Centro de Salud Universitario:</td>
	  <td colspan="3" class="titulo3">					
				<select name="cbocentrouni" id="cbocentrouni" class="inputbox" title="Forma de Centro de Salud Universitaria a Seleccionar">
        			<? if ($centrouni!='' or $centrouni!=NULL ) { 					
						if ($centrouni=='SI') { ?>
							<option value="">=Seleccione=</option>
							<option value="SI" selected="selected">SI</option> 
							<option value="NO">NO</option><? } else { ?>
							<option value="0">=Seleccione=</option>
							<option value="NO" selected="selected">NO</option> 
							<option value="SI" >SI</option><? } }
					else { ?>
						<option value="">=Seleccione=</option>
						<option value="SI">SI</option> 
						<option value="NO">NO</option><? } ?>								
				</select>*
             </td>
		  </tr>
          	<tr>
			  <td class="titulo3">Banco:</td>
			  <td colspan="3" class="titulo3"><select name="cbobanco" id="cbobanco" type="text" maxlength="50" class="inputbox" onkeypress="return V_letra(this,event)" title="Banco de la Clinica a Incluir" value="<?=@$banco?>"> 
			  <option value="">=Seleccione=</option>
              <option value="ACTIVO">ACTIVO</option>			 
			  <option value="CARONI">Caroni</option>
			  <option value="CANARIAS">Canarias</option>
			  <option value="CONFEDERADO">Confederado</option>
              <option value="BANCARIBE">Bancaribe</option>
              <option value="BICENTENARIO">BICENTENARIO</option>
			  <option value="CORPBANCA">CorpBanca</option>			  
			  <option value="VENEZUELA">Venezuela</option>
			  <option value="SOFITASA">Sofitasa</option>
			  <option value="BANPRO">Banpro</option>
			  <option value="PROVINCIAL">Provincial</option>
              <option value="BNC">Banco Nacional de Credito</option>
			  <option value="BANESCO">Banesco</option>
              <option value="BANCRECER">Bancrecer</option>
			  <option value="FONDO COMUN">Fondo Comun</option>			  
			  <option value="BOD">BOD</option>
			  <option value="VENEZOLANO DE CREDITO">Venezolano de Credito</option>			  
			  <option value="GUAYANA">Guayana</option>
			  <option value="EXTERIOR">Exterior</option>
			  <option value="INDUSTRIAL DE VENEZUELA">Industrial de Venezuela</option>
			  <option value="MERCANTIL">Mercantil</option>
			  <option value="PLAZA">Plaza</option>
			  <option value="CITYBANK">CityBank</option>			  
			  <option value="TOTAL BANK">Total Bank</option>
			  <option value="CASA PROPIA">Casa Propia</option>
			  <option value="BANORTE">Banorte</option>
			  <option value="DEL TESORO">Del Tesoro</option>
			  <option value="100% BANCO">100% Banco</option>
			  <option value="DEL SUR">Del Sur</option>
              <option value="BICENTENARIO PUEBLO">Bicentenario del Pueblo</option>
			  </select>		   
		 *</td>
		  </tr>
<!-- Validacion de campo de cuenta bancaria, debe y haber en javascript-->
		  <tr>
			  <td class="titulo3">Cuenta Bancaria: </td>
			  <td colspan="3" class="titulo3"><input name="txtcuentaban" id="txtcuentaban" type="text" size="25" maxlength="20" class="inputbox" onkeypress="return V_all(this,event)" value="<?=@$cuentaban?>" title="N�mero de la Cuenta Bancaria a Incluir" onblur="valida_txtcuentaban()" />
		     *</td>
		  </tr>
		  <tr>
			  <td class="titulo3">Cuenta de Orden del Debe: </td>
			  <td colspan="3" class="titulo3"><input name="txtcuentadebe" id="txtcuentadebe" type="text" size="22" maxlength="17" class="inputbox" onkeydown="return validar_guiones(event,'txtcuentadebe')" onkeypress="return V_all(this,event)" value="<?=@$cuentadebe?>" title="N�mero de la Cuenta DEBE a Incluir" onblur="valida_txtcuentadebe()" />
		     Si no conoce la cuenta contable, rellenar con ceros (0-00-00-00-00-000)</td>
		  </tr>
		  <tr>
			  <td class="titulo3">Cuenta de Orden del Haber: </td>
			  <td colspan="3" class="titulo3"><input name="txtcuentahaber" id="txtcuentahaber" type="text" size="22" maxlength="17" class="inputbox" onkeydown="return validar_guiones(event,'txtcuentahaber')" onkeypress="return V_all(this,event)" value="<?=@$cuentahaber?>" title="N�mero de la Cuenta HABER a Incluir" onblur="valida_txtcuentahaber()"/>
		     Si no conoce la cuenta contable, rellenar con ceros (0-00-00-00-00-000)</td>
		  </tr>
		  <tr>
			  <td class="titulo3">Estatus:</td>
			  <td colspan="3" class="titulo3">
            <? if($_SESSION['permisosid'] !=4 and $_SESSION['permisosid'] !=12) { ?>					
				<select name="cboestatus" id="cboestatus" class="inputbox" title="Estatus de la Cl�nica a Seleccionar">					
        			<? if ($cboestatus!='' or $cboestatus!=NULL ) { 					
						if ($cboestatus=='ACTIVA') { ?>
							<option value="">=Seleccione=</option>
							<option value="ACTIVA" selected="selected">ACTIVA</option> 
							<option value="INACTIVA">INACTIVA</option><? } else { ?>							
							<option value="">=Seleccione=</option>
							<option value="INACTIVA" selected="selected">INACTIVA</option> 
							<option value="ACTIVA" >ACTIVA</option><? } 
					} else { ?>
						<option value="">=Seleccione=</option>
						<option value="ACTIVA">ACTIVA</option> 
						<option value="INACTIVA">INACTIVA</option><? } ?>								
				</select>
		  <? } else {?>
          	<select name="cboestatus" id="cboestatus" class="inputbox" title="Estatus de la Cl�nica a Seleccionar">
          	<option value="">=Seleccione=</option>
			<option value="INACTIVA" selected="selected">INACTIVA</option> 
            </select>       
          <? } ?>
          
          </td>
		 </tr>
		 <tr>
		    <td class="titulo3">Aplicar Impuesto ISLR:</td>
			<td colspan="3" class="titulo3"><select name="cboimpuestoislr" id="cboimpuestoislr" class="inputbox" title="Impuesto ISLR a aplicar a la cl�nica">
			    <? if ($cboimpuestoislr!='' or $cboimpuestoislr!=NULL ) { 					
					if ($cboimpuestoislr=='SI') { ?>
			    	<!--<option value="">=Seleccione=</option>-->
                    <option value="SI" selected="selected">SI</option> 
                    <option value="NO">NO</option><? } else { ?>
                    <!--<option value="">=Seleccione=</option>-->
                    <option value="NO" selected="selected">NO</option> 
                    <option value="SI" >SI</option><? } 
				} else { ?>
                   <!-- <option value="">=Seleccione=</option>-->
                    <option value="SI">SI</option> 
                    <option value="NO">NO</option><? } ?>
		      </select>              
            </td>
		  </tr>	
          <tr>
		    <td class="titulo3">Aplicar Impuesto LTF:</td>
			<td colspan="3" class="titulo3"><select name="cboimpuestoltf" id="cboimpuestoltf" class="inputbox" title="Impuesto LTF a aplicar a la cl�nica">
			    <? if ($cboimpuestoltf!='' or $cboimpuestoltf!=NULL ) { 					
					if ($cboimpuestoltf=='SI') { ?>
			    	<!--<option value="">=Seleccione=</option>-->
                    <option value="SI" selected="selected">SI</option> 
                    <option value="NO">NO</option><? } else { ?>
                    <!--<option value="">=Seleccione=</option>-->
                    <option value="NO" selected="selected">NO</option> 
                    <option value="SI" >SI</option><? } 
				} else { ?>
                    <!--<option value="">=Seleccione=</option>-->
                    <option value="SI">SI</option> 
                    <option value="NO">NO</option><? } ?>
		      </select>              
            </td>
		  </tr>	
          <tr>
		    <td class="titulo3">Aplica Responsabilidad Social:</td>
			<td class="titulo3"><select name="cborespso" id="cborespso" class="inputbox" title="Responsabilidad Social a aplicar al centro de salud">
			  <? if ($cborespso!='' or $cborespso!=NULL ) { 					
					if ($cborespso=='SI') { ?>
			  <!--<option value="">=Seleccione=</option>-->
			  <option value="SI" selected="selected">SI</option> 
			  <option value="NO">NO</option><? } else { ?>
			  <!--<option value="">=Seleccione=</option>-->
			  <option value="NO" selected="selected">NO</option> 
			  <option value="SI" >SI</option><? } 
				} else { ?>
			 <!-- <option value="">=Seleccione=</option>-->
			  <option value="SI">SI</option> 
			  <option value="NO">NO</option><? } ?>
		    </select>  </td>
		  </tr>
          				
	</table>
    	</td>
    </tr>
    		
    <tr>
      <td colspan="2" align="center">
           	<input type="button" class="boton" value="Guardar" name="btnEnviar" onclick="if(valida_campos_vacios(this.form)==true)  { envia_clinica(); }"  />
      </td>
    </tr>
  </table>
     <!--CAMPOS OCULTOS DE ESTUDIANTES -->
     <?php $consulta_datos_ciudad = pg_query("SELECT distinct(ciu_codarea), ciu_ciudad, mun_id FROM siscam.ciudad"); ?>
	<select id="hdncbocodarea" name="Zhdncbocodarea" style="display:none;" >
    
    <?php for($i=0;$i<=pg_num_rows($consulta_datos_ciudad); $i++) { ?>
    	<option value="<?= pg_result($consulta_datos_ciudad,$i,"mun_id"); ?>" ><?= pg_result($consulta_datos_ciudad,$i,"ciu_codarea"); ?></option>
        <?php  } ?>
    </select>
	<input type="hidden" id="hdnaccion" name="hdnaccion" value="INSERTAR"/> 
    <input type="hidden" id="hdnusucedul" name="hdnusucedul" value="<?= $_SESSION['usu_cedul'];?>"/> 
    <input type="hidden" id="hdnusunombre" name="hdnusunombre" value="<?= $_SESSION['usu_nombre'];?>"/> 
    <input type="hidden" id="hdnmod" name="hdnmod" value="<?= $_GET['mod'];?>"/> 
    <input type="hidden" name="hdncodigo" id="hdncodigo"  value="<?= $codigo_area; ?>" />
   	<input type="hidden" id="rif2" name="rif2" value="<?=@$cod?>"/> 
    <input type="hidden" id="oculto_tipo_rif" name="oculto_tipo_rif" value="<?=substr(@$rif,0,1);?>"/>
   	<input type="hidden" id="ocultoclinica" name="ocultoclinica" />
   	<input type="hidden" id="ocultorif" name="ocultorif" />  
   	<input type="hidden" id="codigo_oculto" name="codigo_oculto" />
   	<input type="hidden" id="banco" name="banco" value="<?=$banco;?>" />
   	<input type="hidden" id="oculto" name="oculto" value="insertar" />   
    <input type="hidden" name="hdncel" id="hdncel" value="<?= $cod_cel; ?>" />  
    <input type="hidden" name="hdnmontoacum" id="hdnmontoacum" value="<?=$montoacum; ?>" />               
<!-- FIN CAMPOS OCULTOS -->
</form> 
<div id="res_clinicas" ></div>