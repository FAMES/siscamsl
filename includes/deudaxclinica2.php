<?php
	$rifClinica = $_REQUEST['rif'];
	$fechadesde = $_REQUEST['fechadesde'];
	$fechahasta = $_REQUEST['fechahasta'];
	$anio = $_REQUEST['anio'];
	$consClinica = "select A.cli_nombre, 
						   B.esta_estado, 
						   B.esta_regionp, 
						   A.cli_telefono, 
						   A.cli_celular 
				    from siscam.clinicas A, siscam.estado B 
					where cli_rif = '$rifClinica' and A.esta_id = B.esta_id";
	$exeClinica = pg_query($consClinica);
	$objClinica = pg_fetch_object($exeClinica,0);	 
	$clinica = $objClinica->cli_nombre;
	$estado = $objClinica->esta_estado;
	$region = substr($objClinica->esta_regionp,8,1);
	$telefono = $objClinica->cli_telefono;
	$celular = $objClinica->cli_celular;
?>

<table border="0" width="950">  
    <tr>
      <td colspan="2" class="titulo1">Detalle Deuda por Clinica</td>
    </tr>	
    <tr><td colspan="2"><hr class="linea"></td></tr>
    <tr>
      <td colspan="2" class="titulo2">Datos de Clinica</td>
    </tr>
    <tr><td colspan="2"><hr class="linea"></td></tr>   
   		<table  width="950">
       		<tr class="TR2">
                <td width="20%">              
                  Rif de Clinica : 
                </td>
                <td class="80%">
                  <?= $rifClinica; ?>
                </td>
            </tr>
            <tr class="TR3">
                <td>              
                  Nombre de Clinica : 
                </td>
                <td>
                  <?= $clinica; ?>
                </td>
            </tr>
            <tr class="TR2">
                <td>              
                  Estado : 
                </td>
                <td>
                  <?= $estado; ?>
                </td>
            </tr>  
            <tr class="TR3">
                <td>              
                  Region : 
                </td>
                <td>
                  <?= $region; ?>
                </td>
            </tr>  
             <tr class="TR2">
                <td>              
                  Telefono : 
                </td>
                <td>
                  <?= $telefono; ?>
                </td>
            </tr>
            <tr class="TR3">
                <td>              
                  Celular : 
                </td>
                <td>
                  <?= $celular; ?>
                </td>
            </tr> 
 <? if($anio == '0')
 { ?>
             <tr class="TR2">
                <td>              
                  Fecha Desde : 
                </td>
                <td>
                  <?= CamFormFech($fechadesde); ?>
                </td>
            </tr> 
            <tr class="TR3">
                <td>              
                  Fecha Hasta : 
                </td>
                <td>
                  <?= CamFormFech($fechahasta); ?>
                </td>
            </tr> 
 <? 
 }else {	 
 ?>
 		    <tr class="TR2">
                <td>              
                  A&ntilde;o : 
                </td>
                <td>
                  <?= $anio; ?>
                </td>
            </tr> 
 <? } ?>
            <tr>
            	<td colspan="7" align="center">
              		<input type="button" class="boton" value="Imprimir" onclick="document.form_casosxcli.submit();" name="btnImprimir">
                </td>
            </tr>            
         </table>                	                	
                </td>
             </tr>                               
  	</table>    
    <tr><td colspan="2"><hr class="linea"></td></tr>        	    
</table>	
<table width="950">
	   		<tr class="TR1">
            	<td width="8%"><div align="center">N&deg; Carta</div></td>
                <td width="10%"><div align="center">Cedula</div></td>
                <td width="10%"><div align="center">Estudiante</div></td>
                <td width="10%"><div align="center">Fecha Ingreso</div></td>
                <td width="10%"><div align="center">Fecha Emision</div></td>
				<td width="10%"><div align="center">N&deg; Factura</div></td>
                <td width="10%"><div align="center">Monto</div></td>						
        	</tr>
<?php
	if($anio == '0')
	{
	$consCasosDeuda = "SELECT	A.aut_cartaaval as carta,
								G.est_id as cedula,
								G.est_pnombre as nombre,
								G.est_papellido as apellido,
								D.fechaing as ingreso,
								D.fac_fecha as fecemi,
								D.fac_numero as numero,
								D.fac_monto	as monto
							 FROM 
							 	siscam.autorizaciones A, 
								siscam.estado B, 
								siscam.clinicas C, 
								siscam.factura D,								
								siscam.estudiantes G
							 WHERE 
							 	A.cli_rif = C.cli_rif and
		      					C.esta_id = B.esta_id and
		      					A.est_id = G.est_id   and		      					
		      					A.aut_cartaaval = D.aut_cartaaval and
								A.aut_estatus <> 'PDO' and
								A.aut_modalidad <> 'ESPECIAL' and
		      					D.fechaing >= '$fechadesde' and 
								D.fechaing <= '$fechahasta' and 
		      					C.cli_rif = '$rifClinica' 							
							 ORDER BY B.esta_regionp";
	}
	else
	{
			$fecini = $anio.'-01-01';
			$fechas = $anio.'-12-31';
			$cartaini = $anio.'00000';
			$cartafin = $anio.'99999';
			if($rifClinica != 'TODOS') { $aux = " and C.cli_rif = '$rifClinica' " ; }
			$consCasosDeuda = "SELECT	B.aut_cartaaval as carta, 
										E.est_id as cedula,
										E.est_pnombre as nombre,									 
										E.est_papellido as apellido,
										A.fechaing as ingreso,
										A.fac_fecha as fecemi,
										A.fac_numero as numero,										
										A.fac_monto as monto									
							 FROM 
							 		siscam.factura A, 
									siscam.autorizaciones B, 
									siscam.clinicas C, 
									siscam.estado D, 
									siscam.estudiantes E
							 WHERE 
							 		(A.fechaing >= '$fecini' and A.fechaing <= '$fechas' and A.aut_cartaaval <= $cartafin 
									or (A.fechaing <= '$fechas' and A.fechaing <> '1900-01-01' and A.aut_cartaaval >= $cartaini 
									and A.aut_cartaaval <= $cartafin))
									and A.aut_cartaaval = B.aut_cartaaval
									and B.cli_rif = C.cli_rif
									and C.esta_id = D.esta_id
									and B.est_id = E.est_id
									and B.aut_modalidad <> 'ESPECIAL'						
									and B.aut_Estatus <> 'PDO'
									$aux							 
							 ORDER BY D.esta_regionp, D.esta_estado, c.cli_nombre";		
	}
	$exeCasosDeuda = pg_query($consCasosDeuda);
	$num = 0;
	$total = 0;
	while ($fila = pg_fetch_object($exeCasosDeuda))
	{
?>    
            <tr <? if($num%2==0){?> class="TR2"<? }else{ ?>class="TR3"<? } ?>>
            	<td width="8%" align="center">
                <? if($fila->carta <> '') { echo $fila->carta; } else { echo "NO DEFINIDO"; } ?>
                </td>            
            	<td width="10%" align="center">
                <? if($fila->cedula <> '') { echo $fila->cedula; } else { echo "NO DEFINIDO"; } ?>
                </td>            
            	<td width="10%" align="center">
                <? if($fila->nombre <> '') { echo utf8_decode($fila->nombre. " ". $fila->apellido); } else { echo "NO DEFINIDO"; } ?>
                </td>
                <td width="10%" align="center">
                <? if($fila->ingreso <> '') { echo CamFormFech($fila->ingreso); } else { echo "NO DEFINIDO"; } ?>
                </td>
                 <td width="10%" align="center">
                <? if($fila->fecemi <> '') { echo CamFormFech($fila->fecemi); } else { echo "NO DEFINIDO"; } ?>
                </td>
                <td width="10%" align="center">
                <? if($fila->numero <> '') { echo $fila->numero; } else { echo "NO DEFINIDO"; } ?>
                </td>
                <td width="10%" align="center">
                <? if($fila->monto <> '') { echo $fila->monto; } else { echo "NO DEFINIDO"; } ?>
                </td>
            </tr>       
 <? $num++; $total += $fila->monto; } ?>
 			<tr class="TOTAL">
            	<td align="right" colspan="7">
					<?= "Total : " .number_format($total,2,',','.'); ?> Bs.               
                </td>
            </tr>
            <form id="form_casosxcli" name="form_casosxcli" method="post" action="reports/reportDEUDCASOS.php" target="_blank">
             	<input type="hidden" id="hdnrif" name="hdnrif" value="<?= $rifClinica; ?>"  />
               	<input type="hidden" id="hdnfechadesde" name="hdnfechadesde" value="<?= $fechadesde; ?>"  />
               	<input type="hidden" id="hdnfechahasta" name="hdnfechahasta" value="<?= $fechahasta; ?>"  />
                <input type="hidden" id="hdnanio" name="hdnanio" value="<?= $anio; ?>"  />
            </form>
</table>