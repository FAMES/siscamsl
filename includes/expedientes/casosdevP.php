<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8">
		<link rel="shortcut icon" type="image/ico" href="http://www.sprymedia.co.uk/media/images/favicon.ico">
		
		<title>ColReorder example</title>
		
		<script type="text/javascript" charset="utf-8">
			$(document).ready( function () {
				var oTable = $('#example').dataTable( {
					"sDom": 'R<"H"lfr>t<"F"ip>',
					"bJQueryUI": true,
					"sPaginationType": "full_numbers",
					"oLanguage": { 
"oPaginate": { 
"sPrevious": "<", 
"sNext": ">", 
"sLast": "ULTIMA", 
"sFirst": "PRIMERA" 
}, 

"sLengthMenu": 'Mostrar <select>'+ 
'<option value="10">10</option>'+ 
'<option value="20">20</option>'+ 
'<option value="30">30</option>'+ 
'<option value="40">40</option>'+ 
'<option value="50">50</option>'+ 
'<option value="-1">Todos</option>'+ 
'</select> registros', 

"sInfo": "Mostrando del _START_ a _END_ (Total: _TOTAL_ resultados)", 

"sInfoFiltered": " - filtrados de _MAX_ registros", 

"sInfoEmpty": "No hay resultados de busqueda", 

"sZeroRecords": "No hay registros a mostrar", 

"sProcessing": "Espere, por favor...", 

"sSearch": "Buscar:", 

} 
					
				} );
			} );
		</script>
	</head>
    
<table width="950">
	<tr><td class="titulo1">Casos Devueltos</td></tr>
	<tr><td><hr class="linea"/></td></tr>
<?

 $consulta = Consultar("select distinct(siscam.vst_aut_casos_dev.aut_cartaaval),siscam.vst_aut_casos_dev.as_cedula,siscam.vst_aut_casos_dev.as_pnombre,siscam.vst_aut_casos_dev.as_papellido,siscam.vst_aut_casos_dev.dev_fecha,siscam.vst_aut_casos_dev.dev_tipo,siscam.vst_aut_casos_dev.dev_id,siscam.vst_aut_casos_dev.dev_estatus from siscam.vst_aut_casos_dev where siscam.vst_aut_casos_dev.dev_estatus = 'TRAMITE' order by siscam.vst_aut_casos_dev.aut_cartaaval desc");									
       			$nr=@pg_num_rows($consulta);
?>

<? if($consulta!=false){?>	
	
	<form name="frmestudiantes" method="post">
	<tr><td>	
		
	</td></tr>	
	<tr><td>
    	<? if($_SESSION['peracceso'] !=5) { ?>
    	<input type="button" name="btnNuevo" value="Agregar" class="boton" onclick="document.location.href='?id=casosdev2&mod=nuevoP'">
        <? } ?>
	  	
	  	<input type="button" name="btnListar" value="Ver todos" class="boton" onclick="document.location.href='?id=casosdevP'">
    </td></tr>
	</form>	
    <input type="hidden" name="hdnpg" id="hdnpg" value="<?=$_GET['pg'];?>" />    
	<tr><td><hr class="linea"/></td></tr>
</table>

<body id="dt_example">
		<div id="container">
			<div id="demo">	

<table cellpadding="0" cellspacing="0" border="0" id="example" class="display" style="width:100%">
		  		
		  		 <thead>
                    <tr class="TR1">
		    		<td width="9%"><div align="left">Carta Aval</div></td>
		    		<td width="9%"><div align="left">Cedula</div></td>
		    		<td width="30%"><div align="left">Nombre</div></td>
		    		<td width="17%"><div align="left">Fecha de Devuelto</div></td>
		    		<td width="19%"><div align="left">Tipo de caso</div></td>
		    		<td width="16%"><div align="left">Estatus del caso</div></td>
	      			</tr>
                 </thead>
              <tfoot>
      	
      		  </tfoot>				
		<tbody>      	
		<? while($row = pg_fetch_object($consulta))
				{?>
		  <tr <? if($num%2==0){?> class="TR2"<? }else{ ?>class="TR3"<? } ?>>          	
		    	<td width="9%"><a href="?id=casosdev2&idP=<?=$row->dev_id?>&tipocaso=<?=$row->dev_tipo?>&carta=<?=$row->aut_cartaaval?>&estatus=<?=$row->dev_estatus?>"><?=$row->aut_cartaaval?></a></td>
		    	<td width="9%" ><?=$row->est_id?></td>
		    	<td width="30%" ><? if ($row->est_pnombre <> ''){ echo $row->est_pnombre; }else{ echo 'No definido'; }?>&nbsp;
		    	<? if ($row->est_papellido <> ''){ echo $row->est_papellido; }else{ echo 'No definido'; }?></td>
		    	<td width="17%"><? if ($row->dev_fecha <> ''){ echo CamFormFech($row->dev_fecha); }else{ echo 'No definido'; }?></td>
		    	<td width="19%"><? if ($row->dev_tipo <> ''){ echo $row->dev_tipo; }else{ echo 'No definido'; }?></td>
		    	<td width="16%"><? if ($row->dev_estatus == 'REC'){ echo 'RECEPCIONADO';
							  }elseif ($row->dev_estatus =='TRAMITE'){echo 'TRAMITE';
							  }elseif ($row->dev_estatus =='NEGADO'){echo 'NEGADO';
							  }elseif ($row->dev_estatus =='ESPERA'){echo 'ESPERA';
							  }else{ echo 'No definido'; } ?></td>		    
	      </tr>
     <? $num++; } ?>
		</table>
		<? }else{//end if consulta
			echo '<table cellspacing="0" width="100%"><tr><td class="TR1">Casos Devueltos</td></tr>
								<tr><td class="titulo3">NO SE ENCONTRO LA INFORMACI&oacuteN</td></tr>
				  </table>';	
		}//end else consulta 
?>

</tbody>
  
 </table>
   
   
   <!-- <tr>
		<td>
			<div id="divcbit"></div>
		</td>
	</tr> -->
</table>

</div>
		
			
	
			
			
		</div>
   </body>
</html>
