<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8">
		<link rel="shortcut icon" type="image/ico" href="http://www.sprymedia.co.uk/media/images/favicon.ico">
		
		<title>ColReorder example</title>
		
		<script type="text/javascript" charset="utf-8">
			$(document).ready( function () {
				var oTable = $('#example').dataTable( {
					"sDom": 'R<"H"lfr>t<"F"ip>',
					"bJQueryUI": true,
					"sPaginationType": "full_numbers",
					"oLanguage": { 
"oPaginate": { 
"sPrevious": "<", 
"sNext": ">", 
"sLast": "ULTIMA", 
"sFirst": "PRIMERA" 
}, 

"sLengthMenu": 'Mostrar <select>'+ 
'<option value="10">10</option>'+ 
'<option value="20">20</option>'+ 
'<option value="30">30</option>'+ 
'<option value="40">40</option>'+ 
'<option value="50">50</option>'+ 
'<option value="-1">Todos</option>'+ 
'</select> registros', 

"sInfo": "Mostrando del _START_ a _END_ (Total: _TOTAL_ resultados)", 

"sInfoFiltered": " - filtrados de _MAX_ registros", 

"sInfoEmpty": "No hay resultados de busqueda", 

"sZeroRecords": "No hay registros a mostrar", 

"sProcessing": "Espere, por favor...", 

"sSearch": "Buscar:", 

} 
					
				} );
			} );
		</script>
	</head>

<table width="950">
	<tr><td class="titulo1">Casos Devueltos</td></tr>
	<tr><td><hr class="linea"/></td></tr>
 <?

 $consulta = Consultar("select * from siscam.vst_est_casos_dev where siscam.vst_est_casos_dev.dev_tipo like 'NORMATIVA' 
and siscam.vst_est_casos_dev.dev_estatus like 'NEGADO' or siscam.vst_est_casos_dev.dev_tipo like 'RECAUDOS BASICOS' and  siscam.vst_est_casos_dev.dev_estatus like 'ESPERA' or siscam.vst_est_casos_dev.dev_estatus like 'COMISION' order by siscam.vst_est_casos_dev.as_cedula asc");
		$nr=@pg_num_rows($consulta);
?>

<? if($consulta!=false){?>	
	<form name="frmestudiantes" method="post">
	<tr><td>	
	</td></tr>	
	<tr><td>
    	<? if($_SESSION['peracceso'] !=5) { ?>
        <input type="button" name="btnNuevo" value="Agregar" class="boton" onclick="document.location.href='?id=casosdev2&mod=nuevoA'">
        <? } ?>
	  	<input type="button" name="btnListar" value="Ver todos" class="boton" onclick="document.location.href='?id=casosdevA'">
    </td></tr>
	</form>	
     <!-- Campos Ocultos -->    
    <input type="hidden" name="hdnpg" id="hdnpg" value="<?=$_GET['pg'];?>" />    
    <!-- Fin Campos Ocultos -->
	<tr><td><hr class="linea"/></td></tr>
</table>

<body id="dt_example">
		<div id="container">
			<div id="demo">	
<table cellpadding="0" cellspacing="0" border="0" id="example" class="display" style="width:100%">

       <thead>
        <tr class="TR1">
        <td width="12%"><div align="left">Cedula</div></td>
        <td width="35%"><div align="left">Nombre</div></td>
        <td width="19%"><div align="left">Fecha de Devuelto</div></td>
        <td width="20%"><div align="left">Tipo de caso</div></td>
        <td width="14%"><div align="left">Estatus del caso</div></td>
        </tr>
        </thead>
             <tfoot>
      	
      		  </tfoot>				
		<tbody>      
		<? while($row = pg_fetch_object($consulta)){ ?>
        <tr <? if($num%2==0){?> class="TR2"<? }else{ ?>class="TR3"<? } ?>>
        <td width="12%" ><? if ($row->est_tipo <> ''){ echo $row->est_tipo; }else{ echo 'No definido'; }?>
        -<a href="?id=casosdev2&idA=<?=$row->dev_id?>&tipocaso=<?=$row->dev_tipo?>&estatus=<?=$row->dev_estatus?>"><?=$row->est_id?></a></td>
        <td width="35%" ><? if ($row->est_pnombre <> ''){ echo $row->est_pnombre; }else{ echo 'No definido'; }?>&nbsp;
		<? if ($row->est_papellido <> ''){ echo $row->est_papellido; }else{ echo 'No definido'; }?></td>
		<td width="19%" ><? if ($row->dev_fecha <> ''){ echo CamFormFech($row->dev_fecha); }else{ echo 'No definido'; }?></td>
        <td width="20%" ><? if ($row->dev_tipo <> ''){ echo $row->dev_tipo; }else{ echo 'No definido'; }?></td>
        <td width="14%" ><? if ($row->dev_estatus == 'REC'){ echo 'RECEPCIONADO';
                            }elseif ($row->dev_estatus =='TRAMITE'){echo 'TRAMITE';
                            }elseif ($row->dev_estatus =='NEGADO'){echo 'NEGADO';
                            }elseif ($row->dev_estatus =='ESPERA'){echo 'ESPERA';
                            }elseif ($row->dev_estatus =='COMISION'){echo 'COMISION';
                            }else{ echo 'No definido'; } ?></td>		    
        </tr>
        <? $num++; } ?>
    </table>
    <? }else{//end if consulta
    		echo '<table cellspacing="0" width="100%"><tr><td class="TR1">Estudiantes</td></tr>
    		<tr><td class="titulo3">NO SE ENCONTRO LA INFORMACI&oacuteN </td></tr>
    		</table>';	
    	}//end else consulta
?>

</tbody>
  
 </table>
   
   
   <!-- <tr>
		<td>
			<div id="divcbit"></div>
		</td>
	</tr> -->
</table>

</div>
		
			
	
			
			
		</div>
   </body>
</html>