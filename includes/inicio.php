<!--<script> alert('ATENCION, SE INFORMA QUE EL MODULO DE BENEFICIO SOLIDARIO SE ENCUENTRA EN MANTENIMIENTO ;)'); </script>-->
<!--<table width="200" border="0">
  <tr>
    <td><img src="imagenes/fondo.png" width="900" height="500" /></td>
  </tr>
</table>-->
<!doctype html>
<html lang="en-US">
<head>
<link rel="stylesheet" type="text/css" media="all" href="script/styles.css">
  <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
  <script type="text/javascript" src="js/jquery.glide.min.js"></script>
</head>

<body>
  <div id="w">
    
    
    <div class="slider">
      <ul class="slides">
        <li class="slide">
          <figure>
            <figcaption></figcaption>
            <img src="img/witches.png" alt="dribbble witches">
          </figure>
        </li>
        <li class="slide">
          <figure>
            <figcaption></figcaption>
            <img src="img/taipei-fireworks.png" alt="searing mountain illustration">
          </figure>
        </li>
        <li class="slide">
          <figure>
            <figcaption></figcaption>
            <img src="img/searing-mountain.png" alt="taipei fireworks">
          </figure>
        </li>
        <li class="slide">
          <figure>
            <figcaption></figcaption>
            <img src="img/iphone-mockup.png" alt="coffee cup iphone newspaper">
          </figure>
        </li>
        <li class="slide">
          <figure>
            <figcaption></figcaption>
            <img src="img/super-squirrel.png" alt="flying super squirrel">
          </figure>
        </li>
      </ul>
    </div><!-- @end .slider -->
  </div><!-- @end #w -->
<script type="text/javascript">
$(function(){
  $('.slider').glide({
    autoplay: 3500,
    hoverpause: true, // set to false for nonstop rotate
    arrowRightText: '&rarr;',
    arrowLeftText: '&larr;'
  });
});
</script>
</body>
</html>
