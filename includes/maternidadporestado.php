<script type="text/javascript" src="js/graf/jquery-1.3.1.min.js"></script>
<script type="text/javascript" src="js/graf/fgCharting.jQuery.js"></script>
<script type="text/javascript" src="js/graf/excanvas-compressed.js"></script>
<script>
	function imprimeinforme(){
		if(confirm("¿Desea Imprimir el reporte Maternidad por estado Detallado?")){
			window.open('reports/reportDETME.php?fdes='+document.getElementById('txtfechadesde').value+'&fechahasta='+document.getElementById('txtfechahasta').value+'&tipo='+document.getElementById('cobtipo').value+'','_blank');
		}

	}
</script>

<script language="javascript">
$(document).ready(function() {
	if($.browser.msie) { 
		setTimeout(function(){$.fgCharting();}, 2000);
	} else {
		$.fgCharting();
	}	
});

</script>
<link href="js/graf/style.css" rel="stylesheet" type="text/css" /> 
<?
 	$tipo = strtoupper(@$_POST["cobtipo"]);
	$txtfechadesde = strtoupper(@$_POST["txtfechadesde"]);
	$txtfechahasta = strtoupper(@$_POST["txtfechahasta"]);
?>
<table width="100%">
	<tr>
	  <td class="titulo1">Maternidades por Estado</td>
	</tr>
	<tr><td><hr class="linea"/></td></tr>
	
	<form name="frmautorizaciones" method="post">  
	<tr><td>
	<? /*Abre una tabla*/?>	
			<table width="900">
            
            	<tr>
                <td width="121" class="titulo3">	Tipo de Casos:</td>
		      	 <td class="titulo4"><select name="cobtipo" class="inputbox" id="cobtipo"  title="Reporte a Seleccionar" onChange="validar_tipo()">
						 <? if ($tipo!='' or $tipo!=NULL ) {
							 if ($tipo=='1') { ?>
								 <option value="0">=Seleccione=</option>
								 <option value="1" selected="selected">Autorizados</option>
								 <option value="2">Proceso de Pago</option>
								 <option value="3">Pagados</option>
								 <option value="4">Emitidos</option>
							 <? } else if($tipo == '2'){ ?>
								 <option value="0">=Seleccione=</option>
								 <option value="1">Autorizados</option>
								 <option value="2" selected="selected">Proceso de Pago</option>
								 <option value="3" >Pagados</option>
								 <option value="4" >Emitidos</option>
							 <? } else if($tipo == '3'){ ?>
								 <option value="0">=Seleccione=</option>
								 <option value="1">Autorizados</option>
								 <option value="2" >Proceso de Pago</option>
								 <option value="3" selected="selected">Pagados</option>
								 <option value="4" >Emitidos</option>
							 <? } else if($tipo == '4'){ ?>
								 <option value="0">=Seleccione=</option>
								 <option value="1">Autorizados</option>
								 <option value="2" >Proceso de Pago</option>
								 <option value="3" >Pagados</option>
								 <option value="4" selected="selected">Emitidos</option>

							 <? }
							 else { ?>
								 <option value="0"selected="selected">=Seleccione=</option>
								 <option value="1">Autorizados</option>
								 <option value="2">Proceso de Pago</option>
								 <option value="3">Pagados</option>
								 <option value="4">Emitidos</option>
							 <? }	}
						 else { ?>
							 <option value="0"selected="selected">=Seleccione=</option>
							 <option value="1">Autorizados</option>
							 <option value="2">Proceso de Pago</option>
							 <option value="3">Pagados</option>
							 <option value="4">Emitidos</option>
						 <? }  ?>
          			</select>*</td>


					 	
                 
					<td class="titulo3">Fecha desde: </td>
				    <td><input type="text" id="txtfechadesde" size="12" maxlength="12"  class="inputbox" value="<?=@$txtfechadesde?>" readonly name="txtfechadesde" onFocus="displayCalendar(document.forms[0].txtfechadesde,'yyyy-mm-dd',this)" />*</td>
				
					<td class="titulo3">Fecha Hasta: </td>
				    <td><input type="text" id="txtfechahasta" size="12" maxlength="12"  class="inputbox" value="<?=@$txtfechahasta?>" readonly name="txtfechahasta" onFocus="displayCalendar(document.forms[0].txtfechahasta,'yyyy-mm-dd',this)" />*</td>
				</tr>
                </table>
                <tr><td>
		<input name="submit" type="submit" class="boton" value="Buscar" onclick="return validar(this.form)"/>
			<? if($cboestatus !='0'){ ?>
            <a onClick="javascript:window.print();">
			<input name="btnimprimir" type="button" class="boton" id="btnimprimir" value="Imprimir">
            </a>
				<input name="btnimprimirdetalle" type="button" class="boton" id="btnimprimirdetalle" value="Imprimir Detalle" onclick="imprimeinforme()">
				 <? }else{
				 echo '<script> alert("Disculpe, debe seleccionar un estatus"); </script>';	
				 echo "<script>document.location.href ='?id=autorizaciones';</script>";
				 }?></td>
	</tr>
    </form>	
	<tr><td><hr class="linea"/></td></tr>
    <tr>
    	<td>

       	  <table width="50%" id="dataTable" summary="Maternidades Autorizadas por Estado" border="1">
            
			<?	if ($txtfechadesde !=NULL and $txtfechahasta !=NULL){	?>
            
            	<? if ($tipo==1){ ?>
				
				  <? $consulta2="select distinct(siscam.vst_aut_pat.esta_estado), count(siscam.vst_aut_pat.aut_cartaaval) as cuentacaso, sum(siscam.vst_aut_pat.aut_montoa) as sumacaso from siscam.vst_aut_pat where (siscam.vst_aut_pat.aut_estatus ='AUT' or siscam.vst_aut_pat.aut_estatus ='UMED' or siscam.vst_aut_pat.aut_estatus ='REG' or siscam.vst_aut_pat.aut_estatus ='REV' or siscam.vst_aut_pat.aut_estatus ='NOVER' or siscam.vst_aut_pat.aut_estatus ='ATENDIDO') and siscam.vst_aut_pat.aut_fechaa >= '$txtfechadesde' and siscam.vst_aut_pat.aut_fechaa <= '$txtfechahasta' and siscam.vst_aut_pat.aut_fecreg >= '$txtfechadesde' and siscam.vst_aut_pat.bar_ano =2016 and siscam.vst_aut_pat.bar_id = 9001 group by siscam.vst_aut_pat.esta_estado order by cuentacaso desc"; ?>

					<? //$consulta3="select count(siscam.vst_aut_pat.aut_cartaaval) as cuentacaso, sum(siscam.vst_aut_pat.aut_montoa) as sumacaso from siscam.vst_aut_pat where (siscam.vst_aut_pat.aut_estatus ='AUT' or siscam.vst_aut_pat.aut_estatus ='UMED' or siscam.vst_aut_pat.aut_estatus ='REG' or siscam.vst_aut_pat.aut_estatus ='REV' or siscam.vst_aut_pat.aut_estatus ='NOVER' or siscam.vst_aut_pat.aut_estatus ='ATENDIDO') and siscam.vst_aut_pat.aut_fechaa >= '$txtfechadesde' and siscam.vst_aut_pat.aut_fechaa <= '$txtfechahasta' and siscam.vst_aut_pat.aut_fecreg >= '$txtfechadesde' and siscam.vst_aut_pat.bar_ano =2016 and siscam.vst_aut_pat.bar_id = 9001 order by cuentacaso desc"; ?>

				<? }elseif ($tipo==2){ ?>
                  
                  <? $consulta2="select distinct(siscam.vst_aut_pat.esta_estado), count(siscam.vst_aut_pat.aut_cartaaval) as cuentacaso, sum(siscam.vst_aut_pat.aut_montoa) as sumacaso from siscam.vst_aut_pat where (siscam.vst_aut_pat.aut_estatus ='REC' or siscam.vst_aut_pat.aut_estatus ='RECPAR' or siscam.vst_aut_pat.aut_estatus ='PAGO' or siscam.vst_aut_pat.aut_estatus ='PAGOPAR' or siscam.vst_aut_pat.aut_estatus ='FIN' or siscam.vst_aut_pat.aut_estatus ='FINPAR') and siscam.vst_aut_pat.aut_fechaa >= '$txtfechadesde' and siscam.vst_aut_pat.aut_fechaa <= '$txtfechahasta' and siscam.vst_aut_pat.aut_fecreg >= '$txtfechadesde' and siscam.vst_aut_pat.bar_ano =2016 and siscam.vst_aut_pat.bar_id = 9001 group by siscam.vst_aut_pat.esta_estado order by cuentacaso desc"; ?>
                  
                  <? }elseif ($tipo==3){ ?>
                  
                  <? $consulta2="select distinct(siscam.vst_aut_pat.esta_estado), count(siscam.vst_aut_pat.aut_cartaaval) as cuentacaso, sum(siscam.vst_aut_pat.aut_montoa) as sumacaso from siscam.vst_aut_pat where (siscam.vst_aut_pat.aut_estatus ='PDO' or siscam.vst_aut_pat.aut_estatus ='PDOPAR') and siscam.vst_aut_pat.aut_fechaa >= '$txtfechadesde' and siscam.vst_aut_pat.aut_fechaa <= '$txtfechahasta' and siscam.vst_aut_pat.aut_fecreg >= '$txtfechadesde' and siscam.vst_aut_pat.bar_ano =2016 and siscam.vst_aut_pat.bar_id = 9001 group by siscam.vst_aut_pat.esta_estado order by cuentacaso desc"; ?>
                  
                  <? }elseif ($tipo==4){ ?>
                  
                  <? $consulta2="select distinct(siscam.vst_aut_pat.esta_estado), count(siscam.vst_aut_pat.aut_cartaaval) as cuentacaso, sum(siscam.vst_aut_pat.aut_montoa) as sumacaso from siscam.vst_aut_pat where  siscam.vst_aut_pat.aut_estatus <>'ANULADO' and siscam.vst_aut_pat.aut_fechaa >= '$txtfechadesde' and siscam.vst_aut_pat.aut_fechaa <= '$txtfechahasta'and siscam.vst_aut_pat.aut_fecreg >= '$txtfechadesde' and siscam.vst_aut_pat.bar_ano =2016 and siscam.vst_aut_pat.bar_id = 9001 group by siscam.vst_aut_pat.esta_estado order by cuentacaso desc"; ?>
                  
                  <? } ?>
                  
                  
                 <? } ?>
                  
						<? $aux = Consultar($consulta2); ?>
                
               
              <thead>
                <tr>
                 <th width="263" class="titulo3" id="estado">Estado:</th>
                  <th width="263" class="titulo3" id="cantidad">Cantidad de casos:</th>
					<th width="263" class="titulo3" id="total">Total de casos:</th>
                  
                </tr>
              </thead>
              <tbody>
                <tr>
                <? while($row = pg_fetch_object($aux)){?>
                <th width="263" class="titulo8" headers="members"><? echo $row->esta_estado?></th>
                  <td width="263" class="titulo3" headers="cantidad"><? echo $row->cuentacaso?></td>
					<td width="263" class="titulo3" headers="total"><? echo $row->sumacaso?></td>
                  
                  
			  </tr>
              </tbody>
              	<? } ?>

              <? //}	?> 
              </table>


              
<table>
       	 <thead>
         <tr> <th> <div class="chartBlock" style="position: relative;">Gr&aacute;fica Estadistica
       	    <canvas id="chart1" class="fgCharting_src-dataTable_type-pie" width="400" height="400"></canvas>
</div></th></tr> 
	</thead>
</table>

		</td>
	</tr>

</table>

