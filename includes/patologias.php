<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8">
		<link rel="shortcut icon" type="image/ico" href="http://www.sprymedia.co.uk/media/images/favicon.ico">
		
		<title>ColReorder example</title>
		
		<script type="text/javascript" charset="utf-8">
			$(document).ready( function () {
				var oTable = $('#example').dataTable( {
					"sDom": 'R<"H"lfr>t<"F"ip>',
					"bJQueryUI": true,
					"sPaginationType": "full_numbers",
					"oLanguage": { 
"oPaginate": { 
"sPrevious": "<", 
"sNext": ">", 
"sLast": "ULTIMA", 
"sFirst": "PRIMERA" 
}, 

"sLengthMenu": 'Mostrar <select>'+ 
'<option value="10">10</option>'+ 
'<option value="20">20</option>'+ 
'<option value="30">30</option>'+ 
'<option value="40">40</option>'+ 
'<option value="50">50</option>'+ 
'<option value="-1">Todos</option>'+ 
'</select> registros', 

"sInfo": "Mostrando del _START_ a _END_ (Total: _TOTAL_ resultados)", 

"sInfoFiltered": " - filtrados de _MAX_ registros", 

"sInfoEmpty": "No hay resultados de busqueda", 

"sZeroRecords": "No hay registros a mostrar", 

"sProcessing": "Espere, por favor...", 

"sSearch": "Buscar:", 

} 
					
				} );
			} );
		</script>
	</head>
<table width="950">
	<tr>
	  <td class="titulo1">Patologias</td>
	</tr>
	<tr><td><hr class="linea"/></td></tr>
<?

 $consulta = Consultar("select * from siscam.vst_aut where siscam.vst_aut.aut_estatus = 'UMED' order by siscam.vst_aut.aut_cartaaval asc");
			$nr=@pg_num_rows($consulta);
?>

<? if($consulta!=false){?>	
	
	<form name="frmpatologias" method="post">
	<tr>
	  <td>
	<? /*Abre una tabla*/?>		
		
	</td></tr>
	
	
	<tr><td>
	  <input type="submit" name="btnListar" value="Ver todos" class="boton" onclick="document.location.href='?id=patologia'"></td>
	</tr>
	</form>	
     <!-- Campos Ocultos -->    
    <input type="hidden" name="hdnpg" id="hdnpg" value="<?=$_GET['pg'];?>" />    
    <!-- Fin Campos Ocultos -->
	<tr><td><hr class="linea"/></td></tr>
</table>

<body id="dt_example">
		<div id="container">
			<div id="demo">	
<table cellpadding="0" cellspacing="0" border="0" id="example" class="display" style="width:100%">
				
              <thead>
				<tr class="TR1">
				<td width="16%"><div align="left">Carta Aval</div></td>
				<td width="16%"><div align="left">N&ordm; Control </div></td>
				<td width="14%"><div align="left">Cedula</div></td>
				<td width="41%"><div align="left">Nombre</div></td>
                <td width="13%"><div align="left">Fecha Registro</div></td>
				<td width="13%"><div align="left">Modalidad</div></td>
        		</tr>
              </thead>
              <tfoot>
      	
      		  </tfoot>				
		<tbody>      				
				<? while($row = pg_fetch_object($consulta)){?>
				<tr <? if($num%2==0){?> class="TR2"<? }else{ ?>class="TR3"<? } ?>>
					<td width="16%" ><a href="?id=patologia2&cod=<?=$row->aut_cartaaval?>"><?=$row->aut_cartaaval?></a></td>
					<td width="16%" ><? if ($row->aut_ncontrol <> ''){ echo $row->aut_ncontrol; }else{ echo 'No definido'; }?></td>
					<td width="14%" ><? if ($row->est_id <> ''){ echo $row->est_id; }else{ echo 'No definido'; }?></td>
					<td width="41%" ><? if ($row->est_pnombre <> ''){ echo $row->est_pnombre; echo " ".$row->est_papellido; }else{ echo 'No definido'; }?></td>
					<td width="13%" ><? if ($row->aut_fecreg <> ''){ echo $row->aut_fecreg; }else{ echo 'No definido'; }?></td>                    <td width="13%" ><? if ($row->aut_modalidad <> ''){ echo $row->aut_modalidad; }else{ echo 'No definido'; }?></td>
				</tr>
				<? $num++; } ?>				
			</table>
			<? }else{//end if consulta
				echo '<table cellspacing="0" width="100%">
				<tr>
					<td class="TR1">Casos para Codificar</td>
				</tr>
					<tr>
						<td class="titulo3">NO SE ENCONTRO LA INFORMACIÓN </td>
					</tr>
				</table>';	
		}//end if consulta

?>

</tbody>
  
 </table>
   
   
   <!-- <tr>
		<td>
			<div id="divcbit"></div>
		</td>
	</tr> -->
</table>

</div>
		
			
	
			
			
		</div>
   </body>
</html>
