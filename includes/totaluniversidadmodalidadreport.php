<script type="text/javascript" src="js/jquery-1.2.3.js"></script>
<script type="text/javascript" src="js/jquery.blockUI.js"></script>

<script>
	function imprimeinforme(){
		if(confirm("¿Desea Imprimir el reporte Universidad y Modalidad Detallado?")){
			window.open('reports/reportDETUNI.php?fdes='+document.getElementById('txtfechadesde').value+'&fechahasta='+document.getElementById('txtfechahasta').value+'&tipo='+document.getElementById('cobtipo').value+'&mod='+document.getElementById('cobmod').value+'','_blank');
		}

	}
</script>
<table width="950">
	<tr>
	  <td class="titulo1">Total por Universidad y Modalidad</td>
	</tr>
	<tr><td><hr class="linea"/></td></tr>
	
	<form name="frmautorizaciones" method="post">  
	<tr><td>
	<? /*Abre una tabla*/?>	
	
			<table width="900">
            
            	<tr>
                	<td width="45" class="titulo3">	Tipo de Casos: &nbsp;</td>
		      	 <td class="titulo4"><select name="cobtipo" class="inputbox" id="cobtipo"  title="Reporte a Seleccionar" onchange="validar_tipo()">
            		<option value="0">=Seleccione=</option>
            		<option value="1" <? if ($tipo=='1'){?> selected="selected" <? } ?>>Autorizados</option>
            		<option value="2" <? if ($tipo=='2'){?> selected="selected" <? } ?>>Pendientes</option>
                    <option value="3" <? if ($tipo=='3'){?> selected="selected" <? } ?>>Pagados</option>
                    <option value="4" <? if ($tipo=='4'){?> selected="selected" <? } ?>>Emitidos</option>
          			</select></td>
                    
                    
                    <td width="45" class="titulo3">	Modalidad: &nbsp;</td>
		      	 <td class="titulo4"><select name="cobmod" class="inputbox" id="cobmod"  title="Reporte a Seleccionar" onchange="validar_tipo()">
            		<option value="SELE">=Seleccione=</option>
            		<option value="ELECTIVA" <? if ($mod=='ELECTIVA'){?> selected="selected" <? } ?>>Electiva</option>
            		<option value="EMERGENCIA" <? if ($mod=='EMERGENCIA'){?> selected="selected" <? } ?>>Emergencia</option>
                    <option value="ESPECIAL" <? if ($mod=='ESPECIAL'){?> selected="selected" <? } ?>>Especial</option>
                    <option value="AYUDA SOLIDARIA" <? if ($mod=='AYUDA SOLIDARIA'){?> selected="selected" <? } ?>>Ayuda Solidaria</option>
          			</select></td>
                    				 	
                 
					<td class="titulo3">Fecha desde: </td>
				    <td><input type="text" id="txtfechadesde" size="12" maxlength="12"  class="inputbox" value="<?=@$txtfechadesde?>" readonly name="txtfechadesde" onFocus="displayCalendar(document.forms[0].txtfechadesde,'yyyy-mm-dd',this)" />*</td>
				
					<td class="titulo3">Fecha Hasta: </td>
				    <td><input type="text" id="txtfechahasta" size="12" maxlength="12"  class="inputbox" value="<?=@$txtfechahasta?>" readonly name="txtfechahasta" onFocus="displayCalendar(document.forms[0].txtfechahasta,'yyyy-mm-dd',this)" />*</td>
                    <input type="hidden" name="cobtipo" value="<?=@$tipo?>" disabled="disabled" />
                   
				</tr>
                </table>
                <tr><td>
		<input name="Buscar" type="button" class="boton" value="Buscar" onclick="javascript: espera();  getData('ajax/reportes/reportunimod.php','divusu','&fechadesde='+document.getElementById('txtfechadesde').value+'&fechahasta='+document.getElementById('txtfechahasta').value+'&tipo='+document.getElementById('cobtipo').options[document.getElementById('cobtipo').selectedIndex].value+'&mod='+document.getElementById('cobmod').options[document.getElementById('cobmod').selectedIndex].value+'&var=Agregar&inc=inc','var nulo=null;');"/>
			<? if($cboestatus !='0'){ ?>
            <a onClick="javascript:window.print();">
			<input name="btnimprimir" type="button" class="boton" id="btnimprimir" value="Imprimir">
            </a>
				<input name="btnimprimirdetalle" type="button" class="boton" id="btnimprimirdetalle" value="Imprimir Detalle" onclick="imprimeinforme()">
				 <? }else{
				 echo '<script> alert("Disculpe, debe seleccionar un estatus"); </script>';	
				 echo "<script>document.location.href ='?id=autorizaciones';</script>";
				 }?></td>
	</tr>
    </form>	
	<tr><td><hr class="linea"/></td></tr>
    <tr>
    	<td>
        	<div id="divusu"></div>
		</td>
	</tr>

</table>

<script type="text/javascript">
			
			function espera(){

$.blockUI('<img src="imagenes/loader.gif" />');
setTimeout($.unblockUI,2000);

}

</script>
