<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sin título</title>
</head>

<body>
<? include("../scripts/funciones.php");?>
<script type="text/javascript" src="../js/graf/jquery-1.3.1.min.js"></script>
<script type="text/javascript" src="../js/graf/fgCharting.jQuery.js"></script>
<script type="text/javascript" src="../js/graf/excanvas-compressed.js"></script>
<script language="javascript">
$(document).ready(function() {
	if($.browser.msie) { 
		setTimeout(function(){$.fgCharting();}, 2000);
	} else {
		$.fgCharting();
	}	
});

</script>
<link href="../js/graf/style.css" rel="stylesheet" type="text/css" />		


<? $consulta2="select distinct(siscam.vst_aut.usu_cedul_aut), siscam.vst_aut.usu_nombre_aut, count(siscam.vst_aut.aut_cartaaval) as cuentacaso, sum(siscam.vst_aut.aut_montoa) as sumamonto from siscam.vst_aut where siscam.vst_aut.aut_estatus ='AUT' and siscam.vst_aut.aut_fechaa >= '2010-01-01' and siscam.vst_aut.aut_fechaa <= '2010-05-11' group by siscam.vst_aut.usu_cedul_aut, siscam.vst_aut.usu_nombre_aut"; 
$aux = Consultar($consulta2); ?>

<table id="dataTable" summary="Member Data from 2000 to 2006">
		<thead>
				<tr>
                	<th></th>
                  <th width="263" class="titulo3" id="cantidad">Cantidad de casos Autorizados:</th>
               	 
                </tr>
          </thead>
          	<tbody>
                <tr>
                <? while($row = pg_fetch_object($aux)){?>
                <th width="263" class="titulo3" headers="members"><? echo $row->usu_nombre_aut?></th>
                  <td width="263" class="titulo3" headers="cantidad"><? echo $row->cuentacaso?></td>
                 
			  </tr>
             </tbody>
				<? } ?>
</table>

<div class="chartBlock" style="position: relative;">
	<canvas id="chart1" class="fgCharting_src-dataTable_type-pie" width="400" height="400"></canvas>
</div>   






</body>
</html>