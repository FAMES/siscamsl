<?php
require('../reports/fpdf16/fpdf.php');

	//Conexion al servidor
	$servidor = "190.190.1.3";
	$base = "intranet";
	$usuario = "postgres";
	$password = "postgres";   
	pg_pconnect("host=$servidor dbname=$base user=$usuario password=$password");

	$cedula= @$_GET["cod"];
	
class PDF extends FPDF
{
	//Cabecera de página
	function Header()
	{		
		//UNIDAD
		$this->SetY(26);
		$this->SetX(8);
		$this->SetFont('Arial','',12);
		$this->Cell(0,8,utf8_decode('FAMES/GO-CR '.date('Y').' - Nº'),0,0,'L');		
		
		//FECHA
		$this->SetY(32);
		$this->SetX(8);
		$this->SetFont('Arial','',12);
		$this->Cell(0,8,'Caracas, '.date('d/m/Y'),0,0,'R');
		
		//Logo
		$this->Image('../imagenes/encabezado_gris.jpg',13,8,180,12);
		
		//COMISION DE SALUD
		$this->SetY(38);
		$this->SetX(8);
		$this->SetFont('Arial','',12);
		$this->Cell(0,8,utf8_decode('Señores:'),0,0,'L');
		$this->SetY(44);
		$this->SetX(8);
		$this->SetFont('Arial','B',12);
		$this->MultiCell(0,8,utf8_decode('Comisión de Salud '.$uni),0,"J");
		$this->SetY(50);
		$this->SetX(8);
		$this->SetFont('Arial','',12);
		$this->Cell(0,8,utf8_decode('Coordinador(a): '.$cor),0,0,'L');
		$this->SetY(56);
		$this->SetX(8);
		$this->SetFont('Arial','',12);
		$this->Cell(0,8,utf8_decode('Estado: '.$estado),0,0,'L');
		$this->SetY(62);
		$this->SetX(8);
		$this->SetFont('Arial','',12);
		$this->MultiCell(0,8,utf8_decode('Dirección: '.$direccion),0,"J");
		
		//PIE DE PAGINA			
		$this->SetY(250);
		$this->SetX(8);
		$this->SetFont('Arial','I',8);
		$this->MultiCell(0,8,utf8_decode('HBM/NS/JR NV/ ').$rows->usu_nombre, 0, "J");
		
		$this->SetY(268);
		$this->SetX(8);
		$this->SetFont('Arial','B',8);
		$this->MultiCell(0,8,utf8_decode('Hacia una política preventiva en salud estudiantil'), 0, "C");
		$this->Line(10,275,195,275);
		
		$this->SetY(274);
		$this->SetX(8);
		$this->MultiCell(0,8,utf8_decode('Av. Urdaneta, Esquina de Animas a Plaza España, Edif. Centro Financiero Latino, Piso 10 Oficina 6 y 7. Cod. Postal 1011'), 0, "C");
		
		$this->SetY(278);
		$this->SetX(8);
		$this->MultiCell(0,8,'La Candelaria-Caracas. Telefonos: 564.31.33 / 564.11.38', 0, "C");
		
		$this->SetY(282);
		$this->SetX(8);
		$this->MultiCell(0,8,'Pagina Web: www.fames.gob.ve Correo Electronico: Fames@fames.gob.ve', 0, "C");
	}	

	//Tabla del Reporte
	function CrearTabla($cedula,$tipo)
	{		
		//REDACCION DE LA CARTA
		$this->SetFont('Arial','',12);
		$this->SetY(90);
		$this->SetX(8);
		$this->MultiCell(0,8,utf8_decode('Sirva la presente para notificar la devolución del expediente negado del estudiante '.$row->est_pnombre.' '.$row->est_papellido.', titular de la cédula de identidad No. '.$row->est_tipo.'-'.$row->est_id.', motivado a '.$row->uni_nombre),0,"J");
		$this->Ln(8);
		$this->Cell(0,8,utf8_decode('Sin más a que hacer referencia, se suscribe,'),0,0,'L');
		$this->Ln(15);
		$this->Cell(0,8,utf8_decode('Atentemente,'),0,0,'C');
		$this->Ln(25);
		$this->SetFont('Arial','B',12);
		$this->Cell(0,8,utf8_decode('Lic. Hasdrubal Becerra Miranda'),0,0,'C');
		$this->Ln();
		$this->SetFont('Arial','',12);
		$this->Cell(0,8,utf8_decode('Presidente (E)'),0,0,'C');
	}
}

//Creación del objeto de la clase heredada
$pdf=new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->CrearTabla($cedula,$tipo);
$pdf->Output();
?>
