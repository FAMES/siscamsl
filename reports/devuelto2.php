<?php
require('../reports/fpdf16/fpdf.php');

	//Conexion al servidor
	$servidor = "localhost";
	$base = "intranet_dev";
	$usuario = "postgres";
	$password = "$2011tyspostgresql%";   
	pg_pconnect("host=$servidor dbname=$base user=$usuario password=$password");

	$devid=@$_POST["devid"];
	
class PDF extends FPDF
{
	//Cabecera de página
	function Header()
	{		
		//UNIDAD
		$this->SetY(26);
		$this->SetX(8);
		$this->SetFont('Arial','',10);
		$this->Cell(0,8,utf8_decode('FAMES/GO-CAO '.date('Y').' - Nº'),0,0,'L');		
		
		//FECHA
		$this->SetY(32);
		$this->SetX(8);
		$this->SetFont('Arial','',10);
		$this->Cell(0,8,'Caracas, '.date('d/m/Y'),0,0,'R');
		
		//Logo
		$this->Image('../imagenes/encabezado_gris.jpg',13,8,180,12);
		
		//CONSULTA SQL DE DATOS DE LA COMISION DE SALUD
		$cedula= @$_POST["cedula"]; //POST DE LA CEDULA
		$tipo=@$_POST["tipo"]; //POST DEL TIPO DE DEVUELTO
		$devid=@$_POST["devid"]; //POST DEL ID DE DEVUELTO
		$consultac = "select * from siscam.vst_est_casos_dev where siscam.vst_est_casos_dev.dev_id = $devid";
		$queryc = pg_query($consultac);
		$rowc = pg_fetch_object($queryc);
		
		//COMISION DE SALUD
		$this->SetY(38);
		$this->SetX(8);
		$this->SetFont('Arial','',10);
		$this->Cell(0,8,utf8_decode('Señores:'),0,0,'L');
		$this->SetY(44);
		$this->SetX(8);
		$this->SetFont('Arial','B',10);
		$this->MultiCell(0,8,utf8_decode('Comisión de Salud '.$rowc->uni_nombre),0,"J");
		$this->SetY(50);
		$this->SetX(8);
		$this->SetFont('Arial','',10);
		$this->Cell(0,8,utf8_decode('Jefe(a) de Bienestar Estudiantil: '.$rowc->comi_nombrejefb),0,0,'L');
		$this->SetY(56);
		$this->SetX(8);
		$this->SetFont('Arial','',10);
		$this->Cell(0,8,utf8_decode('Estado: '.$rowc->esta_estado),0,0,'L');	
		
		//PIE DE PAGINA			
		$this->SetY(260);
		$this->SetFont('Arial','B',8);
		$this->Cell(0,4,utf8_decode('Hacia una política preventiva en salud estudiantil'),0,0,'C');
		$this->Ln();
		$this->Cell(0,4,utf8_decode('Av. Urdaneta, Esquina de Animas a Plaza España, Edif. Centro Financiero Latino, Piso 10 Oficina 6 y 7. Cod. Postal 1011'),'T', 0, "C");
		$this->Ln();
		$this->Cell(0,4,'La Candelaria-Caracas. Telefonos: 564.31.33 / 564.11.38',0,0,'C');
		$this->Ln();
		$this->Cell(0,4,'Pagina Web: www.fames.gob.ve Correo Electronico: fames@fames.gob.ve',0,0,'C');
	}	

	//Tabla del Reporte
	function CrearTabla($devid)
	{	
		//CONSULTA SQL DATOS DE ESTUDIANTE
		$consultae = "select * from siscam.vst_est_casos_dev where siscam.vst_est_casos_dev.dev_id = $devid";
		$querye = pg_query($consultae);
		$rowe = pg_fetch_object($querye);
		
		//REDACCION DE LA CARTA
		$this->SetFont('Arial','',10);
		$this->SetY(70);
		$this->SetX(8);
		$this->MultiCell(0,8,utf8_decode('   Nos dirigimos a ustedes muy respetuosamente en la oportunidad de informarles que la solicitud del estudiante '.$rowe->est_pnombre.' '.$rowe->est_papellido.', C.I. '.$rowe->est_tipo.'-'.$rowe->est_id.', llegó a esta fundación sin '.$rowe->dev_motivo),0,"J");
		$this->MultiCell(0,8,utf8_decode('   Es importante recalcar, que después de recibida por ustedes la presente comunicación, el caso permanecerá en la fundación 5 días hábiles en espera del recaudo. De no ser enviado durante este tiempo, el mismo será negado y devuelto a la Comisión de Salud.'),0,"J");
		$this->MultiCell(0,8,utf8_decode('   Sin más a que hacer referencia, seguros de su receptividad y colaboración, en pro del beneficio del estudiante, se despide,'),0,"J");
		$this->Ln(8);
		$this->Cell(0,8,utf8_decode('Atentemente,'),0,0,'C');
		$this->Ln(20);
		$this->SetFont('Arial','B',10);
		$this->Cell(0,8,utf8_decode('Prof. Gilberto Antonio Gutierrez Rodriguez'),0,0,'C');
		$this->Ln();
		$this->SetFont('Arial','',10);
		$this->Cell(0,8,utf8_decode('Presidente'),0,0,'C');
		$this->Ln();
		$this->SetFont('Arial','',9);
		$this->Cell(0,8,utf8_decode('Fundación Para la Asistencia Médica'),0,0,'C');
		$this->Ln();
		$this->SetFont('Arial','',9);
		$this->Cell(0,8,utf8_decode('y Hospitalaria Para Estudiantes de Educación Universitaria'),0,0,'C');
		
		
		//NOMBRE DE USUARIO
		$this->SetY(240);
		$this->SetX(8);
		$this->SetFont('Arial','I',8);
		$this->Cell(0,4,utf8_decode('GGR/NV/JR DV/ '.$rowe->usu_nombre_dev),0,0,'L');
	}
}

//Creación del objeto de la clase heredada
$pdf=new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->CrearTabla($devid);
$pdf->Output();
?>