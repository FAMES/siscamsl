<?php


require('../reports/fpdf16/fpdf.php');

	//Conexion al servidor
	$servidor = "localhost";
	$base = "intranet_dev";
	$usuario = "postgres";
	$password = "$2011tyspostgresql%";   
	pg_pconnect("host=$servidor dbname=$base user=$usuario password=$password");
	
	//Arreglo de los datos seleccionados con los checkbox
			/*$array = $_POST["enviar"];
			$valores = split(",",$array);
			$cont=cou*/

	$relacion = $_POST["relacion"];
	
	/*$consulta = "select * from siscam.vst_autorizaciones where siscam.vst_autorizaciones.aut_cartaaval = ".$carta;
		$query = pg_query($consulta);
		$rowr = pg_fetch_object($query,0);
		$modalidad = $rowr->aut_modalidad;*/
	
class PDF extends FPDF
{
	//Cabecera de página
	function Header()
	{	
		//fecha
		$this->SetY(28);
		$this->SetX(8);
		$this->SetFont('Arial','B',12);
		$this->MultiCell(188, 6,'FAMES: '.date('d/m/Y h:i A'), 0, "J");
		//Logo
		$this->Image('../imagenes/encabezado_carta.jpg',13,8,180,12);
		//Fuente
		$this->SetFont('Arial','B',15);
		//Movernos a la derecha
		$this->SetY(16);
		$this->SetX(96);
		//Título		
		$this->Cell(30,25,utf8_decode('Reporte'),0,0,'C');
	}

	//Numero de Página
	function Footer()
	{
		//Posición: a la derecha
		$this->SetY(17);
		//Arial italic 8
		$this->SetFont('Arial','I',8);
		//Número de página
		$this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'R');
	}

	
	//Tabla del Reporte
		
	function Crearfiniquito($relacion)
	{				
	
	// QUERY PARA TRAERNOS LOS DATOS DE LA CARTA AVAL
	
		$consulta = "select * from siscam.vst_aut_cli_re_esp where siscam.vst_aut_cli_re_esp.re_id = ".$relacion;
		$query = pg_query($consulta);
		$rowr = pg_fetch_object($query,0);
		$totalcan = number_format($rowr->re_totalcan,2,',','.');
		
		
		
		//$montop = number_format($rowr->aut_montop,2,',','.');
		
		$this->SetY(36);
		$this->SetX(58);
		$this->SetFont('Arial','B',12);
			
		$this->SetY(50);
		$this->SetX(8);
		$this->SetFont('Arial','I',12);
		$this->SetFillColor(192,192,192);
		$this->MultiCell(188, 6,utf8_decode('INFORMACIÓN DE RETENCIONES Y APORTES
'), 1, "C",true);
		$this->SetY(72);
		$this->SetX(8);
		$this->MultiCell(188, 6,utf8_decode('La Fundación para el Servicio de Asistencia Médica Hospitalaria para los Estudiantes de Educación Superior (FAMES), considerando el pago realizado de fecha '.$rowr->re_fechapagocheq.' a través de cheque N° '.$rowr->re_nrocheq.' en razón del Beneficio Solidario, en correspondencia con la Carta Aval N°'.$rowr->aut_cartaaval.'  Presenta el siguiente reporte con carácter informativo,  igualmente se compromete en emitir los comprobantes correspondientes una vez recibidas en esta oficina las Facturas Originales que sustenten este gasto, todo esto basados en el artículo 101 del Código Orgánico Tributario, el artículo 91 de la Ley del Impuesto Sobre la Renta y los artículos 175 y 176 del Reglamento de la Ley del Impuesto Sobre la Renta.'), 0, "J");
		
		
		
		
		
		
		$this->SetY(126);
		$this->SetX(8);
		$this->SetFillColor(192,192,192);
		$this->SetFont('Arial','B',12);
		$this->Cell(30,8,utf8_decode('Carta Aval '),1,0,'C',1);
		$this->Cell(26,8,utf8_decode('Cedula '),1,0,'C',1);
		$this->Cell(28,8,utf8_decode('Nº Factura '),1,0,'C',1);		
		$this->Cell(42,8,utf8_decode('Monto Facturado '),1,0,'C',1);
		$this->Cell(22,8,utf8_decode('Tipo '),1,0,'C',1);
		$this->Cell(42,8,utf8_decode('Monto Autorizado '),1,0,'C',1);
		$y = 133;
		$x = 8;
	
		//	query para traernos el usuario
		
		//$consultafin = "select * from siscam.vst_aut_fac_re_fin where siscam.vst_aut_fac_re_fin.re_id = ".$relacion;
		//$query = pg_query($consultafin);
		//$rowfin = pg_fetch_object($query,0);
		$consulta_cartas = "select * from siscam.vst_aut_re where siscam.vst_aut_re.re_id = ".$relacion ." ORDER BY AUT_NCONTROL";
		$query2 = pg_query($consulta_cartas);
		//$resultado = 'AMPLIACION';
		while($rowcarta = pg_fetch_object($query2)){
		$contador++;
		$this->SetY($y);
		$this->SetX($x);
		$this->SetFillColor(255,255,255);
		$this->SetFont('Arial','I',10);
		$this->Cell(30,5,utf8_decode($rowcarta->aut_cartaaval),1,0,'C',1);
		//Consulto numero de factura, monto de factura y cedula de estudiante por cada estudiante
		$consultafin = "select * from siscam.vst_aut_fac_re_fin where siscam.vst_aut_fac_re_fin.aut_cartaaval = ".$rowcarta->aut_cartaaval;
		$query = pg_query($consultafin);
		$rowfin = pg_fetch_object($query,0);
	if($rowcarta->aut_tipo == 'NUEVA' || $rowcarta->aut_tipo == 'EXTENSION')
	{	
		//$rowfin = pg_fetch_object($query,0);
		$this->Cell(26,5,utf8_decode($rowfin->est_id),1,0,'C',1);
		$this->Cell(28,5,utf8_decode($rowfin->fac_numero),1,0,'C',1);
		$this->Cell(42,5,number_format($rowfin->fac_monto,2,',','.'),1,0,'C',1);
		$this->Cell(22,5,utf8_decode($rowfin->aut_tipo),1,0,'C',1);
	}
	else 
	{
		$this->Cell(26,5,utf8_decode($rowfin->est_id),1,0,'C',1);
		$this->Cell(28,5,utf8_decode($rowfin->fac_numero),1,0,'C',1);
		$this->Cell(42,5,number_format($rowfin->fac_monto),1,0,'C',1);
		$this->Cell(22,5,utf8_decode("AMPLIACION"),1,0,'C',1);
	}
		//$this->Cell(26,5,utf8_decode($rowfin->est_id),1,0,'C',1);
		
		
		//$porcentaje = ($row->pat_majust / $row->bar_baremo) * 100;  
		//$this->Cell(42,5,number_format($rowfin->fac_monto,2,',','.'),1,0,'C',1);
		$this->Cell(42,5,number_format($rowcarta->aut_montoa,2,',','.'),1,0,'C',1);
		$y = $y + 5;
		if($contador == 25) { $this->AddPage(); $y = 39; $x=12; $s=0; $contador=0; }
		}
		

		
		//muestra totales del reporte
		$consulta2 = "select sum(fac_monto_exo) as exonerado, 
							  re_total, 
							  re_islr, 
							  re_riam, 
							  re_respso, 
							  re_totalcan 
				from siscam.vst_aut_fac_re_fin where siscam.vst_aut_fac_re_fin.re_id = $relacion 
					group by re_total, re_islr, re_riam, re_respso, re_totalcan";
		$query2 = pg_query($consulta2);
		$row2 = pg_fetch_object($query2);
		$this->Ln(4);
		$this->SetY(180);
		$this->SetX(110);
		$this->Cell(60,5,'MONTO TOTAL: ','LT',0);
		$this->SetX(170);
		$this->Cell(25,5,number_format($row2->re_total,2,',','.'),'TR',0);
		$this->Ln();
		$this->SetX(110);
		$this->Cell(60,5,'MONTO EXONERADO: ','L',0);
		$this->SetX(170);
		$this->Cell(25,5,number_format($row2->exonerado,2,',','.'),'R',0);
		$this->Ln();
		$this->SetX(110);
		$this->Cell(15,5,'RETENCION ISLR ('.$i.'%): ','L',0);
		$this->SetX(170);
		$this->Cell(25,5,number_format($row2->re_islr,2,',','.'),'R',0);		
		$this->Ln();
		$this->SetX(110);
		$this->Cell(60,5,'IMPUESTO DE TIMBRE FISCAL: ','L',0);
		$this->SetX(170);
		$this->Cell(25,5,number_format($row2->re_riam,2,',','.'),'R',0);
		$this->Ln();
		$this->SetX(110);
		$this->Cell(60,5,'RESPONSABILIDAD SOCIAL (3%): ','L',0);
		$this->SetX(170);
		$this->Cell(25,5,number_format($row2->re_respso,2,',','.'),'R',0);
		$this->Ln();
		$this->SetX(110);
		$this->Cell(60,5,'TOTAL A PAGAR: ','LB',0);
		$this->SetX(170);
		$this->Cell(25,5,number_format($row2->re_totalcan,2,',','.'),'BR',0);
		
		
	}
}

$pdf=new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();


$pdf->Crearfiniquito($relacion);
$pdf->Output();
?>
