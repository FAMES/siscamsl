<?php require('../reports/fpdf16/fpdf.php');

	//Conexion al servidor
	$servidor = "localhost";
	$base = "intranet_dev";
	$usuario = "postgres";
	$password = "$2011tyspostgresql%";
	pg_pconnect("host=$servidor dbname=$base user=$usuario password=$password");
	
	//Datos recibidos por POST
	$carta = @$_POST['cartabit'];
	
class PDF extends FPDF
{
	//Cabecera de p�gina
	function Header()
	{		
		//fecha
		$this->SetY(20);
		$this->SetFont('Arial','I',10);
		$this->Cell(0,8,'Fecha: '.date('d/m/Y'),0,0,'L');
		//Logo
		$this->Image('../imagenes/encabezado_gris.jpg',10,8,190,12);
		//Fuente
		$this->SetFont('Arial','B',12);
		//Movernos a la derecha
		$this->SetY(30);
		$this->SetX(86);
		//T�tulo		
		$this->Cell(30,5,'Hoja de Ruta de un Expediente',0,0,'C');
		$this->Ln();
	}
			
	//Tabla del Reporte
	function CrearTabla($carta)
	{
		//CONSULTA DE LA BASE DE DATOS
		//$carta=200900161;
		$consulta = "select * from siscam.vst_aut_bit where siscam.vst_aut_bit.aut_cartaaval = $carta";
		$query = pg_query($consulta);
		$row = pg_fetch_object($query ,0);
		$consultapat = "select * from siscam.vst_aut_pat_bit where siscam.vst_aut_pat_bit.aut_cartaaval = $carta";
		$querypat = pg_query($consultapat);		
		while($rowpat = pg_fetch_object($querypat)){ 
			$patologia.=$rowpat->bar_descripcion.' / ';
		}
		if ($patologia=='' or $patologia==NULL){ $patologia='NO HAY PATOLOGIA AGREGADA A ESTA ATENCI�N'; }
		$this->SetY(45);
		
		//DATOS DE LA ATENCION
		//PRIMER TITULO
		$this->SetFont('Times','B',8);	
		$this->SetFillColor(192,192,192);
		$this->Cell(0,5,utf8_decode('Datos de la Atención'),'LTRB',0,'C',true);
		$this->Ln(8);
		
		//PRIMERA FILA
		$this->Cell(40,5,'Carta Aval: ',0,0,'L');
		$this->Cell(40,5,'Nro de Control: ',0,0,'L');	
		$this->Cell(35,5,utf8_decode('Cédula: '),0,0,'L');		
		$this->Cell(70,5,'Nombre y Apellido: ',0,0,'L');
		$this->Ln();
		$this->SetFont('Times','',8);
		$this->Cell(40,5,$row->aut_cartaaval,0,0,'L');
		$this->Cell(40,5,$row->aut_ncontrol,0,0,'L');	
		$this->Cell(35,5,$row->est_id,0,0,'L');		
		$this->Cell(70,5,utf8_decode($row->est_pnombre.' '.$row->est_papellido),0,0,'L');
		$this->Ln(10);
		
		//SEGUNDA FILA
		$this->SetFont('Times','B',8);
		$this->Cell(80,5,'Carrera / Periodo: ',0,0,'L');		
		$this->Cell(80,5,'Universidad: ',0,0,'L');	
		$this->Ln();
		$this->SetFont('Times','',8);
		$this->Cell(80,5,utf8_decode($row->car_nombre.' / '.$row->bit_est_per_estudio),0,0,'L');		
		$this->Cell(80,5,$row->uni_nombre,0,0,'L');	
		$this->Ln(10);
		
		//TERCERA FILA	
		$moda = $row->aut_modalidad;
		$this->SetFont('Times','B',8);	
		$this->Cell(40,5,'Modalidad: ',0,0,'L');
		$this->Cell(40,5,utf8_decode('Tipo de Solicitud: '),0,0,'L');		
		$this->Cell(80,5,utf8_decode('Clínica: '),0,0,'L');
		$this->Ln();
		$this->SetFont('Times','',8);		
		$this->Cell(40,5,$moda,0,0,'L');
		$this->Cell(40,5,utf8_decode($row->aut_tipo),0,0,'L');		
		$this->Cell(80,5,utf8_decode($row->cli_nombre),0,0,'L');
		$this->Ln(10);
		
		//CUARTA FILA	
		$this->SetFont('Times','B',8);
		$this->Cell(80,5,utf8_decode('Patología: '),0,0,'L');
		$this->Ln();
		$this->SetFont('Times','',8);
		$this->Cell(80,5,$patologia,0,0,'L');
		$this->Ln(20);
		
		
		
		
	if ($moda !='AYUDA SOLIDARIA' and $moda !='AYUDA SOLIDARIA ESPECIAL'){
				
				//SEGUIMIENTO DE LA ATENCI�N
				//SEGUNDO TITULO
				$this->SetFont('Times','B',8);
				$this->SetFillColor(192,192,192);
				$this->Cell(0,5,utf8_decode('Seguimiento de la Atención'),'LTRB',0,'C',true);
				$this->Ln(8);
				
				//PRIMERA FILA
				$this->SetFont('Times','B',8);
				$this->Cell(0,5,'Correspondencia:',0,0,'L');
				$this->Ln();
				$this->SetFont('Times','',8);				
				$this->Cell(60,5,'Usuario: '.$row->usu_cor,0,0,'L');
				$this->Cell(40,5,'Fecha Inicio: '.$row->bit_corfecha,0,0,'L');
				$this->Cell(40,5,'Fecha Fin: '.$row->bit_corfecha,0,0,'L');
				$this->Cell(30,5,'Hora: '.$row->bit_corhora,0,0,'L');
				$this->Cell(40,5,'Sin Estatus',0,0,'L');
				$this->Ln(6);
				$this->Cell(60,5,utf8_decode('Observación: ').$row->bit_corobs,0,0,'L');
				$this->Ln(12);
				$this->Cell(60,5,utf8_decode('Fecha Segundo Ingreso: ').$row->bit_fecingr2,0,0,'L');			
				$this->Ln(10);
				
				
				
				//SEGUNDA FILA
				$this->SetFont('Times','B',8);
				$this->Cell(0,5,utf8_decode('Coordinación Regional:'),0,0,'L');
				$this->Ln();
				$this->SetFont('Times','',8);				
				$this->Cell(60,5,'Usuario: '.$row->usu_reg,0,0,'L');
				$this->Cell(40,5,'Fecha Inicio: '.$row->bit_regfecha,0,0,'L');
				$this->Cell(40,5,'Fecha Fin: '.$row->bit_regfecha,0,0,'L');
				$this->Cell(30,5,'Hora: '.$row->bit_reghora,0,0,'L');
				$this->Cell(40,5,'Estatus: '.$row->bit_regestatus,0,0,'L');			
				$this->Ln(15);
				
				//TERCERA FILA
				$this->SetFont('Times','B',8);
				$this->Cell(0,5,utf8_decode('Unidad Médica (Codificación):'),'T',0,'L');
				$this->Ln();
				$this->SetFont('Times','',8);				
				$this->Cell(60,5,'Usuario: '.$row->usu_med,0,0,'L');
				$this->Cell(40,5,'Fecha Inicio: '.$row->bit_regfecha,0,0,'L');
				$this->Cell(40,5,'Fecha Fin: '.$row->bit_medfecha,0,0,'L');
				$this->Cell(30,5,'Hora: '.$row->bit_medhora,0,0,'L');
				$this->Cell(40,5,'Estatus: '.$row->bit_medestatus,0,0,'L');	
				$this->Ln(15);
				
				//CUARTA FILA
				$this->SetFont('Times','B',8);
				$this->Cell(0,5,utf8_decode('Revisión:'),'T',0,'L');
				$this->Ln();
				$this->SetFont('Times','',8);				
				$this->Cell(60,5,'Usuario: '.$row->usu_rev,0,0,'L');
				$this->Cell(40,5,'Fecha Inicio: '.$row->bit_medfecha,0,0,'L');
				$this->Cell(40,5,'Fecha Fin: '.$row->bit_revfecha,0,0,'L');
				$this->Cell(30,5,'Hora: '.$row->bit_revhora,0,0,'L');
				$this->Cell(40,5,'Estatus: '.$row->bit_revestatus,0,0,'L');	
				$this->Ln(15);
				
				//QUINTA FILA
				$this->SetFont('Times','B',8);
				$this->Cell(0,5,utf8_decode('Aprobación:'),'T',0,'L');
				$this->Ln();
				$this->SetFont('Times','',8);				
				$this->Cell(60,5,'Usuario: '.$row->usu_aut,0,0,'L');
				$this->Cell(40,5,'Fecha Inicio: '.$row->bit_revfecha,0,0,'L');
				$this->Cell(40,5,'Fecha Fin: '.$row->bit_autfecha,0,0,'L');
				$this->Cell(30,5,'Hora: '.$row->bit_authora,0,0,'L');
				$this->Cell(40,5,'Estatus: '.$row->bit_autestatus,0,0,'L');	
				$this->Ln(15);
				
				
				//SEXTA FILA
				$this->SetFont('Times','B',8);
				$this->Cell(0,5,utf8_decode('Envio a Comisión:'),'T',0,'L');
				$this->Ln();
				$this->SetFont('Times','',8);				
				$this->Cell(60,5,'Usuario: '.$row->usu_env,0,0,'L');
				$this->Cell(40,5,'Fecha Inicio: '.$row->bit_autfecha,0,0,'L');
				$this->Cell(40,5,'Fecha Fin: '.$row->bit_envfecha,0,0,'L');
				$this->Cell(30,5,'Hora: '.$row->bit_envhora,0,0,'L');
				$this->Cell(40,5,'Estatus: '.$row->bit_envestatus,0,0,'L');	
				$this->Ln(15);
				
				
				//SEPTIMA FILA
				$this->SetFont('Times','B',8);
				$this->Cell(0,5,utf8_decode('Recepción:'),'T',0,'L');
				$this->Ln();
				$this->SetFont('Times','',8);				
				$this->Cell(60,5,'Usuario: '.$row->usu_rec,0,0,'L');
				$this->Cell(40,5,'Fecha Inicio: '.$row->bit_medfecha,0,0,'L');
				$this->Cell(40,5,'Fecha Fin: '.$row->bit_recfecha,0,0,'L');
				$this->Cell(30,5,'Hora: '.$row->bit_rechora,0,0,'L');
				$this->Cell(40,5,'Estatus: '.$row->bit_recestatus,0,0,'L');	
				$this->Ln(15);
				
				//OCTABA FILA
				$this->SetFont('Times','B',8);
				$this->Cell(0,5,utf8_decode('Coordinación de Pago (Análisis de Factura de Pago):'),'T',0,'L');
				$this->Ln();
				$this->SetFont('Times','',8);				
				$this->Cell(60,5,'Usuario: '.$row->usu_pag,0,0,'L');
				$this->Cell(40,5,'Fecha Inicio: '.$row->bit_recfecha,0,0,'L');
				$this->Cell(40,5,'Fecha Fin: '.$row->bit_pagfecha,0,0,'L');
				$this->Cell(30,5,'Hora: '.$row->bit_paghora,0,0,'L');
				$this->Cell(40,5,'Estatus: '.$row->bit_pagestatus,0,0,'L');	
				$this->Ln(20);
				
				//NOVENA FILA
				$this->SetFont('Times','B',8);
				$this->Cell(0,5,utf8_decode('Coordinación de Pago (Generación de Relación de Pago):'),'T',0,'L');
				$this->Ln();
				$this->SetFont('Times','',8);				
				$this->Cell(60,5,'Usuario: '.$row->usu_rel,0,0,'L');
				$this->Cell(40,5,'Fecha Inicio: '.$row->bit_pagfecha,0,0,'L');
				$this->Cell(40,5,'Fecha Fin: '.$row->bit_relfecha,0,0,'L');
				$this->Cell(30,5,'Hora: '.$row->bit_relhora,0,0,'L');
				$this->Cell(40,5,'Estatus: '.$row->bit_relestatus,0,0,'L');	
				$this->Ln(15);
				
				//DECIMA FILA
				$this->SetFont('Times','B',8);
				$this->Cell(0,5,utf8_decode('Coordinación de Finanzas:'),'T',0,'L');
				$this->Ln();
				$this->SetFont('Times','',8);				
				$this->Cell(60,5,'Usuario: '.$row->usu_che,0,0,'L');
				$this->Cell(40,5,'Fecha Inicio: '.$row->bit_relfecha,0,0,'L');
				$this->Cell(40,5,'Fecha Fin: '.$row->bit_fechafinal,0,0,'L');
				$this->Cell(30,5,'Hora: '.$row->bit_horafinal,0,0,'L');
				$this->Cell(40,5,'Estatus: '.$row->bit_cheestatus,0,0,'L');		
				$this->Ln(15);
				
				$this->SetFont('Times','B',8);
				$this->Cell(0,5,utf8_decode('Coordinación de Contabilidad:'),'T',0,'L');
				$this->Ln();
				$this->SetFont('Times','',8);				
				$this->Cell(60,5,'Usuario: '.$row->usu_con,0,0,'L');
				$this->Cell(40,5,'Fecha Inicio: '.$row->bit_fechafinal,0,0,'L');
				$this->Cell(40,5,'Fecha Fin: '.$row->bit_confecha,0,0,'L');
				$this->Cell(30,5,'Hora: '.$row->bit_conhora,0,0,'L');
				$this->Cell(40,5,'Estatus: '.$row->bit_conestatus,0,0,'L');		
				$this->Ln(19);
				
				
		} else{
			//SEGUIMIENTO DE LA ATENCI�N
				//SEGUNDO TITULO
				$this->SetFont('Times','B',8);
				$this->SetFillColor(192,192,192);
				$this->Cell(0,5,utf8_decode('Seguimiento de la Atención'),'LTRB',0,'C',true);
				$this->Ln(8);
				
				
				//PRIMERA FILA
				$this->SetFont('Times','B',8);
				$this->Cell(0,5,'Correspondencia:',0,0,'L');
				$this->Ln();
				$this->SetFont('Times','',8);				
				$this->Cell(60,5,'Usuario: '.$row->usu_cor,0,0,'L');
				$this->Cell(40,5,'Fecha Inicio: '.$row->bit_corfecha,0,0,'L');
				$this->Cell(40,5,'Fecha Fin: '.$row->bit_corfecha,0,0,'L');
				$this->Cell(30,5,'Hora: '.$row->bit_corhora,0,0,'L');
				$this->Cell(40,5,'Sin Estatus',0,0,'L');			
				$this->Ln(15);
				
				//SEGUNDA FILA
				$this->SetFont('Times','B',8);
				$this->Cell(0,5,utf8_decode('Coordinación Regional:'),0,0,'L');
				$this->Ln();
				$this->SetFont('Times','',8);				
				$this->Cell(60,5,'Usuario: '.$row->usu_reg,0,0,'L');
				$this->Cell(40,5,'Fecha Inicio: '.$row->bit_regfecha,0,0,'L');
				$this->Cell(40,5,'Fecha Fin: '.$row->bit_regfecha,0,0,'L');
				$this->Cell(30,5,'Hora: '.$row->bit_reghora,0,0,'L');
				$this->Cell(40,5,'Estatus: '.$row->bit_regestatus,0,0,'L');			
				$this->Ln(15);
				
				//TERCERA FILA
				$this->SetFont('Times','B',8);
				$this->Cell(0,5,utf8_decode('Coordinación de Pago (Análisis de Factura de Pago):'),'T',0,'L');
				$this->Ln();
				$this->SetFont('Times','',8);				
				$this->Cell(60,5,'Usuario: '.$row->usu_pag,0,0,'L');
				$this->Cell(40,5,'Fecha Inicio: '.$row->bit_recfecha,0,0,'L');
				$this->Cell(40,5,'Fecha Fin: '.$row->bit_pagfecha,0,0,'L');
				$this->Cell(30,5,'Hora: '.$row->bit_paghora,0,0,'L');
				$this->Cell(40,5,'Estatus: '.$row->bit_pagestatus,0,0,'L');	
				$this->Ln(15);
				
				
				//CUARTA FILA
				$this->SetFont('Times','B',8);
				$this->Cell(0,5,utf8_decode('Unidad Médica:'),'T',0,'L');
				$this->Ln();
				$this->SetFont('Times','',8);				
				$this->Cell(60,5,'Usuario: '.$row->usu_med,0,0,'L');
				$this->Cell(40,5,'Fecha Inicio: '.$row->bit_regfecha,0,0,'L');
				$this->Cell(40,5,'Fecha Fin: '.$row->bit_medfecha,0,0,'L');
				$this->Cell(30,5,'Hora: '.$row->bit_medhora,0,0,'L');
				$this->Cell(40,5,'Estatus: '.$row->bit_medestatus,0,0,'L');	
				$this->Ln(15);
				
				//QUINTA FILA
				$this->SetFont('Times','B',8);
				$this->Cell(0,5,utf8_decode('Revisión:'),'T',0,'L');
				$this->Ln();
				$this->SetFont('Times','',8);				
				$this->Cell(60,5,'Usuario: '.$row->usu_rev,0,0,'L');
				$this->Cell(40,5,'Fecha Inicio: '.$row->bit_medfecha,0,0,'L');
				$this->Cell(40,5,'Fecha Fin: '.$row->bit_revfecha,0,0,'L');
				$this->Cell(30,5,'Hora: '.$row->bit_revhora,0,0,'L');
				$this->Cell(40,5,'Estatus: '.$row->bit_revestatus,0,0,'L');	
				$this->Ln(15);
				
				//SEXTA FILA
				$this->SetFont('Times','B',8);
				$this->Cell(0,5,utf8_decode('Autorización:'),'T',0,'L');
				$this->Ln();
				$this->SetFont('Times','',8);				
				$this->Cell(60,5,'Usuario: '.$row->usu_aut,0,0,'L');
				$this->Cell(40,5,'Fecha Inicio: '.$row->bit_revfecha,0,0,'L');
				$this->Cell(40,5,'Fecha Fin: '.$row->bit_autfecha,0,0,'L');
				$this->Cell(30,5,'Hora: '.$row->bit_authora,0,0,'L');
				$this->Cell(40,5,'Estatus: '.$row->bit_autestatus,0,0,'L');	
				$this->Ln(15);
				
							
				//SEPTIMA FILA
				$this->SetFont('Times','B',8);
				$this->Cell(0,5,utf8_decode('Coordinación de Pago (Generación de Relación de Pago):'),'T',0,'L');
				$this->Ln();
				$this->SetFont('Times','',8);				
				$this->Cell(60,5,'Usuario: '.$row->usu_rel,0,0,'L');
				$this->Cell(40,5,'Fecha Inicio: '.$row->bit_pagfecha,0,0,'L');
				$this->Cell(40,5,'Fecha Fin: '.$row->bit_relfecha,0,0,'L');
				$this->Cell(30,5,'Hora: '.$row->bit_relhora,0,0,'L');
				$this->Cell(40,5,'Estatus: '.$row->bit_relestatus,0,0,'L');	
				$this->Ln(15);
				
				//OCTAVA FILA
				$this->SetFont('Times','B',8);
				$this->Cell(0,5,utf8_decode('Coordinación de Finanzas:'),'T',0,'L');
				$this->Ln();
				$this->SetFont('Times','',8);				
				$this->Cell(60,5,'Usuario: '.$row->usu_che,0,0,'L');
				$this->Cell(40,5,'Fecha Inicio: '.$row->bit_relfecha,0,0,'L');
				$this->Cell(40,5,'Fecha Fin: '.$row->bit_fechafinal,0,0,'L');
				$this->Cell(30,5,'Hora: '.$row->bit_horafinal,0,0,'L');
				$this->Cell(40,5,'Estatus: '.$row->bit_cheestatus,0,0,'L');		
				$this->Ln(15);
				
				
				//NOVENA FILA
				$this->SetFont('Times','B',8);
				$this->Cell(0,5,utf8_decode('Coordinación de Contabilidad:'),'T',0,'L');
				$this->Ln();
				$this->SetFont('Times','',8);				
				$this->Cell(60,5,'Usuario: '.$row->usu_con,0,0,'L');
				$this->Cell(40,5,'Fecha Inicio: '.$row->bit_fechafinal,0,0,'L');
				$this->Cell(40,5,'Fecha Fin: '.$row->bit_confecha,0,0,'L');
				$this->Cell(30,5,'Hora: '.$row->bit_conhora,0,0,'L');
				$this->Cell(40,5,'Estatus: '.$row->bit_conestatus,0,0,'L');		
				$this->Ln(19);
				
				
				
		//	query para traernos el usuario
		
				
			
		}
				$this->SetY(120);
				$this->SetX(8);
				$this->SetFont('Times','B',8);
				$this->Cell(0,5,utf8_decode('Coordinación de Archivo:'),'T',0,'L');
				$this->Ln();
				$this->SetY(128);
				$this->SetX(8);
				$this->SetFillColor(192,192,192);
				$this->SetFont('Arial','B',12);
				$this->Cell(100,8,utf8_decode('Usuario '),1,0,'C',1);
				$this->Cell(26,8,utf8_decode('Descripción '),1,0,'C',1);
				$this->Cell(20,8,utf8_decode('Fecha '),1,0,'C',1);
				$this->Cell(42,8,utf8_decode('Hora '),1,0,'C',1);
				$y = 136;
				$x = 8;
		
				$consultapat = "select * from sistar.vst_movim_estat_usua where sistar.vst_movim_estat_usua.mov_id_doc_exped = $carta order by mov_id asc";
				$query = pg_query($consultapat);
				while($row = pg_fetch_object($query)){
				$this->SetY($y);
				$this->SetX($x);
				$this->SetFillColor(255,255,255);
				$this->SetFont('Arial','',10);
				$this->Cell(100,5,utf8_decode($row->usuario),1,0,'C',1);
				$this->Cell(26,5,utf8_decode($row->estat_descripcion),1,0,'L',1);
				$this->Cell(20,5,utf8_decode($row->mov_fecha),1,0,'C',1);
				$this->Cell(42,5,utf8_decode($row->mov_hora),1,0,'C',1);
				$y = $y + 5;
				}
	}
}

//Creaci�n del objeto de la clase heredada
$pdf=new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();

//llamada a la funcion de crear tabla y fecha
$pdf->CrearTabla($carta);
$pdf->Output();

?>
