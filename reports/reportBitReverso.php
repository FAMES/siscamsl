<?php require('../reports/fpdf16/fpdf.php');
	  require('../scripts/funciones.php');

	//Conexion al servidor
	$servidor = "localhost";
	$base = "intranet_dev";
	$usuario = "postgres";
	$password = "$2011tyspostgresql%";   
	pg_pconnect("host=$servidor dbname=$base user=$usuario password=$password");
	
	//Datos recibidos por POST
	$carta = $_POST['carta'];	
	
class PDF extends FPDF
{
	//Cabecera de p�gina
	function Header()
	{		
		//fecha
		$this->SetY(20);
		$this->SetFont('Arial','I',10);
		$this->Cell(0,8,'Fecha: '.date('d/m/Y'),0,0,'L');
		//Logo
		$this->Image('../imagenes/encabezado_gris.jpg',10,8,190,12);
		//Fuente
		$this->SetFont('Arial','B',12);
		//Movernos a la derecha
		$this->SetY(30);
		$this->SetX(86);
		//T�tulo		
		$this->Cell(30,5,'Bitacora de Reversos',0,0,'C');
		$this->Ln();
	}
			
	//Tabla del Reporte
	function CrearTabla($carta)
	{
		//CONSULTA DE LA BASE DE DATOS		
		//$carta=200900161;
		$consulta = "select * from siscam.vst_aut_bit where siscam.vst_aut_bit.aut_cartaaval = $carta";
		$query = pg_query($consulta);
		$row = pg_fetch_object($query ,0);	
		$this->SetY(45);
		
		//DATOS DE LA ATENCION
		//PRIMER TITULO
		$this->SetFont('Times','B',8);	
		$this->SetFillColor(192,192,192);
		$this->Cell(0,5,utf8_decode('DATOS DE LA ATENCION'),'LTRB',0,'C',true);
		$this->Ln(8);
		
		//PRIMERA FILA
		$this->Cell(40,5,'Carta Aval: ',0,0,'L');
		$this->Cell(40,5,'Nro de Control: ',0,0,'L');	
		$this->Cell(35,5,utf8_decode('Cédula: '),0,0,'L');		
		$this->Cell(70,5,'Nombre y Apellido: ',0,0,'L');
		$this->Ln();
		$this->SetFont('Times','',8);
		$this->Cell(40,5,$row->aut_cartaaval,0,0,'L');
		$this->Cell(40,5,$row->aut_ncontrol,0,0,'L');	
		$this->Cell(35,5,$row->est_id,0,0,'L');		
		$this->Cell(70,5,utf8_decode($row->est_pnombre.' '.$row->est_papellido),0,0,'L');
		$this->Ln(10);
		
		//SEGUNDA FILA
		$this->SetFont('Times','B',8);
		$this->Cell(80,5,'Carrera / Periodo: ',0,0,'L');		
		$this->Cell(80,5,'Universidad: ',0,0,'L');	
		$this->Ln();
		$this->SetFont('Times','',8);
		$this->Cell(80,5,utf8_decode($row->car_nombre.' / '.$row->bit_est_per_estudio),0,0,'L');		
		$this->Cell(80,5,$row->uni_nombre,0,0,'L');	
		$this->Ln(10);
		
		//TERCERA FILA	
		$moda = $row->aut_modalidad;
		$this->SetFont('Times','B',8);	
		$this->Cell(40,5,'Modalidad: ',0,0,'L');
		$this->Cell(40,5,'Estatus Actual : ',0,0,'L');		
		$this->Cell(80,5,utf8_decode('Clínica: '),0,0,'L');
		$this->Ln();
		$this->SetFont('Times','',8);		
		$this->Cell(40,5,$moda,0,0,'L');
		$this->Cell(40,5,obtener_estatus($row->aut_estatus),0,0,'L');		
		$this->Cell(80,5,utf8_decode($row->cli_nombre),0,0,'L');
		$this->Ln(10);
	
		
	
			//SEGUIMIENTO DE LA ATENCI�N
				//SEGUNDO TITULO
				
				$this->Cell(0,5,utf8_decode('REVERSOS'),'LTRB',0,'C',true);
				$this->Ln(8);
			//CONSULTA DE HISTORIAL DE REVERSOS DEL CASO
		$consultaRev = "SELECT * from siscam.bit_reverso A, siscam.autorizaciones B, siscam.usuarios C
						where A.aut_cartaaval = $carta 
						and A.aut_cartaaval = B.aut_cartaaval
						and A.accion = 'REVERSO'
						and A.usu_cedula = C.usu_cedul order by A.fecrev";
		$queryRev = pg_query($consultaRev);				
		if (!$queryRev){ $patologia='NO HAY REVERSO REGISTRADOS'; }	
								
			while($fila = pg_fetch_object($queryRev))
		    {				
				$estAnt = obtener_estatus($fila->estant);	
				$estAct = obtener_estatus($fila->estact);				
				
				$this->SetFont('Times','B',8);
				$this->SetFillColor(192,192,192);
				
			 	$this->Cell(30,5,'Estatus Anterior ',0,0,'C');	
				$this->Cell(30,5,'Estatus Nuevo ',0,0,'C');	
				$this->Cell(15,5,'Fecha ',0,0,'C');
				$this->Cell(15,5,'Hora ',0,0,'C');		
				$this->Cell(35,5,'Usuario ',0,0,'C');
				$this->Cell(40,5,'Motivo ',0,0,'C');
				$this->Ln();
				$this->SetFont('Times','',8);
				$this->Cell(30,5,$estAnt,0,0,'C');
				$this->Cell(30,5,$estAct,0,0,'C');	
				$this->Cell(15,5,$fila->fecrev,0,0,'C');
				$this->Cell(15,5,$fila->horrev,0,0,'C');
				$this->Cell(35,5,$fila->usu_nombre,0,0,'C');
				$this->MultiCell(40,5,$fila->desrev,'C');	
				$this->Ln(10); 
				if($this->getY()>=260) { $this->AddPage(); $this->setY(45); }
			}	
	}
}

//Creaci�n del objeto de la clase heredada

$pdf=new PDF();

$pdf->AliasNbPages();

$pdf->AddPage();

//llamada a la funcion de crear tabla y fecha
$pdf->CrearTabla($carta);

$pdf->Output();

?>
