<?php
require('../reports/fpdf16/fpdf.php');

	//Conexion al servidor
	$servidor = "190.190.1.19";
	$base = "intranet_dev";
	$usuario = "postgres";
	$password = "$2011tyspostgresql%";   
	pg_pconnect("host=$servidor dbname=$base user=$usuario password=$password");
	
	
//Referencia

	 $cobtipo = strtoupper(@$_POST["cobtipo"]);
    $cbocarrera = strtoupper(@$_POST["cbocarrera"]);
	$cbouniversidad = @$_POST["cbouniversidad"];
	$txtfechadesde = strtoupper(@$_POST["txtfechadesde"]);
	$txtfechahasta = strtoupper(@$_POST["txtfechahasta"]);
	
class PDF extends FPDF
{


	//Cabecera de página
	function Header()
	{		
		//fecha
		$this->SetY(20);
		$this->SetFont('Arial','I',10);
		$this->Cell(0,8,'Fecha: '.date('d/m/Y h:i A'),0,0,'L');
		//Logo
		$this->Image('../imagenes/encabezado_gris.jpg',13,8,180,12);
		//Fuente
		$this->SetFont('Arial','B',15);
		//Movernos a la derecha
		$this->SetY(16);
		$this->SetX(96);
		//Título		
		$this->Cell(30,25,utf8_decode('Reportes por Carreras'),0,0,'C');
		

	}

	//Numero de Página
	function Footer()
	{
		//Posición: a la derecha
		$this->SetY(19);
		//Arial italic 8
		$this->SetFont('Arial','I',10);
		//Número de página
		$this->Cell(0,10,utf8_decode('Página '.$this->PageNo().'/{nb}'),0,0,'R');
	}
	
	//Tabla del Reporte
	function CrearTabla($cobtipo,$cbocarrera,$cbouniversidad,$txtfechadesde,$txtfechahasta)
	{		
	
		$this->SetFont('Times','B',10);
		$this->SetY(26);
		$this->SetX(58);
		if ($cobtipo =='1'){
		 	$titulo1 = 'Autorizados';
		}
		if ($cobtipo =='2'){
		 	$titulo1 = 'Pendientes';
		}
		if ($cobtipo =='3'){
		 	$titulo1 = 'Pagados';
		}
		
		 
		$this->Cell(30,25,utf8_decode(' Casos '.$titulo1.' Desde '.$txtfechadesde.' Hasta '.$txtfechahasta),0);
		$this->Ln();
		
		//Anchuras de las columnas
		$this->SetY(49);
		$this->SetFont('Times','',9);
		$w=array(22,22,22,22,55,25);
		$this->Ln(8);
		//Subtítulos
		$this->SetY(51);
		$this->SetFont('Times','B',9);
		$this->Cell(93,5,utf8_decode('Carrera'),0);
		$this->Cell(33,5,utf8_decode('Total Casos'),0);
		if($cobtipo =='1' or $cobtipo =='2'){
				$this->Cell(33,5,utf8_decode('Total Autorizado'),0);
				} elseif($cobtipo =='3'){
				$this->Cell(33,5,utf8_decode('Total Pagado'),0);
		}
		
		$this->Line(10,56,195,56);
		$this->Ln();
		
		
		if ($cobtipo =='1' and $cbocarrera =='0' and $txtfechadesde !=NULL and $txtfechahasta !=NULL and $cbouniversidad =='0'){	
	
						
			$condi =" where siscam.vst_aut.aut_estatus ='AUT' and siscam.vst_aut.aut_fechaa >='$txtfechadesde' and siscam.vst_aut.aut_fechaa <='$txtfechahasta'";
			
			$consulta ="SELECT DISTINCT vst_aut.car_id, vst_aut.car_nombre, count(vst_aut.aut_cartaaval) AS conteo, sum(vst_aut.aut_montoa) AS montoaut
   FROM siscam.vst_aut $condi 
  GROUP BY vst_aut.car_id, vst_aut.car_nombre
  ORDER BY vst_aut.car_id, vst_aut.car_nombre, count(vst_aut.aut_cartaaval), sum(vst_aut.aut_montoa) asc";
		
		} else if ($cobtipo =='1' and $cbocarrera !='0' and $txtfechadesde !=NULL and $txtfechahasta !=NULL and $cbouniversidad =='0'){
			
				
			$condi =" where siscam.vst_aut.car_id =$cbocarrera and siscam.vst_aut.aut_estatus ='AUT' and siscam.vst_aut.aut_fechaa >='$txtfechadesde' and siscam.vst_aut.aut_fechaa <='$txtfechahasta'";
			
			$consulta ="SELECT DISTINCT vst_aut.car_id, vst_aut.car_nombre, count(vst_aut.aut_cartaaval) AS conteo, sum(vst_aut.aut_montoa) AS montoaut
   FROM siscam.vst_aut $condi 
  GROUP BY vst_aut.car_id, vst_aut.car_nombre
  ORDER BY vst_aut.car_id, vst_aut.car_nombre, count(vst_aut.aut_cartaaval), sum(vst_aut.aut_montoa) asc";
		
		}else if ($cobtipo =='1' and $cbocarrera !='0' and $txtfechadesde !=NULL and $txtfechahasta !=NULL and $cbouniversidad !=NULL){
			
				
			$condi =" where siscam.vst_aut.car_id =$cbocarrera and siscam.vst_aut.uni_id =$cbouniversidad and siscam.vst_aut.aut_estatus ='AUT' and siscam.vst_aut.aut_fechaa >='$txtfechadesde' and siscam.vst_aut.aut_fechaa <='$txtfechahasta'";
			
			$consulta ="SELECT DISTINCT vst_aut.car_id, vst_aut.car_nombre, count(vst_aut.aut_cartaaval) AS conteo, sum(vst_aut.aut_montoa) AS montoaut
   FROM siscam.vst_aut $condi 
  GROUP BY vst_aut.car_id, vst_aut.car_nombre
  ORDER BY vst_aut.car_id, vst_aut.car_nombre, count(vst_aut.aut_cartaaval), sum(vst_aut.aut_montoa) asc";
		
		}else if ($cobtipo =='2' and $cbocarrera =='0' and $txtfechadesde !=NULL and $txtfechahasta !=NULL and $cbouniversidad =='0'){
			
			$condi =" where vst_aut.aut_estatus = 'REC' OR vst_aut.aut_estatus = 'PAGO' OR vst_aut.aut_estatus = 'FIN' and siscam.vst_aut.aut_fechaa >='$txtfechadesde' and siscam.vst_aut.aut_fechaa <='$txtfechahasta'";
			
			$consulta ="SELECT DISTINCT vst_aut.car_id, vst_aut.car_nombre, count(vst_aut.aut_cartaaval) AS conteo, sum(vst_aut.aut_montoa) AS montoaut
   FROM siscam.vst_aut $condi
  GROUP BY vst_aut.car_id, vst_aut.car_nombre
  ORDER BY vst_aut.car_id, vst_aut.car_nombre, count(vst_aut.aut_cartaaval), sum(vst_aut.aut_montoa) asc";
		
		}else if ($cobtipo =='2' and $cbocarrera !='0' and $txtfechadesde !=NULL and $txtfechahasta !=NULL and $cbouniversidad !=NULL){
			
				
			$condi =" where siscam.vst_aut.car_id =$cbocarrera and siscam.vst_aut.uni_id =$cbouniversidad and vst_aut.aut_estatus = 'REC' or vst_aut.aut_estatus ='PAGO' or vst_aut.aut_estatus = 'FIN' and siscam.vst_aut.aut_fechaa >='$txtfechadesde' and siscam.vst_aut.aut_fechaa <='$txtfechahasta'";
			
			$consulta ="SELECT DISTINCT vst_aut.car_id, vst_aut.car_nombre, count(vst_aut.aut_cartaaval) AS conteo, sum(vst_aut.aut_montoa) AS montoaut
   FROM siscam.vst_aut $condi 
  GROUP BY vst_aut.car_id, vst_aut.car_nombre
  ORDER BY vst_aut.car_id, vst_aut.car_nombre, count(vst_aut.aut_cartaaval), sum(vst_aut.aut_montoa) asc";
		
		}else if ($cobtipo =='2' and $cbocarrera !='0' and $txtfechadesde !=NULL and $txtfechahasta !=NULL and $cbouniversidad =='0'){
			
			$condi =" where siscam.vst_aut.car_id =$cbocarrera and vst_aut.aut_estatus = 'REC' or vst_aut.aut_estatus ='PAGO' or vst_aut.aut_estatus = 'FIN' and siscam.vst_aut.aut_fechaa >='$txtfechadesde' and siscam.vst_aut.aut_fechaa <='$txtfechahasta'";
			
			$consulta ="SELECT DISTINCT vst_aut.car_id, vst_aut.car_nombre, count(vst_aut.aut_cartaaval) AS conteo, sum(vst_aut.aut_montoa) AS montoaut
   FROM siscam.vst_aut $condi
  GROUP BY vst_aut.car_id, vst_aut.car_nombre
  ORDER BY vst_aut.car_id, vst_aut.car_nombre, count(vst_aut.aut_cartaaval), sum(vst_aut.aut_montoa) asc";
		
		}else if ($cobtipo =='3' and $cbocarrera =='0' and $txtfechadesde !=NULL and $txtfechahasta !=NULL and $cbouniversidad =='0'){
			
			$condi =" where siscam.vst_aut_fac_re.aut_estatus = 'PDO' and siscam.vst_aut_fac_re.aut_fechaa >='$txtfechadesde' and siscam.vst_aut_fac_re.aut_fechaa <='$txtfechahasta'";
			
			$consulta ="SELECT DISTINCT vst_aut_fac_re.car_id, vst_aut_fac_re.car_nombre, count(vst_aut_fac_re.aut_cartaaval) AS conteo, sum(vst_aut_fac_re.re_totalcan) AS montopag
   FROM siscam.vst_aut_fac_re $condi
  GROUP BY vst_aut_fac_re.car_id, vst_aut_fac_re.car_nombre
  ORDER BY vst_aut_fac_re.car_id, vst_aut_fac_re.car_nombre, count(vst_aut_fac_re.aut_cartaaval), sum(vst_aut_fac_re.re_totalcan) asc";
		
		}else if ($cobtipo =='3' and $cbocarrera !='0' and $txtfechadesde !=NULL and $txtfechahasta !=NULL and $cbouniversidad =='0'){
			
			$condi =" where siscam.vst_aut_fac_re.car_id =$cbocarrera and vst_aut_fac_re.aut_estatus = 'PDO' and siscam.vst_aut_fac_re.aut_fechaa >='$txtfechadesde' and siscam.vst_aut_fac_re.aut_fechaa <='$txtfechahasta'";
			
			$consulta ="SELECT DISTINCT vst_aut_fac_re.car_id, vst_aut_fac_re.car_nombre, count(vst_aut_fac_re.aut_cartaaval) AS conteo, sum(vst_aut_fac_re.re_totalcan) AS montopag
   FROM siscam.vst_aut_fac_re $condi
  GROUP BY vst_aut_fac_re.car_id, vst_aut_fac_re.car_nombre
  ORDER BY vst_aut_fac_re.car_id, vst_aut_fac_re.car_nombre, count(vst_aut_fac_re.aut_cartaaval), sum(vst_aut_fac_re.re_totalcan) asc";
		}else if ($cobtipo =='3' and $cbocarrera !='0' and $txtfechadesde !=NULL and $txtfechahasta !=NULL and $cbouniversidad !=NULL){
			
			$condi =" where siscam.vst_aut_fac_re.car_id =$cbocarrera and siscam.vst_aut_fac_re.uni_id =$cbouniversidad and vst_aut_fac_re.aut_estatus = 'PDO' and siscam.vst_aut_fac_re.aut_fechaa >='$txtfechadesde' and siscam.vst_aut_fac_re.aut_fechaa <='$txtfechahasta'";
			
			$consulta ="SELECT DISTINCT vst_aut_fac_re.car_id, vst_aut_fac_re.car_nombre, count(vst_aut_fac_re.aut_cartaaval) AS conteo, sum(vst_aut_fac_re.re_totalcan) AS montopag
   FROM siscam.vst_aut_fac_re $condi
  GROUP BY vst_aut_fac_re.car_id, vst_aut_fac_re.car_nombre
  ORDER BY vst_aut_fac_re.car_id, vst_aut_fac_re.car_nombre, count(vst_aut_fac_re.aut_cartaaval), sum(vst_aut_fac_re.re_totalcan) asc";
		}
		
		$query = pg_query($consulta);
		while($rowrs = pg_fetch_object($query)) { 
			$this->Cell(93,5,utf8_decode(substr($rowrs->car_nombre,0,35)),0);
			$this->Cell(33,5,utf8_decode($rowrs->conteo),0);
			if ($cobtipo =='1' or $cobtipo =='2'){
					$this->Cell(33,5,number_format($rowrs->montoaut,2,',','.'),0);
			} elseif ($cobtipo =='3'){
					$this->Cell(33,5,number_format($rowrs->montopag,2,',','.'),0);
			}
			
			
			
			$this->Ln();
			$c = $c + $rowrs->conteo;
			if ($cobtipo =='1' or $cobtipo =='2'){
					$montotal = $montotal + $rowrs->montoaut;
			} elseif ($cobtipo =='3'){
					$montotal = $montotal + $rowrs->montopag;
			}
			
		}
		
		//muestra totales del reporte
		//$consulta2 = "select * from siscam.relacion where siscam.relacion.re_id like '$re'";
		//$query2 = pg_query($consulta2);
		//$row2 = pg_fetch_object($query2);
		$this->Ln(4);
		$this->SetX(130);
		$this->Cell(40,5,'TOTAL CASOS: ','LT',0);
		$this->SetX(170);
		$this->Cell(25,5,number_format($c,0,',','.'),'TR',0);
		$this->Ln();
		$this->SetX(130);
		$this->Cell(40,5,'TOTAL GENERAL: ','LB',0);
		$this->SetX(170);
		$this->Cell(25,5,number_format($montotal,2,',','.'),'BR',0);
		//$this->Ln();
		/*$this->SetX(130);
		$this->Cell(30,5,'IMPUESTO DEL 1*1000: ','L',0);
		$this->SetX(170);
		$this->Cell(25,5,number_format($row2->re_riam,2,',','.'),'R',0);
		$this->Ln();
		$this->SetX(130);
		$this->Cell(40,5,'TOTAL A CANCELAR: ','LB',0);
		$this->SetX(170);
		$this->Cell(25,5,number_format($row2->re_totalcan,2,',','.'),'BR',0);*/
		
		//////////////////////////////////////////////////////////
		//CODIGO EXTRA PARA GENERAR VARIAS PAGINAS DE PRUEBA    //
		//for($i=1;$i<=40;$i++)                                 //
		//$this->Cell(0,10,'Imprimiendo Linea de Prueba Numero '.$i,0,1); //
		//$this->Ln();                                          //
		//////////////////////////////////////////////////////////
		
		//Línea de cierre
		//$this->Cell(array_sum($w),0,'',0);
	}
	}

//Creación del objeto de la clase heredada
$pdf=new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();

//llamada a la funcion de crear tabla y fecha
$pdf->CrearTabla($cobtipo,$cbocarrera,$cbouniversidad,$txtfechadesde,$txtfechahasta);
$pdf->Output();

?>
