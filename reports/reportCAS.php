<?php
	require('../reports/fpdf16/fpdf.php');

	//Conexion al servidor
	$servidor = "localhost";
	$base = "intranet_dev";
	$usuario = "postgres";
	$password = "$2011tyspostgresql%";   
	pg_pconnect("host=$servidor dbname=$base user=$usuario password=$password");
	
	
	//Referencia
	$cbostatus = strtoupper(@$_POST["cboestatus"]);
	$cbomodalidad = strtoupper(@$_POST["cbomodalidad"]);
	$cbouniversidad = strtoupper(@$_POST["cbouniversidad"]);
	$cboestado = strtoupper(@$_POST["cboestado"]);
	$txtfechadesde = strtoupper(@$_POST["txtfechadesde"]);
	$txtfechahasta = strtoupper(@$_POST["txtfechahasta"]);
	
class PDF extends FPDF
{


	//Cabecera de página
	function Header()
	{		
		//fecha
		$this->SetY(22);
		$this->SetFont('Arial','I',8);
		$this->Cell(0,8,'Fecha: '.date('d/m/Y h:i A'),0,0,'L');
		//Logo		
		$this->Image('../imagenes/encabezado_gris3.jpg',10,8,273,14);
		//Fuente
		$this->SetFont('Arial','B',13);
		//Movernos a la derecha
		$this->SetY(20);
		$this->SetX(130);
		//Título		
		$this->Cell(35,25,utf8_decode('Reporte de Cartas Avales'),0,0,'C');
		
		//Referencia
		$cbostatus = strtoupper(@$_POST["cboestatus"]);
		$cbomodalidad = strtoupper(@$_POST["cbomodalidad"]);
	
		//Subtítulos		
		$this->SetFont('Times','B',10);
		$this->SetY(30);
		$this->SetX(80);
		if ($cbostatus !='0' and $cbomodalidad !='0'){		 
			$this->Cell(30,25,utf8_decode('CASOS CON ESTATUS:  '.$cbostatus.' Y MODALIDAD: '.$cbomodalidad),0);
			$this->Ln();
		}elseif ($cbostatus !='0' and $cbomodalidad =='0'){
			$this->Cell(30,25,utf8_decode('CASOS CON ESTATUS:  '.$cbostatus),0);
			$this->Ln();
		}
		
		//Subtítulos
		$this->SetY(51);
		$this->SetFont('Times','B',9);
		$this->Cell(18,5,utf8_decode('Carta Aval'),0);
        $this->Cell(18,5,utf8_decode('Nº Control'),0);
		$this->Cell(13,5,utf8_decode('Cédula'),0);
        $this->Cell(50,5,utf8_decode('Nombre y Apellido'),0);
		$this->Cell(22,5,utf8_decode('Modalidad'),0);
		$this->Cell(22,5,utf8_decode('Telefono'),0);
		$this->Cell(35,5,utf8_decode('Fecha Autorizacion'),0);
		$this->Cell(22,5,utf8_decode('Estatus'),0);
		$this->Cell(26,5,utf8_decode('Monto Autorizado'),0);
		$this->Cell(50,5,utf8_decode('Observación'),0);
		$this->Line(10,56,280,56);
		$this->Ln();
	}

	//Numero de Página
	function Footer()
	{
		//Posición: a la derecha
		$this->SetY(21);
		//Arial italic 8
		$this->SetFont('Arial','I',8);
		//Número de página
		$this->Cell(0,10,utf8_decode('Página '.$this->PageNo().'/{nb}'),0,0,'R');
	}
	
	//Tabla del Reporte
	function CrearTabla($cbomodalidad,$cbostatus,$cbouniversidad,$cboestado,$txtfechadesde,$txtfechahasta)
	{					
		//Anchuras de las columnas
		$this->SetY(49);
		$this->SetFont('Times','',9);
		$w=array(18,18,13,50,22,35,22,24,50);
		$this->Ln(8);	
		
		//Datos	
		//filtro según el estatus
		if ($cboestatus!=NULL or $cbomodalidad!=NULL or $txtfechadesde!=NULL or $txtfechahasta!=NULL){	
					
				//filtro según el estatus
				if (($cbostatus!='0') and ($cbostatus!=NULL)){
					$condi = $condi." where siscam.vst_aut.aut_estatus ='$cbostatus'";
				}
				//********************************************************
				//filtro según la Modalidad
				if (($cbomodalidad!='0') and ($cbomodalidad!=NULL)){
					$condi = $condi." and siscam.vst_aut.aut_modalidad ='$cbomodalidad'";
				}
				//********************************************************
				//filtro según las fechas
				if (($txtfechadesde!='0') and ($txtfechadesde!=NULL) and ($txtfechahasta!='0') and ($txtfechahasta!=NULL)){
					$condi = $condi." and siscam.vst_aut.aut_fechaa >='$txtfechadesde' and siscam.vst_aut.aut_fechaa <='$txtfechahasta'";
				}
				//********************************************************
				
			$consulta ="select * from siscam.vst_aut $condi order by siscam.vst_aut.aut_cartaaval asc";
			
		}elseif ($cboestatus!=NULL or $cbomodalidad!=NULL or $txtfechadesde!=NULL or $txtfechahasta!=NULL and $cbouniversidad !=NULL and $cboestado !=NULL){	
					
				//filtro según el estatus
				if (($cbostatus!='0') and ($cbostatus!=NULL)){
					$condi = $condi." where siscam.vst_aut.aut_estatus ='$cbostatus'";
				}
				//********************************************************
				//filtro según la Modalidad
				if (($cbomodalidad!='0') and ($cbomodalidad!=NULL)){
					$condi = $condi." and siscam.vst_aut.aut_modalidad ='$cbomodalidad'";
				}
				//********************************************************
				//filtro según las fechas
				if (($txtfechadesde!='0') and ($txtfechadesde!=NULL) and ($txtfechahasta!='0') and ($txtfechahasta!=NULL)){
					$condi = $condi." and siscam.vst_aut.aut_fechaa >='$txtfechadesde' and siscam.vst_aut.aut_fechaa <='$txtfechahasta' and vst_aut.uni_id =$cbouniversidad and vst_aut.esta_id =$cboestado";
				}
				//********************************************************
				
			$consulta ="select * from siscam.vst_aut $condi order by siscam.vst_aut.aut_cartaaval asc";
		}
		$queryest2 = pg_query($consulta);
		while($rowrs = pg_fetch_object($queryest2)) { 
			$this->Cell($w[0],5,utf8_decode($rowrs->aut_cartaaval),0);
			$this->Cell($w[1],5,utf8_decode($rowrs->aut_ncontrol),0);
			$this->Cell($w[2],5,utf8_decode($rowrs->est_id),0);
			$this->Cell($w[3],5,utf8_decode(substr($rowrs->est_pnombre.' '.$rowrs->est_papellido,0,25)),0);
			$this->Cell($w[4],5,utf8_decode(substr($rowrs->aut_modalidad,0,28)),0);
			$this->Cell($w[4],5,utf8_decode(substr($rowrs->est_tlfcel,0,28)),0);
			$this->Cell($w[5],5,utf8_decode(CamFormFech($rowrs->aut_fechaa)),0);
			$this->Cell($w[6],5,utf8_decode($rowrs->aut_estatus),0);
			$this->Cell($w[7],5,number_format($rowrs->aut_montoa,2,',','.'),0);
			$this->Cell($w[4],5,utf8_decode(substr($rowrs->aut_observacion,0,28)),0);
			$this->Ln();
			$c = $c +1;
			$montotal = $montotal + $rowrs->aut_montoa;
		}
		
		//muestra totales del reporte
		$consulta2 = "select * from siscam.relacion where siscam.relacion.re_id like '$re'";
		$query2 = pg_query($consulta2);
		$row2 = pg_fetch_object($query2);
		$this->Ln(4);
		$this->SetX(225);
		$this->Cell(40,5,'TOTAL CASOS: ','LT',0);
		$this->SetX(255);
		$this->Cell(25,5,number_format($c,0,',','.'),'TR',0);
		$this->Ln();
		$this->SetX(225);
		$this->Cell(40,5,'TOTAL GENERAL: ','LB',0);
		$this->SetX(255);
		$this->Cell(25,5,number_format($montotal,2,',','.'),'BR',0);
	}
}

//Creación del objeto de la clase heredada
$pdf=new PDF();
$pdf->AliasNbPages();
$pdf->AddPage('l');

//llamada a la funcion de crear tabla y fecha
$pdf->CrearTabla($cbomodalidad,$cbostatus,$cbouniversidad,$cboestado,$txtfechadesde,$txtfechahasta);
$pdf->Output();


function CamFormFech($contenido)
{
 		if($contenido!=null)
		{
 			$fech=explode('-', $contenido,3);
 			$fech2=explode(' ', $fech[2],3);
 				if(count($fech2) == 2)
				{
				return $fech2[0]."/".$fech[1]."/".$fech[0]." ".$fech2[1];
 				}
				else
				{
 				return $fech2[0]."/".$fech[1]."/".$fech[0];
 				}
 		}
 }

?>
