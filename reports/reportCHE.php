<?php
require('../reports/fpdf16/fpdf.php');

	//Conexion al servidor
	$servidor = "190.190.1.19";
	$base = "intranet_dev";
	$usuario = "postgres";
	$password = "$2011tyspostgresql%";   
	pg_pconnect("host=$servidor dbname=$base user=$usuario password=$password");
	
 	//Referencia
	$txtfechacheqdesde = strtoupper(@$_POST["txtfechacheqdesde"]);
	$txtfechacheqhasta = strtoupper(@$_POST["txtfechacheqhasta"]);
	
class PDF extends FPDF
{
	//Cabecera de página
	function Header()
	{		
		//fecha
		$this->SetY(20);
		$this->SetFont('Arial','I',10);
		$this->Cell(0,8,'Fecha: '.date('d/m/Y h:i A'),0,0,'L');
		//Logo
		$this->Image('../imagenes/encabezado_gris.jpg',10,8,190,12);
		//Fuente
		$this->SetFont('Arial','B',15);
		//Movernos a la derecha
		$this->SetY(16);
		$this->SetX(96);
		//Título		
		$this->Cell(30,25,utf8_decode('Reporte de Relaciones Actualizadas'),0,0,'C');
		
		//Referencia
		$txtfechacheqdesde = strtoupper(@$_POST["txtfechacheqdesde"]);
		$txtfechacheqhasta = strtoupper(@$_POST["txtfechacheqhasta"]);		
		
		$consultarfec = "select * from siscam.vst_aut_fac_re_fin where siscam.vst_aut_fac_re_fin.re_fecha >= '$txtfechacheqdesde' and siscam.vst_aut_fac_re_fin.re_fecha <= '$txtfechacheqhasta' and siscam.vst_aut_fac_re_fin.re_nrocheq <> 0";
		$queryfec = pg_query($consultarfec);
		$rowr = pg_fetch_object($queryfec); 
		$this->SetFont('Times','B',10);
		$this->SetY(26);
		$this->SetX(78);
		$this->Cell(30,25,utf8_decode('DESDE:  '.$txtfechacheqdesde.' HASTA: '.$txtfechacheqhasta),0);
		$this->Ln();
		
		//Subtítulos
		$this->SetY(51);
		$this->SetFont('Times','B',9);
		$this->Cell(22,5,utf8_decode('Relación'),0);
        $this->Cell(22,5,utf8_decode('Rif Clinica'),0);
		$this->Cell(72,5,utf8_decode('Clinica Nombre'),0);
        $this->Cell(26,5,utf8_decode('Total Cancelar'),0);
		$this->Cell(25,5,utf8_decode('Fecha de Pago'),0);
		$this->Cell(22,5,utf8_decode('Nro. Cheque'),0);
		$this->Line(10,56,195,56);
		$this->Ln();
	}

	//Numero de Página
	function Footer()
	{
		//Posición: a la derecha
		$this->SetY(19);
		//Arial italic 8
		$this->SetFont('Arial','I',10);
		//Número de página
		$this->Cell(0,10,utf8_decode('Página '.$this->PageNo().'/{nb}'),0,0,'R');
	}
	
	//Tabla del Reporte
	function CrearTabla($txtfechacheqdesde,$txtfechacheqhasta)
	{				
		//Anchuras de las columnas
		$this->SetY(49);
		$this->SetFont('Times','',9);
		$w=array(22,22,22,22,55,25);
		$this->Ln(8);
		
		//Datos		
		$consultarfec = "select distinct(siscam.vst_aut_fac_re_fin.re_id), siscam.vst_aut_fac_re_fin.cli_rif, siscam.vst_aut_fac_re_fin.cli_nombre, siscam.vst_aut_fac_re_fin.re_totalcan, siscam.vst_aut_fac_re_fin.re_fecha, siscam.vst_aut_fac_re_fin.re_nrocheq, siscam.vst_aut_fac_re_fin.re_fechapagocheq from siscam.vst_aut_fac_re_fin where siscam.vst_aut_fac_re_fin.re_fecha >= '$txtfechacheqdesde' and siscam.vst_aut_fac_re_fin.re_fecha <= '$txtfechacheqhasta' and siscam.vst_aut_fac_re_fin.re_nrocheq <> 0";
		$queryfec = pg_query($consultarfec);
		while($rowr = pg_fetch_object($queryfec)) {
			$contador++;
			$this->Cell(22,5,utf8_decode($rowr->re_id),0);
			$this->Cell(22,5,utf8_decode($rowr->cli_rif),0);
			$this->Cell(72,5,utf8_decode(substr($rowr->cli_nombre,0,36)),0);
			$this->Cell(26,5,utf8_decode($rowr->re_totalcan),0);
			$this->Cell(25,5,utf8_decode(CamFormFech($rowr->re_fechapagocheq)),0);
			$this->Cell(22,5,utf8_decode($rowr->re_nrocheq),0);
			$this->Ln();
			$y = $y + 5;
			if($contador == 25) { $this->AddPage(); $y = 39; $x=12; $s=0; $contador=0; }
			}
			
		//muestra totales del reporte
		$consulta2 = "select sum(re_totalcan) as total, count(re_id) as conteo from siscam.relacion where siscam.relacion.re_fecha >= '$txtfechacheqdesde' and siscam.relacion.re_fecha <= '$txtfechacheqhasta' and siscam.relacion.re_nrocheq <> 0";
		$query2 = pg_query($consulta2);
		$row2 = pg_fetch_object($query2);
		$this->Ln(4);
		$this->SetY(240);
		$this->SetX(110);
		$this->Cell(60,5,'TOTAL RELACIONES GENERADAS: ','LT',0);
		$this->SetX(170);
		$this->Cell(25,5,number_format($row2->conteo,0,',','.'),'TR',0);
		$this->Ln();
		$this->SetX(110);
		$this->Cell(60,5,'TOTAL: ','LB',0);
		$this->SetX(170);
		$this->Cell(25,5,number_format($row2->total,2,',','.'),'BR',0);
		$this->Ln();
	}
}

function CamFormFech($contenido) {
	if($contenido!=null) {
		$fech=explode('-', $contenido,3);
		$fech2=explode(' ', $fech[2],3);
		if(count($fech2) == 2) {
			return $fech2[0]."/".$fech[1]."/".$fech[0]." ".$fech2[1];
		} else {
			return $fech2[0]."/".$fech[1]."/".$fech[0];
		}
	}
}

//Creación del objeto de la clase heredada
$pdf=new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();

//llamada a la funcion de crear tabla y fecha
$pdf->CrearTabla($txtfechacheqdesde,$txtfechacheqhasta);
$pdf->Output();




?>
