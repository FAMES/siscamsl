<?php
require('../reports/fpdf16/fpdf.php');

	//Conexion al servidor
	$servidor = "190.190.1.19";
	$base = "intranet_dev";
	$usuario = "postgres";
	$password = "$2011tyspostgresql%";   
	pg_pconnect("host=$servidor dbname=$base user=$usuario password=$password");
	
	
//Referencia

	$tipo = strtoupper(@$_POST["cobtipo"]);
    $cboclinica = strtoupper(@$_POST["cboclinica"]);
	$cboestado = strtoupper(@$_POST["cboestado"]);
	$txtfechadesde = strtoupper(@$_POST["txtfechadesde"]);
	$txtfechahasta = strtoupper(@$_POST["txtfechahasta"]);
    
	
class PDF extends FPDF
{


	//Cabecera de página
	function Header()
	{		
		//fecha
		$this->SetY(20);
		$this->SetFont('Arial','I',10);
		$this->Cell(0,8,'Fecha: '.date('d/m/Y h:i A'),0,0,'L');
		//Logo
		$this->Image('../imagenes/encabezado_gris.jpg',13,8,180,12);
		//Fuente
		$this->SetFont('Arial','B',15);
		//Movernos a la derecha
		$this->SetY(16);
		$this->SetX(96);
		//Título		
		$this->Cell(30,25,utf8_decode('Reportes por Clinicas'),0,0,'C');
		

	}

	//Numero de Página
	function Footer()
	{
		//Posición: a la derecha
		$this->SetY(19);
		//Arial italic 8
		$this->SetFont('Arial','I',10);
		//Número de página
		$this->Cell(0,10,utf8_decode('Página '.$this->PageNo().'/{nb}'),0,0,'R');
	}
	
	//Tabla del Reporte
	function CrearTabla($tipo,$cboclinica,$cboestado,$txtfechadesde,$txtfechahasta)
	{		
	
		$this->SetFont('Times','B',10);
		$this->SetY(26);
		$this->SetX(58);
		if ($tipo =='1'){
		 	$titulo1 = 'Autorizados';
		}
		
		if($tipo =='2'){
			$titulo1 = 'Atendidos';
		}
		if($tipo =='3'){
			$titulo1 = 'Pagados';
		}
		
		
		 
		$this->Cell(30,25,utf8_decode(' Casos '.$titulo1.' Desde '.CamFormFech($txtfechadesde).' Hasta '.CamFormFech($txtfechahasta)),0);
		$this->Ln();
		
		//Anchuras de las columnas
		$this->SetY(49);
		$this->SetFont('Times','',9);
		$w=array(22,22,22,22,55,25);
		$this->Ln(8);
		//Subtítulos
		$this->SetY(51);
		$this->SetFont('Times','B',9);
		$this->Cell(29,5,utf8_decode('RIF Clinica'),0);
        $this->Cell(83,5,utf8_decode('Nombre Clinica'),0);
		if($tipo =='1' or $tipo =='2'){
				$this->Cell(33,5,utf8_decode('Total Autorizado'),0);
				} else if($tipo =='3'){
				$this->Cell(33,5,utf8_decode('Total Pagado'),0);
		}
		$this->Cell(23,5,utf8_decode('Total casos'),0);
		$this->Line(10,56,195,56);
		$this->Ln();
		$y = 58;
		$x = 11;
		
		if ($tipo =='1' and $cboclinica =='0' and $txtfechadesde !='0' and $txtfechahasta !='0' and $cboestado =='0'){
								
			$condi =" where siscam.vst_aut.aut_estatus ='AUT' and siscam.vst_aut.aut_fechaa >= '$txtfechadesde' and siscam.vst_aut.aut_fechaa <= '$txtfechahasta'";
			
			$consulta ="SELECT DISTINCT vst_aut.cli_rif, vst_aut.cli_nombre, sum(vst_aut.aut_montoa) AS totalaut, count(vst_aut.aut_cartaaval) AS totalcasos
   FROM siscam.vst_aut $condi GROUP BY vst_aut.cli_rif, vst_aut.cli_nombre
  ORDER BY vst_aut.cli_rif, vst_aut.cli_nombre, sum(vst_aut.aut_montoa), count(vst_aut.aut_cartaaval)";
		
		} else if ($tipo =='1' and $cboclinica !='0' and $txtfechadesde !='0' and $txtfechahasta !='0' and $cboestado =='0'){
						
			$condi =" where siscam.vst_aut.cli_rif ='$cboclinica' and siscam.vst_aut.aut_estatus ='AUT' and siscam.vst_aut.aut_fechaa >= '$txtfechadesde' and siscam.vst_aut.aut_fechaa <= '$txtfechahasta'";
			
			$consulta ="SELECT DISTINCT vst_aut.cli_rif, vst_aut.cli_nombre, sum(vst_aut.aut_montoa) AS totalaut, count(vst_aut.aut_cartaaval) AS totalcasos
   FROM siscam.vst_aut $condi GROUP BY vst_aut.cli_rif, vst_aut.cli_nombre
  ORDER BY vst_aut.cli_rif, vst_aut.cli_nombre, sum(vst_aut.aut_montoa), count(vst_aut.aut_cartaaval)";
		
		}else if ($tipo =='1' and $cboclinica =='0' and $txtfechadesde !='0' and $txtfechahasta !='0' and $cboestado !='0'){
						
			$condi =" where siscam.vst_aut.aut_estatus ='AUT' and siscam.vst_aut.esta_id =$cboestado and siscam.vst_aut.aut_fechaa >= '$txtfechadesde' and siscam.vst_aut.aut_fechaa <= '$txtfechahasta'";
			
			$consulta ="SELECT DISTINCT vst_aut.cli_rif, vst_aut.cli_nombre, sum(vst_aut.aut_montoa) AS totalaut, count(vst_aut.aut_cartaaval) AS totalcasos
   FROM siscam.vst_aut $condi GROUP BY vst_aut.cli_rif, vst_aut.cli_nombre
  ORDER BY vst_aut.cli_rif, vst_aut.cli_nombre, sum(vst_aut.aut_montoa), count(vst_aut.aut_cartaaval)";
		
		}else if ($tipo =='2' and $cboclinica =='0' and $txtfechadesde !='0' and $txtfechahasta !='0' and $cboestado =='0'){
			
			$condi =" where siscam.vst_aut.aut_estatus <>'AUT' and siscam.vst_aut.aut_estatus <>'PDO' and siscam.vst_aut.aut_fechaa >= '$txtfechadesde' and siscam.vst_aut.aut_fechaa <= '$txtfechahasta'";
			
			$consulta ="SELECT DISTINCT vst_aut.cli_rif, vst_aut.cli_nombre, sum(vst_aut.aut_montoa) AS totalaut, count(vst_aut.aut_cartaaval) AS totalcasos
   FROM siscam.vst_aut $condi GROUP BY vst_aut.cli_rif, vst_aut.cli_nombre
  ORDER BY vst_aut.cli_rif, vst_aut.cli_nombre, sum(vst_aut.aut_montoa), count(vst_aut.aut_cartaaval)";
		
		}else if ($tipo =='2' and $cboclinica !='0' and $txtfechadesde !='0' and $txtfechahasta !='0' and $cboestado =='0'){
			
			$condi =" where siscam.vst_aut.cli_rif ='$cboclinica' and siscam.vst_aut.aut_estatus <>'AUT' and siscam.vst_aut.aut_estatus <>'PDO' and siscam.vst_aut.aut_estatus <>'PDOPAR' and siscam.vst_aut.aut_fechaa >= '$txtfechadesde' and siscam.vst_aut.aut_fechaa <= '$txtfechahasta'";
			
			$consulta ="SELECT DISTINCT vst_aut.cli_rif, vst_aut.cli_nombre, sum(vst_aut.aut_montoa) AS totalaut, count(vst_aut.aut_cartaaval) AS totalcasos
   FROM siscam.vst_aut $condi GROUP BY vst_aut.cli_rif, vst_aut.cli_nombre
  ORDER BY vst_aut.cli_rif, vst_aut.cli_nombre, sum(vst_aut.aut_montoa), count(vst_aut.aut_cartaaval)";
		
		}else if ($tipo =='2' and $cboclinica =='0' and $txtfechadesde !='0' and $txtfechahasta !='0' and $cboestado !='0'){
			
			$condi =" where siscam.vst_aut.aut_estatus <>'AUT' and siscam.vst_aut.esta_id =$cboestado and siscam.vst_aut.aut_estatus <>'PDO' and siscam.vst_aut.aut_estatus <>'PDOPAR' and siscam.vst_aut.aut_estatus <>'UMED' and siscam.vst_aut.aut_estatus <>'TRAMITE' and siscam.vst_aut.aut_estatus <>'ANULADO' and siscam.vst_aut.aut_fechaa >= '$txtfechadesde' and siscam.vst_aut.aut_fechaa <= '$txtfechahasta'";
			
			$consulta ="SELECT DISTINCT vst_aut.cli_rif, vst_aut.cli_nombre, sum(vst_aut.aut_montoa) AS totalaut, count(vst_aut.aut_cartaaval) AS totalcasos
   FROM siscam.vst_aut $condi GROUP BY vst_aut.cli_rif, vst_aut.cli_nombre
  ORDER BY vst_aut.cli_rif, vst_aut.cli_nombre, sum(vst_aut.aut_montoa), count(vst_aut.aut_cartaaval)";
		
		}else if ($tipo =='3' and $cboclinica =='0' and $txtfechadesde !='0' and $txtfechahasta !='0' and $cboestado =='0'){
			
			$condi =" where siscam.vst_aut_re_cli_car.aut_fechaa >= '$txtfechadesde' and siscam.vst_aut_re_cli_car.aut_fechaa <= '$txtfechahasta' and siscam.vst_aut_re_cli_car.aut_estatus='PDO'";
			
			$consulta ="SELECT DISTINCT vst_aut_re_cli_car.cli_rif, vst_aut_re_cli_car.cli_nombre, sum(vst_aut_re_cli_car.aut_montoa) AS totalfac, count(vst_aut_re_cli_car.aut_cartaaval) AS totalcasos
   FROM siscam.vst_aut_re_cli_car $condi GROUP BY vst_aut_re_cli_car.cli_rif, vst_aut_re_cli_car.cli_nombre
  ORDER BY vst_aut_re_cli_car.cli_rif, vst_aut_re_cli_car.cli_nombre, sum(vst_aut_re_cli_car.aut_montoa), count(vst_aut_re_cli_car.aut_cartaaval)";
		
		}else if ($tipo =='3' and $cboclinica !='0' and $txtfechadesde !='0' and $txtfechahasta !='0' and $cboestado =='0'){
			
			$condi =" where siscam.vst_aut_re_cli_car.aut_fechaa >= '$txtfechadesde' and siscam.vst_aut_re_cli_car.aut_fechaa <= '$txtfechahasta' and siscam.vst_aut_re_cli_car.aut_estatus='PDO'";
			
			$consulta ="SELECT DISTINCT vst_aut_re_cli_car.cli_rif, vst_aut_re_cli_car.cli_nombre, sum(vst_aut_re_cli_car.aut_montoa) AS totalfac, count(vst_aut_re_cli_car.aut_cartaaval) AS totalcasos
   FROM siscam.vst_aut_re_cli_car $condi GROUP BY vst_aut_re_cli_car.cli_rif, vst_aut_re_cli_car.cli_nombre
  ORDER BY vst_aut_re_cli_car.cli_rif, vst_aut_re_cli_car.cli_nombre, sum(vst_aut_re_cli_car.aut_montoa), count(vst_aut_re_cli_car.aut_cartaaval)";
		
		}else if ($tipo =='3' and $cboclinica =='0' and $txtfechadesde !='0' and $txtfechahasta !='0' and $cboestado !='0'){
			
			$condi =" where siscam.vst_aut_re_cli_car.esta_id =$cboestado and siscam.vst_aut_re_cli_car.aut_fechaa >= '$txtfechadesde' and siscam.vst_aut_re_cli_car.aut_fechaa <= '$txtfechahasta' and siscam.vst_aut_re_cli_car.aut_estatus='PDO'";
			
			$consulta ="SELECT DISTINCT vst_aut_re_cli_car.cli_rif, vst_aut_re_cli_car.cli_nombre, sum(vst_aut_re_cli_car.aut_montoa) AS totalfac, count(vst_aut_re_cli_car.aut_cartaaval) AS totalcasos
   FROM siscam.vst_aut_re_cli_car $condi GROUP BY vst_aut_re_cli_car.cli_rif, vst_aut_re_cli_car.cli_nombre
  ORDER BY vst_aut_re_cli_car.cli_rif, vst_aut_re_cli_car.cli_nombre, sum(vst_aut_re_cli_car.aut_montoa), count(vst_aut_re_cli_car.aut_cartaaval)";
		}
		
		
		
		$query = pg_query($consulta);
		$montotal =0;
		while($rowrs = pg_fetch_object($query)) { 
		$contador++;
		$this->SetY($y);
		$this->SetX($x);
			$this->Cell(29,5,utf8_decode($rowrs->cli_rif),0);
			$this->Cell(83,5,utf8_decode(substr($rowrs->cli_nombre,0,35)),0);
			if ($tipo =='1' or $tipo =='2'){
					$this->Cell(33,5,number_format($rowrs->totalaut,2,',','.'),0);
			} else if ($tipo =='3'){
					$this->Cell(33,5,number_format($rowrs->totalfac,2,',','.'),0);
			}
			
			$this->Cell(23,5,utf8_decode($rowrs->totalcasos),0);
			$this->Ln();
			
			$c = $c + $rowrs->totalcasos;
			if ($tipo =='1' or $tipo =='2'){
					$montotal = $montotal + $rowrs->totalaut;
			} else if ($tipo =='3'){
					$montotal = $montotal + $rowrs->totalfac;
			}
			
			$y = $y + 5;
			if($contador == 40) { $this->AddPage(); $y = 38; $x=11; $s=0; $contador=0; }
			
		}
		
		//muestra totales del reporte
		//$consulta2 = "select * from siscam.relacion where siscam.relacion.re_id like '$re'";
		//$query2 = pg_query($consulta2);
		//$row2 = pg_fetch_object($query2);
		$this->Ln(4);
		$this->SetX(130);
		$this->Cell(40,5,'TOTAL CASOS: ','LT',0);
		$this->SetX(170);
		$this->Cell(25,5,number_format($c,0,',','.'),'TR',0);
		$this->Ln();
		$this->SetX(130);
		$this->Cell(40,5,'TOTAL GENERAL: ','LB',0);
		$this->SetX(170);
		$this->Cell(25,5,number_format($montotal,2,',','.'),'BR',0);
		//$this->Ln();
		/*$this->SetX(130);
		$this->Cell(30,5,'IMPUESTO DEL 1*1000: ','L',0);
		$this->SetX(170);
		$this->Cell(25,5,number_format($row2->re_riam,2,',','.'),'R',0);
		$this->Ln();
		$this->SetX(130);
		$this->Cell(40,5,'TOTAL A CANCELAR: ','LB',0);
		$this->SetX(170);
		$this->Cell(25,5,number_format($row2->re_totalcan,2,',','.'),'BR',0);*/
		
		//////////////////////////////////////////////////////////
		//CODIGO EXTRA PARA GENERAR VARIAS PAGINAS DE PRUEBA    //
		//for($i=1;$i<=40;$i++)                                 //
		//$this->Cell(0,10,'Imprimiendo Linea de Prueba Numero '.$i,0,1); //
		//$this->Ln();                                          //
		//////////////////////////////////////////////////////////
		
		//Línea de cierre
		//$this->Cell(array_sum($w),0,'',0);
	}
	 }

//Creación del objeto de la clase heredada
$pdf=new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();

//llamada a la funcion de crear tabla y fecha
$pdf->CrearTabla($tipo,$cboclinica,$cboestado,$txtfechadesde,$txtfechahasta);
$pdf->Output();



function CamFormFech($contenido)
{
 		if($contenido!=null)
		{
 			$fech=explode('-', $contenido,3);
 			$fech2=explode(' ', $fech[2],3);
 				if(count($fech2) == 2)
				{
				return $fech2[0]."/".$fech[1]."/".$fech[0]." ".$fech2[1];
 				}
				else
				{
 				return $fech2[0]."/".$fech[1]."/".$fech[0];
 				}
 		}
 }

?>
