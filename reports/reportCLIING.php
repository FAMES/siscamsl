<?php
require('../reports/fpdf16/fpdf.php');

	//Conexion al servidor
	$servidor = "localhost";
	$base = "intranet_dev";
	$usuario = "postgres";
	$password = "$2011tyspostgresql%";   
	pg_pconnect("host=$servidor dbname=$base user=$usuario password=$password");
	
	
//Referencia

	 $cboana = strtoupper(@$_POST["cboana"]);
	$txtfecha = @$_POST["txtfecha"];
	$txtfechahasta = @$_POST["txtfechahasta"];
	
class PDF extends FPDF
{


	//Cabecera de página
	function Header()
	{		
		//fecha
		$this->SetY(24);
		$this->SetFont('Arial','I',10);
		$this->Cell(0,8,'Fecha: '.date('d/m/Y h:i A'),0,0,'L');
		//Logo
		$this->Image('../imagenes/encabezado_gris.jpg',13,8,270,16);
		//Fuente
		$this->SetFont('Arial','B',15);
		//Movernos a la derecha
		$this->SetY(16);
		$this->SetX(100);
		//Título		
		$this->Cell(100,25,utf8_decode(' Clinicas Creadas por los Analistas Integrales'),0,0,'C');
		

	}

	//Numero de Página
	function Footer()
	{
		//Posición: a la derecha
		$this->SetY(19);
		//Arial italic 8
		$this->SetFont('Arial','I',10);
		//Número de página
		$this->Cell(0,10,utf8_decode('Página '.$this->PageNo().'/{nb}'),0,0,'R');
	}
	
	//Tabla del Reporte
	function CrearTabla($cboana,$txtfecha,$txtfechahasta)
	{		
	
		$this->SetFont('Times','B',12);
		$this->SetY(36);
		$this->SetX(120);
		if($txtfecha !=NULL or $txtfecha !=''){
			$this->Cell(100,6,utf8_decode(' Clinicas Codificados el '.CamFormFech($txtfecha)),0);
		}else{
			$this->Cell(100,6,utf8_decode(' Lista de Clinicas Codificados'),0);
		}
			
		$this->Ln();
		
		//Anchuras de las columnas
		$this->SetY(49);
		$this->SetFont('Times','',9);
		$w=array(22,22,22,22,55,25);
		$this->Ln(8);
		//Subtítulos
		$this->SetY(51);
		$this->SetFont('Arial','B',9);
		$this->Cell(25,5,utf8_decode('Rif'),0);
		$this->Cell(80,5,utf8_decode('Clinica'),0);
		$this->Cell(40,5,utf8_decode('Estado'),0);
		$this->Cell(25,5,utf8_decode('Fecha Ingreso'),0,0,'L');
		$this->Cell(30,5,utf8_decode('Estatus'),0,0,'C');
		$this->Cell(40,5,utf8_decode('Usuario Ingreso'),0,0,'C');
		$this->Cell(40,5,utf8_decode('Usuario Modificaciòn'),0,0,'C');
		
		
		$this->Line(10,56,272,56);
		$this->Ln();
		$y=59;
		$x=32;
		$contador=0;
		
		if ($cboana !='' and $txtfecha !='' and $txtfechahasta !=''){	
				
			$condi =" where siscam.vst_cli.usu_nombre_cli ='$cboana' and siscam.vst_cli.cli_fechaing >='$txtfecha' and siscam.vst_cli.cli_fechaing <='$txtfechahasta'";
				
			$consulta ="select * from siscam.vst_cli $condi order by siscam.vst_cli.cli_rif asc";
		
	}else{
			$consulta = "select * from siscam.vst_cli where siscam.vst_cli.cli_fechaing ='$fecha' order by siscam.vst_cli.cli_rif asc";
		}
		
		$query = pg_query($consulta);
		while($rowrs = pg_fetch_object($query)) {
		$contador++;
			$this->Cell(25,5,utf8_decode($rowrs->cli_rif),0);
			if($rowrs->cli_nombre == 'AYUDA SOLIDARIA')
			{
				$consulta_nombre = "select * from siscam.vst_aut_fac_re where vst_aut_fac_re.re_id = $rowrs->re_id";
				$cons = pg_query($consulta_nombre);
				$fila = pg_fetch_object($cons,0);
			$this->Cell(25,5,utf8_decode($fila->est_id),0);	
			$this->Cell(80,5,substr($fila->est_pnombre. " " .$fila->est_papellido,0,27),0);
			}
			else
			{
			$this->Cell(80,5,substr($rowrs->cli_nombre,0,35),0);	
			$this->Cell(40,5,substr($rowrs->esta_estado,0,35),0);
			}
			$this->Cell(25,5,utf8_decode($rowrs->cli_fechaing),0);
			$this->Cell(30,5,utf8_decode($rowrs->cli_estatus),0,0,'C');
			$this->Cell(30,5,utf8_decode($rowrs->usu_nombre_cli),0,0,'R');
			$this->Cell(30,5,utf8_decode($rowrs->cli_usu_mod),0,0,'R');
			$this->Cell(30,5,utf8_decode('_______________'),0,0,'R');
			//$montototal= $montototal + $rowrs->re_total;	
			$this->Ln(4);
			$s=$s+22;
			
			
			if($contador == 20) { $this->AddPage(); $y = 130; $x=72; $s=0; $contador=0; }
			
			
			
		}
		
		//muestra totales del reporte
		//$consulta2 = "select * from siscam.relacion where siscam.relacion.re_id like '$re'";
		//$query2 = pg_query($consulta2);
		//$row2 = pg_fetch_object($query2);
		//$this->Ln(4);
		//$this->SetX(140);
		//$this->Cell(40,5,utf8_decode('TOTAL GENERAL:'),'C',0);
		//$this->SetX(180);
		//$this->Cell(25,5,number_format($montototal,2,',','.'),0);
		//$this->Ln();
		/*$this->SetX(130);
		$this->Cell(30,5,'IMPUESTO DEL 1*1000: ','L',0);
		$this->SetX(170);
		$this->Cell(25,5,number_format($row2->re_riam,2,',','.'),'R',0);
		$this->Ln();
		$this->SetX(130);
		$this->Cell(40,5,'TOTAL A CANCELAR: ','LB',0);
		$this->SetX(170);
		$this->Cell(25,5,number_format($row2->re_totalcan,2,',','.'),'BR',0);*/
		
		//////////////////////////////////////////////////////////
		//CODIGO EXTRA PARA GENERAR VARIAS PAGINAS DE PRUEBA    //
		//for($i=1;$i<=40;$i++)                                 //
		//$this->Cell(0,10,'Imprimiendo Linea de Prueba Numero '.$i,0,1); //
		//$this->Ln();                                          //
		//////////////////////////////////////////////////////////
		
		//Línea de cierre
		//$this->Cell(array_sum($w),0,'',0);
	}
	}

//Creación del objeto de la clase heredada
$pdf=new PDF('L');
$pdf->AliasNbPages();
$pdf->AddPage();

//llamada a la funcion de crear tabla y fecha
$pdf->CrearTabla($cboana,$txtfecha,$txtfechahasta);
$pdf->Output();


function CamFormFech($contenido)
{
 		if($contenido!=null)
		{
 			$fech=explode('-', $contenido,3);
 			$fech2=explode(' ', $fech[2],3);
 				if(count($fech2) == 2)
				{
				return $fech2[0]."/".$fech[1]."/".$fech[0]." ".$fech2[1];
 				}
				else
				{
 				return $fech2[0]."/".$fech[1]."/".$fech[0];
 				}
 		}
 }

?>
