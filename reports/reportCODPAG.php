<?php
require('../reports/fpdf16/fpdf.php');

	//Conexion al servidor
	$servidor = "localhost";
	$base = "intranet_dev";
	$usuario = "postgres";
	$password = "$2011tyspostgresql%";   
	pg_pconnect("host=$servidor dbname=$base user=$usuario password=$password");
	
	
//Referencia

	 $cboana = strtoupper(@$_POST["cboana"]);
	$txtfecha = @$_POST["txtfecha"];
	
class PDF extends FPDF
{


	//Cabecera de página
	function Header()
	{		
		//fecha
		$this->SetY(20);
		$this->SetFont('Arial','I',10);
		$this->Cell(0,8,'Fecha: '.date('d/m/Y h:i A'),0,0,'L');
		//Logo
		$this->Image('../imagenes/encabezado_gris.jpg',13,8,180,12);
		//Fuente
		$this->SetFont('Arial','B',15);
		//Movernos a la derecha
		$this->SetY(16);
		$this->SetX(96);
		//Título		
		$this->Cell(30,25,utf8_decode('Codificados Analistas de Pago'),0,0,'C');
		

	}

	//Numero de Página
	function Footer()
	{
		//Posición: a la derecha
		$this->SetY(19);
		//Arial italic 8
		$this->SetFont('Arial','I',10);
		//Número de página
		$this->Cell(0,10,utf8_decode('Página '.$this->PageNo().'/{nb}'),0,0,'R');
	}
	
	//Tabla del Reporte
	function CrearTabla($cboana,$txtfecha)
	{		
	
		$this->SetFont('Times','B',10);
		$this->SetY(36);
		$this->SetX(66);
		if($txtfecha !=NULL or $txtfecha !=''){
			$this->Cell(30,6,utf8_decode(' Casos Codificados el '.CamFormFech($txtfecha)),0);
		}else{
			$this->Cell(30,6,utf8_decode(' Lista de Casos Codificados'),0);
		}
			
		$this->Ln();
		
		//Anchuras de las columnas
		$this->SetY(49);
		$this->SetFont('Times','',9);
		$w=array(22,22,22,22,55,25);
		$this->Ln(8);
		//Subtítulos
		$this->SetY(51);
		$this->SetFont('Times','B',9);
		$this->Cell(18,5,utf8_decode('Carta Aval'),0);
		$this->Cell(18,5,utf8_decode('Nro.Control'),0);
		$this->Cell(20,5,utf8_decode('Cédula'),0);
		$this->Cell(53,5,utf8_decode('Nombre y Apellido'),0);
		$this->Cell(53,5,utf8_decode('Usuario'),0);
		$this->Cell(33,5,utf8_decode('Modalidad'),0);
		
		
		$this->Line(10,56,195,56);
		$this->Ln();
		
		
		if ($cboana !='' and $txtfecha !=''){	
				
			$condi =" where siscam.vst_aut_bit.usu_pag ='$cboana' and siscam.vst_aut_bit.bit_pagfecha ='$txtfecha' ";
				
			$consulta ="select * from siscam.vst_aut_bit $condi order by siscam.vst_aut_bit.aut_cartaaval asc";
		
	}else{
			$consulta = "select * from siscam.vst_aut_bit order by siscam.vst_aut_bit.aut_cartaaval asc";
		}
		
		
		$query = pg_query($consulta);
		while($rowrs = pg_fetch_object($query)) { 
			$this->Cell(18,5,utf8_decode($rowrs->aut_cartaaval),0);
			$this->Cell(18,5,utf8_decode($rowrs->aut_ncontrol),0);
			$this->Cell(20,5,utf8_decode($rowrs->est_id),0);
			$this->Cell(53,5,utf8_decode($rowrs->est_pnombre.' '.$rowrs->est_papellido),0);
			$this->Cell(53,5,utf8_decode($rowrs->usu_pag),0);
			$this->Cell(33,5,utf8_decode($rowrs->aut_modalidad),0);
						
			$this->Ln();
			
			
			
		}
		
		//muestra totales del reporte
		//$consulta2 = "select * from siscam.relacion where siscam.relacion.re_id like '$re'";
		//$query2 = pg_query($consulta2);
		//$row2 = pg_fetch_object($query2);
	/*	$this->Ln(4);
		$this->SetX(130);
		$this->Cell(40,5,'TOTAL CASOS: ','LT',0);
		$this->SetX(170);
		$this->Cell(25,5,number_format($c,0,',','.'),'TR',0);
		$this->Ln();
		$this->SetX(130);
		$this->Cell(40,5,'TOTAL GENERAL: ','LB',0);
		$this->SetX(170);
		$this->Cell(25,5,number_format($montotal,2,',','.'),'BR',0);*/
		//$this->Ln();
		/*$this->SetX(130);
		$this->Cell(30,5,'IMPUESTO DEL 1*1000: ','L',0);
		$this->SetX(170);
		$this->Cell(25,5,number_format($row2->re_riam,2,',','.'),'R',0);
		$this->Ln();
		$this->SetX(130);
		$this->Cell(40,5,'TOTAL A CANCELAR: ','LB',0);
		$this->SetX(170);
		$this->Cell(25,5,number_format($row2->re_totalcan,2,',','.'),'BR',0);*/
		
		//////////////////////////////////////////////////////////
		//CODIGO EXTRA PARA GENERAR VARIAS PAGINAS DE PRUEBA    //
		//for($i=1;$i<=40;$i++)                                 //
		//$this->Cell(0,10,'Imprimiendo Linea de Prueba Numero '.$i,0,1); //
		//$this->Ln();                                          //
		//////////////////////////////////////////////////////////
		
		//Línea de cierre
		//$this->Cell(array_sum($w),0,'',0);
	}
	}

//Creación del objeto de la clase heredada
$pdf=new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();

//llamada a la funcion de crear tabla y fecha
$pdf->CrearTabla($cboana,$txtfecha);
$pdf->Output();


function CamFormFech($contenido)
{
 		if($contenido!=null)
		{
 			$fech=explode('-', $contenido,3);
 			$fech2=explode(' ', $fech[2],3);
 				if(count($fech2) == 2)
				{
				return $fech2[0]."/".$fech[1]."/".$fech[0]." ".$fech2[1];
 				}
				else
				{
 				return $fech2[0]."/".$fech[1]."/".$fech[0];
 				}
 		}
 }

?>
