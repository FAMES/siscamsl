<?php


require('../reports/fpdf16/fpdf.php');

	//Conexion al servidor
	$servidor = "190.190.1.19";
	$base = "intranet_dev";
	$usuario = "postgres";
	$password = "$2011tyspostgresql%";   
	pg_pconnect("host=$servidor dbname=$base user=$usuario password=$password");
	
	//Arreglo de los datos seleccionados con los checkbox
			/*$array = $_POST["enviar"];
			$valores = split(",",$array);
			$cont=cou*/

	$rif = $_POST["rif"];
	
	/*$consulta = "select * from siscam.vst_autorizaciones where siscam.vst_autorizaciones.aut_cartaaval = ".$carta;
		$query = pg_query($consulta);
		$rowr = pg_fetch_object($query,0);
		$modalidad = $rowr->aut_modalidad;*/
	
class PDF extends FPDF
{
	//Cabecera de página
	function Header()
	{	
		//fecha
		$this->SetY(28);
		$this->SetX(8);
		$this->SetFont('Arial','B',12);
		$this->MultiCell(188, 6,'FAMES: '.date('d/m/Y h:i A'), 0, "J");
		//Logo
		$this->Image('../imagenes/encabezado_gris.jpg',13,8,180,12);
		//Fuente
		$this->SetFont('Arial','B',15);
		//Movernos a la derecha
		$this->SetY(16);
		$this->SetX(96);
		//Título		
		$this->Cell(30,25,utf8_decode('Relaciones Pagadas'),0,0,'C');
	}

	//Numero de Página
	function Footer()
	{
		//Posición: a la derecha
		$this->SetY(17);
		//Arial italic 8
		$this->SetFont('Arial','I',8);
		//Número de página
		$this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'R');
	}

	
	//Tabla del Reporte
		
	function Crearfiniquito($rif)
	{				
	
	// QUERY PARA TRAERNOS LOS DATOS DE LA CARTA AVAL
	
		$condi = " where siscam.clinicas.cli_rif = '$rif' ";
		$consulta = "select * from siscam.clinicas $condi ";
		$query = pg_query($consulta);
		$rowr = pg_fetch_object($query,0);
		//$totalcan = number_format($rowr->re_totalcan,2,',','.');
		
		
		
		//$montop = number_format($rowr->aut_montop,2,',','.');
		
		$this->SetY(36);
		$this->SetX(58);
		$this->SetFont('Arial','B',12);
			
		$this->SetY(50);
		$this->SetX(8);
		$this->SetFont('Arial','I',12);
		$this->SetFillColor(192,192,192);
		$this->MultiCell(188, 6,utf8_decode('DATOS CLINICA'), 1, "C",true);
		$this->SetY(56);
		$this->SetX(8);
		$this->SetFillColor(255,255,255);
		$this->SetFont('Arial','B',12);
		$this->Cell(94,5,utf8_decode('Nro. Rif: '),1,0,'R',1);
		$this->SetFont('Arial','I',10);
		$this->Cell(94,5,$rif,1,0,'L',1);
		$this->SetY(61);
		$this->SetX(8);
		$this->SetFillColor(255,255,255);
		$this->SetFont('Arial','B',12);
		$this->Cell(94,5,utf8_decode('Razón Social: '),1,0,'R',1);
		$this->SetFont('Arial','I',10);
		$this->Cell(94,5,$rowr->cli_nombre,1,0,'L',1);
		$this->SetY(66);
		$this->SetX(8);
		$this->SetFillColor(255,255,255);
		$this->SetFont('Arial','B',12);
		$this->Cell(94,5,utf8_decode('Dirección: '),1,0,'R',1);
		$this->SetFont('Arial','I',10);
		$this->Cell(94,5,$rowr->cli_direccion,1,0,'L',1);
		$this->SetY(71);
		$this->SetX(8);
		$this->SetFillColor(255,255,255);
		$this->SetFont('Arial','B',12);
		$this->Cell(94,5,utf8_decode('Telefono: '),1,0,'R',1);
		$this->SetFont('Arial','I',10);
		$this->Cell(94,5,$rowr->cli_telefono,1,0,'L',1);
		
		
		
		
		$this->SetY(82);
		$this->SetX(8);
		$this->SetFillColor(192,192,192);
		$this->SetFont('Arial','B',12);
		$this->Cell(30,8,utf8_decode('Relación '),1,0,'C',1);
		$this->Cell(26,8,utf8_decode('Fecha '),1,0,'C',1);
		$this->Cell(28,8,utf8_decode('Monto Bruto '),1,0,'C',1);
		$this->Cell(42,8,utf8_decode('Monto Total '),1,0,'C',1);
		$this->Cell(62,8,utf8_decode('Nro de Cheque '),1,0,'C',1);
		$y = 90;
		$x = 8;
	
		//	query para traernos el usuario
		$condi = "where siscam.vst_aut_cli_re_esp.cli_rif = '$rif'";
		$consultafin = "select * from siscam.vst_aut_cli_re_esp $condi";
		$query = pg_query($consultafin);
		while($rows = pg_fetch_object($query)){
		$this->SetY($y);
		$this->SetX($x);
		$this->SetFillColor(255,255,255);
		$this->SetFont('Arial','I',10);
		$this->Cell(30,5,utf8_decode($rows->re_id),1,0,'L',1);
		$this->Cell(26,5,utf8_decode($rows->re_fecha),1,0,'C',1);
		$this->Cell(28,5,utf8_decode($rows->re_total),1,0,'C',1);
		
		//$porcentaje = ($row->pat_majust / $row->bar_baremo) * 100;  
		$this->Cell(42,5,number_format($rows->re_totalcan,2,',','.'),1,0,'C',1);
		$this->Cell(62,5,utf8_decode($rows->re_nrocheq),1,0,'C',1);
		$y = $y + 5;
		}
		
		//muestra totales del reporte
		$condi = "where siscam.vst_aut_cli_re_esp.cli_rif = '$rif'";
		$consulta2 = "select * from siscam.vst_aut_cli_re_esp $condi";
		$query2 = pg_query($consulta2);
		$row2 = pg_fetch_object($query2);
		$this->Ln(4);
		$this->SetY(190);
		$this->SetX(130);
		$this->Cell(40,5,'TOTAL: ','LT',0);
		$this->SetX(170);
		$this->Cell(25,5,number_format($row2->re_total,2,',','.'),'TR',0);
		$this->Ln();
		$this->SetX(130);
		$this->Cell(15,5,'I.S.L.R. ('.$i.'%): ','L',0);
		$this->SetX(170);
		$this->Cell(25,5,number_format($row2->re_islr,2,',','.'),'R',0);
		$this->Ln();
		$this->SetX(130);
		$this->Cell(30,5,'IMPUESTO DEL 1*1000: ','L',0);
		$this->SetX(170);
		$this->Cell(25,5,number_format($row2->re_riam,2,',','.'),'R',0);
		$this->Ln();
		$this->SetX(130);
		$this->Cell(40,5,'TOTAL A CANCELAR: ','LB',0);
		$this->SetX(170);
		$this->Cell(25,5,number_format($row2->re_totalcan,2,',','.'),'BR',0);
		
		
	}
}

$pdf=new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();


$pdf->Crearfiniquito($rif);
$pdf->Output();
?>
