<?php
require('../reports/fpdf16/fpdf.php');

	//Conexion al servidor
	$servidor = "localhost";
	$base = "intranet_dev";
	$usuario = "postgres";
	$password = "$2011tyspostgresql%";   
	pg_pconnect("host=$servidor dbname=$base user=$usuario password=$password");
	
 	//Referencia
	$fechadesde = strtoupper(@$_POST["txtfechadesde"]);
	$fechahasta = strtoupper(@$_POST["txtfechahasta"]);	
	$clinica = $_REQUEST["clinica"];
	
class PDF extends FPDF
{
	//Cabecera de página
	function Header()
	{		
		//fecha
		$this->SetY(20);
		$this->SetFont('Arial','I',10);
		$this->Cell(0,8,'Fecha: '.date('d/m/Y h:i A'),0,0,'L');
		//Logo
		$this->Image('../imagenes/encabezado_gris.jpg',10,8,270,12);
		//Fuente
		$this->SetFont('Arial','B',15);
		//Movernos a la derecha
		$this->SetY(16);
		$this->SetX(96);
		//Título		
		$this->Cell(85,25,utf8_decode('Reporte de Deuda por Analista'),0,0,'C');
		
		//Referencia
		$fechadesde = strtoupper(@$_POST["txtfechadesde"]);
		$fechahasta = strtoupper(@$_POST["txtfechahasta"]);	
		$clinica = $_REQUEST["clinica"];
		$fechad = CamFormFech($fechadesde);
		$fechah = CamFormFech($fechahasta);	
		
		if($fechadesde != NULL and $fechahasta != NULL)
		{
		$consultarcli = "select * from siscam.clinicas where siscam.clinicas.cli_rif = '$clinica'";
		$querycli = pg_query($consultarcli);
		$rowr = pg_fetch_object($querycli,0); 
		$this->SetFont('Times','B',10);
		$this->SetY(26);
		$this->SetX(30);
		if($rowr->cli_nombre != '')
		$this->Cell(30,25,utf8_decode('Clinica : '.$rowr->cli_nombre),0);
		else
		$this->Cell(30,25,utf8_decode('Clinica : TODOS'),0);
		$this->SetX(200);
		$this->Cell(30,25,utf8_decode('DESDE:  '.$fechad.' HASTA: '.$fechah),0);
		$this->Ln();
		}
		//Subtítulos
		$this->SetY(51);
		$this->SetFont('Times','B',9);
		$this->Cell(30,5,utf8_decode('Rif.'),0,'C');
		$this->Cell(120,5,utf8_decode('Clinica '),0);
        $this->Cell(50,5,utf8_decode('Estado'),0);
		$this->Cell(32,5,utf8_decode('Region'),0,'C');						
		$this->Cell(10,5,utf8_decode('Deuda'),0,'R');		
		
		$this->Line(10,56,280,56);
		$this->Ln();
	}

	//Numero de Página
	function Footer()
	{
		//Posición: a la derecha
		$this->SetY(19);
		//Arial italic 8
		$this->SetFont('Arial','I',10);
		//Número de página
		$this->Cell(0,10,utf8_decode('Página '.$this->PageNo().'/{nb}'),0,0,'R');
	}
	
	//Tabla del Reporte
	function CrearTabla($fechadesde,$fechahasta,$clinica)
	{				
		//Anchuras de las columnas
		$this->SetY(49);
		$this->SetFont('Times','',9);
		$w=array(22,22,22,22,55,25);
		$this->Ln(8);
		
		//Datos	
		if($fechadesde != NULL and $fechahasta != NULL)
		{
			if($clinica != 'TODOS')
			{
				$consulta = "SELECT	C.cli_rif, 
									C.cli_nombre,
									C.cli_direccion,									 
									B.esta_estado,
									B.esta_regionp,
									sum(D.fac_monto) as monto									
							 FROM 
							 		siscam.autorizaciones A, 
									siscam.estado B, 
									siscam.clinicas C, 
									siscam.factura D,
									siscam.bitacora F, 
									siscam.estudiantes G
							 WHERE 
							 		A.cli_rif = C.cli_rif and
		      						C.esta_id = B.esta_id and
		      						A.est_id = G.est_id   and
		      						A.aut_cartaaval = F.aut_cartaaval and
		      						A.aut_cartaaval = D.aut_cartaaval and
									A.aut_estatus <> 'PDO' and
									A.aut_modalidad <> 'ESPECIAL' and
		      						D.fechaing >= '$fechadesde' and 
									D.fechaing <= '$fechahasta' and 
		      						C.cli_rif = '$clinica' 
							 GROUP BY C.cli_rif, B.esta_estado,c.cli_nombre, c.cli_direccion, B.esta_regionp 
							 ORDER BY B.esta_regionp";	
			}
			else
			{
				$consulta = "SELECT C.cli_rif, 
									C.cli_nombre,
									C.cli_direccion,									 
									B.esta_estado,
									B.esta_regionp,
									sum(D.fac_monto) as monto									
							 FROM 
							 		siscam.autorizaciones A, 
									siscam.estado B, 
									siscam.clinicas C, 
									siscam.factura D,
									siscam.bitacora F, 
									siscam.estudiantes G
							 WHERE 
							 		A.cli_rif = C.cli_rif and
		      						C.esta_id = B.esta_id and
		      						A.est_id = G.est_id   and
		      						A.aut_cartaaval = F.aut_cartaaval and
		      						A.aut_cartaaval = D.aut_cartaaval and
									A.aut_estatus <> 'PDO' and
									A.aut_modalidad <> 'ESPECIAL' and
		      						D.fechaing >= '$fechadesde' and 
									D.fechaing <= '$fechahasta'		      						
							 GROUP BY C.cli_rif, c.cli_nombre, B.esta_estado, c.cli_direccion, B.esta_regionp 
							 ORDER BY B.esta_regionp";
			}
		}
		$queryfec = pg_query($consulta);
		while($rowr = pg_fetch_object($queryfec))
		{ 
			$this->Cell(30,5,utf8_decode($rowr->cli_rif),0);
			$this->Cell(120,5,utf8_decode(substr($rowr->cli_nombre,0,33)),0);			
			$this->Cell(50,5,utf8_decode($rowr->esta_estado),0,0,'L');			
			$this->Cell(32,5,utf8_decode($rowr->esta_regionp),0,0,'L');
			$this->Cell(25,5,utf8_decode($rowr->monto),0,0,'R');
			$this->Ln();	
			$total += $rowr->monto;			
		}
		$this->SetXY(110,$this->GetY()+10);	
		$this->setFont('Times','B',12);
		$this->Cell(75,5,'TOTAL DEUDA : '.number_format($total,2,',','.') .' Bs.','RBLT',0,'C');			
	}
}

function CamFormFech($contenido) {
	if($contenido!=null) {
		$fech=explode('-', $contenido,3);
		$fech2=explode(' ', $fech[2],3);
		if(count($fech2) == 2) {
			return $fech2[0]."/".$fech[1]."/".$fech[0]." ".$fech2[1];
		} else {
			return $fech2[0]."/".$fech[1]."/".$fech[0];
		}
	}
}

//Creación del objeto de la clase heredada
$pdf=new PDF('L');
$pdf->AliasNbPages();
$pdf->AddPage();

//llamada a la funcion de crear tabla y fecha
$pdf->CrearTabla($fechadesde,$fechahasta,$clinica);
$pdf->Output();




?>
