<?php
require('../reports/fpdf16/fpdf.php');

	//Conexion al servidor
	$servidor = "localhost";
	$base = "intranet_dev";
	$usuario = "postgres";
	$password = "$2011tyspostgresql%";   
	pg_pconnect("host=$servidor dbname=$base user=$usuario password=$password");
	
 	//Referencia
	$fechadesde = strtoupper(@$_POST["txtfechadesde"]);
	$fechahasta = strtoupper(@$_POST["txtfechahasta"]);	
	$analista = $_REQUEST["analista"];
	$cboano = $_REQUEST["cboano"];
	
class PDF extends FPDF
{
	//Cabecera de página
	function Header()
	{		
		//fecha
		$this->SetY(20);
		$this->SetFont('Arial','I',10);
		$this->Cell(0,8,'Fecha: '.date('d/m/Y h:i A'),0,0,'L');
		//Logo
		$this->Image('../imagenes/encabezado_gris.jpg',10,8,270,12);
		//Fuente
		$this->SetFont('Arial','B',15);
		//Movernos a la derecha
		$this->SetY(16);
		$this->SetX(96);
		//Título		
		$this->Cell(85,25,utf8_decode('Reporte de Deuda por Analista'),0,0,'C');
		
		//Referencia
		$fechadesde = strtoupper(@$_POST["txtfechadesde"]);
		$fechahasta = strtoupper(@$_POST["txtfechahasta"]);	
		$analista = $_REQUEST["analista"];
		$cboano = $_REQUEST["cboano"];
		$fechad = CamFormFech($fechadesde);
		$fechah = CamFormFech($fechahasta);	
		
		if($fechadesde != NULL and $fechahasta != NULL)
		{
		$consultarfec = "select * from siscam.vst_aut_fac_re where siscam.vst_aut_fac_re.aut_fechaa >= '$fechadesde' and siscam.vst_aut_fac_re.aut_fechaa <= '$fechahasta'";
		$queryfec = pg_query($consultarfec);
		$rowr = pg_fetch_object($queryfec); 
		$this->SetFont('Times','B',10);
		$this->SetY(26);
		$this->SetX(30);
		$this->Cell(30,25,utf8_decode('Analista : '.$analista),0);
		$this->SetX(200);
		$this->Cell(30,25,utf8_decode('DESDE:  '.$fechad.' HASTA: '.$fechah),0);
		$this->Ln();
		}else
		{
			$this->SetY(26);
			$this->SetX(30);
			$this->Cell(30,25,utf8_decode('Analista : '.substr($analista,8,1)),0);
			$this->SetX(200);
			$this->Cell(30,25,utf8_decode('Año :'. $cboano),0);	
		}
		//Subtítulos
		$this->SetY(51);
		$this->SetFont('Times','B',9);
		$this->Cell(30,5,utf8_decode('Estado'),0,'C');
		$this->Cell(70,5,utf8_decode('Clinica '),0);
		$this->Cell(20,5,utf8_decode('Cedula'),0,0,'C');
        $this->Cell(50,5,utf8_decode('Estudiante'),0);
		if($cboano != 0) 
		{
		$this->Cell(25,5,utf8_decode('Fecha Ingreso'),0,0,'C');		
		}else 
		{
		$this->Cell(25,5,utf8_decode('Fecha Recepcion'),0,0,'C');	
		}		
		$this->Cell(25,5,utf8_decode('Nro Factura'),0);
		if($cboano == 0) 
		{		
		$this->Cell(32,5,utf8_decode('Fecha Emision'),0);		
		}
		$this->Cell(25,5,utf8_decode('Monto Factura'),0,0,'C');		
		
		$this->Line(10,56,280,56);
		$this->Ln();
	}

	//Numero de Página
	function Footer()
	{
		//Posición: a la derecha
		$this->SetY(19);
		//Arial italic 8
		$this->SetFont('Arial','I',10);
		//Número de página
		$this->Cell(0,10,utf8_decode('Página '.$this->PageNo().'/{nb}'),0,0,'R');
	}
	
	//Tabla del Reporte
	function CrearTabla($fechadesde,$fechahasta,$analista,$cboano)
	{				
		//Anchuras de las columnas
		$this->SetY(49);
		$this->SetFont('Times','',9);
		$w=array(22,22,22,22,55,25);
		$this->Ln(8);
		
		//Datos	
		if($fechadesde != NULL and $fechahasta != NULL)
		{
			if($analista != 'TODOS')
			{
				$consulta = "SELECT B.esta_estado, 
									C.cli_nombre, 
									C.cli_rif, 
									G.est_pnombre, 
									G.est_papellido, 
									G.est_id, 
									F.bit_recfecha as recepcion, 
									D.fac_numero, 
									D.fac_fecha as emision, 
									D.fac_monto 
							 FROM 
							 		siscam.autorizaciones A, 
									siscam.estado B, 
									siscam.clinicas C, 
									siscam.factura D,
									siscam.bitacora F, 
									siscam.estudiantes G
							 WHERE 
							 		A.cli_rif = C.cli_rif and
		      						C.esta_id = B.esta_id and
		      						A.est_id = G.est_id   and
		      						A.aut_cartaaval = F.aut_cartaaval and
		      						A.aut_cartaaval = D.aut_cartaaval and
									A.aut_estatus <> 'PDO' and
									A.aut_modalidad <> 'ESPECIAL' and
		      						D.fechaing >= '$fechadesde' and D.fechaing <= '$fechahasta' and 
		      						B.esta_regionp = '$analista' order by C.cli_rif";	
			}
			else
			{
				$consulta = "SELECT B.esta_estado, 
									C.cli_nombre, 
									C.cli_rif, 
									G.est_pnombre, 
									G.est_papellido, 
									G.est_id, 
									F.bit_recfecha as recepcion, 
									D.fac_numero, 
									D.fac_fecha as emision, 
									D.fac_monto 
							 FROM 
							 		siscam.autorizaciones A, 
									siscam.estado B, 
									siscam.clinicas C, 
									siscam.factura D,
									siscam.bitacora F, 
									siscam.estudiantes G
							 WHERE 
							 		A.cli_rif = C.cli_rif and
		      						C.esta_id = B.esta_id and
		      						A.est_id = G.est_id   and
		      						A.aut_cartaaval = F.aut_cartaaval and
		      						A.aut_cartaaval = D.aut_cartaaval and
									A.aut_estatus <> 'PDO' and
									A.aut_modalidad <> 'ESPECIAL' and
		      						D.fechaing >= '$fechadesde' and D.fechaing <= '$fechahasta' 
		      						order by C.cli_rif";
			}
		}elseif($cboano != '0')
		{
			if($analista != 'TODOS') { $aux = " and D.esta_regionp = '$analista' " ; } 
			$consulta = "SELECT A.aut_cartaaval, 
								A.fac_numero,
								D.esta_estado, 
								C.cli_nombre,
								B.est_id,   
								E.est_pnombre, 
								E.est_papellido, 
								A.fechaing as recepcion, 
								A.fac_monto															
						 FROM siscam.factura A, 
						 	  siscam.autorizaciones B, 
							  siscam.clinicas C, 
							  siscam.estado D, 
							  siscam.estudiantes E 
						 WHERE  
						(A.fechaing >= '2014-01-01' or (A.fechaing < '2014-01-01' and A.fechaing <> '1900-01-01' and A.aut_cartaaval >= 201400000))
						and A.aut_cartaaval = B.aut_cartaaval
						and B.cli_rif = C.cli_rif
						and C.esta_id = D.esta_id
						and B.est_id = E.est_id
						and B.aut_modalidad <> 'ESPECIAL'						
						and B.aut_Estatus <> 'PDO'		
						$aux									
						order by D.esta_estado, A.fechaing";
		}
		
		$queryfec = pg_query($consulta);
		while($rowr = pg_fetch_object($queryfec))
		{ 
			$this->Cell(30,5,utf8_decode($rowr->esta_estado),0);
			$this->Cell(70,5,utf8_decode(substr($rowr->cli_nombre,0,33)),0);
			$this->Cell(20,5,utf8_decode(substr($rowr->est_id,0,33)),0,0,'C');			
			$this->Cell(50,5,utf8_decode($rowr->est_pnombre.' '.$rowr->est_papellido),0,0,'L');			
			$this->Cell(25,5,utf8_decode($rowr->recepcion),0,0,'C');
			$this->Cell(25,5,utf8_decode($rowr->fac_numero),0,0,'L');
			if($cboano == 0) 
			{
			$this->Cell(32,5,utf8_decode($rowr->emision),0,0,'L');
			}
			$this->Cell(25,5,utf8_decode($rowr->fac_monto),0,0,'R');
			$this->Ln();	
			$total += $rowr->fac_monto;			
		}
		$this->SetXY(110,$this->GetY()+10);	
		$this->setFont('Times','B',12);
		$this->Cell(75,5,'TOTAL DEUDA : '.number_format($total,2,',','.') .' Bs.','RBLT',0,'C');			
	}
}

function CamFormFech($contenido) {
	if($contenido!=null) {
		$fech=explode('-', $contenido,3);
		$fech2=explode(' ', $fech[2],3);
		if(count($fech2) == 2) {
			return $fech2[0]."/".$fech[1]."/".$fech[0]." ".$fech2[1];
		} else {
			return $fech2[0]."/".$fech[1]."/".$fech[0];
		}
	}
}

//Creación del objeto de la clase heredada
$pdf=new PDF('L');
$pdf->AliasNbPages();
$pdf->AddPage();

//llamada a la funcion de crear tabla y fecha
$pdf->CrearTabla($fechadesde,$fechahasta,$analista, $cboano);
$pdf->Output();




?>
