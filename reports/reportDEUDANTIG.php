<?php
require('../reports/fpdf16/fpdf.php');

	//Conexion al servidor
	$servidor = "localhost";
	$base = "intranet_dev";
	$usuario = "postgres";
	$password = "$2011tyspostgresql%";   
	pg_pconnect("host=$servidor dbname=$base user=$usuario password=$password");
	
 	//Referencia	
	$antig = $_REQUEST["antig"];
	
class PDF extends FPDF
{
	//Cabecera de página
	function Header()
	{		
		//fecha
		$this->SetY(20);
		$this->SetFont('Arial','I',10);
		$this->Cell(0,8,'Fecha: '.date('d/m/Y h:i A'),0,0,'L');
		//Logo
		$this->Image('../imagenes/encabezado_gris.jpg',10,8,270,12);
		//Fuente
		$this->SetFont('Arial','B',15);
		//Movernos a la derecha
		$this->SetY(16);
		$this->SetX(140);
		$antig = $_REQUEST["antig"];
		
		if($antig == 1) { $mens = 'Menor a 30 Dias'; }
		if($antig == 2) { $mens = 'Menor a 60 Dias'; }
		if($antig == 3) { $mens = 'Menor a 90 Dias'; }
		if($antig == 4) { $mens = 'Mayor a 90 Dias'; }
		//Título		
		$this->Cell(20,25,utf8_decode('Reporte de Deuda por Antiguedad'),0,0,'C');
		$this->SetXY(25,21);
		$this->SetFont('Times','B',9);
		$this->Cell(20,25,utf8_decode('Antiguedad : '. $mens),0,0,'C');		
		//Referencia				
		$this->SetY(41);
		$this->SetFont('Times','B',9);
		$this->Cell(70,5,utf8_decode('Clinica.'),0,'C');
		$this->Cell(30,5,utf8_decode('Telefono'),0,'C');
		$this->Cell(30,5,utf8_decode('Banco'),0,'C');
		$this->Cell(35,5,utf8_decode('Cuenta'),0,'C');			
		if($antig == 'TODOS')
		{
		$this->SetXY(180,36);
		$this->MultiCell(30,5,"Menos de \n 30 Dias",0,'L');		
		$this->SetXY(200,36);
		$this->MultiCell(20,5,utf8_decode("Menos de \n 60 Dias"),0,'L');
		$this->SetXY(220,36);
		$this->MultiCell(20,5,utf8_decode("Menos de \n 90 Dias"),0,'L');
		$this->SetXY(240,36);
		$this->MultiCell(20,5,utf8_decode("Mayor de \n 90 Dias"),0,'L');
		$pos = 240;
		}
		elseif($antig == 1)
		{
		$this->SetXY(180,36);
		$this->MultiCell(30,5,"Menos de \n 30 Dias",0,'L');
		$pos = 180;
		}
		elseif($antig == 2)
		{
		$this->SetXY(180,36);
		$this->MultiCell(20,5,utf8_decode("Menos de \n 60 Dias"),0,'L');
		$pos = 180;
		}
		elseif($antig == 3)
		{
		$this->SetXY(180,36);
		$this->MultiCell(20,5,utf8_decode("Menos de \n 90 Dias"),0,'L');
		$pos = 180;
		}
		if($antig == 4)
		{
		$this->SetXY(180,36);
		$this->MultiCell(20,5,utf8_decode("Mayor de \n 90 Dias"),0,'L');
		$pos = 180;
		}
		$this->SetXY($pos+20,41);
		$this->Cell(20,5,utf8_decode('Totales'),0,'C');		
		$this->Line(10,46,280,46);
		$this->Ln();
	}

	//Numero de Página
	function Footer()
	{
		//Posición: a la derecha
		$this->SetY(19);
		//Arial italic 8
		$this->SetFont('Arial','I',10);
		//Número de página
		$this->Cell(0,10,utf8_decode('Página '.$this->PageNo().'/{nb}'),0,0,'R');
	}
	
	//Tabla del Reporte
	function CrearTabla($antig)
	{				
		//Anchuras de las columnas
		$this->SetY(39);
		$this->SetFont('Times','',9);
		$w=array(22,22,22,22,55,25);
		$this->Ln(8);
		$total = 0;
		
		//Datos		
			if($antig == 'TODOS')
			{
				$consulta = "SELECT distinct(B.cli_rif) as rif
						 	 FROM  
									siscam.bitacora A, siscam.clinicas B, siscam.Estado C, siscam.Factura D, siscam.Autorizaciones E									
						 	 WHERE 
									D.aut_cartaaval = E.aut_cartaaval and
									E.cli_rif = B.cli_rif and
									C.esta_id = B.esta_id and
									A.aut_cartaaval = E.aut_cartaaval and
									E.aut_modalidad <> 'ESPECIAL' and									
									D.fechaing > '2011-01-01'
						 	 ORDER BY 
									B.cli_rif";	
			}elseif($antig == 1)
			{
				$consulta = "SELECT distinct(B.cli_rif) as rif
						 	 FROM  
									siscam.bitacora A, siscam.clinicas B, siscam.Estado C, siscam.Factura D, siscam.Autorizaciones E									
						 	 WHERE 
									D.aut_cartaaval = E.aut_cartaaval and
									E.cli_rif = B.cli_rif and
									C.esta_id = B.esta_id and
									A.aut_cartaaval = E.aut_cartaaval and
									E.aut_modalidad <> 'ESPECIAL' and
									(date('now') - A.bit_recfecha) <= 30 and									
									D.fechaing > '2011-01-01'
						 	 ORDER BY 
									B.cli_rif";	
						$aux = " (date('now') - A.bit_recfecha) <= 30 and ";
			}elseif($antig == 2)
			{
				$consulta = "SELECT distinct(B.cli_rif) as rif
						 	 FROM  
									siscam.bitacora A, siscam.clinicas B, siscam.Estado C, siscam.Factura D, siscam.Autorizaciones E									
						 	 WHERE 
									D.aut_cartaaval = E.aut_cartaaval and
									E.cli_rif = B.cli_rif and
									C.esta_id = B.esta_id and
									A.aut_cartaaval = E.aut_cartaaval and
									E.aut_modalidad <> 'ESPECIAL' and
									(date('now') - A.bit_recfecha) > 30 and (date('now') - A.bit_recfecha) <= 60 and									
									D.fechaing > '2011-01-01'
						 	 ORDER BY 
									B.cli_rif";	
					$aux = " (date('now') - A.bit_recfecha) > 30 and (date('now') - A.bit_recfecha) <= 60 and ";
			}elseif($antig == 3)
			{
				$consulta = "SELECT distinct(B.cli_rif) as rif
						 	 FROM  
									siscam.bitacora A, siscam.clinicas B, siscam.Estado C, siscam.Factura D, siscam.Autorizaciones E									
						 	 WHERE 
									D.aut_cartaaval = E.aut_cartaaval and
									E.cli_rif = B.cli_rif and
									C.esta_id = B.esta_id and
									A.aut_cartaaval = E.aut_cartaaval and
									E.aut_modalidad <> 'ESPECIAL' and	
									(date('now') - A.bit_recfecha) > 60 and (date('now') - A.bit_recfecha) <= 90 and								
									D.fechaing > '2011-01-01'
						 	 ORDER BY 
									B.cli_rif";	
				$aux = " (date('now') - A.bit_recfecha) > 60 and (date('now') - A.bit_recfecha) <= 90 and ";
			}elseif($antig == 4)
			{
				$consulta = "SELECT distinct(B.cli_rif) as rif
						 	 FROM  
									siscam.bitacora A, siscam.clinicas B, siscam.Estado C, siscam.Factura D, siscam.Autorizaciones E									
						 	 WHERE 
									D.aut_cartaaval = E.aut_cartaaval and
									E.cli_rif = B.cli_rif and
									C.esta_id = B.esta_id and
									A.aut_cartaaval = E.aut_cartaaval and
									E.aut_modalidad <> 'ESPECIAL' and
									(date('now') - A.bit_recfecha) > 90 and									
									D.fechaing > '2011-01-01'
						 	 ORDER BY 
									B.cli_rif";	
				$aux = " (date('now') - A.bit_recfecha) > 90 and "; 
			}
			
			
								
			$queryfec = pg_query($consulta);
		while($rowr = pg_fetch_object($queryfec))
		{ 
			$monto1=0.00;
			$monto2=0.00;
			$monto3=0.00;
			$monto4=0.00;
			$totalAntig=0;														
			$rif = $rowr->rif; 							
			$consAntig = "SELECT B.cli_rif, 
								 B.cli_nombre, 
								 B.cli_telefono, 
								 B.cli_banco, 
								 B.cli_cuentaban, 
							 	 B.cli_convenio, 
								 B.cli_estatus, 
								 C.esta_estado, 
								 C.esta_regionp, 
								 D.fac_monto as monto,
					   		     (date('now') - A.bit_recfecha) as dias
						  FROM siscam.bitacora A, siscam.clinicas B, siscam.Estado C, siscam.Factura D, siscam.Autorizaciones E									
						  WHERE 
						 		 D.aut_cartaaval = E.aut_cartaaval and
								 E.cli_rif = B.cli_rif and
								 C.esta_id = B.esta_id and
								 A.aut_cartaaval = E.aut_cartaaval and
								 E.aut_modalidad <> 'ESPECIAL' and
								 B.cli_rif = '$rif' and	
								 $aux								
								 D.fechaing > '2011-01-01'						
						  ORDER BY 
								 B.cli_rif, dias";
			 $queryAntig = pg_query($consAntig);
			 	while($fila = pg_fetch_object($queryAntig))
				{
					$nombre = $fila->cli_nombre;
					$telefono = $fila->cli_telefono;
					$banco = $fila->cli_banco;
					$cuentaban = $fila->cli_cuentaban;																		 
					if($fila->dias <= 30) { $monto1 += $fila->monto; }	
					if($fila->dias > 30 and $fila->dias <=60) { $monto2 += $fila->monto; }
					if($fila->dias > 60 and $fila->dias <=90) { $monto3 += $fila->monto; }
					if($fila->dias > 90) { $monto4 += $fila->monto; }		
					$totalAntig = $monto1+$monto2+$monto3+$monto4;							
				}			
			$this->Cell(70,5,utf8_decode(substr($nombre,0,30)),0,0,'L');			
			$this->Cell(30,5,utf8_decode($telefono),0,0,'L');			
			$this->Cell(30,5,utf8_decode($banco),0,0,'L');			
			$this->Cell(35,5,utf8_decode($cuentaban),0,0,'L');	
			if($antig == 'TODOS' or $antig == 1)
			{		
			$this->Cell(20,5,number_format($monto1,2,',','.'),0,0,'R');
			}
			if($antig == 'TODOS' or $antig == 2)
			{
			$this->Cell(20,5,number_format($monto2,2,',','.'),0,0,'R');			
			}
			if($antig == 'TODOS' or $antig == 3)
			{
			$this->Cell(20,5,number_format($monto3,2,',','.'),0,0,'R');			
			}
			if($antig == 'TODOS' or $antig == 4)
			{
			$this->Cell(20,5,number_format($monto4,2,',','.'),0,0,'R');		
			}
			$this->Cell(20,5,number_format($totalAntig,2,',','.'),0,0,'R');
			$this->Ln();			
			$total+=$totalAntig;		
		}
		$this->SetXY(110,$this->GetY()+10);	
		$this->setFont('Times','B',12);
		$this->Cell(75,5,'TOTAL DEUDA : '.number_format($total,2,',','.') .' Bs.','RBLT',0,'C');			
	}
}

function CamFormFech($contenido) {
	if($contenido!=null) {
		$fech=explode('-', $contenido,3);
		$fech2=explode(' ', $fech[2],3);
		if(count($fech2) == 2) {
			return $fech2[0]."/".$fech[1]."/".$fech[0]." ".$fech2[1];
		} else {
			return $fech2[0]."/".$fech[1]."/".$fech[0];
		}
	}
}

//Creación del objeto de la clase heredada
$pdf=new PDF('L');
$pdf->AliasNbPages();
$pdf->AddPage();

//llamada a la funcion de crear tabla y fecha
$pdf->CrearTabla($antig);
$pdf->Output();
?>
