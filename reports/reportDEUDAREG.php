<?php
require('../reports/fpdf16/fpdf.php');

	//Conexion al servidor
	$servidor = "localhost";
	$base = "intranet_dev";
	$usuario = "postgres";
	$password = "$2011tyspostgresql%";   
	pg_pconnect("host=$servidor dbname=$base user=$usuario password=$password");
	
 	//Referencia
	$fechadesde = strtoupper(@$_POST["txtfechadesde"]);
	$fechahasta = strtoupper(@$_POST["txtfechahasta"]);	
	$region = $_REQUEST["region"]; 
	$anio = $_REQUEST["cboano"];
	
class PDF extends FPDF
{
	//Cabecera de página
	function Header()
	{		
		//fecha
		$this->SetY(20);
		$this->SetFont('Arial','I',10);
		$this->Cell(0,8,'Fecha: '.date('d/m/Y h:i A'),0,0,'L');
		//Logo
		$this->Image('../imagenes/encabezado_gris.jpg',10,8,270,12);
		//Fuente
		$this->SetFont('Arial','B',15);
		//Movernos a la derecha
		$this->SetY(16);
		$this->SetX(96);
		//Título		
		$this->Cell(20,25,utf8_decode('Reporte de Deuda por Región'),0,0,'C');
		
		//Referencia
		$fechadesde = strtoupper(@$_POST["txtfechadesde"]);
		$fechahasta = strtoupper(@$_POST["txtfechahasta"]);	
		$clinica = $_REQUEST["clinica"];
		$region = $_REQUEST["region"];
		$anio = $_REQUEST['cboano'];
		
		$fechad = CamFormFech($fechadesde);
		$fechah = CamFormFech($fechahasta);	
		
		$consultarcli = "select * from siscam.clinicas where siscam.clinicas.cli_rif = '$clinica'";
		$querycli = pg_query($consultarcli);
		$rowr = pg_fetch_object($querycli,0); 
		$this->SetFont('Times','B',10);
		$this->SetY(26);
		$this->SetX(30);
		if($fechadesde != NULL and $fechahasta != NULL)
		{		
		if(!$rowr->cli_nombre)
			$this->Cell(30,25,utf8_decode('Región : TODOS'),0);
		else
			$this->Cell(30,25,utf8_decode('Región : '.substr($region,2,1)),0);
			$this->SetX(120);
			$this->Cell(30,25,utf8_decode('DESDE:  '.$fechad.' HASTA: '.$fechah),0);
			$this->Ln();
		}elseif($anio != '0')
		{
		if(!$rowr->cli_nombre)
			$this->Cell(30,25,utf8_decode('Región : TODOS'),0);
		else			
			$this->Cell(30,25,utf8_decode('Región : '.substr($region,2,1)),0);
			$this->SetX(120);
			$this->Cell(30,25,utf8_decode('AÑO : '.$anio),0);
			$this->Ln();	
		}
		
		//Subtítulos
		$this->SetY(51);
		$this->SetFont('Times','B',9);
		$this->Cell(70,5,utf8_decode('Estado.'),0,'C');
		$this->Cell(70,5,utf8_decode('Región'),0,'C');
		$this->Cell(100,5,utf8_decode('Deuda'),0,'C');			
		$this->Line(10,56,280,56);
		$this->Ln();
	}

	//Numero de Página
	function Footer()
	{
		//Posición: a la derecha
		$this->SetY(19);
		//Arial italic 8
		$this->SetFont('Arial','I',10);
		//Número de página
		$this->Cell(0,10,utf8_decode('Página '.$this->PageNo().'/{nb}'),0,0,'R');
	}
	
	//Tabla del Reporte
	function CrearTabla($fechadesde,$fechahasta,$region,$anio)
	{				
		//Anchuras de las columnas
		$this->SetY(49);
		$this->SetFont('Times','',9);
		$w=array(22,22,22,22,55,25);
		$this->Ln(8);
		
		//Datos	
		if($fechadesde != NULL and $fechahasta != NULL)
		{
			if($region != 'TODOS')
			{
				$consulta = "SELECT
									B.esta_estado,
									B.esta_regionp,
									sum(D.fac_monto) as monto									
							 FROM 
							 		siscam.autorizaciones A, 
									siscam.estado B, 
									siscam.clinicas C, 
									siscam.factura D,
									siscam.bitacora F, 
									siscam.estudiantes G
							 WHERE 
							 		A.cli_rif = C.cli_rif and
		      						C.esta_id = B.esta_id and
		      						A.est_id = G.est_id   and
		      						A.aut_cartaaval = F.aut_cartaaval and
		      						A.aut_cartaaval = D.aut_cartaaval and
									A.aut_estatus <> 'PDO' and
									A.aut_modalidad <> 'ESPECIAL' and
		      						D.fechaing >= '$fechadesde' and 
									D.fechaing <= '$fechahasta' and 
		      						B.esta_regionp = '$region' 
							 GROUP BY B.esta_estado, B.esta_regionp
							 ORDER BY B.esta_estado";	
			}
			else
			{
				$consulta = "SELECT
									B.esta_estado,
									B.esta_regionp,
									sum(D.fac_monto) as monto									
							 FROM 
							 		siscam.autorizaciones A, 
									siscam.estado B, 
									siscam.clinicas C, 
									siscam.factura D,
									siscam.bitacora F, 
									siscam.estudiantes G
							 WHERE 
							 		A.cli_rif = C.cli_rif and
		      						C.esta_id = B.esta_id and
		      						A.est_id = G.est_id   and
		      						A.aut_cartaaval = F.aut_cartaaval and
		      						A.aut_cartaaval = D.aut_cartaaval and
								A.aut_modalidad <> 'ESPECIAL' and
		      						D.fechaing >= '$fechadesde' and 
								D.fechaing <= '$fechahasta' 
							 GROUP BY B.esta_estado, B.esta_regionp
							 ORDER BY B.esta_regionp, B.esta_estado";
			}
		}elseif($anio != '0')
		{
			$fecini = $anio.'-01-01';
			$fechas = $anio.'-12-31';
			$cartaini = $anio.'00000';
			$cartafin = $anio.'99999';
			if($region != 'TODOS') { $aux = " and D.esta_regionp = '$region' " ; }
			$consulta = "SELECT		D.esta_estado,
									D.esta_regionp,
									sum(A.fac_monto) as monto									
							 FROM 
							 		siscam.factura A, 
									siscam.autorizaciones B, 
									siscam.clinicas C, 
									siscam.estado D, 
									siscam.estudiantes E
							 WHERE 
							 		(A.fechaing >= '$fecini' and A.fechaing <= '$fechas' and A.aut_cartaaval <= $cartafin 
									or (A.fechaing <= '$fechas' and A.fechaing <> '1900-01-01' and A.aut_cartaaval >= $cartaini 
									and A.aut_cartaaval <= $cartafin))
									and A.aut_cartaaval = B.aut_cartaaval
									and B.cli_rif = C.cli_rif
									and C.esta_id = D.esta_id
									and B.est_id = E.est_id
									and B.aut_modalidad <> 'ESPECIAL'						
									and B.aut_Estatus <> 'PDO'
									$aux
							 GROUP BY D.esta_estado, D.esta_regionp 
							 ORDER BY D.esta_regionp, D.esta_estado";	
		}
		$queryfec = pg_query($consulta);
		while($rowr = pg_fetch_object($queryfec))
		{ 						
			$this->Cell(70,5,utf8_decode($rowr->esta_estado),0,0,'L');			
			$this->Cell(70,5,utf8_decode(substr($rowr->esta_regionp,8,1)),0,0,'L');
			$this->Cell(20,5,utf8_decode($rowr->monto),0,0,'R');
			$this->Ln();	
			$total += $rowr->monto;			
		}
		$this->SetXY(110,$this->GetY()+10);	
		$this->setFont('Times','B',12);
		$this->Cell(75,5,'TOTAL DEUDA : '.number_format($total,2,',','.') .' Bs.','RBLT',0,'C');			
	}
}

function CamFormFech($contenido) {
	if($contenido!=null) {
		$fech=explode('-', $contenido,3);
		$fech2=explode(' ', $fech[2],3);
		if(count($fech2) == 2) {
			return $fech2[0]."/".$fech[1]."/".$fech[0]." ".$fech2[1];
		} else {
			return $fech2[0]."/".$fech[1]."/".$fech[0];
		}
	}
}

//Creación del objeto de la clase heredada
$pdf=new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();

//llamada a la funcion de crear tabla y fecha
$pdf->CrearTabla($fechadesde,$fechahasta,$region,$anio);
$pdf->Output();




?>
