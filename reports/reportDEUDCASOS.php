<?php
require('../reports/fpdf16/fpdf.php');

	//Conexion al servidor
	$servidor = "localhost";
	$base = "intranet_dev";
	$usuario = "postgres";
	$password = "$2011tyspostgresql%";   
	pg_pconnect("host=$servidor dbname=$base user=$usuario password=$password");
	
 	//Referencia
	$fechadesde = strtoupper(@$_POST["hdnfechadesde"]);
	$fechahasta = strtoupper(@$_POST["hdnfechahasta"]);	
	$rif = $_REQUEST["hdnrif"];
	$anio = $_REQUEST["hdnanio"];
	
class PDF extends FPDF
{
	//Cabecera de página
	function Header()
	{		
		//fecha
		$this->SetY(20);
		$this->SetFont('Arial','I',10);
		$this->Cell(0,8,'Fecha: '.date('d/m/Y h:i A'),0,0,'L');
		//Logo
		$this->Image('../imagenes/encabezado_gris.jpg',10,8,270,12);
		//Fuente
		$this->SetFont('Arial','B',15);
		//Movernos a la derecha
		$this->SetY(16);
		$this->SetX(96);
		//Título		
		$this->Cell(85,25,utf8_decode('Reporte de Casos Deuda por Clinica'),0,0,'C');
		
		//Referencia
		$fechadesde = strtoupper(@$_POST["hdnfechadesde"]);
		$fechahasta = strtoupper(@$_POST["hdnfechahasta"]);	
		$rif = $_REQUEST["hdnrif"];
		$anio = $_REQUEST["hdnanio"];
		$fechad = CamFormFech($fechadesde);
		$fechah = CamFormFech($fechahasta);	
		
		
		$consultarfec = "select * from siscam.clinicas where siscam.clinicas.cli_rif = '$rif'";
		$queryfec = pg_query($consultarfec);
		$rowr = pg_fetch_object($queryfec); 
		$this->SetFont('Times','B',10);
		$this->SetY(26);
		$this->SetX(30);
		$this->Cell(30,25,utf8_decode('CLINICA : '.$rowr->cli_nombre),0);
		$this->SetX(200);
		if($fechadesde != NULL and $fechahasta != NULL)
		{
		$this->Cell(30,25,utf8_decode('DESDE:  '.$fechad.' HASTA: '.$fechah),0);
		$this->Ln();
		}
		else
		{
		$this->Cell(30,25,utf8_decode('Año : '.$anio),0);
		$this->Ln();		
		}
		//Subtítulos
		$this->SetY(51);
		$this->SetFont('Times','B',9);
		$this->Cell(20,5,utf8_decode('Carta'),0);
		$this->Cell(20,5,utf8_decode('Cedula '),0);
        $this->Cell(60,5,utf8_decode('Estudiante'),0,0,'C');
		$this->Cell(40,5,utf8_decode('Fecha Ingreso'),0,0,'C');		
		$this->Cell(40,5,utf8_decode('Fecha Emision'),0,0,'C');					
		$this->Cell(35,5,utf8_decode('Nro Factura'),0,0,'C');	
		$this->Cell(35,5,utf8_decode('Monto Factura'),0,0,'C');				
		$this->Line(10,56,280,56);
		$this->Ln();
	}

	//Numero de Página
	function Footer()
	{
		//Posición: a la derecha
		$this->SetY(19);
		//Arial italic 8
		$this->SetFont('Arial','I',10);
		//Número de página
		$this->Cell(0,10,utf8_decode('Página '.$this->PageNo().'/{nb}'),0,0,'R');
	}
	
	//Tabla del Reporte
	function CrearTabla($fechadesde,$fechahasta,$rif,$anio)
	{				
		//Anchuras de las columnas
		$this->SetY(49);
		$this->SetFont('Times','',9);
		$w=array(22,22,22,22,55,25);
		$this->Ln(8);		
		if($anio == '0')
		{
					$consulta = "SELECT	A.aut_cartaaval as carta,
								G.est_id as cedula,
								G.est_pnombre as nombre,
								G.est_papellido as apellido,
								D.fechaing as ingreso,
								D.fac_fecha as fecemi,
								D.fac_numero as numero,
								D.fac_monto	as monto
							 FROM 
							 	siscam.autorizaciones A, 
								siscam.estado B, 
								siscam.clinicas C, 
								siscam.factura D,								
								siscam.estudiantes G
							 WHERE 
							 	A.cli_rif = C.cli_rif and
		      					C.esta_id = B.esta_id and
		      					A.est_id = G.est_id   and		      					
		      					A.aut_cartaaval = D.aut_cartaaval and
								A.aut_estatus <> 'PDO' and
								A.aut_modalidad <> 'ESPECIAL' and
		      					D.fechaing >= '$fechadesde' and 
								D.fechaing <= '$fechahasta' and 
		      					C.cli_rif = '$rif' 							
							 ORDER BY D.fechaing";				
		}else
		{
					$fecini = $anio.'-01-01';
					$fechas = $anio.'-12-31';
					$cartaini = $anio.'00000';
					$cartafin = $anio.'99999';
					if($rif != 'TODOS') { $aux = " and C.cli_rif = '$rif' " ; }
						$consulta = "SELECT	B.aut_cartaaval as carta, 
										E.est_id as cedula,
										E.est_pnombre as nombre,									 
										E.est_papellido as apellido,
										A.fechaing as ingreso,
										A.fac_fecha as fecemi,
										A.fac_numero as numero,										
										A.fac_monto as monto									
							 FROM 
							 		siscam.factura A, 
									siscam.autorizaciones B, 
									siscam.clinicas C, 
									siscam.estado D, 
									siscam.estudiantes E
							 WHERE 
							 		(A.fechaing >= '$fecini' and A.fechaing <= '$fechas' and A.aut_cartaaval <= $cartafin 
									or (A.fechaing <= '$fechas' and A.fechaing <> '1900-01-01' and A.aut_cartaaval >= $cartaini 
									and A.aut_cartaaval <= $cartafin))
									and A.aut_cartaaval = B.aut_cartaaval
									and B.cli_rif = C.cli_rif
									and C.esta_id = D.esta_id
									and B.est_id = E.est_id
									and B.aut_modalidad <> 'ESPECIAL'						
									and B.aut_Estatus <> 'PDO'
									$aux							 
							 ORDER BY D.esta_regionp, D.esta_estado, c.cli_nombre";		
		}
		
		$queryfec = pg_query($consulta);
		while($rowr = pg_fetch_object($queryfec))
		{ 
			$this->Cell(20,5,utf8_decode($rowr->carta),0);
			$this->Cell(20,5,utf8_decode($rowr->cedula),0);			
			$this->Cell(60,5,utf8_decode($rowr->nombre.' '.$rowr->apellido),0,0,'C');			
			$this->Cell(40,5,utf8_decode($rowr->ingreso),0,0,'C');
			$this->Cell(40,5,utf8_decode($rowr->fecemi),0,0,'C');
			$this->Cell(35,5,utf8_decode($rowr->numero),0,0,'C');
			$this->Cell(35,5,utf8_decode($rowr->monto),0,0,'R');
			$this->Ln();	
			$total += $rowr->monto;			
		}
		$this->SetXY(110,$this->GetY()+10);	
		$this->setFont('Times','B',12);
		$this->Cell(75,5,'TOTAL DEUDA : '.number_format($total,2,',','.') .' Bs.','RBLT',0,'C');			
	}
}

function CamFormFech($contenido) {
	if($contenido!=null) {
		$fech=explode('-', $contenido,3);
		$fech2=explode(' ', $fech[2],3);
		if(count($fech2) == 2) {
			return $fech2[0]."/".$fech[1]."/".$fech[0]." ".$fech2[1];
		} else {
			return $fech2[0]."/".$fech[1]."/".$fech[0];
		}
	}
}

//Creación del objeto de la clase heredada
$pdf=new PDF('L');
$pdf->AliasNbPages();
$pdf->AddPage();

//llamada a la funcion de crear tabla y fecha
$pdf->CrearTabla($fechadesde,$fechahasta,$rif,$anio);
$pdf->Output();




?>
