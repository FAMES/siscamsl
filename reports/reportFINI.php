<?php


require('../reports/fpdf16/fpdf.php');

	//Conexion al servidor
	$servidor = "localhost";
	$base = "intranet_dev";
	$usuario = "postgres";
	$password = "$2011tyspostgresql%";   
	pg_pconnect("host=$servidor dbname=$base user=$usuario password=$password");
	
	//Arreglo de los datos seleccionados con los checkbox
			/*$array = $_POST["enviar"];
			$valores = split(",",$array);
			$cont=cou*/

	$relacion = $_POST["relacion"];
	
	/*$consulta = "select * from siscam.vst_autorizaciones where siscam.vst_autorizaciones.aut_cartaaval = ".$carta;
		$query = pg_query($consulta);
		$rowr = pg_fetch_object($query,0);
		$modalidad = $rowr->aut_modalidad;*/
	
class PDF extends FPDF
{
	//Cabecera de página
	function Header()
	{	
		//fecha
		$this->SetY(28);
		$this->SetX(8);
		$this->SetFont('Arial','B',12);
		$this->MultiCell(188, 6,'FAMES: '.date('d/m/Y h:i A'), 0, "J");
		//Logo
		$this->Image('../imagenes/encabezado_gris.jpg',13,8,180,12);
		//Fuente
		$this->SetFont('Arial','B',15);
		//Movernos a la derecha
		$this->SetY(16);
		$this->SetX(96);
		//Título		
		$this->Cell(30,25,utf8_decode('Reporte de Finiquitos'),0,0,'C');
	}

	//Numero de Página
	function Footer()
	{
		//Posición: a la derecha
		$this->SetY(17);
		//Arial italic 8
		$this->SetFont('Arial','I',8);
		//Número de página
		$this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'R');
	}

	
	//Tabla del Reporte
		
	function Crearfiniquito($relacion)
	{				
	
	// QUERY PARA TRAERNOS LOS DATOS DE LA CARTA AVAL
	
		$consulta = "select * from siscam.vst_aut_cli_re_esp where siscam.vst_aut_cli_re_esp.re_id = ".$relacion;
		$query = pg_query($consulta);
		$rowr = pg_fetch_object($query,0);
		$totalcan = number_format($rowr->re_totalcan,2,',','.');
		
		
		
		//$montop = number_format($rowr->aut_montop,2,',','.');
		
		$this->SetY(36);
		$this->SetX(58);
		$this->SetFont('Arial','B',12);
			
		$this->SetY(50);
		$this->SetX(8);
		$this->SetFont('Arial','I',12);
		$this->SetFillColor(192,192,192);
		$this->MultiCell(188, 6,utf8_decode('DATOS RELACION'), 1, "C",true);
		$this->SetY(56);
		$this->SetX(8);
		$this->SetFillColor(255,255,255);
		$this->SetFont('Arial','B',12);
		$this->Cell(94,5,utf8_decode('Nro. Relación: '),1,0,'R',1);
		$this->SetFont('Arial','I',10);
		$this->Cell(94,5,$relacion,1,0,'L',1);
		$this->SetY(61);
		$this->SetX(8);
		$this->SetFillColor(255,255,255);
		$this->SetFont('Arial','B',12);
		$this->Cell(94,5,utf8_decode('Rif Clinica: '),1,0,'R',1);
		$this->SetFont('Arial','I',10);
		$this->Cell(94,5,$rowr->cli_rif,1,0,'L',1);
		$this->SetY(66);
		$this->SetX(8);
		$this->SetFillColor(255,255,255);
		$this->SetFont('Arial','B',12);
		$this->Cell(94,5,utf8_decode('Razón Social: '),1,0,'R',1);
		$this->SetFont('Arial','I',10);
		$this->Cell(94,5,$rowr->cli_nombre,1,0,'L',1);
		$this->SetY(71);
		$this->SetX(8);
		$this->SetFillColor(255,255,255);
		$this->SetFont('Arial','B',12);
		$this->Cell(94,5,utf8_decode('Estado: '),1,0,'R',1);
		$this->SetFont('Arial','I',10);
		$this->Cell(94,5,$rowr->esta_estado,1,0,'L',1);
		$this->SetY(76);
		$this->SetX(8);
		$this->SetFillColor(255,255,255);
		$this->SetFont('Arial','B',12);
		$this->Cell(94,5,utf8_decode('Dirección: '),1,0,'R',1);
		$this->SetFont('Arial','I',10);
		$this->Cell(94,5,$rowr->cli_direccion,1,0,'L',1);
		$this->SetY(81);
		$this->SetX(8);
		$this->SetFillColor(255,255,255);
		$this->SetFont('Arial','B',12);
		$this->Cell(94,5,utf8_decode('Telefono: '),1,0,'R',1);
		$this->SetFont('Arial','I',10);
		$this->Cell(94,5,$rowr->cli_telefono,1,0,'L',1);
		$this->SetY(86);
		$this->SetX(8);
		$this->SetFillColor(255,255,255);
		$this->SetFont('Arial','B',12);
		$this->Cell(94,5,utf8_decode('Convenio: '),1,0,'R',1);
		$this->SetFont('Arial','I',10);
		$this->Cell(94,5,$rowr->cli_convenio,1,0,'L',1);
		$this->SetY(91);
		$this->SetX(8);
		$this->SetFillColor(255,255,255);
		$this->SetFont('Arial','B',12);
		$this->Cell(94,5,utf8_decode('Banco: '),1,0,'R',1);
		$this->SetFont('Arial','I',10);
		$this->Cell(94,5,$rowr->cli_banco,1,0,'L',1);
		$this->SetY(96);
		$this->SetX(8);
		$this->SetFillColor(255,255,255);
		$this->SetFont('Arial','B',12);
		$this->Cell(94,5,utf8_decode('Cuenta Bancaria: '),1,0,'R',1);
		$this->SetFont('Arial','I',10);
		$this->Cell(94,5,$rowr->cli_cuentaban,1,0,'L',1);
		$this->SetY(101);
		$this->SetX(8);
		$this->SetFillColor(255,255,255);
		$this->SetFont('Arial','B',12);
		$this->Cell(94,5,utf8_decode('Total Cancelado: '),1,0,'R',1);
		$this->SetFont('Arial','I',10);
		$this->Cell(94,5,$totalcan,1,0,'L',1);
		$this->SetY(106);
		$this->SetX(8);
		$this->SetFillColor(255,255,255);
		$this->SetFont('Arial','B',12);
		$this->Cell(94,5,utf8_decode('Nro. Cheque: '),1,0,'R',1);
		$this->SetFont('Arial','I',10);
		$this->Cell(94,5,$rowr->re_nrocheq,1,0,'L',1);
		$this->SetY(111);
		$this->SetX(8);
		$this->SetFillColor(255,255,255);
		$this->SetFont('Arial','B',12);
		$this->Cell(94,5,utf8_decode('Fecha Pago Cheque: '),1,0,'R',1);
		$this->SetFont('Arial','I',10);
		$this->Cell(94,5,$rowr->re_fechapagocheq,1,0,'L',1);
		
		
		
		
		$this->SetY(126);
		$this->SetX(8);
		$this->SetFillColor(192,192,192);
		$this->SetFont('Arial','B',12);
		$this->Cell(30,8,utf8_decode('Carta Aval '),1,0,'C',1);
		$this->Cell(26,8,utf8_decode('Cedula '),1,0,'C',1);
		$this->Cell(28,8,utf8_decode('Nº Factura '),1,0,'C',1);		
		$this->Cell(42,8,utf8_decode('Monto Facturado '),1,0,'C',1);
		$this->Cell(22,8,utf8_decode('Tipo '),1,0,'C',1);
		$this->Cell(42,8,utf8_decode('Monto Autorizado '),1,0,'C',1);
		$y = 133;
		$x = 8;
	
		//	query para traernos el usuario
		
		//$consultafin = "select * from siscam.vst_aut_fac_re_fin where siscam.vst_aut_fac_re_fin.re_id = ".$relacion;
		//$query = pg_query($consultafin);
		//$rowfin = pg_fetch_object($query,0);
		$consulta_cartas = "select * from siscam.vst_aut_re where siscam.vst_aut_re.re_id = ".$relacion ." ORDER BY AUT_NCONTROL";
		$query2 = pg_query($consulta_cartas);
		//$resultado = 'AMPLIACION';
		while($rowcarta = pg_fetch_object($query2)){
		$contador++;
		$this->SetY($y);
		$this->SetX($x);
		$this->SetFillColor(255,255,255);
		$this->SetFont('Arial','I',10);
		$this->Cell(30,5,utf8_decode($rowcarta->aut_cartaaval),1,0,'C',1);
		//Consulto numero de factura, monto de factura y cedula de estudiante por cada estudiante
		$consultafin = "select * from siscam.vst_aut_fac_re_fin where siscam.vst_aut_fac_re_fin.aut_cartaaval = ".$rowcarta->aut_cartaaval;
		$query = pg_query($consultafin);
		$rowfin = pg_fetch_object($query,0);
	if($rowcarta->aut_tipo == 'NUEVA' || $rowcarta->aut_tipo == 'EXTENSION')
	{	
		//$rowfin = pg_fetch_object($query,0);
		$this->Cell(26,5,utf8_decode($rowfin->est_id),1,0,'C',1);
		$this->Cell(28,5,utf8_decode($rowfin->fac_numero),1,0,'C',1);
		$this->Cell(42,5,number_format($rowfin->fac_monto,2,',','.'),1,0,'C',1);
		$this->Cell(22,5,utf8_decode($rowfin->aut_tipo),1,0,'C',1);
	}
	else 
	{
		$sqlaux = "select A.fac_numero, A.fac_monto, B.est_id from siscam.factura A, siscam.autorizaciones B where A.fac_cartaaso like '%$rowcarta->aut_cartaaval' and A.aut_cartaaval = B.aut_cartaaval";
		$qryaux = pg_query($sqlaux);
		$rowaux = pg_fetch_object($qryaux, 0);
		$this->Cell(26,5,utf8_decode($rowaux->est_id),1,0,'C',1);
		$this->Cell(28,5,utf8_decode($rowaux->fac_numero),1,0,'C',1);
		$this->Cell(42,5,number_format(0.00),1,0,'C',1);
		$this->Cell(22,5,utf8_decode("AMPLIACION"),1,0,'C',1);
	}
		//$this->Cell(26,5,utf8_decode($rowfin->est_id),1,0,'C',1);
		
		
		//$porcentaje = ($row->pat_majust / $row->bar_baremo) * 100;  
		//$this->Cell(42,5,number_format($rowfin->fac_monto,2,',','.'),1,0,'C',1);
		$this->Cell(42,5,number_format($rowcarta->aut_montoa,2,',','.'),1,0,'C',1);
		$y = $y + 5;
		if($contador == 25) { $this->AddPage(); $y = 39; $x=12; $s=0; $contador=0; }
		}
		

		
		//muestra totales del reporte
		$consulta2 = "select sum(fac_monto_exo) as exonerado, 
							  re_total, 
							  re_islr, 
							  re_riam, 
							  re_respso, 
							  re_totalcan 
				from siscam.vst_aut_fac_re_fin where siscam.vst_aut_fac_re_fin.re_id = $relacion 
					group by re_total, re_islr, re_riam, re_respso, re_totalcan";
		$query2 = pg_query($consulta2);
		$row2 = pg_fetch_object($query2);
		$this->Ln(4);
		$this->SetY(240);
		$this->SetX(110);
		$this->Cell(60,5,'TOTAL: ','LT',0);
		$this->SetX(170);
		$this->Cell(25,5,number_format($row2->re_total,2,',','.'),'TR',0);
		$this->Ln();
		$this->SetX(110);
		$this->Cell(60,5,'MONTO EXONERADO: ','L',0);
		$this->SetX(170);
		$this->Cell(25,5,number_format($row2->exonerado,2,',','.'),'R',0);
		$this->Ln();
		$this->SetX(110);
		$this->Cell(15,5,'I.S.L.R. ('.$i.'%): ','L',0);
		$this->SetX(170);
		$this->Cell(25,5,number_format($row2->re_islr,2,',','.'),'R',0);		
		$this->Ln();
		$this->SetX(110);
		$this->Cell(60,5,'IMPUESTO DE TIMBRE FISCAL: ','L',0);
		$this->SetX(170);
		$this->Cell(25,5,number_format($row2->re_riam,2,',','.'),'R',0);
		$this->Ln();
		$this->SetX(110);
		$this->Cell(60,5,'RESPONSABILIDAD SOCIAL (3%): ','L',0);
		$this->SetX(170);
		$this->Cell(25,5,number_format($row2->re_respso,2,',','.'),'R',0);
		$this->Ln();
		$this->SetX(110);
		$this->Cell(60,5,'TOTAL A CANCELAR: ','LB',0);
		$this->SetX(170);
		$this->Cell(25,5,number_format($row2->re_totalcan,2,',','.'),'BR',0);
		
		
	}
}

$pdf=new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();


$pdf->Crearfiniquito($relacion);
$pdf->Output();
?>
