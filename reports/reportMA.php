<?php
require('../reports/fpdf16/fpdf.php');

	//VISTA UTILIZADA ANTERIOR vst_aut_relacion
	//Conexion al servidor
	$servidor = "localhost";
	$base = "intranet_dev";
	$usuario = "postgres";
	$password = "$2011tyspostgresql%";   
	pg_pconnect("host=$servidor dbname=$base user=$usuario password=$password");
	
	//Datos recibidos por POST
	$array = $_POST["array"];
	$valores = split(",",$array);
	$cont=count($valores);
	
class PDF extends FPDF
{
	//Cabecera de página
	function Header()
	{		
		//fecha
		$this->SetY(20);
		$this->SetFont('Arial','I',10);
		$this->Cell(0,8,'Fecha: '.date('d/m/Y h:i A'),0,0,'L');
		//Logo
		$this->Image('../imagenes/encabezado_gris.jpg',10,8,190,12);
		//Fuente
		$this->SetFont('Arial','B',15);
		//Movernos a la derecha
		$this->SetY(16);
		$this->SetX(96);
		//Título		
		$this->Cell(30,25,utf8_decode('Maestro de Relación de Casos Pagados'),0,0,'C');		
			
		//Subtítulos
		$this->SetY(43);
		$this->SetFont('Times','B',9);
        $this->Cell(27,5,utf8_decode('N. Relación'),0);		
		$this->Cell(25,5,utf8_decode('RIF./Cedula'),0);
        $this->Cell(110,5,utf8_decode('Clínica/Estudiante'),0);
		$this->Cell(22,5,utf8_decode('Monto'),0);
		$this->Line(10,48,195,48);
		$this->Ln();
	}

	//Numero de Página
	function Footer()
	{
		//Posición: a la derecha
		$this->SetY(19);
		//Arial italic 8
		$this->SetFont('Arial','I',10);
		//Número de página
		$this->Cell(0,10,utf8_decode('Página '.$this->PageNo().'/{nb}'),0,0,'R');
	}
	
	//Tabla del Reporte
	function CrearTabla($cont,$valores)
	{		
		//Anchuras de las columnas
		$this->SetY(41);
		$this->SetFont('Arial','',10);
		$w=array(27,25,110,22);
		$this->Ln(8);
		
		//Datos	
		for ($i=0;$i<=$cont;$i++) {
			$consulta = "select distinct(siscam.vst_aut_fac_re.re_id), siscam.vst_aut_fac_re.cli_rif, siscam.vst_aut_fac_re.cli_nombre, siscam.vst_aut_fac_re.re_totalcan, siscam.vst_aut_fac_re.re_fecha from siscam.vst_aut_fac_re
 where cast(siscam.vst_aut_fac_re.re_id as varchar(12)) like cast('$valores[$i]' as varchar(12))";			
			//echo $consulta;
			$query = pg_query($consulta);
			while($row = pg_fetch_object($query)) { 
				$this->Cell($w[0],5,utf8_decode($row->re_id),0);				
				if($row->aut_modalidad == 'AYUDA SOLIDARIA' or $row->aut_modalidad == 'AYUDA SOLIDARIA ESPECIAL')
				{
					$consulta = "select siscam.vst_aut_fac_re.est_id, 
										siscam.vst_aut_fac_re.est_pnombre, 
										siscam.vst_aut_fac_re.est_papellido 
								 from 
								 		siscam.vst_aut_fac_re 
								 where 
								 	cast(siscam.vst_aut_fac_re.re_id as varchar(12)) like cast('$valores[$i]' as varchar(12))";
					$query2 = pg_query($consulta);
					while($row2 = pg_fetch_object($query2))
					{
					$this->Cell($w[1],5,utf8_decode($row2->est_id),0);
					$this->Cell($w[2],5,utf8_decode(substr($row2->est_pnombre .' '. $row2->est_papellido,0,55)),0);					
					}
				}
				else
				{
				$this->Cell($w[1],5,utf8_decode($row->cli_rif),0);
				$this->Cell($w[2],5,utf8_decode(substr($row->cli_nombre,0,55)),0);				
				}
				$this->Cell($w[3],5,utf8_decode(number_format($row->re_totalcan,2,',','.')),0);
				$suma=$suma+$row->re_totalcan;
				$this->Ln();
			} 
		}
		
		//muestra totales del reporte		
		$this->Ln(4);
		$this->SetX(140);
		$this->Cell(40,5,'TOTAL MONTO: ','LT',0);
		$this->SetX(170);
		$this->Cell(25,5,number_format($suma,2,',','.'),'TR',0);
		$this->Ln();
		$this->SetX(140);
		$this->Cell(40,5,'N. REGISTROS: ','LB',0);
		$this->SetX(170);
		$this->Cell(25,5,$cont,'BR',0);
	}
}

//Creación del objeto de la clase heredada
$pdf=new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();

//llamada a la funcion de crear tabla y fecha
$pdf->CrearTabla($cont,$valores);
$pdf->Output();

?>
