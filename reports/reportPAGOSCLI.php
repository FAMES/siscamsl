<?php
require('../reports/fpdf16/fpdf.php');

	//Conexion al servidor
	$servidor = "localhost";
	$base = "intranet_dev";
	$usuario = "postgres";
	$password = "$2011tyspostgresql%";   
	pg_pconnect("host=$servidor dbname=$base user=$usuario password=$password");
	
 	//Referencia
	$fechadesde = strtoupper(@$_POST["txtfechadesde"]);
	$fechahasta = strtoupper(@$_POST["txtfechahasta"]);
	$rif_clinica = strtoupper(@$_POST["clinicas"]);
	
class PDF extends FPDF
{
	//Cabecera de página
	function Header()
	{		
		//fecha
		$this->SetY(20);
		$this->SetFont('Arial','I',10);
		$this->Cell(0,8,'Fecha: '.date('d/m/Y h:i A'),0,0,'L');
		//Logo
		$this->Image('../imagenes/encabezado_gris.jpg',10,8,190,12);
		//Fuente
		$this->SetFont('Arial','B',15);
		//Movernos a la derecha
		$this->SetY(16);
		$this->SetX(96);
		//Título		
		$this->Cell(30,25,utf8_decode('Reporte de Pagos por Clinicas'),0,0,'C');
		
		//Referencia
		$fechadesde = strtoupper(@$_POST["txtfechadesde"]);
		$fechahasta = strtoupper(@$_POST["txtfechahasta"]);		
		
		if($fechadesde != NULL and $fechahasta != NULL)
		{
		$consultarfec = "select * from siscam.vst_aut_fac_re where siscam.vst_aut_fac_re.aut_fechaa >= '$fechadesde' and siscam.vst_aut_fac_re.aut_fechaa <= '$fechahasta'";
		$queryfec = pg_query($consultarfec);
		$rowr = pg_fetch_object($queryfec); 
		$this->SetFont('Times','B',10);
		$this->SetY(26);
		$this->SetX(78);
		$this->Cell(30,25,utf8_decode('DESDE:  '.$fechadesde.' HASTA: '.$fechahasta),0);
		$this->Ln();
		}
		//Subtítulos
		$this->SetY(51);
		$this->SetFont('Times','B',9);
		$this->Cell(22,5,utf8_decode('Rif Clinica'),0,'C');
		$this->Cell(100,5,utf8_decode('Clinica '),0);
        $this->Cell(26,5,utf8_decode('Numero de Pagos'),0);
		$this->Cell(32,5,utf8_decode('Monto Cancelado'),0);		
		$this->Line(10,56,195,56);
		$this->Ln();
	}

	//Numero de Página
	function Footer()
	{
		//Posición: a la derecha
		$this->SetY(19);
		//Arial italic 8
		$this->SetFont('Arial','I',10);
		//Número de página
		$this->Cell(0,10,utf8_decode('Página '.$this->PageNo().'/{nb}'),0,0,'R');
	}
	
	//Tabla del Reporte
	function CrearTabla($fechadesde,$fechahasta,$rif_clinica)
	{				
		//Anchuras de las columnas
		$this->SetY(49);
		$this->SetFont('Times','',9);
		$w=array(22,22,22,22,55,25);
		$this->Ln(8);
		
		//Datos	
		if($fechadesde != NULL and $fechahasta != NULL)
		{
			if($rif_clinica != NULL)
			{
				$consulta = "select count(distinct(r.re_id)) as cuenta, 
							 r.cli_rif, 
							 c.cli_nombre, 
							 sum(r.re_totalcan) as total 
					  from 
	 						siscam.relacion r, siscam.clinicas c
					  where 
					  		r.re_fecha between '$fechadesde' and '$fechahasta' and  
							r.cli_rif = '$rif_clinica' and
							r.cli_rif = c.cli_rif							
					  group by 
					  		r.cli_rif, c.cli_nombre";	
			}
			else
			{
				$consulta = "select count(distinct(r.re_id)) as cuenta, 
							 r.cli_rif, 
							 c.cli_nombre, 
							 sum(r.re_totalcan) as total 
					  from 
	 						siscam.relacion r, siscam.clinicas c
					  where 
					  		r.re_fecha between '$fechadesde' and '$fechahasta' and 
							r.cli_rif = c.cli_rif
					  group by 
					  		r.cli_rif, c.cli_nombre";
			}
		}
		elseif($rif_clinica != NULL)
		{
			$consulta = "select count(distinct(r.re_id)) as cuenta, 
							 r.cli_rif, 
							 c.cli_nombre, 
							 sum(r.re_totalcan) as total 
					  from 
	 						siscam.relacion r, siscam.clinicas c
					  where 					  		
							r.cli_rif = '$rif_clinica' and
							r.cli_rif = c.cli_rif
					  group by 
					  		r.cli_rif, c.cli_nombre";					
		}
		else
		{ 
			$fechadesde = '2011-01-01';
			$fechahasta = date('Y-m-d');
			$consulta = "select distinct(r.cli_rif), 
							r.cli_rif,
							c.cli_nombre, 
							sum(r.re_totalcan) as total,
							count(r.re_id) as cuenta
					   from 
					 		siscam.relacion r, siscam.clinicas c
					   where
							c.cli_rif = r.cli_rif and
							r.re_fecha between '$fechadesde' and '$fechahasta'
					   group by r.cli_rif, c.cli_nombre";
		}
		$queryfec = pg_query($consulta);
		while($rowr = pg_fetch_object($queryfec)) { 
			$this->Cell(22,5,utf8_decode($rowr->cli_rif),0);
			$this->Cell(100,5,utf8_decode(substr($rowr->cli_nombre,0,36)),0);			
			$this->Cell(25,5,utf8_decode($rowr->cuenta),0,0,'C');			
			$this->Cell(32,5,number_format(utf8_decode($rowr->total),2,',','.'),0,0,'C');
			$this->Ln();	
			$cuenta += $rowr->cuenta;
			$total += $rowr->total;
		}
		$this->SetY(180);
		$this->SetX(110);
		$this->Cell(85,5,utf8_decode('NRO DE PAGOS :'),'LTR',0);
		$this->SetX(140);
		$this->Cell(30,5,$cuenta,0);
		$this->Ln(4);
		$this->SetY(185);
		$this->SetX(110);
		$this->Cell(85,5,'TOTAL : ','LRB',0);
		$this->SetX(140);
		$this->Cell(30,5,number_format($total,2,',','.') .' Bs.',0);
	}
}

function CamFormFech($contenido) {
	if($contenido!=null) {
		$fech=explode('-', $contenido,3);
		$fech2=explode(' ', $fech[2],3);
		if(count($fech2) == 2) {
			return $fech2[0]."/".$fech[1]."/".$fech[0]." ".$fech2[1];
		} else {
			return $fech2[0]."/".$fech[1]."/".$fech[0];
		}
	}
}

//Creación del objeto de la clase heredada
$pdf=new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();

//llamada a la funcion de crear tabla y fecha
$pdf->CrearTabla($fechadesde,$fechahasta,$rif_clinica);
$pdf->Output();




?>
