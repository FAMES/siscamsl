<?php
require('../reports/fpdf16/fpdf.php');
	
	//Conexion al servidor
	$servidor = "localhost";
	$base = "intranet_dev";
	$usuario = "postgres";
	$password = "$2011tyspostgresql%";    
	pg_pconnect("host=$servidor dbname=$base user=$usuario password=$password");	
	$re = $_POST['hdnrelacion'];
	$i = $_POST['hdnimpuesto'];
	
class PDF extends FPDF
{
	//Cabecera de página
	function Header()
	{		
		//fecha
		$this->SetY(20);
		$this->SetFont('Arial','BI',12);
		$this->Cell(0,8,'Fecha: '.date('d/m/Y h:i A'),0,0,'L');
		//$this->Cell(0,8,'Fecha: 20/12/2013 ',0,0,'L');
		//$this->Cell(0,8,'Fecha: 19/01/2012');
		//Logo
		$this->Image('../imagenes/encabezado_gris.jpg',10,8,190,12);
		//Fuente
		$this->SetFont('Arial','B',15);
		//Movernos a la derecha
		$this->SetY(16);
		$this->SetX(96);
		//Título		
		$this->Cell(30,25,utf8_decode('Relación de casos a Pagar'),0,0,'C');
		
		//Referencia
		$re = $_POST['hdnrelacion']; //POST de la CLINICA
		$consultarif = "select * from siscam.vst_aut_fac_re where siscam.vst_aut_fac_re.re_id = $re";
		$queryrif = pg_query($consultarif);
		$rowr = pg_fetch_object($queryrif); 
		$this->SetY(36);
		$this->SetFont('Arial','B',11);
		$this->Cell(42,5,utf8_decode('N. RELACIÓN: '.$rowr->re_id),0);
		$this->Ln();
		$this->SetFont('Arial','BI',11);
		$this->Cell(70,5,utf8_decode('ESTADO: '.$rowr->esta_estado),0);
		$this->Cell(40,5,utf8_decode('RIF: '.$rowr->cli_rif),0);
		$this->MultiCell(80,5,utf8_decode('CLÍNICA: '.$rowr->cli_nombre),0);
		$this->Ln();
		
		//Subtítulos
		$this->SetY(51);
		$this->SetFont('Arial','B',11);
		$moda = $rowr->aut_modalidad;
		$facturaesp = $rowr->fac_numero;
		if ($moda=='ESPECIAL' and $facturaesp ==0){	
		$this->Cell(22,5,utf8_decode('Carta Aval'),0);
        $this->Cell(22,5,utf8_decode('N. Control'),0);
		$this->Cell(30,5,utf8_decode('N. Presupuesto'),0);
        $this->Cell(20,5,utf8_decode('Cédula'),0);
		$this->Cell(49,5,utf8_decode('Nombre y Apellido'),0);
		$this->Cell(30,5,utf8_decode('Universidad'),0);
		$this->Cell(22,5,utf8_decode('Monto Aut.'),0);
		$this->Line(10,56,205,56);
		$this->Ln();
		} else{
		$this->Cell(22,5,utf8_decode('Carta Aval'),0);
        $this->Cell(22,5,utf8_decode('N. Control'),0);
		$this->Cell(30,5,utf8_decode('N. Factura'),0);
        $this->Cell(20,5,utf8_decode('Cédula'),0);
		$this->Cell(49,5,utf8_decode('Nombre y Apellido'),0);
		$this->Cell(30,5,utf8_decode('Universidad'),0);
		$this->Cell(22,5,utf8_decode('Monto Pago'),0);
		$this->Line(10,56,205,56);
		$this->Ln();
		}
	}

	//Numero de Página
	function Footer()
	{
		//Posición: a la derecha
		$this->SetY(19);
		//Arial italic 8
		$this->SetFont('Arial','BI',11);
		//Número de página
		$this->Cell(0,10,utf8_decode('Página '.$this->PageNo().'/{nb}'),0,0,'R');
	}
	
	//Tabla del Reporte
	function CrearTabla($re,$i)
	{				
		//Anchuras de las columnas
		$this->SetY(49);
		$this->SetFont('Arial','B',11);
		$w=array(24,22,30,20,45,32,22);
		$this->Ln(8);
		$y=59;
		$x=32;
		$contador=0;
		//Datos		
		$consulta = "select * from siscam.vst_aut_fac_re where siscam.vst_aut_fac_re.re_id = $re";
		$query = pg_query($consulta);
		while($row = pg_fetch_object($query)) {
			$contador++;
			$moda = $row->aut_modalidad;
			$facturaesp = $row->fac_numero;
			if ($moda=='ESPECIAL' and $facturaesp == '0'){	
			$this->SetXY(10,$y+$s);
			$this->MultiCell($w[0],5,$row->fac_cartaaso,0);
			$this->SetXY($x,$y+$s);
			$this->Cell($w[1],5,$row->aut_ncontrol,0);
			$this->Cell($w[2],5,substr($row->aut_numerop,0,15),0);
			$this->Cell($w[3],5,$row->est_id,0);
			$this->Cell($w[4],5,utf8_decode(substr($row->est_pnombre.' '.$row->est_papellido,0,19)),0);
			$this->SetFont('Arial','B',9);
			$this->Cell($w[5],5,utf8_decode(substr($row->uni_nombre,0,17)),0);
			$this->SetFont('Arial','B',11);
			$this->SetXY(185,$y+$s);
			$this->Cell($w[6],5,number_format($row->fac_total_factura,2,',','.'),0);
			$s=$s+11;
			$this->Ln(8);
			} else {
			$this->SetXY(10,$y+$s);
			$this->MultiCell($w[0],5,$row->fac_cartaaso,0);
			$this->SetXY($x,$y+$s);
			$this->Cell($w[1],5,$row->aut_ncontrol,0);
			$this->Cell($w[2],5,substr($row->fac_numero,0,15),0);
			$this->Cell($w[3],5,$row->est_id,0);
			$this->Cell($w[4],5,utf8_decode(substr($row->est_pnombre.' '.$row->est_papellido,0,19)),0);
			$this->SetFont('Arial','B',9);
			$this->Cell($w[5],5,utf8_decode(substr($row->uni_nombre,0,17)),0);
			$this->SetFont('Arial','B',11);
			$this->SetXY(185,$y+$s);
			$this->Cell($w[6],5,number_format($row->fac_monto,2,',','.'),0);
			$s=$s+11;
			$this->Ln(8);			
			}			
			if($contador == 18) { $this->AddPage(); $y = 59; $x=32; $s=0; $contador=0; }
		}
		$consultatotal = "select count(aut_cartaaval) as cuenta from siscam.vst_aut_fac_re where siscam.vst_aut_fac_re.re_id = $re";
		$querytotal = pg_query($consultatotal);
		$row4 = pg_fetch_object($querytotal); 
		if($contador >= 17) { $this->AddPage(); $y = 59; $x=32; $s=0; }
		//muestra totales del reporte
		$consulta2 = "select sum(fac_monto_exo) as exonerado,
							 re_total as total_bruto,
							 re_islr as islr,
							 re_riam as riam,
							 re_respso as respso,
                             moniva as iva,
							 retiva as retiva,
							 re_totalcan as total_neto
							 from siscam.vst_aut_fac_re where siscam.vst_aut_fac_re.re_id = $re group by
							 re_total, re_islr, re_riam, re_respso, re_totalcan, moniva, retiva";
		$query2 = pg_query($consulta2);
		$row2 = pg_fetch_object($query2);
		$total_exonerado = $row2->exonerado;
		$mod = $row2->aut_modalidad;
		$facturaesp = $row2->fac_numero;
		if ($mod=='ESPECIAL' and $facturaesp == '0'){			
		$this->SetY(180);
		$this->SetX(10);
		$this->Cell(70,5,utf8_decode('Nro de Casos:'),'C',0);
		$this->SetX(38);
		$this->Cell(30,5,$row4->cuenta,0);
		$this->Ln(4);
		$this->SetY(180);
		$this->SetX(110);
		$this->Cell(70,5,'TOTAL: ','LT',0);
		$this->SetX(175);
		$this->Cell(30,5,number_format($row2->total_bruto,2,',','.'),'TR',0);
		$this->Ln();
		$this->SetX(110);
		$this->Cell(40,5,'MONTO EXONERADO: ','L',0);
		$this->SetX(175);
		$this->Cell(30,5,number_format($total_exonerado,2,',','.'),'R',0);
		$this->Ln();
		$this->SetX(110);
		$this->Cell(15,5,'I.S.L.R. ('.$i.'%): ','L',0);
		$this->SetX(175);
		$this->Cell(30,5,number_format($row2->islr,2,',','.'),'R',0);
		$this->Ln();
		$this->SetX(110);
		$this->Cell(30,5,'IMPUESTO DE TIMBRE FISCAL: ','L',0);
		$this->SetX(175);
		$this->Cell(30,5,number_format($row2->riam,2,',','.'),'R',0);
		$this->Ln();		
		$this->SetX(110);
		$this->Cell(30,5,'RESPONSABILIDAD SOCIAL (3%): ','L',0);
		$this->SetX(175);
		$this->Cell(30,5,number_format($row2->respso,2,',','.'),'R',0);
                if($row2->retiva != 0.00)
                {
		$this->Ln();
		$this->SetX(110);
		$this->Cell(30,5,'MONTO IVA : ','L',0);
		$this->SetX(175);
		$this->Cell(30,5,number_format($row2->iva,2,',','.'),'R',0);
        $this->Ln();
		$this->SetX(110);
		$this->Cell(30,5,'RETENCION IVA : ','L',0);
		$this->SetX(175);
		$this->Cell(30,5,number_format($row2->retiva,2,',','.'),'R',0);
                }
		$this->Ln();		
		$this->SetX(110);
		$this->Cell(70,5,'TOTAL A CANCELAR: ','LB',0);
		$this->SetX(175);
		$this->Cell(30,5,number_format($row2->total_neto,2,',','.'),'BR',0);
		//if($contador >= 15) { $this->AddPage(); $y = 59; $x=32; $s=0; }
		$this->SetY(260);
	 	$this->SetX(98);
		$this->Cell(70,5,'________________________________________________','C',0);
		$this->SetY(265);
		$this->SetX(98);		
		$this->Cell(30,5,utf8_decode('CONFORMADO POR: JEFE GESTIÓN ADMINISTRATIVA'),'C',0);
		$this->SetY(260);
		$this->SetX(8);
		$this->Cell(70,5,'_________________________________________','C',0);
		$this->SetY(265);
		$this->SetX(8);
		$this->Cell(30,5,utf8_decode('GENERADO POR: COORDINACIÓN DE PAGO'),'C',0);
	}else{
		
		
		$this->SetX(10);
		$this->Cell(70,5,utf8_decode('Nro de Casos:'),'C',0);
		$this->SetX(38);
		$this->Cell(30,5,$row4->cuenta,0);
		$this->Ln(4);
		$this->SetX(110);
		$this->Cell(70,5,'TOTAL: ','LT',0);
		$this->SetX(175);
		$this->Cell(30,5,number_format($row2->total_bruto,2,',','.'),'TR',0);
		$this->Ln();
		$this->SetX(110);
		$this->Cell(40,5,'MONTO EXONERADO: ','L',0);
		$this->SetX(175);
		$this->Cell(30,5,number_format($total_exonerado,2,',','.'),'R',0);
		$this->Ln();
		$this->SetX(110);
		$this->Cell(15,5,'I.S.L.R. ('.$i.'%): ','L',0);
		$this->SetX(175);
		$this->Cell(30,5,number_format($row2->islr,2,',','.'),'R',0);
		$this->Ln();
		$this->SetX(110);
		$this->Cell(30,5,'IMPUESTO DE TIMBRE FISCAL: ','L',0);
		$this->SetX(175);
		$this->Cell(30,5,number_format($row2->riam,2,',','.'),'R',0);
		$this->Ln();
		$this->SetX(110);
		$this->Cell(30,5,'RESPONSABILIDAD SOCIAL (3%): ','L',0);
		$this->SetX(175);
		$this->Cell(30,5,number_format($row2->respso,2,',','.'),'R',0);
                if($row2->retiva != 0.00)
                {
		$this->Ln();
		$this->SetX(110);
		$this->Cell(30,5,'MONTO IVA : ','L',0);
		$this->SetX(175);
		$this->Cell(30,5,number_format($row2->iva,2,',','.'),'R',0);			
        $this->Ln();
		$this->SetX(110);
		$this->Cell(30,5,'RETENCION IVA (75%) Bs. : ','L',0);
		$this->SetX(175);
		$this->Cell(30,5,number_format($row2->retiva,2,',','.'),'R',0);
                }
		$this->Ln();		
		$this->SetX(110);
		$this->Cell(70,5,'TOTAL A CANCELAR: ','LB',0);
		$this->SetX(175);
		$this->Cell(30,5,number_format($row2->total_neto,2,',','.'),'BR',0);
		//if($contador >= 15) { $this->AddPage(); $y = 59; $x=32; $s=0; }
		
		
		$this->SetY(260);
		$this->SetX(98);
		$this->Cell(70,5,'________________________________________________','C',0);
		$this->SetY(265);
		$this->SetX(98);
		$this->Cell(30,5,utf8_decode('CONFORMADO POR: JEFE GESTIÓN ADMINISTRATIVA'),'C',0);
		$this->SetY(260);
		$this->SetX(8);
		$this->Cell(70,5,'_________________________________________','C',0);
		$this->SetY(265);
		$this->SetX(8);
		$this->Cell(30,5,utf8_decode('GENERADO POR: COORDINACIÓN DE PAGO'),'C',0);
	}
			
	}
}


//Creación del objeto de la clase heredada
$pdf=new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
//$pdf->SetAutoPageBreak(0,0);'P','Legal'

//llamada a la funcion de crear tabla y fecha
$pdf->CrearTabla($re,$i);
$pdf->Output();

?>
