<?php
require('../reports/fpdf16/fpdf.php');

	//Conexion al servidor
	$servidor = "localhost";
	$base = "intranet_dev";
	$usuario = "postgres";
	$password = "$2011tyspostgresql%";   
	pg_pconnect("host=$servidor dbname=$base user=$usuario password=$password");
	
	
//Referencia

	 $cobtipo = @$_POST["cbotipo"];
    $txtcedula = @$_POST["txtcedula"];
	$txtcartaval = @$_POST["txtcartaval"];
	$estatus = @$_POST["estatus"];
	$txtfechadesde = strtoupper(@$_POST["txtfechadesde"]);
	$txtfechahasta = strtoupper(@$_POST["txtfechahasta"]);
	
class PDF extends FPDF
{


	//Cabecera de página
	function Header()
	{		
		//fecha
		$this->SetY(20);
		$this->SetFont('Arial','I',10);
		$this->Cell(0,8,'Fecha: '.date('d/m/Y h:i A'),0,0,'L');
		//Logo
		$this->Image('../imagenes/encabezado_gris3.jpg',10,8,273,14);
		//Fuente
		$this->SetFont('Arial','B',15);
		//Movernos a la derecha
		$this->SetY(20);
		$this->SetX(130);
		//Título		
		$this->Cell(35,25,utf8_decode('Reportes Recepcionados'),0,0,'C');
		

	}

	//Numero de Página
	function Footer()
	{
		//Posición: a la derecha
		$this->SetY(19);
		//Arial italic 8
		$this->SetFont('Arial','I',10);
		//Número de página
		$this->Cell(0,10,utf8_decode('Página '.$this->PageNo().'/{nb}'),0,0,'R');
	}
	
	//Tabla del Reporte
	function CrearTabla($cobtipo,$txtcedula,$txtcartaval,$estatus,$txtfechadesde,$txtfechahasta)
	{		
	
		$this->SetFont('Times','B',10);
		$this->SetY(30);
		$this->SetX(100);
		
		
		if($cobtipo=='ESPECIAL'){
			
			$this->Cell(30,25,utf8_decode(' Casos  Especiales Desde '.$txtfechadesde.' Hasta '.$txtfechahasta),0);
		
		}else{
			
		
		$this->Cell(30,25,utf8_decode(' Casos del '.$cobtipo.' Desde '.$txtfechadesde.' Hasta '.$txtfechahasta),0);
		
		}
		$this->Ln();
		
		//Anchuras de las columnas
		$this->SetY(49);
		$this->SetFont('Times','',9);
		//$w=array(22,22,22,22,55,25);
		$this->Ln(8);
		//Subtítulos
		$this->SetY(51);
		$this->SetFont('Times','B',9);
		$this->Cell(17,5,utf8_decode('Cédula'),0);
		$this->Cell(17,5,utf8_decode('Carta'),0);
		$this->Cell(28,5,utf8_decode('Fecha Autorización'),0);
		$this->Cell(19,5,utf8_decode('Rif'),0);
		$this->Cell(38,5,utf8_decode('Clinica'),0);
		$this->Cell(34,5,utf8_decode('Universidad'),0);
		$this->Cell(27,5,utf8_decode('Monto Autorizado'),0);
		$this->Cell(18,5,utf8_decode('Modalidad'),0);
		$this->Cell(12,5,utf8_decode('Estatus'),0);
		$this->Cell(24,5,utf8_decode('Tipo Recepcion'),0);
		$this->Cell(20,5,utf8_decode('Fech Factura'),0);
		$this->Cell(22,5,utf8_decode('Firma'),0);
		
		
		$this->Line(10,56,290,56);
		$this->Ln();
		
				if($cobtipo!='ESPECIAL'){
					
					$condi = " where siscam.vst_rec.aut_estatus = '$estatus' and siscam.vst_rec.esta_regionp = '$cobtipo' and siscam.vst_rec.bit_recfecha >= '$txtfechadesde' and siscam.vst_rec.bit_recfecha <= '$txtfechahasta' and vst_rec.aut_modalidad!='ESPECIAL'";
					
				}else{
					
				$condi = " where siscam.vst_rec.aut_estatus = '$estatus' and siscam.vst_rec.bit_recfecha >= '$txtfechadesde' and siscam.vst_rec.bit_recfecha <= '$txtfechahasta' and vst_rec.aut_modalidad='ESPECIAL'";
				
				}
					
					
					$consulta ="select * from siscam.vst_rec $condi order by siscam.vst_rec.est_id";
					
				
		$y=59;
		$x=32;		
		$contador=0;
		$query = pg_query($consulta);
		while($rowrs = pg_fetch_object($query)) { 
		$contador++;
			$this->SetXY(10,$y+$s);
			$this->Cell(17,5,utf8_decode($rowrs->est_id),0);
			$this->Cell(17,5,utf8_decode($rowrs->aut_cartaaval),0);
			$this->Cell(28,5,utf8_decode($rowrs->aut_fechaa),0);
			$this->Cell(19,5,utf8_decode($rowrs->cli_rif),0);
			$this->Cell(38,5,utf8_decode(substr($rowrs->cli_nombre,0,17)),0);
			$this->Cell(34,5,utf8_decode(substr($rowrs->uni_nombre,0,17)),0);
			$this->Cell(27,5,number_format($rowrs->aut_montoa,2,',','.'),0);
			$this->Cell(18,5,utf8_decode($rowrs->aut_modalidad),0);
			$this->Cell(12,5,utf8_decode($rowrs->aut_estatus),0);
			$this->Cell(24,5,utf8_decode($rowrs->bit_rectipo),0);
			$this->Cell(20,5,utf8_decode($rowrs->bit_fecrecfac),0);
			$this->Cell(22,5,utf8_decode('________________'),0);
			$s=$s+5;
			$this->Ln();
			
			if($contador == 23) { $this->AddPage(); $y = 53; $x=32; $s=0; $contador=0; }
			
		}
		
		//muestra totales del reporte
		
		if($cobtipo!='ESPECIAL'){
		
		$condi = " where siscam.vst_rec.aut_estatus = '$estatus' and siscam.vst_rec.esta_regionp = '$cobtipo' and siscam.vst_rec.bit_recfecha >= '$txtfechadesde' and siscam.vst_rec.bit_recfecha <= '$txtfechahasta' and vst_rec.aut_modalidad!='ESPECIAL'";
		
		}else{
			
			$condi = " where siscam.vst_rec.aut_estatus = '$estatus' and siscam.vst_rec.bit_recfecha >= '$txtfechadesde' and siscam.vst_rec.bit_recfecha <= '$txtfechahasta' and vst_rec.aut_modalidad='ESPECIAL'";
		
		}
				
					$consulta2 ="select count(siscam.vst_rec.aut_cartaaval) as conteo from siscam.vst_rec $condi";
		$query2 = pg_query($consulta2);
		$row2 = pg_fetch_object($query2);
		$this->Ln(1);
		$this->SetX(230);
		$this->Cell(40,5,utf8_decode('TOTAL CASOS:'),0);
		$this->SetX(260);
		$this->Cell(25,5,number_format($row2->conteo,0,',','.'),0);
		//$this->Ln();
		//$this->SetX(130);
		//$this->Cell(40,5,'TOTAL GENERAL: ','LB',0);
		//$this->SetX(170);
		//$this->Cell(25,5,number_format($montotal,2,',','.'),'BR',0);
		//$this->Ln();
		/*$this->SetX(130);
		$this->Cell(30,5,'IMPUESTO DEL 1*1000: ','L',0);
		$this->SetX(170);
		$this->Cell(25,5,number_format($row2->re_riam,2,',','.'),'R',0);
		$this->Ln();
		$this->SetX(130);
		$this->Cell(40,5,'TOTAL A CANCELAR: ','LB',0);
		$this->SetX(170);
		$this->Cell(25,5,number_format($row2->re_totalcan,2,',','.'),'BR',0);*/
		
		//////////////////////////////////////////////////////////
		//CODIGO EXTRA PARA GENERAR VARIAS PAGINAS DE PRUEBA    //
		//for($i=1;$i<=40;$i++)                                 //
		//$this->Cell(0,10,'Imprimiendo Linea de Prueba Numero '.$i,0,1); //
		//$this->Ln();                                          //
		//////////////////////////////////////////////////////////
		
		//Línea de cierre
		//$this->Cell(array_sum($w),0,'',0);
	}
}

//Creación del objeto de la clase heredada
$pdf=new PDF('L');
$pdf->AliasNbPages();
$pdf->AddPage();

//llamada a la funcion de crear tabla y fecha
$pdf->CrearTabla($cobtipo,$txtcedula,$txtcartaval,$estatus,$txtfechadesde,$txtfechahasta);
$pdf->Output();

?>
