<?php
require('../reports/fpdf16/fpdf.php');

	//Conexion al servidor
	$servidor = "localhost";
	$base = "intranet_dev";
	$usuario = "postgres";
	$password = "$2011tyspostgresql%";   
	pg_pconnect("host=$servidor dbname=$base user=$usuario password=$password");
	
 	//Referencia
	$fechadesde = strtoupper(@$_POST["txtfechadesde"]);
	$fechahasta = strtoupper(@$_POST["txtfechahasta"]);
	$rif_clinica = strtoupper(@$_POST["clinicas"]);	
	
class PDF extends FPDF
{
	//Cabecera de página
	function Header()
	{		
		//fecha
		$this->SetY(20);
		$this->SetFont('Arial','I',10);
		$this->Cell(0,8,'Fecha: '.date('d/m/Y h:i A'),0,0,'L');
		//Logo
		$this->Image('../imagenes/encabezado_gris.jpg',10,8,190,12);
		//Fuente
		$this->SetFont('Arial','B',15);
		//Movernos a la derecha
		$this->SetY(16);
		$this->SetX(96);
		//Título		
		$this->Cell(30,25,utf8_decode('Reporte de Relaciones por Clinicas'),0,0,'C');
		
		//Referencia
		$fechadesde = strtoupper(@$_POST["txtfechadesde"]);
		$fechahasta = strtoupper(@$_POST["txtfechahasta"]);		
		$clinica_nombre = strtoupper(@$_POST["clinica_nombre"]);
		$rif_clinica = strtoupper(@$_POST["clinicas"]);	
		
		$this->SetFont('Times','BI',10);
		$this->SetY(26);		
		$this->Cell(0,25,utf8_decode('Clinica : '.$clinica_nombre),0,0,'L');
		$this->Ln(4);
		$this->Cell(0,26,utf8_decode('Rif : '.$rif_clinica),0,0,'L');
		$this->Ln();
		if($fechadesde != NULL and $fechahasta != NULL)
		{
		$consultarfec = "select * from siscam.vst_aut_fac_re where siscam.vst_aut_fac_re.aut_fechaa >= '$fechadesde' and siscam.vst_aut_fac_re.aut_fechaa <= '$fechahasta'";
		$queryfec = pg_query($consultarfec);
		$rowr = pg_fetch_object($queryfec); 
		$this->SetFont('Times','B',10);
		$this->SetY(35);
		$this->SetX(78);
		$this->Cell(100,25,utf8_decode('DESDE:  '.$fechadesde.'     HASTA: '.$fechahasta),0,0,'R');
		$this->Ln();
		}
		//Subtítulos
		$this->SetY(51);
		$this->SetFont('Times','B',9);
		$this->Cell(22,5,utf8_decode('Nº Relacion'),0,0,'C');
		$this->Cell(20,5,utf8_decode('Nº Casos '),0);
        $this->Cell(50,5,utf8_decode('Fecha de Registro'),0);
		$this->Cell(32,5,utf8_decode('Monto Relacion'),0);
		$this->Cell(50,5,utf8_decode('Estatus'),0,0,'C');	
		$this->Line(10,56,195,56);
		$this->Ln();
	}

	//Numero de Página
	function Footer()
	{
		//Posición: a la derecha
		$this->SetY(19);
		//Arial italic 8
		$this->SetFont('Arial','I',10);
		//Número de página
		$this->Cell(0,10,utf8_decode('Página '.$this->PageNo().'/{nb}'),0,0,'R');
	}
	
	//Tabla del Reporte
	function CrearTabla($fechadesde,$fechahasta,$rif_clinica)
	{				
		//Anchuras de las columnas
		$this->SetY(49);
		$this->SetFont('Times','',9);
		$w=array(22,22,22,22,55,25);
		$this->Ln(8);
		
		//Datos	
		if($fechadesde != NULL and $fechahasta != NULL)
			{
				if($rif_clinica != NULL)
				{
				$consulta = "SELECT distinct(vst_aut_fac_re.re_id),
									vst_aut_fac_re.re_fecha, 
									vst_aut_fac_re.re_totalcan, 
									vst_aut_fac_re.aut_estatus
							 from siscam.vst_aut_fac_re
							 where 
							 	re_fecha between '$fechadesde' and '$fechahasta' 
								and cli_rif = '$rif_clinica' order by re_fecha desc";	
				}
				else
				{
					$consulta = "SELECT distinct(vst_aut_fac_re.re_id),
									vst_aut_fac_re.re_fecha, 
									vst_aut_fac_re.re_totalcan, 
									vst_aut_fac_re.aut_estatus
									from siscam.vst_aut_fac_re
							 where 
							 	re_fecha between '$fechadesde' and '$fechahasta' order by re_fecha desc";	
				}
			}
			elseif($rif_clinica != NULL)
			{
				$consulta = "SELECT distinct(vst_aut_fac_re.re_id),
									vst_aut_fac_re.re_fecha, 
									vst_aut_fac_re.re_totalcan, 
									vst_aut_fac_re.aut_estatus
							 from siscam.vst_aut_fac_re
							 where 				 	
								 cli_rif = '$rif_clinica' order by re_fecha desc";					
			}
			else
			{ 
			$consulta = "SELECT * from siscam.vst_aut_fac_re";
			}
		$queryfec = pg_query($consulta);
		while($rowr = pg_fetch_object($queryfec)) { 
		//consulta para saber cantidad de Casos pro Relacion
		$query_casos = "SELECT count(aut_cartaaval) as cuenta 
										from 
											siscam.vst_aut_fac_re 
										where 
											vst_aut_fac_re.re_id = '$rowr->re_id'";
		$consulta = pg_query($query_casos);
			$this->Cell(22,5,utf8_decode($rowr->re_id),0);
			$this->Cell(20,5,utf8_decode(pg_result($consulta,0,"cuenta")),0);
			$this->Cell(50,5,utf8_decode(CamFormFech($rowr->re_fecha)),0);			
			$this->Cell(32,5,utf8_decode($rowr->re_totalcan),0,0,'C');	
		 if($rowr->aut_estatus == 'FIN') { $estatus = 'FINANZAS'; }
			elseif($rowr->aut_estatus == 'FINPAR') { $estatus = "FINANZAS PARCIAL"; } 
			elseif($rowr->aut_estatus == 'PDO') { $estatus = "PAGADO"; }
			 elseif($rowr->aut_estatus == 'PDOPAR')	{ $estatus = "PAGADO PARCIAL"; } 
			$this->Cell(50,5,utf8_decode($estatus),0,0,'C');
			$this->Ln();			
		}		
	}
}

function CamFormFech($contenido) {
	if($contenido!=null) {
		$fech=explode('-', $contenido,3);
		$fech2=explode(' ', $fech[2],3);
		if(count($fech2) == 2) {
			return $fech2[0]."/".$fech[1]."/".$fech[0]." ".$fech2[1];
		} else {
			return $fech2[0]."/".$fech[1]."/".$fech[0];
		}
	}
}

//Creación del objeto de la clase heredada
$pdf=new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();

//llamada a la funcion de crear tabla y fecha
$pdf->CrearTabla($fechadesde,$fechahasta,$rif_clinica);
$pdf->Output();




?>
