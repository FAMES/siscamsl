<?php
require('../reports/fpdf16/fpdf.php');

	//Conexion al servidor
	$servidor = "190.190.1.3";
	$base = "intranet";
	$usuario = "postgres";
	$password = "postgres";   
	pg_pconnect("host=$servidor dbname=$base user=$usuario password=$password");
	
	$estatus = $_POST['hdnestatus'];
	$desde = $_POST['hdndesde'];
	$hasta = $_POST['hdnhasta'];
	
class PDF extends FPDF
{
	//Cabecera de p�gina
	function Header()
	{	
		$estatus = $_POST['hdnestatus'];
		//fecha
		$this->SetY(22);
		$this->SetFont('Arial','I',10);
		$this->Cell(0,8,'Fecha: '.date('d/m/Y h:i A'),0,0,'L');
		//Logo		
		$this->Image('../imagenes/encabezado_gris3.jpg',10,8,273,14);
		//Fuente
		$this->SetFont('Arial','B',12);
		//Movernos a la derecha
		$this->SetY(25);
		$this->SetX(130);
		//T�tulo		
		$this->Cell(30,25,'REPORTE DE MATERIAL DE S�NTESIS REUTILIZABLE CON ESTATUS '.$estatus,0,0,'C');
		
		//Subt�tulos
		$this->SetY(50);
		$this->SetX(10);
		$this->SetFont('Times','B',8);
		$this->Cell(25,5,'CARTA AVAL',0);
        $this->Cell(25,5,'C�DULA',0);
		$this->Cell(60,5,'NOMBRE Y APELLIDO',0);
        $this->Cell(60,5,'UNIVERSIDAD',0);
		$this->Cell(60,5,'CL�NICA',0);
		$this->Cell(25,5,'MONTO',0);
		$this->Cell(20,5,'FECHA',0);
		$this->Line(10,55,285,55);
		$this->Ln();
	}

	//Numero de P�gina
	function Footer()
	{
		//Posici�n: a la derecha
		$this->SetY(21);
		//Arial italic 8
		$this->SetFont('Arial','I',10);
		//N�mero de p�gina
		$this->Cell(0,10,'P�gina '.$this->PageNo().'/{nb}',0,0,'R');
	}

	
	//Tabla del Reporte
	function CrearTabla($estatus,$desde,$hasta)
	{				
		//Anchuras de las columnas
		$this->SetY(50);
		$this->SetFont('Times','',8);
		$w=array(25,25,60,60,60,25,20);
		$this->Ln(8);
		
		//Datos
		$consulta = "select * from siscam.vst_aut_sin where siscam.vst_aut_sin.sin_estatus like '$estatus' and siscam.vst_aut_sin.sin_fecha >= '$desde' and siscam.vst_aut_sin.sin_fecha <= '$hasta' order by siscam.vst_aut_sin.aut_cartaaval asc";
		$query = pg_query($consulta);
		while($row = pg_fetch_object($query)) { 
			$this->Cell($w[0],5,$row->aut_cartaaval,0);
			$this->Cell($w[1],5,$row->est_id,0);
			$this->Cell($w[2],5,$row->est_pnombre.' '.$row->est_papellido,0);
			$this->Cell($w[3],5,$row->uni_nombre,0);
			$this->Cell($w[4],5,$row->cli_nombre,0);
			$this->Cell($w[5],5,$row->sin_monto,0);
			$this->Cell($w[6],5,$row->sin_fecha,0);
			$this->Ln();
		} 
		
		//L�nea de cierre
		$this->Cell(array_sum($w),0,'',0);
	}
}

//Creaci�n del objeto de la clase heredada
$pdf=new PDF();
$pdf->AliasNbPages();
//$pdf->FPDF($orientation='l', $unit='mm', $format='A4');
$pdf->AddPage('l','A4');

//LLAMADA A LA FUNCION PARA CREAR LA TABLA
$pdf->CrearTabla($estatus,$desde,$hasta);
$pdf->Output();

?>
