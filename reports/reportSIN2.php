<?php require('../reports/fpdf16/fpdf.php');

	//VISTA UTILIZADA ANTERIOR vst_pat_socio 
	//Conexion al servidor
	$servidor = "190.190.1.3";
	$base = "intranet";
	$usuario = "postgres";
	$password = "postgres";   
	pg_pconnect("host=$servidor dbname=$base user=$usuario password=$password");
	
	//Datos recibidos por POST
	$carta = @$_POST['carta'];
	
class PDF extends FPDF
{
	//Cabecera de p�gina
	function Header()
	{		
		//fecha
		$this->SetY(20);
		$this->SetFont('Arial','I',10);
		$this->Cell(0,8,'Fecha: '.date('d/m/Y h:i A'),0,0,'L');
		//Logo
		$this->Image('../imagenes/encabezado_gris.jpg',10,8,190,12);
		//Fuente
		$this->SetFont('Arial','B',12);
		//Movernos a la derecha
		$this->SetY(30);
		$this->SetX(96);
		//T�tulo		
		$this->Cell(30,5,'Reporte Individual de Material de S�ntesis Reutilizable',0,0,'C');
		$this->Ln();
	}
	
	//Numero de P�gina
	function Footer()
	{
		//Posici�n: a la derecha
		$this->SetY(19);
		//Arial italic 8
		$this->SetFont('Arial','I',10);
		//N�mero de p�gina
		$this->Cell(0,10,'P�gina '.$this->PageNo().'/{nb}',0,0,'R');
	}
		
	//Tabla del Reporte
	function CrearTabla($carta)
	{
		//Datos	
		$consulta = "select * from siscam.vst_aut_sin where siscam.vst_aut_sin.aut_cartaaval = $carta";
		$query = pg_query($consulta);
		$row = pg_fetch_object($query ,0);
		
		$this->SetY(45);
		
		//DATOS PERSONALES
		//PRIMER TITULO
		$this->SetFont('Times','B',8);	
		$this->SetFillColor(192,192,192);
		$this->Cell(0,5,'Datos Personales','LTRB',0,'C',true);
		$this->Ln(8);
		//PRIMERA FILA
		$this->Cell(70,5,'Nombres y Apellidos:',0,0,'L');		
		$this->Cell(35,5,'Cedula:',0,0,'L');		
		$this->Cell(28,5,'Edad:',0,0,'L');		
		$this->Cell(40,5,'Sexo:',0,0,'L');
		$this->Ln();		
		$this->SetFont('Times','',8);
		$this->Cell(70,5,$row->est_pnombre.' '.$row->est_papellido,0,0,'L');
		$this->Cell(35,5,$row->est_id,0,0,'L'); 
		$this->Cell(28,5,$row->est_edad,0,0,'L');
		$this->Cell(40,5,$row->est_sexo,0,0,'L');
		$this->Ln(10);
		
		//SEGUNDA FILA
		$this->SetFont('Times','B',8);
		$this->Cell(70,5,'Nacionalidad',0,0,'L');		
		$this->Cell(63,5,'Tel�fonos (Hab. / Cel.):',0,0,'L');		
		$this->Cell(64,5,'Estado / Ciudad:',0,0,'L');
		$this->Ln();
		$this->SetFont('Times','',8);
		$this->Cell(70,5,$row->est_nacionalidad,0,0,'L');		
		$this->Cell(63,5,$row->est_tlfhabitacion.' / '.$row->est_tlfcel,0,0,'L');		
		$this->Cell(64,5,$row->esta_estado.' / '.$row->ciu_ciudad,0,0,'L');
		$this->Ln(10);
		
		//TERCERA FILA
		$this->SetFont('Times','B',8);
		$this->Cell(0,5,'Direcci�n Habitaci�n:',0,0,'L');
		$this->Ln();
		$this->SetFont('Times','',8);
		$this->Cell(0,5,$row->est_direccion,0,0,'L');
		$this->Ln(10);
		
		//CUARTA FILA		
		$this->SetFont('Times','B',8);		
		$this->Cell(70,5,'Carrera:',0,0,'L');
		$this->Cell(64,5,'Universidad:',0,0,'L');
		$this->Ln();
		$this->SetFont('Times','',8);		
		$this->Cell(70,5,$row->car_nombre,0,0,'L');
		$this->Cell(64,5,$row->uni_nombre,0,0,'L');
		$this->Ln(20);
		
		//DATOS DE LA ATENCI�N
		//SEGUNDO TITULO
		$this->SetFont('Times','B',8);
		$this->SetFillColor(192,192,192);
		$this->Cell(0,5,'Datos de la Atenci�n','LTRB',0,'C',true);
		$this->Ln(8);
		
		//PRIMERA FILA
		$this->Cell(20,5,'Carta Aval:',0,0,'L');		
		$this->Cell(25,5,'N� de Control:',0,0,'L');
		$this->Cell(75,5,'Cl�nica:',0,0,'L');	
		$this->Cell(20,5,'Monto:',0,0,'L');	
		$this->Cell(20,5,'Fecha:',0,0,'L');	
		$this->Cell(20,5,'Estatus:',0,0,'L');
		$this->Ln();		
		$this->SetFont('Times','',8);
		$this->Cell(20,5,$row->aut_cartaaval,0,0,'L');
		$this->Cell(25,5,$row->aut_ncontrol,0,0,'L');
		$this->Cell(75,5,$row->cli_nombre,0,0,'L');
		$this->Cell(20,5,$row->sin_monto,0,0,'L');
		$this->Cell(20,5,$row->sin_fecha,0,0,'L');
		$this->Cell(20,5,$row->sin_estatus,0,0,'L');
		$this->Ln();
	}
}

//Creaci�n del objeto de la clase heredada
$pdf=new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();

//llamada a la funcion de crear tabla y fecha
$pdf->CrearTabla($carta);
$pdf->Output();

?>
