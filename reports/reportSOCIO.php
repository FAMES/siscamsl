<?php require('../reports/fpdf16/fpdf.php');

	//VISTA UTILIZADA ANTERIOR vst_pat_socio 
	//Conexion al servidor
	$servidor = "localhost";
	$base = "intranet_dev";
	$usuario = "postgres";
	$password = "$2011tyspostgresql%";   
	pg_pconnect("host=$servidor dbname=$base user=$usuario password=$password");
	
	//Datos recibidos por POST
	$carta = @$_POST["carta"];
	$consulta = "select * from siscam.vst_aut_pat where siscam.vst_aut_pat.aut_cartaaval = $carta";
	$aux = pg_query($consulta);
	//CADENA DE LAS PATOLOGIAS SEGUN LA CARTA AVAL Y SUMA DE LOS MONTO DE CADA PATOLOG�A
	while($row = pg_fetch_object($aux)){ 
		$patologia.=$row->bar_descripcion.' / ';				
		$montobar = $montobar + $row->pat_majust;
	}	
	
class PDF extends FPDF
{
	//Cabecera de p�gina
	function Header()
	{		
		//fecha
		$this->SetY(20);
		$this->SetFont('Arial','I',10);
		$this->Cell(0,8,'Fecha: '.date('d/m/Y h:i A'),0,0,'L');
		//Logo
		$this->Image('../imagenes/encabezado_gris.jpg',10,8,190,12);
		//Fuente
		$this->SetFont('Arial','B',15);
		//Movernos a la derecha
		$this->SetY(25);
		$this->SetX(96);
		//T�tulo		
		$this->Cell(30,5,'Resumen Socio Econ�mico',0,0,'C');
		$this->Ln();
	}
		
	//Tabla del Reporte
	function CrearTabla($carta,$patologia,$montobar)
	{
		//Datos	
		$consulta = "select * from siscam.vst_pat_socio where siscam.vst_pat_socio.aut_cartaaval = $carta ";
		$query = pg_query($consulta);
		$row = pg_fetch_object($query ,0);		
		$tbjosuel = split("/",$row->socio_tbjosuel);
		$tcvivienda =  split("/",$row->socio_tcvivienda);
		$bienesn = split("/",$row->socio_bienesn);	
		$arreglo1 = split("!",$row->socio_criesgo);
		$arreg = split(";",$arreglo1[0]);
		$cont=(count($arreg))-1;
		
		$this->SetY(35);
		
		//DATOS PERSONALES
		//PRIMER TITULO
		$this->SetFont('Times','B',9);	
		$this->SetFillColor(192,192,192);
		$this->Cell(0,5,'1. Datos Personales','LTRB',0,'C',true);
		$this->Ln();
		//PRIMERA FILA
		$this->Cell(80,5,'1. Nombres y Apellidos:','LR',0,'L');		
		$this->Cell(35,5,'2. Cedula:','R',0,'L');		
		$this->Cell(35,5,'3. Edad:','LR',0,'L');		
		$this->Cell(40,5,'4. N� Carta Aval:','R',0,'L');
		$this->Ln();		
		$this->SetFont('Times','',9);
		$this->Cell(80,5,$row->est_pnombre.' '.$row->est_papellido,'LR',0,'L');
		$this->Cell(35,5,$row->est_id,'R',0,'L'); 
		$this->Cell(35,5,$row->est_edad,'R',0,'L');
		$this->Cell(40,5,$row->aut_cartaaval,'R',0,'L');
		$this->Ln();
		
		//SEGUNDA FILA
		$this->SetFont('Times','B',9);
		$this->Cell(0,5,'5. Direcci�n Habitaci�n:','LTR',0,'L');
		$this->Ln();
		$this->SetFont('Times','',9);
		$this->Cell(0,5,$row->est_direccion,'LR',0,'L');
		$this->Ln();
		
		//TERCERA FILA		
		$this->SetFont('Times','B',9);
		$this->Cell(115,5,'6. Universidad:','LTR',0,'L');		
		$this->Cell(75,5,'7. Carrera:','TR',0,'L');
		$this->Ln();
		$this->SetFont('Times','',9);
		$this->Cell(115,5,$row->uni_nombre,'LR',0,'L');		
		$this->Cell(75,5,$row->car_nombre,'R',0,'L');
		$this->Ln();
		
		//CUARTA FILA
		$this->SetFont('Times','B',9);
		$this->Cell(63,5,'8. Trabaja:','LTR',0,'L');		
		$this->Cell(63,5,'9. Sueldo Mensual:','TR',0,'L');		
		$this->Cell(64,5,'10. Posee P�liza de Seguro:','TR',0,'L');
		$this->Ln();
		$this->SetFont('Times','',9);
		$this->Cell(63,5,$tbjosuel[0],'LR',0,'L');		
		$this->Cell(63,5,$tbjosuel[1].' Bs.','R',0,'L');		
		$this->Cell(64,5,$row->socio_seguro,'R',0,'L');
		$this->Ln();
		
		//QUINTA FILA		
		$this->SetFillColor(255,255,255);
		$this->SetFont('Times','B',9);
		$this->Cell(0,5,'11. Patolog�a y/o Diagn�stico:','LTR',0,'J');
		$this->Ln();
		$this->SetFont('Times','',9);
		$this->MultiCell(0,5,$patologia,'LR','L');
		
		//DATOS SOCIO ECONOMICO
		//SEGUNDO TITULO
		$this->SetFont('Times','B',9);
		$this->SetFillColor(192,192,192);
		$this->Cell(0,5,'2. Datos Socio Econ�micos del Grupo Familiar','LTR',0,'C',true);
		$this->Ln();
		
		//PRIMERA FILA
		$this->Cell(38,5,'12. N� de Miembros:','LTR',0,'L');		
		$this->Cell(38,5,'13. N� de Estudiantes:','TR',0,'L');		
		$this->Cell(38,5,'14. N� de Trabajadores:','TR',0,'L');		
		$this->Cell(38,5,'15. N� de Desempleados:','TR',0,'L');	
		$this->Cell(38,5,'16. Responsable Eco.:','TR',0,'L');
		$this->Ln();		
		$this->SetFont('Times','',9);
		$this->Cell(38,5,$row->socio_nmbroflia,'LR',0,'L');
		$this->Cell(38,5,$row->socio_nest,'R',0,'L'); 
		$this->Cell(38,5,$row->socio_ntbjo,'R',0,'L'); 
		$this->Cell(38,5,$row->socio_ndesemp,'R',0,'L');
		$this->Cell(38,5,$row->socio_repeco,'R',0,'L');
		$this->Ln();
		
		//SEGUNDA FILA
		$this->SetFont('Times','B',9);
		$this->Cell(63,5,'17. Ingresos del Grupo Familiar:','LTR',0,'L');		
		$this->Cell(63,5,'18. Egresos del Grupo Familiar:','TR',0,'L');		
		$this->Cell(64,5,'19. Balance Econ�mico:','LTR',0,'L');
		$this->Ln();		
		$this->SetFont('Times','',9);
		$this->Cell(63,5,$row->socio_ingflia.' Bs.','LR',0,'L');
		$this->Cell(63,5,$row->socio_egsoflia.' Bs.','R',0,'L'); 
		$this->Cell(64,5,$row->socio_blceco.' Bs.','R',0,'L');
		$this->Ln();
		
		//TERCERA FILA
		$this->SetFont('Times','B',9);
		$this->Cell(63,5,'20. N� de Menores de Edad:','LTR',0,'L');		
		$this->Cell(63,5,'21. N� de Jubilados o Pensionados:','TR',0,'L');		
		$this->Cell(64,5,'22. N� de Incapacitados:','LTR',0,'L');
		$this->Ln();		
		$this->SetFont('Times','',9);
		$this->Cell(63,5,$row->socio_nmenores,'LR',0,'L');
		$this->Cell(63,5,$row->socio_njubi,'R',0,'L'); 
		$this->Cell(64,5,$row->socio_nincap,'R',0,'L');
		$this->Ln();
		
		//CUARTA FILA
		$this->SetFont('Times','B',9);
		$this->Cell(38,5,'23. Vivienda:','LTR',0,'L');		
		$this->Cell(38,5,'24. Condici�n Vivienda:','TR',0,'L');		
		$this->Cell(38,5,'25. Apoyo Familiar:','TR',0,'L');		
		$this->Cell(38,5,'26. Posee Bienes:','TR',0,'L');	
		$this->Cell(38,5,'27. Categor�a Eco.:','TR',0,'L');
		$this->Ln();		
		$this->SetFont('Times','',9);
		$this->Cell(38,5,$tcvivienda[0],'LR',0,'L');
		$this->Cell(38,5,$tcvivienda[1],'R',0,'L'); 
		$this->Cell(38,5,$row->socio_apoyoflia,'R',0,'L'); 
		$this->Cell(38,5,$bienesn[0].', '.$bienesn[1],'R',0,'L');
		$this->Cell(38,5,$row->socio_categoria,'R',0,'L');
		$this->Ln();
		
		//CONDICIONES DE RIESGO
		//TERCER TITULO	
		$this->SetFont('Times','B',9);	
		$this->SetFillColor(192,192,192);
		$this->Cell(0,5,'3. Condiciones de Riesgo','LTRB',0,'C',true);
		$this->Ln();
		
		//CONTENIDO		
		$this->SetFillColor(255,255,255);
		$this->SetFont('Times','',9);
		for ($i=0;$i<=$cont;$i++) {
			if ($arreg[$i]!='' or $arreg[$i]!=NULL) { $this->MultiCell(0,5,'- '.$arreg[$i],'LR','L'); } 
		}
		
		//CALCULO DE PORCENTAJE DE AYUDA A OTORGAR
		//CUARTO TITULO	
		$this->SetFont('Times','B',9);	
		$this->SetFillColor(192,192,192);
		$this->Cell(0,5,'4. Calculo del Porcentaje de Ayuda a Otorgar','LTR',0,'C',true);
		$this->Ln();
		
		//CONTENIDO	
		$this->SetFont('Times','',9);
		$this->Cell(115,5,'Monto del Presupuesto y/o Factura del Centro de Salud','LT',0,'L');
		$this->Cell(35,5,' ','T',0,'L');
		$this->Cell(40,5,$row->aut_montop.' Bs.','TR',0,'L');
		$this->Ln();
		$this->Cell(115,5,'Monto de Material de S�ntesis 100%','L',0,'L');
		$this->Cell(35,5,'100 %',0,0,'L');
		$this->Cell(40,5,$row->socio_msintesis.' Bs.','R',0,'L');
		$this->Ln();
		$this->Cell(115,5,'Monto de la Ayuda Seg�n Baremo','L',0,'L');
		$this->Cell(35,5,' ',0,0,'L');
		$this->Cell(40,5,$montobar.' Bs.','R',0,'L');
		$this->Ln();		
		$this->Cell(0,5,'','LR',0,'L');
		$this->Ln();
		$this->Cell(115,5,'Monto de la Ayuda Seg�n Instrumento de casos Especiales','L',0,'L');
		$this->Cell(35,5,$row->socio_poresp.' %',0,0,'L');
		$this->Cell(40,5,$row->socio_montoesp.' Bs.','R',0,'L');
		$this->Ln();
		$this->Cell(115,5,'Monto de la Ayuda Seg�n Condici�n de Riesgo','L',0,'L');
		$this->Cell(35,5,$row->socio_porrgo.' %',0,0,'L');
		$this->Cell(40,5,$row->socio_montorgo.' Bs.','R',0,'L');
		$this->Ln();
		$this->Cell(115,5,'Monto de la Ayuda a Cancelar por FAMES','L',0,'L');
		$this->Cell(35,5,$row->socio_por.' %',0,0,'L');
		$this->Cell(40,5,$row->socio_monto.' Bs.','R',0,'L');
		$this->Ln();
		$this->Cell(115,5,'Diferencia entre la Factura - Presupuesto y la Ayuda','L',0,'L');
		$this->Cell(35,5,$row->socio_pordif.' %',0,0,'L');
		$this->Cell(40,5,$row->socio_diferencia.' Bs.','R',0,'L');
		$this->Ln();
		
		//CALCULO DE PORCENTAJE DE AYUDA A OTORGAR
		//QUINTO TITULO	
		$this->SetFont('Times','B',9);	
		$this->SetFillColor(192,192,192);
		$this->Cell(0,5,'5. Observaciones','LTR',0,'C',true);
		$this->Ln();
		$this->SetFont('Times','',9);
		$this->SetFillColor(255,255,255);
		$this->MultiCell(0,5,$row->socio_observacion,'LTRB','J');
		
		//ULTIMA FILA
		$this->SetFont('Times','B',9);
		$this->Cell(63,5,'Fecha:','LTR',0,'L');
		$this->Cell(63,5,'Nombre del Trabajador Social:','TR',0,'L');
		$this->Cell(64,5,'Firma del Trabajador Social:','TR',0,'L');
		$this->Ln();
		$this->SetFont('Times','',9);
		$this->Cell(63,10,date('d/m/Y'),'LRB',0,'C'); 
		$this->Cell(63,10,'','RB',0,'C'); 
		$this->Cell(64,10,'','RB',0,'C'); 
	}
}

//Creaci�n del objeto de la clase heredada
$pdf=new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();

//llamada a la funcion de crear tabla y fecha
$pdf->CrearTabla($carta,$patologia,$montobar);
$pdf->Output();

?>
