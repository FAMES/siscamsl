<?php
require('../reports/fpdf16/fpdf.php');

	//Conexion al servidor
	$servidor = "190.190.1.19";
	$base = "intranet_dev";
	$usuario = "postgres";
	$password = "$2011tyspostgresql%";   
	pg_pconnect("host=$servidor dbname=$base user=$usuario password=$password");
	
	
//Referencia

	$cbouni = strtoupper(@$_POST["cbouni"]);
    $cboestatus = strtoupper(@$_POST["cboestatus"]);
	$cboestado = strtoupper(@$_POST["cboestado"]);
	$txtfechadesde = strtoupper(@$_POST["txtfechadesde"]);
	$txtfechahasta = strtoupper(@$_POST["txtfechahasta"]);
	
class PDF extends FPDF
{


	//Cabecera de página
	function Header()
	{		
		//fecha
		$this->SetY(20);
		$this->SetFont('Arial','I',10);
		$this->Cell(0,8,'Fecha: '.date('d/m/Y h:i A'),0,0,'L');
		//Logo
		$this->Image('../imagenes/encabezado_gris.jpg',13,8,180,12);
		//Fuente
		$this->SetFont('Arial','B',15);
		//Movernos a la derecha
		$this->SetY(16);
		$this->SetX(96);
		//Título		
		$this->Cell(30,25,utf8_decode('Reportes por Universidad'),0,0,'C');
		

	}

	//Numero de Página
	function Footer()
	{
		//Posición: a la derecha
		$this->SetY(19);
		//Arial italic 8
		$this->SetFont('Arial','I',10);
		//Número de página
		$this->Cell(0,10,utf8_decode('Página '.$this->PageNo().'/{nb}'),0,0,'R');
	}
	
	//Tabla del Reporte
	function CrearTabla($cbouni,$cboestatus,$cboestado,$txtfechadesde,$txtfechahasta)
	{		
	
		$this->SetFont('Times','B',10);
		$this->SetY(26);
		$this->SetX(58);
		if ($cboestatus =='1'){
		 	$titulo1 = 'Autorizados';
		}
		if ($cboestatus =='2'){
		 	$titulo1 = 'Pendientes por Pago';
		}
		if ($cboestatus =='3'){
		 	$titulo1 = 'Pagados';
		}
		
		
		$this->SetY(36);
		$this->SetX(66);
		$this->Cell(30,6,utf8_decode(' Casos '.$titulo1.' Desde '.CamFormFech($txtfechadesde).' Hasta '.CamFormFech($txtfechahasta)),0);
		$this->Ln();
		
		//Anchuras de las columnas
		$this->SetY(49);
		$this->SetFont('Times','',9);
		$w=array(22,22,22,22,55,25);
		$this->Ln(8);
		//Subtítulos
		$this->SetY(51);
		$this->SetFont('Times','B',9);
		$this->Cell(93,5,utf8_decode('Universidad'),0);
		$this->Cell(33,5,utf8_decode('Total Casos'),0);
		if($cboestatus =='PDO'){
				$this->Cell(33,5,utf8_decode('Total Pagado'),0);
				} else{
				$this->Cell(33,5,utf8_decode('Total Autorizado'),0);
		}
		
		$this->Line(10,56,195,56);
		$this->Ln();
		
		
		if ($cbouni !=NULL and $cboestatus =='0'){	
				
			$condi =" where siscam.vst_uni_aut_report.uni_id =$cbouni";
				
			$consulta ="select * from siscam.vst_uni_aut_report $condi order by siscam.vst_uni_aut_report.uni_id asc";
		
		} else if ($cbouni =='0' and $cboestatus !=NULL and $txtfechadesde !=NULL and $txtfechahasta !=NULL){
			
			
			if ($cboestatus =='1'){
					
						
						$condi =" where siscam.vst_aut.aut_estatus ='AUT' and siscam.vst_aut.aut_fechaa >= '$txtfechadesde' and siscam.vst_aut.aut_fechaa <= '$txtfechahasta'";
					
				
				}else if($cboestatus =='2'){
											
						$condi =" where (siscam.vst_aut.aut_estatus ='REC' or siscam.vst_aut.aut_estatus ='PAGO' or siscam.vst_aut.aut_estatus ='FIN' or siscam.vst_aut.aut_estatus ='PAGOPAR' or siscam.vst_aut.aut_estatus ='FINPAR') and siscam.vst_aut.aut_fechaa >= '$txtfechadesde' and siscam.vst_aut.aut_fechaa <= '$txtfechahasta'";
						
									
				}else if($cboestatus =='3'){
											
						$condi =" where (siscam.vst_aut.aut_estatus ='PDO' or siscam.vst_aut.aut_estatus ='PDOPAR') and siscam.vst_aut.aut_fechaa >= '$txtfechadesde' and siscam.vst_aut.aut_fechaa <= '$txtfechahasta'";
						
									
				}
			
				
			$consulta ="SELECT DISTINCT siscam.vst_aut.uni_id, siscam.vst_aut.uni_nombre, count(siscam.vst_aut.aut_cartaaval) AS conteo, sum(siscam.vst_aut.aut_montoa) AS monto_autorizado
   FROM siscam.vst_aut $condi
  GROUP BY siscam.vst_aut.uni_id, siscam.vst_aut.uni_nombre
  ORDER BY conteo desc";
					
		}else if ($cbouni !=NULL and $cboestatus !=NULL and $txtfechadesde !=NULL and $txtfechahasta !=NULL){
			
			if ($cboestatus =='1'){
			
			
			$condi =" where siscam.vst_aut.aut_estatus ='AUT' and siscam.vst_aut.uni_id =$cbouni and siscam.vst_aut.aut_fechaa >= '$txtfechadesde' and siscam.vst_aut.aut_fechaa <= '$txtfechahasta'";
			
			}else if($cboestatus =='2'){
				
			$condi =" where (siscam.vst_aut.aut_estatus ='REC' or siscam.vst_aut.aut_estatus ='PAGO' or siscam.vst_aut.aut_estatus ='FIN' or siscam.vst_aut.aut_estatus ='PAGOPAR' or siscam.vst_aut.aut_estatus ='FINPAR') and siscam.vst_aut.uni_id =$cbouni and siscam.vst_aut.aut_fechaa >= '$txtfechadesde' and siscam.vst_aut.aut_fechaa <= '$txtfechahasta'";	
			
			}else if($cboestatus =='3'){
				
			$condi =" where (siscam.vst_aut.aut_estatus ='PDO' or siscam.vst_aut.aut_estatus ='PDOPAR') and siscam.vst_aut.uni_id =$cbouni and siscam.vst_aut.aut_fechaa >= '$txtfechadesde' and siscam.vst_aut.aut_fechaa <= '$txtfechahasta'";
			
			}
				
			$consulta ="SELECT DISTINCT siscam.vst_aut.uni_id, siscam.vst_aut.uni_nombre, count(siscam.vst_aut.aut_cartaaval) AS conteo, sum(siscam.vst_aut.aut_montoa) AS monto_autorizado
   FROM siscam.vst_aut $condi
  GROUP BY siscam.vst_aut.uni_id, siscam.vst_aut.uni_nombre
  ORDER BY conteo desc";			
			
		}else if ($cbouni =='0' and $cboestatus =='3' and $txtfechadesde !=NULL and $txtfechahasta !=NULL){
			
			//echo 'Estoy en el 6to if';
			$condi =" where siscam.vst_aut.aut_estatus ='PDO' and siscam.vst_aut.aut_fechaa >= '$txtfechadesde' and siscam.vst_aut.aut_fechaa <= '$txtfechahasta'";
				
			$consulta ="SELECT DISTINCT siscam.universidad.uni_id, siscam.universidad.uni_nombre, count(siscam.vst_aut.aut_cartaaval) AS conteo, sum(siscam.vst_aut.aut_montofac) AS monto_pagado, siscam.vst_aut.aut_estatus
   FROM siscam.universidad
   JOIN siscam.vst_aut ON siscam.universidad.uni_id = siscam.vst_aut.uni_id $condi
  GROUP BY siscam.universidad.uni_id, siscam.universidad.uni_nombre, siscam.vst_aut.aut_estatus
  ORDER BY siscam.universidad.uni_id, siscam.universidad.uni_nombre, count(siscam.vst_aut.aut_cartaaval), sum(siscam.vst_aut.aut_montofac), siscam.vst_aut.aut_estatus asc";			
		}else{
			
			$consulta = "SELECT DISTINCT siscam.vst_aut.uni_id, siscam.vst_aut.uni_nombre, count(siscam.vst_aut.aut_cartaaval) AS conteo, sum(siscam.vst_aut.aut_montoa) AS monto_autorizado
   FROM siscam.vst_aut GROUP BY siscam.vst_aut.uni_id, siscam.vst_aut.uni_nombre
  ORDER BY conteo desc";
		}	
		
		$query = pg_query($consulta);
		while($rowrs = pg_fetch_object($query)) { 
		$contador++;
			$this->Cell(93,5,utf8_decode(substr($rowrs->uni_nombre,0,35)),0);
			$this->Cell(33,5,utf8_decode($rowrs->conteo),0);
			if ($cboestatus =='PDO'){
					$this->Cell(33,5,number_format($rowrs->monto_pagado,2,',','.'),0);
			} else{
					$this->Cell(33,5,number_format($rowrs->monto_autorizado,2,',','.'),0);
			}
			$y = $y + 5;
			if($contador == 25) { $this->AddPage(); $y = 39; $x=12; $s=0; $contador=0; }
			
			
			
			$this->Ln();
			$c = $c + $rowrs->conteo;
			if ($cboestatus =='PDO'){
					$montotal = $montotal + $rowrs->monto_pagado;
			} else{
					$montotal = $montotal + $rowrs->monto_autorizado;
			}
			
		}
		
		//muestra totales del reporte
		$consulta2 = "select * from siscam.relacion where siscam.relacion.re_id like '$re'";
		//$query2 = pg_query($consulta2);
		//$row2 = pg_fetch_object($query2);
		$this->Ln(4);
		$this->SetX(130);
		$this->Cell(40,5,'TOTAL CASOS: ','LT',0);
		$this->SetX(170);
		$this->Cell(25,5,number_format($c,0,',','.'),'TR',0);
		$this->Ln();
		$this->SetX(130);
		$this->Cell(40,5,'TOTAL GENERAL: ','LB',0);
		$this->SetX(170);
		$this->Cell(25,5,number_format($montotal,2,',','.'),'BR',0);
		//$this->Ln();
		/*$this->SetX(130);
		$this->Cell(30,5,'IMPUESTO DEL 1*1000: ','L',0);
		$this->SetX(170);
		$this->Cell(25,5,number_format($row2->re_riam,2,',','.'),'R',0);
		$this->Ln();
		$this->SetX(130);
		$this->Cell(40,5,'TOTAL A CANCELAR: ','LB',0);
		$this->SetX(170);
		$this->Cell(25,5,number_format($row2->re_totalcan,2,',','.'),'BR',0);*/
		
		//////////////////////////////////////////////////////////
		//CODIGO EXTRA PARA GENERAR VARIAS PAGINAS DE PRUEBA    //
		//for($i=1;$i<=40;$i++)                                 //
		//$this->Cell(0,10,'Imprimiendo Linea de Prueba Numero '.$i,0,1); //
		//$this->Ln();                                          //
		//////////////////////////////////////////////////////////
		
		//Línea de cierre
		//$this->Cell(array_sum($w),0,'',0);
	}
	}

//Creación del objeto de la clase heredada
$pdf=new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();

//llamada a la funcion de crear tabla y fecha
$pdf->CrearTabla($cbouni,$cboestatus,$cboestado,$txtfechadesde,$txtfechahasta);
$pdf->Output();


function CamFormFech($contenido)
{
 		if($contenido!=null)
		{
 			$fech=explode('-', $contenido,3);
 			$fech2=explode(' ', $fech[2],3);
 				if(count($fech2) == 2)
				{
				return $fech2[0]."/".$fech[1]."/".$fech[0]." ".$fech2[1];
 				}
				else
				{
 				return $fech2[0]."/".$fech[1]."/".$fech[0];
 				}
 		}
 }

?>
