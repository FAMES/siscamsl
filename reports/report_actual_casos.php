<?php 
		require('../reports/fpdf16/fpdf.php'); 
		class PDF extends FPDF
		{
			var $yO;
			var $col = 0;
			function Header()//Cabecera de la pagina
			{
				global $title;
				$this->Image('../imagenes/encabezado_gris.jpg',13,8,180,12);
				$this->Ln(12);
				$this->SetFont('Arial','B',15);
				//Calculamos ancho del titulo
				$w = $this->GetStringWidth($title) + 6;
				$this->SetX((210 - $w)/2);
				//Colores de fondo, borde y texto
				$this->SetDrawColor(0,80,180);
				$this->SetFillColor(230,230,0);
				$this->SetTextColor(220,50,50);
				//Ancho del borde
				$this->SetLineWidth(1);
				//Titulo
				$this->Cell($w,9,$title,1,1,'C',true);
				//Salto de linea
				$this->Ln(10);
				//Guardo la posicion Y
				$this->yO=$this->getY();
			}
			function Footer()
			{
				//Posicion a 1,5cm del final
				$this->setY(-15);
				$this->SetFont('Arial','I',8);
				$this->SetTextColor(128);
				$this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'C');				
			}
			function setCol($col)
			{
				$this->col = $col;
				$x = 10+$col*65;
				$this->SetLeftMargin($x);
				$this->SetX($x);
			}
			function AcceptPageBreak()
			{
				//Metodo que acepta o no el salto automatico de pagina
				if($this->col<2)
				{
					$this->SetCol($this->col+1);
					$this->SetY($this->yO);
					return false;
				}
				else
				{
				//Vuelve a la primera columna
				$this->SetCol(0);
				return true;
				}
			}
function ChapterTitle($num,$label)
			{
				$this->SetFont('Arial','',12);
				$this->SetFillcolor(200,220,255);
				$this->Cell(0,6,"Capitulo $num : $label",0,1,'L',true);
				$this->Ln(4);				
			}
			function ChapterBody($file)
			{
				//Leemos el fichero
				$f = fopen($file,'r');
				$txt = fread($f,filesize($file));
				fclose($f);	
				$this->SetFont('Times','',12);
				//Se imprime el texto justificado
				$this->MultiCell(60,5,$txt);
				$this->Ln();
				$this->SetFont('','I');
				$this->Cell(0,5,'(Final del extracto)');
				$this->SetCol(0);
			}
			function printChapter($num,$title,$file)
			{
				$this->AddPage();	
				$this->ChapterTitle($num,$title);
				$this->ChapterBody($file);
			}
		}
		
      $report_pdf = new PDF(); //OBJETO PDF 
	  $title = '20000 leguas de viaje Submarino';
	  $report_pdf->SetTitle($title); 
	  $report_pdf->SetAuthor('Julio Verne');
	  $report_pdf->PrintChapter(1,'Un rizo de Huida','texto.txt');
	  $report_pdf->PrintChapter(2,'Los pro y los contra','texto2.txt');
   	  $report_pdf->Output();
?>


