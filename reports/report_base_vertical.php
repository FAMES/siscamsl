<?php
require('../reports/fpdf16/fpdf.php');

	//Conexion al servidor
	$servidor = "190.190.1.3";
	$base = "intranet";
	$usuario = "postgres";
	$password = "postgres";   
	pg_pconnect("host=$servidor dbname=$base user=$usuario password=$password");
	
	//Arreglo de los datos seleccionados con los checkbox
			/*$array = $_POST["enviar"];
			$valores = split(",",$array);
			$cont=count($valores);*/	
	
class PDF extends FPDF
{
	//Cabecera de página
	function Header()
	{		
		//fecha
		$this->SetY(18);
		$this->SetFont('Arial','I',8);
		$this->Cell(0,8,'Fecha: '.date('d/m/Y'),0,0,'L');
		//Logo
		$this->Image('../imagenes/encabezado_gris.jpg',10,8,190,10);
		//Fuente
		$this->SetFont('Arial','B',13);
		//Movernos a la derecha
		$this->SetY(14);
		$this->SetX(90);
		//Título		
		$this->Cell(30,25,utf8_decode('TÍTULO DEL REPORTE'),0,0,'C');
		
		//Subtítulos
		$this->SetY(35);
		$this->SetFont('Times','B',8);
		$this->Cell(23,5,utf8_decode('TÍTULO 1'),0);
        $this->Cell(23,5,utf8_decode('TÍTULO 2'),0);
		$this->Cell(23,5,utf8_decode('TÍTULO 3'),0);
        $this->Cell(23,5,utf8_decode('TÍTULO 4'),0);
		$this->Cell(23,5,utf8_decode('TÍTULO 5'),0);
		$this->Cell(23,5,utf8_decode('TÍTULO 6'),0);
		$this->Cell(23,5,utf8_decode('TÍTULO 7'),0);
		$this->Cell(23,5,utf8_decode('TÍTULO 8'),0);
		$this->Line(10,40,195,40);
		$this->Ln();
	}

	//Numero de Página
	function Footer()
	{
		//Posición: a la derecha
		$this->SetY(17);
		//Arial italic 8
		$this->SetFont('Arial','I',8);
		//Número de página
		$this->Cell(0,10,utf8_decode('Pagina '.$this->PageNo().'/{nb}'),0,0,'R');
	}

	
	//Tabla del Reporte
	function CrearTabla($valores,$cont)
	{				
		//Anchuras de las columnas
		$this->SetY(45);
		$this->SetFont('Times','',7);
		$w=array(26,26,26,26,26,26,26,26);
		//$this->Ln(8);
		
		//Datos
				/*for ($i=0;$i<=$cont;$i++) { 
				//consulta SQL del arreglo de checkbox seleccionados
				$consulta = "select * from siscam.vst_autorizaciones where siscam.vst_autorizaciones.aut_cartaaval like '$valores[$i]'";
				$query = pg_query($consulta);
				while($row = pg_fetch_object($query)) { 
					$this->Cell($w[0],5,$row->aut_cartaaval,0);
					$this->Cell($w[1],5,$row->aut_ncontrol,0);
					$this->Cell($w[2],5,$row->cli_rif,0);
					$this->Cell($w[3],5,$row->est_id,0);
					$this->Cell($w[4],5,$row->est_pnombre.' '.$row->est_papellido,0);
					$this->Cell($w[5],5,$row->aut_montoa,0);
					$this->Ln();
				} }*/
		
		//////////////////////////////////////////////////////////
		//CODIGO EXTRA PARA GENERAR VARIAS PAGINAS DE PRUEBA    //
		for($i=1;$i<=40;$i++)                                 //
		$this->Cell(0,10,utf8_decode('Imprimiendo Linea de Prueba Número '.$i),0,1); //
		$this->Ln();                                          //
		//////////////////////////////////////////////////////////
		
		//Línea de cierre
		//$this->Cell(array_sum($w),0,'',0);
	}
}

//Creación del objeto de la clase heredada
$pdf=new PDF();
$pdf->AliasNbPages();
$pdf->AddPage('P','A4');

//llamada a la funcion de crear tabla
//$pdf->CrearTabla($valores,$cont);
$pdf->Output();

?>
