<?php


require('../reports/fpdf16/fpdf.php');

	//Conexion al servidor
	$servidor = "localhost";
	$base = "intranet_dev";
	$usuario = "postgres";
	$password = "$2011tyspostgresql%";   
	pg_pconnect("host=$servidor dbname=$base user=$usuario password=$password");
	
	//Arreglo de los datos seleccionados con los checkbox
			/*$array = $_POST["enviar"];
			$valores = split(",",$array);
			$cont=cou*/

	$carta= @$_POST["carta"];
	$consulta = "select * from siscam.vst_aut where siscam.vst_aut.aut_cartaaval = ".$carta;
		$query = pg_query($consulta);
		$rowr = pg_fetch_object($query,0);
		$modalidad = $rowr->aut_modalidad;
		$fechaa = $rowr->aut_fechaa;
	
class PDF extends FPDF
{
	//Cabecera de página
	function Header()
	{	
	$carta= @$_POST["carta"];
	$consulta = "select * from siscam.vst_aut where siscam.vst_aut.aut_cartaaval = ".$carta;
		$query = pg_query($consulta);
		$rowr = pg_fetch_object($query,0);
		$fechaa = $rowr->aut_fechaa;
		//fecha
		$this->SetY(28);
		$this->SetX(8);
		$this->SetFont('Arial','B',12);
		$this->MultiCell(188, 6,'Caracas: '.CamFormFech($fechaa), 0, "R");
		//$this->MultiCell(188, 6,'Caracas: '.date('d/m/Y'), 0, "R");
		//Logo
		$this->Image('../imagenes/encabezado_carta.jpg',10,8,190,10);		
	}

	//Numero de Página
	function Footer()
	{
		//Posición: a la derecha
		$this->SetY(17);
		//Arial italic 8
		$this->SetFont('Arial','I',8);
		//Número de página
		$this->Cell(0,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'R');
	}

	
	//Tabla del Reporte
	function Crearcarta($carta)
	{				
	
	// QUERY PARA TRAERNOS LOS DATOS DE LA CARTA AVAL
	
		$consulta = "select * from siscam.vst_aut where siscam.vst_aut.aut_cartaaval = ".$carta;
		$query = pg_query($consulta);
		$rowr = pg_fetch_object($query,0);
		$modalidad = $rowr->aut_modalidad;
		
		
	// QUERY PARA TRAERNOS LOS DATOS DE LA PERSONA QUE REVISO LA CARTA AVAL EN BITACORA
		
		$consulta2 = "select * from siscam.bitacora where siscam.bitacora.aut_cartaaval = ".$carta;
		$query2 = pg_query($consulta2);
		$rowr2 = pg_fetch_object($query2,0);
		$revisado = $rowr2->usu_rev;
		
		
		
	
		
		//query para traernos la region
		
		$consulta = "select * from siscam.estado where siscam.estado.esta_id = ".$rowr->esta_id;
		$query = pg_query($consulta);
		$rowt = pg_fetch_object($query,0); 	
		
		
		$this->SetY(34);
		$this->SetX(8);
		$this->SetFont('Arial','',12);
		$this->MultiCell(188, 6,utf8_decode('Señores: '), 0, "J");
		
		$this->SetY(40);
		$this->SetX(8);
		$this->SetFont('Arial','B',12);
		$this->MultiCell(188, 6,$rowr->cli_nombre, 0, "J");

		
		$this->SetY(45);
		$this->SetX(8);
		$this->SetFont('Arial','U',12);
		$this->MultiCell(188, 6,$rowr->esta_estado, 0, "J");
		
		$this->SetY(53);
		$this->SetX(8);
		$this->SetFont('Arial','U',12);
		$this->MultiCell(188, 6,'Beneficio Solidario No:'.$carta, 0, "C");

		
		//Formato
		$this->SetY(60);
		$this->SetX(8);
		$this->SetFont('Arial','',12);
		$this->MultiCell(188, 6,utf8_decode('Estimados Señores:'), 0, "J");	
		$this->SetY(72);
		$this->SetX(8);
		$this->MultiCell(188, 6,utf8_decode('Sirva la presente para notificarle que la Fundación para el Servicio de Asistencia Médica Hospitalaria para los Estudiantes de Educación Superior se compromete a pagar los gastos por servicios prestados en esa institución al Br. '.$rowr->est_pnombre.' '.$rowr->est_papellido.' Cédula de Identidad No. '.$rowr->est_tipo.'-'.$rowr->est_id.' estudiante de la '.$rowr->uni_nombre.', hasta el monto de: '.num2letras($rowr->aut_montoa).' BOLIVARES (Bs.F.'.$rowr->aut_montoa.') por: '.$rowr->aut_diagnostico.', basado en el Baremo Social de FAMES.'), 0, "J");	

		$this->SetY(110);
		$this->SetX(8);
		$this->MultiCell(188, 6,utf8_decode('Asimismo es necesario, una vez verifique el ingreso y/o egreso del paciente, se debe establecer comunicación a los telefonos (0212)564.31.33 / 564.82.10 / 563.20.55.'), 0, "J"); 
		
		$this->SetY(124);
		$this->SetX(8);
		$this->MultiCell(188, 6,utf8_decode('Es condición indispensable para el pago de los servicios prestados, enviar a FAMES los siguientes documentos:'), 0, "J"); 
		
		
		$this->SetY(136);
		$this->SetX(8);
		$this->SetFont('Arial','',12);
		$this->MultiCell(188, 6, utf8_decode('A).- Factura Original elaborada de acuerdo a la Providencia Administrativa del Servicio Nacional Integrado de Administración Aduanera y Tributaria SNAT/2011/00071, publicada en Gaceta Oficial de la República Bolivariana de Venezuela N°39.795 de fecha 08/11/2011, donde se establecen "LAS NORMAS GENERALES DE EMISIÓN DE FACTURAS Y OTROS DOCUMENTOS". Por otro lado, es necesario que la Factura Original este  Firmada y Sellada por la Administración del Centro de Salud.'), 0, "J");
		
		
		$this->SetY(172);
		$this->SetX(8);
		$this->SetFont('Arial','',12);
		$this->MultiCell(188, 6,utf8_decode('B).- Informe médico de ingreso y de egreso, original, sellado y firmado por el especialista, igualmente todos los estudios realizados que soporten el caso.'), 0, "J");
		
		$this->SetY(185);
		$this->SetX(8);
		$this->MultiCell(188, 6,utf8_decode('C).- Desglose de material y medicinas utilizadas durante su estadía.'), 0, "J");
		
		$this->SetY(192);
		$this->SetX(8);
		$this->MultiCell(188, 6,utf8_decode('D).- En caso de retiro de Material de Síntesis, el mismo es propiedad de esta Fundación, por lo tanto el pago de la factura esta condicionada a la recepción del material.'), 0, "J");
		
		$this->SetY(205);
		$this->SetX(8);
		$this->MultiCell(188, 6,utf8_decode('Sin más que hacer referencia,'), 0, "J");
		
		$this->SetY(213);
		$this->SetX(8);
		$this->MultiCell(188, 6,utf8_decode('Atentamente'), 0, "C");
		
		if ($modalidad =='ESPECIAL'){	
		//$this->SetY(228);
		//$this->SetX(8);
		//$this->Image('../imagenes/firmaprof.jpg',66,218,70,40);
		
		$this->SetY(238);
		$this->SetX(8);
		$this->MultiCell(188, 6,utf8_decode('Pltgo. Alejandro Sanchez'), 0, "C");
		
		$this->SetY(243);
		$this->SetX(8);
		$this->MultiCell(188, 6,utf8_decode('Presidente de F.A.M.E.S'), 0, "C");

		$this->SetY(248);
		$this->SetX(8);
		$this->SetFont('Arial','',06);
		$this->MultiCell(188, 6,utf8_decode('Fundación para el Servicio de Asistencia Médica Hospitalaria para los Estudiantes de Educación Superior'), 0, "C");

		$this->SetY(251);
		$this->SetX(8);
		$this->SetFont('Arial','',06);
		$this->MultiCell(188, 6,utf8_decode('Resolución del MPPEUCT N° 015 de fecha 19/01/2017 publicada en G. O. R. B. V. N° 41.089 de fecha 06/02/2017.'), 0, "C");
	} else{
		
		
		//$this->SetY(228);
		//$this->SetX(8);
		//$this->Image('../imagenes/firmanereida.jpg',66,218,70,40);
		
		
		$this->SetY(238);
		$this->SetX(8);
		$this->MultiCell(188, 6,utf8_decode('Pltgo. Alejandro Sanchez'), 0, "C");
		
		$this->SetY(243);
		$this->SetX(8);
		$this->MultiCell(188, 6,utf8_decode('Presidente de F.A.M.E.S'), 0, "C");
		
		$this->SetY(248);
		$this->SetX(8);
		$this->SetFont('Arial','',06);
		$this->MultiCell(188, 6,utf8_decode('Fundación para el Servicio de Asistencia Médica Hospitalaria para los Estudiantes de Educación Superior'), 0, "C");

		$this->SetY(251);
		$this->SetX(8);
		$this->SetFont('Arial','',06);
		$this->MultiCell(188, 6,utf8_decode('Resolución del MPPEUCT N° 015 de fecha 19/01/2017 publicada en G. O. R. B. V. N° 41.089 de fecha 06/02/2017.'), 0, "C");
		
		
	}
		
		$this->SetY(256);
		$this->SetX(8);
		$this->SetFont('Arial','',12);
		$this->MultiCell(188, 6,utf8_decode('NOTA: LA PRESENTE TIENE UNA VALIDEZ DE ('.$rowr->aut_diasv.') DIAS CONTINUOS, A PARTIR DE LA FECHA DE EMISIÓN'), 0, "J");
		if ($modalidad =='ESPECIAL'){	
		$this->SetY(268);
		$this->SetX(8);
		$this->SetFont('Arial','',8);
		$this->MultiCell(188, 6,utf8_decode('AJS/CA/JR-DV/ ').$rowr->usu_nombre_aut, 0, "J");
		}else{
		$this->SetY(268);
		$this->SetX(8);
		$this->SetFont('Arial','',8);
		$this->MultiCell(188, 6,utf8_decode('AJS/CA/JR-DV/ ').$rowr->usu_nombre_aut, 0, "J");
		}
		
		//CONDICION PARA DETERMINAR LA PERSONA QUE REVISO EL CASO EN LA HOJA DE RUTA
		if ($revisado =='DENNYS VILLEGAS'){
		
			$this->SetY(274);
			$this->SetX(8);
			$this->Image('../imagenes/mediafirmadennys.jpg',16,274,8,6);
		
		}else{
			
			$this->SetY(274);
			$this->SetX(8);
			$this->Image('../imagenes/mediafirmajohan.jpg',16,274,8,6);
		
		}
		
		
	
		
		
		
		
		
		
		
		
		$this->SetY(301);
		$this->SetX(8);
		$this->SetFont('Arial','B',8);
		$this->MultiCell(188, 6,utf8_decode('EN TIEMPO DE TRANSFORMACIÓN CONSTRUIMOS UN NUEVO FAMES'), 0, "C");
		$this->Line(10,308,195,308);
		
		$this->SetY(308);
		$this->SetX(8);
		$this->SetFont('Arial','B',8);
		$this->MultiCell(188, 6,utf8_decode('Av. Urdaneta, Esquina de Animas a Plaza España, Edif. Centro Financiero Latino, Piso 10 Oficina 6 y 7. Cod. Postal 1011'), 0, "C");
		
		$this->SetY(312);
		$this->SetX(8);
		$this->MultiCell(188, 6, 'La Candelaria-Caracas. Telefonos: 564.31.33 / 564.11.38', 0, "C");
		
		$this->SetY(316);
		$this->SetX(8);
		$this->MultiCell(188, 6, 'Pagina Web: www.fames.gob.ve Correo Electronico: Fames@fames.gob.ve', 0, "C");
		$this->SetY(320);
		$this->SetX(8);
		$this->SetFont('Arial','B',8);
		$this->MultiCell(188, 6,'Rif: G-20004056-4', 0, "C");
		
	
		
			
		
		
		
		
		
		
		
		
		
		
		
		
		
		//////////////////////////////////////////////////////////
		//CODIGO EXTRA PARA GENERAR VARIAS PAGINAS DE PRUEBA    //
		/*for($i=1;$i<=40;$i++)                                 //
		$this->Cell(0,10,'Imprimiendo Linea de Prueba Numero '.$i,0,1); //
		$this->Ln();*/                                         //
		//////////////////////////////////////////////////////////
		
		//Línea de cierre
		//$this->Cell(array_sum($w),0,'',0);
		}
	
	
	function Crearcodificacion($carta)
	{				
	
	// QUERY PARA TRAERNOS LOS DATOS DE LA CARTA AVAL
	
		$consulta = "select * from siscam.vst_aut where siscam.vst_aut.aut_cartaaval = ".$carta;
		$query = pg_query($consulta);
		$rowr = pg_fetch_object($query,0);
		$montoa = number_format($rowr->aut_montoa,2,',','.');
		
		$edad =	edad($rowr->est_fecnac);
		
		
		
		$montop = number_format($rowr->aut_montop,2,',','.');
		
		$this->SetY(340);
		$this->SetX(8);
		$this->SetFont('Arial','B',12);
		$this->MultiCell(188, 6,utf8_decode('Carta Aval No: '.$carta.'                (CODIFICACIÓN SEGÚN BAREMO)'), 0, "J");
		$this->SetX(8);
		$this->MultiCell(188, 5,utf8_decode('Nro. Control: '.$rowr->aut_ncontrol), 0, "J");
		
		$this->SetY(50);
		$this->SetX(8);
		$this->SetFont('Arial','B',12);
		$this->SetFillColor(192,192,192);
		$this->MultiCell(188, 6,utf8_decode('DATOS PERSONALES'), 1, "C",true);
		$this->SetY(56);
		$this->SetX(8);
		$this->SetFillColor(255,255,255);
		$this->SetFont('Arial','B',12);
		$this->Cell(94,5,'Nombre y Apellido: ',1,0,'R',1);
		$this->SetFont('Arial','',10);
		$this->Cell(94,5,$rowr->est_pnombre.' '.$rowr->est_snombre.' '.$rowr->est_papellido.' '.$rowr->est_sapellido,1,0,'L',1);
		$this->SetY(61);
		$this->SetX(8);
		$this->SetFillColor(255,255,255);
		$this->SetFont('Arial','B',12);
		$this->Cell(94,5,utf8_decode('Cédula: '),1,0,'R',1);
		$this->SetFont('Arial','',10);
		$this->Cell(94,5,$rowr->est_tipo.'-'.$rowr->est_id,1,0,'L',1);
		$this->SetY(66);
		$this->SetX(8);
		$this->SetFillColor(255,255,255);
		$this->SetFont('Arial','B',12);
		$this->Cell(94,5,utf8_decode('Edad: '),1,0,'R',1);
		$this->SetFont('Arial','',10);
		$this->Cell(94,5,$edad.utf8_decode(' Años'),1,0,'L',1);
		$this->SetY(71);
		$this->SetX(8);
		$this->SetFillColor(255,255,255);
		$this->SetFont('Arial','B',12);
		$this->Cell(94,5,utf8_decode('Sexo: '),1,0,'R',1);
		$this->SetFont('Arial','',10);
		$this->Cell(94,5,$rowr->est_sexo,1,0,'L',1);
		$this->SetY(76);
		$this->SetX(8);
		$this->SetFillColor(255,255,255);
		$this->SetFont('Arial','B',12);
		$this->Cell(94,5,utf8_decode('Carrera: '),1,0,'R',1);
		$this->SetFont('Arial','',10);
		$this->Cell(94,5,$rowr->car_nombre,1,0,'L',1);
		$this->SetY(81);
		$this->SetX(8);
		$this->SetFillColor(255,255,255);
		$this->SetFont('Arial','B',12);
		$this->Cell(94,5,utf8_decode('Universidad: '),1,0,'R',1);
		$this->SetFont('Arial','',10);
		$this->Cell(94,5,$rowr->uni_nombre,1,0,'L',1);
		$this->SetY(86);
		$this->SetX(8);
		$this->SetFillColor(255,255,255);
		$this->SetFont('Arial','B',12);
		$this->Cell(94,5,utf8_decode('Estado: '),1,0,'R',1);
		$this->SetFont('Arial','',10);
		$this->Cell(94,5,$rowr->esta_estado,1,0,'L',1);
		$this->SetY(91);
		$this->SetX(8);
		$this->SetFillColor(255,255,255);
		$this->SetFont('Arial','B',12);
		$this->Cell(94,5,utf8_decode('Centro de Salud: '),1,0,'R',1);
		$this->SetFont('Arial','',10);
		$this->Cell(94,5,$rowr->cli_nombre,1,0,'L',1);
		
		$this->SetY(96);
		$this->SetX(8);
		$this->SetFillColor(255,255,255);
		$this->SetFont('Arial','B',12);
		$this->Cell(94,5,utf8_decode('Monto Presupuesto: '),1,0,'R',1);
		$this->SetFont('Arial','',10);
		$this->Cell(94,5,$montop,1,0,'L',1);
		
		
			
		
		$this->SetY(96);
		$this->SetX(8);
		$this->SetFillColor(255,255,255);
		$this->SetFont('Arial','B',12);
		$this->Cell(94,12,utf8_decode('Diagnostico: '),1,0,'R',1);
		$this->SetFont('Arial','',10);
		$this->Cell(94,12,$rowr->aut_diagnostico,1,0,'L',1);
		
		$this->SetY(115);
		$this->SetX(8);
		$this->SetFillColor(192,192,192);
		$this->SetFont('Arial','B',12);
		$this->Cell(100,8,utf8_decode('Descripción '),1,0,'C',1);
		$this->Cell(26,8,utf8_decode('Código '),1,0,'C',1);
		$this->Cell(20,8,utf8_decode('% '),1,0,'C',1);
		$this->Cell(42,8,utf8_decode('Monto '),1,0,'C',1);
		$y = 123;
		$x = 8;
		//	query para traernos el usuario
		
		$consultapat = "select * from siscam.patologia where siscam.patologia.aut_cartaaval = ".$carta;
		$query = pg_query($consultapat);
		while($row = pg_fetch_object($query)){
		$this->SetY($y);
		$this->SetX($x);
		$this->SetFillColor(255,255,255);
		$this->SetFont('Arial','',10);
		$this->Cell(100,5,utf8_decode($row->bar_descripcion),1,0,'L',1);
		$this->Cell(26,5,utf8_decode($row->bar_id),1,0,'C',1);
		$porcentaje = ($row->pat_majust / $row->bar_baremo) * 100;  
		$this->Cell(20,5,number_format($porcentaje,2,',','.'),1,0,'C',1);
		$this->Cell(42,5,number_format($row->pat_majust,2,',','.'),1,0,'C',1);
		$y = $y + 5;
		}
		//$y =$y + 5;
		$this->SetY($y);
		$this->SetX(76);
		$this->SetFont('Arial','B',12);
		$this->Cell(100,8,utf8_decode('Monto Total Autorizado: '),0,0,'R',1);
		$this->SetFont('Arial','',10);
		$this->Cell(20,8,number_format($rowr->aut_montoa,2,',','.'),1,0,'R',1);
		$y =$y + 7;
		$this->SetY($y);
		$this->SetX(76);
		$diferencia =  $rowr->aut_montop - $rowr->aut_montoa;
		if ($modalidad =='ESPECIAL'){
		$this->SetFont('Arial','B',12);
		$this->Cell(100,8,utf8_decode('Diferencia a pagar por Estudiante: '),0,0,'R',1);
		$this->SetFont('Arial','',10);
		$this->Cell(20,8,number_format($diferencia,2,',','.'),1,0,'R',1);
		}
		$this->SetY(220);
		$this->SetX(8);
		$this->SetFont('Arial','B',8);
		$this->MultiCell(188,10,utf8_decode('Observaciones: '.$rowr->aut_observacion),1,"J",1);
		//$this->Cell(188,30,utf8_decode('Observaciones: '.$rowr->aut_observacion),1,0,'l',1);
		$this->SetY(225);
		$this->SetX(8);
		$this->SetFillColor(192,192,192);
		$this->SetFont('Arial','B',12);
		$this->Cell(62,10,utf8_decode('Fecha Elaboración: '),1,0,'C',1);
		$this->Cell(62,10,utf8_decode('Médico Evaluador: '),1,0,'C',1);
		$this->Cell(63,10,utf8_decode('Firma Del Médico: '),1,0,'C',1);
		$this->SetY(235);
		$this->SetX(8);
		//	query para traernos el usuario
		
		//	query para traernos el usuario
		$consultapat = "select distinct(siscam.patologia.usu_cedul_pat) from siscam.patologia where siscam.patologia.aut_cartaaval = ".$carta;
		$query = pg_query($consultapat);
		$row = pg_fetch_object($query,0);
		$consulta = "select * from siscam.usuarios where siscam.usuarios.usu_cedul = ".$row->usu_cedul_pat;
		$query = pg_query($consulta);
		$rows = pg_fetch_object($query,0);
		$this->SetFillColor(255,255,255);
		$this->Cell(62,20,CamFormFech($fechaa),1,0,'C',1);
		$this->Cell(62,20,$rows->usu_nombre,1,0,'C',1);
		$this->Cell(63,20,utf8_decode(''),1,0,'C',1);
		$this->SetY(260);
		$this->SetX(8);
		$this->SetFillColor(255,255,255);
		$this->SetFont('Arial','B',20);
		$cedula = number_format(@$rowr->est_id,0,'','.');
		$this->Cell(90,10,utf8_decode($rowr->est_tipo.'-'.$cedula.' '.$rowr->est_pnombre.' '.substr($rowr->est_papellido,0,1)),1,0,'C',1);
		
		$this->SetY(260);
		$this->SetX(99);
		$this->SetFillColor(255,255,255);
		$this->SetFont('Arial','B',20);
		$this->Cell(90,10,utf8_decode($rowr->est_tipo.'-'.$cedula.' '.$rowr->est_pnombre.' '.substr($rowr->est_papellido,0,1)),1,0,'C',1);
		$this->SetY(275);
		$this->SetX(8);
		$this->SetFillColor(255,255,255);
		$this->SetFont('Arial','B',20);
		$this->Cell(62,10,utf8_decode($carta),1,0,'C',1);
		
		/*$this->SetY(280);
		$this->SetX(8);
		$this->SetFillColor(255,255,255);
		$this->SetFont('Arial','B',14);
		$this->Cell(62,10,utf8_decode('Forma No: '.$rowr->aut_ncontrol),1,0,'C',1);
		$this->SetY(280);
		$this->SetX(70);
		$this->Cell(62,10,utf8_decode('Forma No: '.$rowr->aut_ncontrol),1,0,'C',1);*/
		
		
		
		
		/*$this->SetY(44);
		$this->SetX(8);
		$this->SetFont('Arial','B',12);
		$this->MultiCell(188, 6,$rowr->cli_nombre, 0, "J");
		
		$this->SetY(355);
		$this->SetX(8);
		$this->SetFont('Arial','I',12);
		$this->MultiCell(188, 6,$rowr->esta_estado, 0, "J");

		
		//Formato
		$this->SetY(360);
		$this->SetX(8);
		$this->MultiCell(188, 6,utf8_decode('Estimados Señores:'), 0, "J");	
		$this->SetY(72);
		$this->SetX(8);
		$this->MultiCell(188, 6,utf8_decode('Sirva la presente para notificarle que nos comprometemos a cancelar los gastos por servicios prestados en esa institución al Br.'.$rowr->est_pnombre.' '.$rowr->est_papellido.' Titular de la Cedula de Identidad No. '.$rowr->est_tipo.'-'.$rowr->est_id.' Estudiante de la '.$rowr->uni_nombre.', aprobado hasta el monto de (Bs.F.'.$montoa.') por: '.$rowr->aut_diagnostico.''), 0, "J");	

		$this->SetY(365);
		$this->SetX(8);
		$this->MultiCell(188, 6,utf8_decode('Con la Intención de darle validez al presente compromiso, agradecemos que una vez que se tenga conocimiento del ingreso y egreso del paciente, se comuniquen con nosotros por los telefonos (0212)564.31.33 / 564.82.10 / 563.20.55, a objeto de establecer los controles iherentes al caso. De igual forma debe proceder a informarse del egreso que se incurra.'), 0, "J"); 
		
		$this->SetY(376);
		$this->SetX(8);
		$this->MultiCell(188, 6,utf8_decode('Es condición indispensable para el pago de los honorarios y gastos por servicios clinicos asistenciales, que ustedes nos envíen;'), 0, "J"); 
		
		$this->SetY(384);
		$this->SetX(8);
		$this->MultiCell(188, 6, 'A).- Las facturas originales debidamente firmadas por el paciente o su representante.', 0, "J");*/	
	
		
		
	}
}

//Creación del objeto de la clase heredada
$pdf=new PDF();
$pdf->AliasNbPages();
$pdf->AddPage('P','Legal');


//llamada a la funcion de crear tabla y fecha
if($modalidad != 'AYUDA SOLIDARIA' and $modalidad != 'AYUDA SOLIDARIA ESPECIAL'){
$pdf->Crearcarta($carta);
}
//$pdf->Crearcodificacion($carta);
$pdf->Output();
?>

<?php
function num2letras($num, $fem = true, $dec = true) { 
//if (strlen($num) > 14) die("El n?mero introducido es demasiado grande"); 
   $matuni[2]  = "DOS"; 
   $matuni[3]  = "TRES"; 
   $matuni[4]  = "CUATRO"; 
   $matuni[5]  = "CINCO"; 
   $matuni[6]  = "SEIS"; 
   $matuni[7]  = "SIETE"; 
   $matuni[8]  = "OCHO"; 
   $matuni[9]  = "NUEVE"; 
   $matuni[10] = "DIEZ"; 
   $matuni[11] = "ONCE"; 
   $matuni[12] = "DOCE"; 
   $matuni[13] = "TRECE"; 
   $matuni[14] = "CATORCE"; 
   $matuni[15] = "QUINCE"; 
   $matuni[16] = "DIECISEIS"; 
   $matuni[17] = "DIECISIETE"; 
   $matuni[18] = "DIECIOCHO"; 
   $matuni[19] = "DIECINUEVE"; 
   $matuni[20] = "VEINTE"; 
   $matunisub[2] = "DOS"; 
   $matunisub[3] = "TRES"; 
   $matunisub[4] = "CUATRO"; 
   $matunisub[5] = "QUIN"; 
   $matunisub[6] = "SEIS"; 
   $matunisub[7] = "SETE"; 
   $matunisub[8] = "OCHO"; 
   $matunisub[9] = "NOVE"; 

   $matdec[2] = "VEINTE"; 
   $matdec[3] = "TREINTA"; 
   $matdec[4] = "CUARENTA"; 
   $matdec[5] = "CINCUENTA"; 
   $matdec[6] = "SESENTA"; 
   $matdec[7] = "SETENTA"; 
   $matdec[8] = "OCHENTA"; 
   $matdec[9] = "NOVENTA"; 
   $matsub[3]  = 'MILL'; 
   $matsub[5]  = 'BILL'; 
   $matsub[7]  = 'MILL'; 
   $matsub[9]  = 'TRILL'; 
   $matsub[11] = 'MILL'; 
   $matsub[13] = 'BILL'; 
   $matsub[15] = 'MILL'; 
   $matmil[4]  = 'MILLONES'; 
   $matmil[6]  = 'BILLONES'; 
   $matmil[7]  = 'DE BILLONES'; 
   $matmil[8]  = 'MILLONES DE BILLONES'; 
   $matmil[10] = 'TRILLONES'; 
   $matmil[11] = 'DE TRILLONES'; 
   $matmil[12] = 'MILLONES DE TRILLONES'; 
   $matmil[13] = 'DE TRILLONES'; 
   $matmil[14] = 'BILLONES DE TRILLONES'; 
   $matmil[15] = 'DE BILLONES DE TRILLONES'; 
   $matmil[16] = 'MILLONES DE BILLONES DE TRILLONES'; 

   $num = trim((string)@$num); 
   if ($num[0] == '-') { 
      $neg = 'menos '; 
      $num = substr($num, 1); 
   }else 
      $neg = ''; 
   while ($num[0] == '0') $num = substr($num, 1); 
   if ($num[0] < '1' or $num[0] > 9) $num = '0' . $num; 
   $zeros = true; 
   $punt = false; 
   $ent = ''; 
   $fra = ''; 
   for ($c = 0; $c < strlen($num); $c++) { 
      $n = $num[$c]; 
      if (! (strpos(".,'''", $n) === false)) { 
         if ($punt) break; 
         else{ 
            $punt = true; 
            continue; 
         } 

      }elseif (! (strpos('0123456789', $n) === false)) { 
         if ($punt) { 
            if ($n != '0') $zeros = false; 
            $fra .= $n; 
         }else 

            $ent .= $n; 
      }else 

         break; 

   } 
   $ent = '     ' . $ent; 
   if ($dec and $fra and ! $zeros) { 
      $fin = ' COMA'; 
      for ($n = 0; $n < strlen($fra); $n++) { 
         if (($s = $fra[$n]) == '0') 
            $fin .= ' CERO'; 
         elseif ($s == '1') 
            $fin .= $fem ? ' UNA' : ' UN'; 
         else 
            $fin .= ' ' . $matuni[$s]; 
      } 
   }else 
      $fin = ''; 
   if ((int)$ent === 0) return 'CERO ' . $fin; 
   $tex = ''; 
   $sub = 0; 
   $mils = 0; 
   $neutro = false; 
   while ( ($num = substr($ent, -3)) != '   ') { 
      $ent = substr($ent, 0, -3); 
      if (++$sub < 3 and $fem) { 
         $matuni[1] = 'UNO'; 
         $subcent = 'OS'; 
      }else{ 
         $matuni[1] = $neutro ? 'UN' : 'UNO'; 
         $subcent = 'OS'; 
      } 
      $t = ''; 
      $n2 = substr($num, 1); 
      if ($n2 == '00') { 
      }elseif ($n2 < 21) 
         $t = ' ' . $matuni[(int)$n2]; 
      elseif ($n2 < 30) { 
         $n3 = $num[2]; 
         if ($n3 != 0) $t = 'i' . $matuni[$n3]; 
         $n2 = $num[1]; 
         $t = ' ' . $matdec[$n2] . $t; 
      }else{ 
         $n3 = $num[2]; 
         if ($n3 != 0) $t = ' y ' . $matuni[$n3]; 
         $n2 = $num[1]; 
         $t = ' ' . $matdec[$n2] . $t; 
      } 
      $n = $num[0]; 
      if ($n == 1) { 
         $t = ' CIENTO' . $t; 
      }elseif ($n == 5){ 
         $t = ' ' . $matunisub[$n] . 'IENT' . $subcent . $t; 
      }elseif ($n != 0){ 
         $t = ' ' . $matunisub[$n] . 'CIENT' . $subcent . $t; 
      } 
      if ($sub == 1) { 
      }elseif (! isset($matsub[$sub])) { 
         if ($num == 1) { 
            $t = ' MIL'; 
         }elseif ($num > 1){ 
            $t .= ' MIL'; 
         } 
      }elseif ($num == 1) { 
         $t .= ' ' . $matsub[$sub] . '?n'; 
      }elseif ($num > 1){ 
         $t .= ' ' . $matsub[$sub] . 'ONES'; 
      }   
      if ($num == '000') $mils ++; 
      elseif ($mils != 0) { 
         if (isset($matmil[$sub])) $t .= ' ' . $matmil[$sub]; 
         $mils = 0; 
      } 
      $neutro = true; 
      $tex = $t . $tex; 
   } 
   $tex = $neg . substr($tex, 1) . $fin; 
   return ucfirst($tex); 
} 


//FUNCION PARA CALCULAR LA EDAD
function edad($edad){
	list($anio,$mes,$dia) = explode("-",$edad);
	$anio_dif = date("Y") - $anio;
	$mes_dif = date("m") - $mes;
	$dia_dif = date("d") - $dia;
	if ($mes_dif < 0) $anio_dif--;
	if ($mes_dif == 0 and $dia_dif < 0) $anio_dif--;
	return $anio_dif;
}

function CamFormFech($contenido) {
	if($contenido!=null) {
		$fech=explode('-', $contenido,3);
		$fech2=explode(' ', $fech[2],3);
		if(count($fech2) == 2) {
			return $fech2[0]."/".$fech[1]."/".$fech[0]." ".$fech2[1];
		} else {
			return $fech2[0]."/".$fech[1]."/".$fech[0];
		}
	}
}
?>
