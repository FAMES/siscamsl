<?php
		require('../reports/fpdf16/fpdf.php');
		
				//Conexion al servidor
			$servidor = "localhost";
			$base = "intranet_dev";
			$usuario = "postgres";
			$password = "$2011tyspostgresql%";   
			pg_pconnect("host=$servidor dbname=$base user=$usuario password=$password");	
					
			
				
	class PDF extends fpdf
	{
		function Header()
		{
			$this->SetY(28); 
			$this->SetX(8); 
			$this->SetFont('Arial','B',12);
			$this->multiCell(188,6,'FAMES : '.date("d/m/Y h:i A"),0,"J");
			$this->Image('../imagenes/encabezado_gris.jpg',10,8,190,10);
		}
		function Footer()
		{
			$this->SetY(17);
			$this->SetFont('Arial','I',8);
			$this->Cell(0,10,"Pagina Nro : ".$this->PageNo().'/{nb}',0,0,'R');
		}
		function datos()
		{
			$carta = $_POST["hdncarta"];
			$consulta = "SELECT * FROM siscam.vst_aut WHERE siscam.vst_aut.aut_cartaaval = ".$carta;
				$query = pg_query($consulta);
				$row = pg_fetch_object($query,0);
		//Primera Fila
		$this->SetY(35);
		$this->SetFont('Times','B',9);
		$this->Cell(0,5,'1. DATOS PERSONALES','LTRB',0,'C');
		$this->Ln();		
		$this->Cell(80,4,'1. Nombres y Apellidos:','LR',0,'L');		
		$this->Cell(35,4,'2. Cedula:','R',0,'L');		
		$this->Cell(15,4,'3. Edad:','LR',0,'L');		
		$this->Cell(30,4,'3. Sexo:','LR',0,'L');
		$this->Cell(0,4,'4. N� Carta Aval:','R',0,'L');
		$this->Ln();		
		$this->SetFont('Times','',9);
		$this->Cell(80,3,$row->est_pnombre.' '.$row->est_snombre.' '.$row->est_papellido.' '.$row->est_sapellido,'LR',0,'C');
		$this->Cell(35,3,$row->est_id,'R',0,'C'); 
		$this->Cell(15,3,$row->est_edad,'R',0,'C');
		$this->Cell(30,3,$row->est_sexo,'R',0,'C');
		$this->Cell(0,3,$row->aut_cartaaval,'R',0,'C');
		$this->Ln();
		
		//Segunda Fila
		$this->SetFont('Times','B',9);
		$this->Cell(115,4,'Universidad: ','LTR',0,'L');
		$this->Cell(45,4,'','TR',0,'L');
		$this->Cell(0,4,'','TR',0,'L');
		$this->Ln();
		$this->SetFont('Times','',9);
		$this->Cell(115,3,$row->uni_nombre,'LR',0,'C');
		$this->Cell(45,3,$row->ciu_ciudad,'LR',0,'C');
		$this->Cell(0,3,'','LR',0,'C');
		$this->Ln();
		
		//Tercera Fila
		$this->SetFont('Times','B',9);
		$this->Cell(0,4,'Patologia (Codigo) : ','LTR',0,'L');		
		$this->Ln();
		$this->SetFont('Times','',9);
		$this->MultiCell(0,4,$_POST['patologias'].$_POST['cod_pat'],'LR','L');	
		
		//Tercera Fila		
		$this->SetFont('Times','B',9);
		$this->Cell(0,5,'2. RESUMEN MEDICO','LTRB',0,'C');	
		$this->Ln();
		$this->SetFont('Times','',9);		
		$this->MultiCell(0,4,$_POST['txtresumen'],1,"J");
		
		//Cuarta Fila
		$this->SetFont('Times','B',9);
		$this->Cell(0,5,'MODALIDAD DE ATENCION','LTRB',0,'C');
		$this->Ln();
		$this->SetFont('Times','',9);
		$this->Cell(60,5,'1. Tratamiento Medico-Quirurgico : ','L',0,'L');
		$this->Cell(55,5,$_POST['trat_med_quiru'],0,0,'L');
		$this->Cell(45,5,'2. Emergencias : ',0,0,'L');
		$this->Cell(0,5,$_POST['emergencias'],'R',0,'L');
		$this->Ln();
		$this->Cell(60,5,'3. Pago a Centros de Salud : ','LB',0,'L');
		$this->Cell(55,5,$_POST['centros_salud'],'B',0,'L');
		$this->Cell(45,5,'4. Ayuda Solidaria : ','B',0,'L');
		$this->Cell(0,5,$_POST['ayuda_solidaria'],'RB',0,'L');
		$this->Ln();
		
		//Quinta Fila
		$this->SetFont('Times','B',9);
		$this->Cell(10,5,'','LR',0,'C');
		$this->Cell(150,5,'Centro de Salud','B',0,'C');
		$this->Cell(0,5,'','LR',0,'C');
		$this->Ln();
		$this->Cell(10,7,'N�','LR',0,'C');
		if($_POST['doc'] == 'fact') { $sel_fact = X; }
		else if($_POST['doc'] == 'pres') { $sel_pres = X; }
		$this->Cell(75,7,'Presupuesto : '.$sel_pres,'R',0,'C');
		$this->Cell(75,7,'Factura : '.$sel_fact,'L',0,'C');
		$this->Cell(0,7,'Monto (Bs.)','LR',0,'C');
		$this->Ln();
		$this->SetFont('Times','',9);		
		$this->Cell(10,5,'1.','LTRB',0,'C');
		$this->Cell(150,5,$_POST['clinica1'],'TRB',0,'C');
		$this->Cell(0,5,number_format($_POST['monto_centro1'],2,',','.'),'TRB',0,'C');
		$this->Ln();
		$this->Cell(10,5,'2.','LTRB',0,'C');
		$this->Cell(150,5,$_POST['clinica2'],'TRB',0,'C');
		$this->Cell(0,5,number_format($_POST['monto_centro2'],2,',','.'),'TRB',0,'C');
		$this->Ln();
		$this->Cell(10,5,'3.','LTRB',0,'C');
		$this->Cell(150,5,$_POST['clinica3'],'TRB',0,'C');
		$this->Cell(0,5,number_format($_POST['monto_centro3'],2,',','.'),'TRB',0,'C');
		$this->Ln();
		$this->Cell(10,5,'4.','LTRB',0,'C');
		$this->Cell(150,5,$_POST['clinica4'],'TRB',0,'C');
		$this->Cell(0,5,number_format($_POST['monto_centro4'],2,',','.'),'TRB',0,'C');
		$this->Ln();
		$this->Cell(10,5,'',0,0,'C');
		$this->SetFont('Arial','B',9);
		$this->Cell(150,5,'SubTotal : ','LTRB',0,'R');
		$this->Cell(0,5,number_format($_POST['subTotal_centro'],2,',','.'),'LTRB',0,'C');
		$this->Ln(); 
		
		//Sexta Fila
		$this->SetFont('Times','B',9);
		$this->Cell(10,5,'','LTR',0,'C');
		$this->Cell(150,5,'Proveedor','LB',0,'C');
		$this->Cell(0,5,'','LR',0,'C');
		$this->Ln();
		$this->Cell(10,10,'N�','LR',0,'C');
		if($_POST['doc1'] == 'fact') { $sel_fact1 = X; }
		else if($_POST['doc1'] == 'pres') { $sel_pres1 = X; }
		$this->Cell(75,7,'Presupuesto : '.$sel_pres1,'R',0,'C');
		$this->Cell(75,7,'Factura : '.$sel_fact1,'L',0,'C');
		$this->Cell(0,7,'Monto (Bs.)','LR',0,'C');
		$this->Ln();
		$this->SetFont('Times','',9);		
		$this->Cell(10,5,'1.','LTRB',0,'C');
		$this->Cell(150,5,$_POST['proveedor1'],'TRB',0,'C');
		$this->Cell(0,5,number_format($_POST['monto_material1'],2,',','.'),'TRB',0,'C');
		$this->Ln();
		$this->Cell(10,5,'2.','LTRB',0,'C');
		$this->Cell(150,5,$_POST['proveedor2'],'TRB',0,'C');
		$this->Cell(0,5,number_format($_POST['monto_material2'],2,',','.'),'TRB',0,'C');
		$this->Ln();
		$this->Cell(10,5,'3.','LTRB',0,'C');
		$this->Cell(150,5,$_POST['proveedor3'],'TRB',0,'C');
		$this->Cell(0,5,number_format($_POST['monto_material3'],2,',','.'),'TRB',0,'C');
		$this->Ln();
		$this->Cell(10,5,'4.','LTRB',0,'C');
		$this->Cell(150,5,$_POST['proveedor4'],'TRB',0,'C');
		$this->Cell(0,5,number_format($_POST['monto_material4'],2,',','.'),'TRB',0,'C');
		$this->Ln();
		$this->SetFont('Arial','B',9);
		$this->Cell(10,5,'',0,0,'C');
		$this->Cell(150,5,'SubTotal : ','LRB',0,'R');		
		$this->Cell(0,5,number_format($_POST['subTotal_material'],2,',','.'),'LRB',0,'C');
		$this->Ln();
		
		//Septima Fila
		$this->Cell(10,4,'',0,0,'C');
		$this->Cell(150,4,'Monto Total (Presupuesto y/o Factura) (Bs.) : ','LRB',0,'R');
		$this->Cell(0,4,number_format($_POST['total_proc'],2,',','.'),'LRB',0,'C');		
		
		//Octava Fila
		$this->Ln(8);
		$this->Cell(40,4,'Fecha de Elaboracion : ','TLR',0,'L');
		$this->Cell(80,4,'Medico Tratante Evaluador : ','TLR',0,'L');
		$this->Cell(0,4,'Firma del Medico Evaluador : ','TLR',0,'L');
		$this->Ln();
		$this->SetFont('Times','',9);
		$this->Cell(40,3,date("d/m/Y"),'LRB',0,'L');
		$this->Cell(80,3,$_POST['usuario'],'LRB',0,'L');
		$this->Cell(0,3,'','LRB',0,'L');
		$this->Ln();
		
		//Novena Fila
		$this->Ln(8);
		$this->SetFont('Times','B',9);
		$this->Cell(0,5,'3. DECISION DE LA COMISION DE ESTUDIO DE CASOS ESPECIALES','LTRB',0,'C');
		$this->Ln();
		$this->Cell(120,4,'',0,0,'C');
		$this->Cell(0,4,'Fecha de Discusion y Aprobacion : ','LTRB',0,'C');
		$this->Ln();
		$this->SetFont('Times','',9);
		$this->Cell(120,4,'',0,0,'C');
		$this->Cell(25,4,'Dia','LRB',0,'C');
		$this->Cell(25,4,'Mes','LRB',0,'C');
		$this->Cell(0,4,'A�o','LRB',0,'C');
		$this->Ln();		
		$this->Cell(120,7,'',0,0,'C');
		$this->Cell(25,7,'','LRB',0,'C');
		$this->Cell(25,7,'','LRB',0,'C');
		$this->Cell(0,7,'','LRB',0,'C');
		$this->Ln();
				
		//Decima Fila
		$this->SetFont('Times','B',9);
		$this->Cell(60,4,'Emitir','TLB',0,'C');
		$this->Cell(80,4,'A Nombre de : ','TLB',0,'C');
		$this->Cell(0,4,'Monto en Bs. : ','TLRB',0,'C');
		$this->Ln();
		$this->Cell(60,1,'','LR',0,'C');
		$this->Cell(80,1,'','LR',0,'C');
		$this->Cell(0,1,'','LR',0,'C');
		$this->Ln();
		$this->SetFont('Times','',9);
		$this->Cell(40,3,'Cheque : ','L',0,'C');
		$this->Cell(10,3,'','TLBR',0,'C');
		$this->Cell(10,3,'','R',0,'C');
		$this->Cell(80,3,'','R',0,'C');
		$this->Cell(0,3,'','R',0,'C');
		$this->Ln();
		$this->Cell(60,3,'','LR',0,'C');
		$this->Cell(80,3,'','LR',0,'C');
		$this->Cell(0,3,'','LR',0,'C');
		$this->Ln();
		$this->Cell(40,3,'Carta Aval : ','L',0,'C');
		$this->Cell(10,3,'','TLBR',0,'C');
		$this->Cell(10,3,'','R',0,'C');
		$this->Cell(80,3,'','R',0,'C');
		$this->Cell(0,3,'','R',0,'C');
		$this->Ln();
		$this->Cell(60,1,'','LR',0,'C');
		$this->Cell(80,1,'','LR',0,'C');
		$this->Cell(0,1,'','LR',0,'C');
		$this->Ln();
		
		//Decima Primera Fila
		$this->SetFont('Times','B',9);
		$this->Cell(60,4,'Emitir','TLB',0,'C');
		$this->Cell(80,4,'A Nombre de : ','TLB',0,'C');
		$this->Cell(0,4,'Monto en Bs. : ','TLRB',0,'C');
		$this->Ln();
		$this->Cell(60,1,'','LR',0,'C');
		$this->Cell(80,1,'','LR',0,'C');
		$this->Cell(0,1,'','LR',0,'C');
		$this->Ln();
		$this->SetFont('Times','',9);
		$this->Cell(40,3,'Cheque : ','L',0,'C');
		$this->Cell(10,3,'','TLBR',0,'C');
		$this->Cell(10,3,'','R',0,'C');
		$this->Cell(80,3,'','R',0,'C');
		$this->Cell(0,3,'','R',0,'C');
		$this->Ln();
		$this->Cell(60,3,'','LR',0,'C');
		$this->Cell(80,3,'','LR',0,'C');
		$this->Cell(0,3,'','LR',0,'C');
		$this->Ln();
		$this->Cell(40,3,'Carta Aval : ','L',0,'C');
		$this->Cell(10,3,'','TLBR',0,'C');
		$this->Cell(10,3,'','R',0,'C');
		$this->Cell(80,3,'','R',0,'C');
		$this->Cell(0,3,'','R',0,'C');
		$this->Ln();
		$this->Cell(60,1,'','LBR',0,'C');
		$this->Cell(80,1,'','LBR',0,'C');
		$this->Cell(0,1,'','LBR',0,'C');
		$this->Ln();
		
		//decima Segunda Fila		
		$this->Cell(0,3,'','LR',0,'C');		
		$this->Ln();
		$this->Cell(40,4,'Total Aprobado : ','L',0,'C');
		$this->Cell(30,4,'','TLBR',0,'C');
		$this->Cell(0,4,' % ','R',0,'L');
		$this->Ln();
		$this->Cell(0,3,'','LBR',0,'C');		
		$this->Ln();
		
		//Decima Tercera Fila
		$this->Cell(140,4,'Diferencia a Pagar por el Estudiante : ','RL',0,'L');
		$this->Cell(0,4,'Monto en (Bs.) : ','RL',0,'L');
		$this->Ln();
		$this->Cell(140,4,' ','BRL',0,'C');
		$this->Cell(0,4,' ','BRL',0,'C');
		$this->Ln();
		
		//Decima Cuarta Fila
		$this->SetFont('Times','B',9);
		$this->Cell(0,20,'','LBR',0,'L');
		$this->Ln();
		$this->Cell(50,4,' Presidente(a) : ','LBR',0,'L');
		$this->Cell(50,4,' Jefe(a) de Unidad Operativa : ','LBR',0,'L');
		$this->Cell(50,4,' Jefe(a) de Unidad Medica : ','LBR',0,'L');
		$this->Cell(0,4,' Coor. Analisis de Operaciones: ','LBR',0,'L');
		$this->Ln();
		$this->Cell(50,15,'','LBR',0,'L');
		$this->Cell(50,15,'','LBR',0,'L');
		$this->Cell(50,15,'','LBR',0,'L');
		$this->Cell(0,15,'','LBR',0,'L');
		}
		}
	$new = new PDF();
	$new->AliasNbPages();
	$new->AddPage('P','Legal');
	$new->datos();
	$new->Output(); 	
?>
