<?php
		require('../reports/fpdf16/fpdf.php');
		
				//Conexion al servidor
			$servidor = "localhost";
			$base = "intranet_dev";
			$usuario = "postgres";
			$password = "$2011tyspostgresql%";   
			pg_pconnect("host=$servidor dbname=$base user=$usuario password=$password");	
					
			
				
	class PDF extends fpdf
	{
		function Header()
		{
			$this->SetY(28); 
			$this->SetX(8); 
			$this->SetFont('Arial','B',12);
			$this->multiCell(188,6,'FAMES : '.date("d/m/Y h:i A"),0,"J");
			$this->Image('../imagenes/encabezado_carta.jpg',10,8,190,10);
		}
		function Footer()
		{
			$this->SetY(17);
			$this->SetFont('Arial','I',8);
			$this->Cell(0,10,"Pagina Nro : ".$this->PageNo().'/{nb}',0,0,'R');
		}
		function datos()
		{
			$carta = $_POST["hdncarta"];
			$consulta = "SELECT * FROM siscam.vst_aut WHERE siscam.vst_aut.aut_cartaaval = ".$carta;
				$query = pg_query($consulta);
				$row = pg_fetch_object($query,0);
		//Primera Fila
		$this->SetY(35);
		$this->SetFont('Times','B',9);
		$this->Cell(0,5,'1. DATOS PERSONALES','LTRB',0,'C');
		$this->Ln();		
		$this->Cell(80,4,'1. Nombres y Apellidos:','LR',0,'L');		
		$this->Cell(35,4,'2. Cedula:','R',0,'L');		
		$this->Cell(15,4,'3. Edad:','LR',0,'L');		
		$this->Cell(30,4,'3. Sexo:','LR',0,'L');
		$this->Cell(0,4,'4. Nro Carta Aval:','R',0,'L');
		$this->Ln();		
		$this->SetFont('Times','',9);
		$this->Cell(80,3,$row->est_pnombre.' '.$row->est_snombre.' '.$row->est_papellido.' '.$row->est_sapellido,'LR',0,'C');
		$this->Cell(35,3,$row->est_id,'R',0,'C'); 
		$this->Cell(15,3,$row->est_edad,'R',0,'C');
		$this->Cell(30,3,$row->est_sexo,'R',0,'C');
		$this->Cell(0,3,$carta,'R',0,'C');
		$this->Ln();
		
		//Segunda Fila
		$this->SetFont('Times','B',9);
		$this->Cell(115,4,'Universidad: ','LTR',0,'L');
		$this->Cell(45,4,utf8_decode('Fecha de la Atención'),'TR',0,'L');
		$this->Cell(0,4,utf8_decode('Días Hospitalización'),'TR',0,'L');
		$this->Ln();
		$this->SetFont('Times','',9);
		$this->Cell(115,3,$row->uni_nombre,'LR',0,'C');
		$this->Cell(45,3,CamFormFech($_POST['txtfechaaten']),'LR',0,'C');
		$this->Cell(0,3,$_POST['txtdias'],'LR',0,'C');
		$this->Ln();
		
		//Tercera Fila
		$this->SetFont('Times','B',9);
		$this->Cell(0,4,'Patologia (Codigo) : ','LTR',0,'L');		
		$this->Ln();
		$this->SetFont('Times','',9);
		$this->MultiCell(0,4,$row->aut_diagnostico,'LR','L');
		
		//Tercera Fila		
		$this->SetFont('Times','B',9);
		$this->Cell(0,5,utf8_decode('2. SEGUIMIENTO ATENCIÓN'),'LTRB',0,'C');
		$this->Ln();
		$this->SetFont('Times','',9);		
		$this->MultiCell(0,4,$_POST['txtmotivo'],1,"J");
		
		//Cuarta Fila
		$this->SetFont('Times','B',9);
		$this->Cell(0,5,utf8_decode('3. Clinica donde Recibió la Atención'),'LTRB',0,'L');
		$this->Ln();
		$this->SetFont('Times','',9);
		$this->MultiCell(0,4,$_POST['clinica1'],1,"J");

			$consulta = "SELECT * FROM siscam.seguimiento WHERE siscam.seguimiento.aut_cartaaval =".$carta;
			$query = pg_query($consulta);
			$row2 = pg_fetch_object($query,0);

		//Quinta Fila
		$this->SetFont('Times','B',9);
		$this->Cell(0,5,utf8_decode('4. Las Patologías coincide con Informe Médico:'),'LTRB',0,'L');
		$this->Ln();
		$this->SetFont('Times','',9);

		if($row2->seg_compare=='SI'){ $sel_si = X;} else if($row2->seg_compare =='NO'){ $sel_no = X;}

		$this->Cell(60,5,'1. SI : ','L',0,'L');
		$this->Cell(55,5,$sel_si,0,0,'L');
		$this->Cell(45,5,'2. NO : ',0,0,'L');
		$this->Cell(0,5,$sel_no,'R',0,'L');
		$this->Ln();
		$this->Cell(60,5,'','LB',0,'L');
		$this->Cell(55,5,'','B',0,'L');
		$this->Cell(45,5,'','B',0,'L');
		$this->Cell(0,5,'','RB',0,'L');
		$this->Ln();

		//Sexta Fila


		$this->SetFont('Times','B',9);
		$this->Cell(0,5,utf8_decode('5. Tipo de Tratamiento:'),'LTRB',0,'L');
		$this->Ln();
		$this->SetFont('Times','',9);

		if($row2->seg_tiptra=='AMBULATORIO'){ $sel_amb = X;} else if($row2->seg_tiptra =='QUIRURGICO'){ $sel_quir = X;}

		$this->Cell(60,5,'1. AMBULATORIO : ','L',0,'L');
		$this->Cell(55,5,$sel_amb,0,0,'L');
		$this->Cell(45,5,'2. QUIRURGICO : ',0,0,'L');
		$this->Cell(0,5,$sel_quir,'R',0,'L');
		$this->Ln();
		$this->Cell(60,5,'','LB',0,'L');
		$this->Cell(55,5,'','B',0,'L');
		$this->Cell(45,5,'','B',0,'L');
		$this->Cell(0,5,'','RB',0,'L');
		$this->Ln();


		//Septima Fila

		$this->SetFont('Times','B',9);
		$this->Cell(0,5,utf8_decode('6. La Atención fue Realizada en un solo ingreso:'),'LTRB',0,'L');
		$this->Ln();
		$this->SetFont('Times','',9);

			if($row2->seg_singre=='SI'){ $sel_singresi = X;} else if($row2->seg_singre =='NO'){ $sel_singreno = X;}

		$this->Cell(60,5,'1. SI : ','L',0,'L');
		$this->Cell(55,5,$sel_singresi,0,0,'L');
		$this->Cell(45,5,'2. NO : ',0,0,'L');
		$this->Cell(0,5,$sel_singreno,'R',0,'L');
		$this->Ln();
		$this->Cell(60,5,'','LB',0,'L');
		$this->Cell(55,5,'','B',0,'L');
		$this->Cell(45,5,'','B',0,'L');
		$this->Cell(0,5,'','RB',0,'L');
		$this->Ln();


		//Septima Fila

		$this->SetFont('Times','B',9);
		$this->Cell(0,5,utf8_decode('7. Se Requirío Material de Sintesis en la Intervención: '),'LTRB',0,'L');
		$this->Ln();
		$this->SetFont('Times','',9);

		if($row2->seg_msinte=='SI'){ $sel_msintesi = X;} else if($row2->seg_msinte =='NO'){ $sel_msinteno = X;}

		$this->Cell(60,5,'1. SI : ','L',0,'L');
		$this->Cell(55,5,$sel_msintesi,0,0,'L');
		$this->Cell(45,5,'2. NO : ',0,0,'L');
		$this->Cell(0,5,$sel_msinteno,'R',0,'L');
		$this->Ln();
		$this->Cell(60,5,'','LB',0,'L');
		$this->Cell(55,5,'','B',0,'L');
		$this->Cell(45,5,'','B',0,'L');
		$this->Cell(0,5,'','RB',0,'L');
		$this->Ln();


		//Octava Fila
		$this->SetFont('Times','B',9);
		$this->Cell(0,5,utf8_decode('8. Casa Comercial:'),'LTRB',0,'L');
		$this->Ln();
		$this->SetFont('Times','',9);
		$this->MultiCell(0,4,$_POST['txtcasacomercial'],1,"J");


		//Septima Fila

		$this->SetFont('Times','B',9);
		$this->Cell(0,5,utf8_decode('9. Se evidencia directamente el Tratamiento Quirurgico: '),'LTRB',0,'L');
		$this->Ln();
		$this->SetFont('Times','',9);

		if($row2->seg_tquirur=='SI'){ $sel_tquirursi = X;} else if($row2->seg_tquirur =='NO'){ $sel_tquirurno = X;}

		$this->Cell(60,5,'1. SI : ','L',0,'L');
		$this->Cell(55,5,$sel_tquirursi,0,0,'L');
		$this->Cell(45,5,'2. NO : ',0,0,'L');
		$this->Cell(0,5,$sel_tquirurno,'R',0,'L');
		$this->Ln();
		$this->Cell(60,5,'','LB',0,'L');
		$this->Cell(55,5,'','B',0,'L');
		$this->Cell(45,5,'','B',0,'L');
		$this->Cell(0,5,'','RB',0,'L');
		$this->Ln();


		//Novena Fila
		$this->SetFont('Times','B',9);
		$this->Cell(0,20,'','LBR',0,'L');
		$this->Ln();
		$this->Cell(50,4,' Presidente(a) : ','LBR',0,'L');
		$this->Cell(50,4,' Jefe(a) de Unidad Operativa : ','LBR',0,'L');
		$this->Cell(50,4,' Analista Integral  : ','LBR',0,'L');
		$this->Cell(0,4,' Coor. Analisis de Operaciones: ','LBR',0,'L');
		$this->Ln();
		$this->Cell(50,15,'','LBR',0,'L');
		$this->Cell(50,15,'','LBR',0,'L');
		$this->Cell(50,15,'','LBR',0,'L');
		$this->Cell(0,15,'','LBR',0,'L');
		}
		}
	$new = new PDF();
	$new->AliasNbPages();
	$new->AddPage('P','Legal');
	$new->datos();
	$new->Output(); 	
?>

<?
function CamFormFech($contenido) {
	if($contenido!=null) {
		$fech=explode('-', $contenido,3);
		$fech2=explode(' ', $fech[2],3);
		if(count($fech2) == 2) {
			return $fech2[0]."/".$fech[1]."/".$fech[0]." ".$fech2[1];
		} else {
			return $fech2[0]."/".$fech[1]."/".$fech[0];
		}
	}
}

?>
