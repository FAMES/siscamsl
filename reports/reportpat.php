<?php
require('../reports/fpdf16/fpdf.php');

	//Conexion al servidor
	$servidor = "190.190.1.19";
	$base = "intranet_dev";
	$usuario = "postgres";
	$password = "$2011tyspostgresql%";   
	pg_pconnect("host=$servidor dbname=$base user=$usuario password=$password");
	
	
//Referencia

	$tipo = strtoupper(@$_POST["cobtipo"]);
    $cboano = strtoupper(@$_POST["cboano"]);
	if($cboano ==''){
	 $cboano =0;
	}
    $cbopato = strtoupper(@$_POST["cbopato"]);
	if($cbopato ==''){
	 $cbopato =0;
	}
	$caso = strtoupper(@$_POST["cobcaso"]);
	$txtfechadesde = strtoupper(@$_POST["txtfechadesde"]); 
	$txtfechahasta = strtoupper(@$_POST["txtfechahasta"]);
	
class PDF extends FPDF
{


	//Cabecera de página
	function Header()
	{		
		//fecha
		$this->SetY(20);
		$this->SetFont('Arial','I',10);
		$this->Cell(0,8,'Fecha: '.date('d/m/Y h:i A'),0,0,'L');
		//Logo
		$this->Image('../imagenes/encabezado_gris.jpg',13,8,180,12);
		//Fuente
		$this->SetFont('Arial','B',15);
		//Movernos a la derecha
		$this->SetY(16);
		$this->SetX(96);
		//Título		
		$this->Cell(30,25,utf8_decode('Reportes por Patologias'),0,0,'C');
		

	}

	//Numero de Página
	function Footer()
	{
		//Posición: a la derecha
		$this->SetY(19);
		//Arial italic 8
		$this->SetFont('Arial','I',10);
		//Número de página
		$this->Cell(0,10,utf8_decode('Página '.$this->PageNo().'/{nb}'),0,0,'R');
	}
	
	//Tabla del Reporte
	function CrearTabla($tipo,$cboano,$cbopato,$caso,$txtfechadesde,$txtfechahasta)
	{		
	
		$this->SetFont('Times','B',10);
		$this->SetY(26);
		$this->SetX(58);
		if ($tipo =='10'){
		 	$titulo1 = 'Las 10 primeras Patologias';
		}
		
		if($tipo =='4'){
			$titulo1 = 'Resumen por Patologia';
		}
		
		if($caso =='1'){
			$titulo2 ='Autorizadas';
		}if($caso=='2'){
			$titulo2='Atendidas';
		}
		
		if ($tipo !='0' and $caso !='0' and $txtfechadesde !='0' and $txtfechahasta !='0'){
		 
		$this->Cell(30,25,utf8_decode(''.$titulo1.' '.$titulo2.' Desde '.CamFormFech($txtfechadesde).' Hasta '.CamFormFech($txtfechahasta)),0);
		$this->Ln();
		}
		//Anchuras de las columnas
		$this->SetY(49);
		$this->SetFont('Times','',9);
		$w=array(22,22,22,22,55,25);
		$this->Ln(8);
		//Subtítulos
		$this->SetY(51);
		$this->SetFont('Times','B',9);
		$this->Cell(29,5,utf8_decode('Codigo Patologia'),0);
        $this->Cell(83,5,utf8_decode('Descripcion'),0);
		$this->Cell(23,5,utf8_decode('Total Caso'),0);
        $this->Cell(33,5,utf8_decode('Total General'),0);
		$this->Cell(23,5,utf8_decode('Año'),0);
		$this->Line(10,56,195,56);
		$this->Ln();
		
		
		if ($tipo=='10' and $caso!='0' and $cboano =='0' and $cbopato=='0'){	
	//echo 'estoy aqui';
					
				if (($tipo!='0') and ($tipo!=NULL)){
					$gancho =" limit '$tipo'";
				}
				
					if($caso=='1'){
						//echo 'estoy con caso =1';
					
					$condi =" where siscam.vst_aut_pat.aut_estatus ='AUT' and siscam.vst_aut_pat.aut_fechaa >= '$txtfechadesde' and siscam.vst_aut_pat.aut_fechaa <= '$txtfechahasta'";
					} elseif($caso =='2'){
						//echo 'estoy con caso =2';
					$condi =" where siscam.vst_aut_pat.aut_estatus <>'AUT' and siscam.vst_aut_pat.aut_estatus <>'PDO' and siscam.vst_aut_pat.aut_estatus <>'UMED' and siscam.vst_aut_pat.aut_estatus <>'TRAMITE' and siscam.vst_aut_pat.aut_estatus <>'ANULADO' and siscam.vst_aut_pat.aut_fechaa > '$txtfechadesde' and siscam.vst_aut_pat.aut_fechaa < '$txtfechahasta' and siscam.vst_aut_pat.bar_ano ='2012'";
					}
				
			$consulta ="SELECT DISTINCT siscam.vst_aut_pat.bar_id, siscam.vst_aut_pat.bar_descripcion, 			count(siscam.vst_aut_pat.bar_id) AS conteo, sum(siscam.vst_aut_pat.pat_majust) AS suma, siscam.vst_aut_pat.bar_ano FROM siscam.vst_aut_pat $condi GROUP BY siscam.vst_aut_pat.bar_id, siscam.vst_aut_pat.bar_ano, siscam.vst_aut_pat.bar_descripcion ORDER BY conteo desc $gancho";
}
		if ($tipo =='4' and $caso!='0' and $cboano !='0' and $cbopato!='0'){		
				
			if($caso=='1'){
					
					$condi =" where siscam.vst_aut_pat.aut_estatus ='AUT' and siscam.vst_aut_pat.aut_fechaa >= '$txtfechadesde' and siscam.vst_aut_pat.aut_fechaa <= '$txtfechahasta' and siscam.vst_aut_pat.bar_ano =$cboano and siscam.vst_aut_pat.bar_id =$cbopato";
					} elseif($caso =='2'){
					$condi =" where siscam.vst_aut_pat.aut_estatus <>'AUT' and siscam.vst_aut_pat.aut_estatus <>'PDO' and siscam.vst_aut_pat.aut_estatus <>'UMED' and siscam.vst_aut_pat.aut_estatus <>'TRAMITE' and siscam.vst_aut_pat.aut_estatus <>'ANULADO' and siscam.vst_aut_pat.aut_fechaa > '$txtfechadesde' and siscam.vst_aut_pat.aut_fechaa < '$txtfechahasta' and siscam.vst_aut_pat.bar_ano =$cboano and siscam.vst_aut_pat.bar_id =$cbopato";
					}
				
			$consulta ="SELECT DISTINCT siscam.vst_aut_pat.bar_id, siscam.vst_aut_pat.bar_descripcion, 			count(siscam.vst_aut_pat.bar_id) AS conteo, sum(siscam.vst_aut_pat.pat_majust) AS suma, siscam.vst_aut_pat.bar_ano FROM siscam.vst_aut_pat $condi GROUP BY siscam.vst_aut_pat.bar_id, siscam.vst_aut_pat.bar_ano, siscam.vst_aut_pat.bar_descripcion ORDER BY siscam.vst_aut_pat.bar_id, siscam.vst_aut_pat.bar_descripcion, count(siscam.vst_aut_pat.bar_id), sum(siscam.vst_aut_pat.pat_majust), siscam.vst_aut_pat.bar_ano asc";		
		}
		
		//$this->Cell(29,5,utf8_decode($consulta),0);
		
		$query = pg_query($consulta);
		while($rowrs = pg_fetch_object($query)) { 
			$this->Cell(29,5,utf8_decode($rowrs->bar_id),0);
			$this->Cell(83,5,utf8_decode(substr($rowrs->bar_descripcion,0,35)),0);
			$this->Cell(23,5,utf8_decode($rowrs->conteo),0);
			$this->Cell(33,5,number_format($rowrs->suma,2,',','.'),0);
			$this->Cell(23,5,utf8_decode($rowrs->bar_ano),0);
			$this->Ln();
			$c = $c +1;
			$montotal = $montotal + $rowrs->suma;
		}
		
		//muestra totales del reporte
		//$consulta2 = "select * from siscam.relacion where siscam.relacion.re_id=$re";
		//$query2 = pg_query($consulta2);
		//$row2 = pg_fetch_object($query2);
		$this->Ln(4);
		$this->SetX(130);
		$this->Cell(40,5,'TOTAL CASOS: ','LT',0);
		$this->SetX(170);
		$this->Cell(25,5,number_format($c,0,',','.'),'TR',0);
		$this->Ln();
		$this->SetX(130);
		$this->Cell(40,5,'TOTAL GENERAL: ','LB',0);
		$this->SetX(170);
		$this->Cell(25,5,number_format($montotal,2,',','.'),'BR',0);
		//$this->Ln();
		/*$this->SetX(130);
		$this->Cell(30,5,'IMPUESTO DEL 1*1000: ','L',0);
		$this->SetX(170);
		$this->Cell(25,5,number_format($row2->re_riam,2,',','.'),'R',0);
		$this->Ln();
		$this->SetX(130);
		$this->Cell(40,5,'TOTAL A CANCELAR: ','LB',0);
		$this->SetX(170);
		$this->Cell(25,5,number_format($row2->re_totalcan,2,',','.'),'BR',0);*/
		
		//////////////////////////////////////////////////////////
		//CODIGO EXTRA PARA GENERAR VARIAS PAGINAS DE PRUEBA    //
		//for($i=1;$i<=40;$i++)                                 //
		//$this->Cell(0,10,'Imprimiendo Linea de Prueba Numero '.$i,0,1); //
		//$this->Ln();                                          //
		//////////////////////////////////////////////////////////
		
		//Línea de cierre
		//$this->Cell(array_sum($w),0,'',0);
	}
	}

//Creación del objeto de la clase heredada
$pdf=new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();

//llamada a la funcion de crear tabla y fecha
$pdf->CrearTabla($tipo,$cboano,$cbopato,$caso,$txtfechadesde,$txtfechahasta);
$pdf->Output();


function CamFormFech($contenido)
{
 		if($contenido!=null)
		{
 			$fech=explode('-', $contenido,3);
 			$fech2=explode(' ', $fech[2],3);
 				if(count($fech2) == 2)
				{
				return $fech2[0]."/".$fech[1]."/".$fech[0]." ".$fech2[1];
 				}
				else
				{
 				return $fech2[0]."/".$fech[1]."/".$fech[0];
 				}
 		}
 }

?>