//FUNCION QUE DESBLOQUEA TODOS LOS CAMPOS DE UN FORMULARIO
function desbloquear (formulario)
{	
	for (i=0;i<formulario.elements.length;i++)
	{ 	    
		if (formulario.elements[i].type=="text" || formulario.elements[i].type=="select-one" || formulario.elements[i].type=="submit" || formulario.elements[i].type=="textarea" || formulario.elements[i].type=="file" || formulario.elements[i].type=="checkbox" || formulario.elements[i].type=="radio" || formulario.elements[i].type=="password") 
		{
			formulario.elements[i].disabled=false;	 		
		}
	}
}
//FUNCION QUE BLOQUEA TODOS LOS CAMPOS DE UN FORMULARIO SIN EXCEPCION
function bloquear (formulario)
{ 
	for (i=0;i<formulario.elements.length;i++){ 
		if (formulario.elements[i].type=="text" || formulario.elements[i].type=="select-one" || formulario.elements[i].type=="textarea" || formulario.elements[i].type=="file") {
			formulario.elements[i].disabled=true;			
		}
	}
}

//FUNCION QUE DESBLOQUEA TODOS LOS CAMPOS DE UN FORMULARIO EXCEPTO LOS QUE COMIENZEN POR 'Z' MAYUSCULA
function desbloquear_secundario(formulario)
{	   
		for (i=0;i<formulario.elements.length;i++)
		{ 
			var corto = formulario.elements[i].name.substr(0,1);			
			if(corto != "Z")
			{
				formulario.elements[i].disabled = false;
			}
		}		
}

//FUNCION QUE BLOQUEA TODOS LOS CAMPOS DE UN FORMULARIO QUE COMIENZEN POR 'Z' MAYUSCULA 
function bloquear_secundario(formulario)
{	   
		for (i=0;i<formulario.elements.length;i++)
		{ 
			var corto = formulario.elements[i].name.substr(0,1);			
			if(corto == "Z")
			{
				formulario.elements[i].disabled = true;
			}
		}		
}

function desbloquear_principal(formulario)
{
		for (i=0;i<formulario.elements.length;i++)
		{ 
			var corto = formulario.elements[i].name.substr(0,1);			
			if(corto == "Z")
			{
				formulario.elements[i].disabled = false;
			}
		}		
}

function desbaremo(){
	document.frmbaremo.txtid.disabled=false;
	document.frmbaremo.txtdescripcion.disabled=false;
	document.frmbaremo.txtbaremo.disabled=false;
	document.frmbaremo.id2.disabled=false;
	document.frmbaremo.ano2.disabled=false;	
	document.frmbaremo.btnEnviar.disabled=false;			
}	