function bloqueamonto(formulario) {
	var monto = "monto"+0;
	var j = 1;
	for (var i=0;i<formulario.elements.length;i++){
		if (formulario.elements[i].name==monto) {
			var k = j++;
			monto = "monto" + k;
			formulario.elements[i].disabled=true;
		}
	}
	document.getElementById("txtsustraendo").disabled=true;
	document.getElementById("txtislrm").disabled=true;
}

function desbloqueamonto(formulario) {
	var monto = "monto"+0;
	var j = 1;
	for (var i=0;i<formulario.elements.length;i++){
		if (formulario.elements[i].name==monto) {
			var k = j++;
			monto = "monto" + k;
			formulario.elements[i].disabled=false;
		}
	}
	document.getElementById("txtsustraendo").disabled=false;
	document.getElementById("txtislrm").disabled=false;
}

function utributaria(formulario) {		
	var montout = Number(document.getElementById("txtut").value);
	var ut = "ut"+0;
	var monto = "monto"+0;
	var j = 1;
	var total = 0;
	for (var i=0;i<formulario.elements.length;i++){
		if (formulario.elements[i].name==ut && formulario.elements[i+1].name==monto) {
			var k = j++;
			ut = "ut" + k;
			monto = "monto" + k;
			var ute = Number(formulario.elements[i].value);			
			total = montout * ute;
			formulario.elements[i+1].value = total;	
			//alert("montoUT: "+montout+" valor: "+ute+" total: "+total);
		}
	}	
	//sustraendo
	var ut = Number(document.getElementById("txtut").value);
	var monislrn = Number(document.getElementById("txtislrm").value);
	var isrln = Number(document.getElementById("txtislrn").value);
	totals = (monislrn * isrln)/100;
	var totals2 = parseInt(Math.round(totals*100)/100);	
	document.getElementById("txtsustraendo").value = totals2;	
	//ISRL persona natural
	var ut = Number(document.getElementById("txtut").value);
	var fac = Number(document.getElementById("txtfactor").value);
	totali = ut * fac;
	var totali2 = parseInt(Math.round(totali*100)/100);
	document.getElementById("txtislrm").value = totali2;	
}

function montofactor(e) {
	//cambio el monto del sustraendo
	var ut = Number(document.getElementById("txtut").value);
	var monislrn = Number(document.getElementById("txtislrm").value);
	var isrln = Number(document.getElementById("txtislrn").value);
	totals = (monislrn * isrln)/100;
	document.getElementById("txtsustraendo").value = totals;
	
	//cambio el monto para aplicar islr
	var fac = Number(document.getElementById("txtfactor").value);
	totali = ut * fac;
	document.getElementById("txtislrm").value = totali;
}

function montoislrn(e) {	
	var ut = Number(document.getElementById("txtut").value);
	var monislrn = Number(document.getElementById("txtislrm").value);
	var isrln = Number(document.getElementById("txtislrn").value);
	totals = (monislrn * isrln)/100;
	document.getElementById("txtsustraendo").value = totals;
}

function montoimp(e) {
	var nom = e.name;
	var ind = nom.substr(2);
	var total = 0;
	var monto = "monto"+ind;
	var ut = Number(document.getElementById("txtut").value);
	var ute = Number(document.getElementById(nom).value);
	total = ut * ute;	
	//alert("indice: "+ind+" ut: "+ut+" nombre: "+nom+" valor: "+ute+" total: "+total);
	document.getElementById(monto).value = total;		
}

function estado (formulario)
{
	var ut = "ut"+0;
	var monto = "monto"+0;
	var riam = "riam"+0;
	var j = 1;
	var cont = 0;
	var arr1 = new Array(cont);
	var arr2 = new Array(cont);
	var arr3 = new Array(cont);
	var arreglo1 = "";
	var arreglo2 = "";
	var arreglo3 = "";
	for (var i=0;i<formulario.elements.length;i++){ 
		//alert(formulario.elements[i].name);
		if (formulario.elements[i].name==ut && formulario.elements[i+1].name==monto && formulario.elements[i+2].name==riam) {
			var k = j++;
			ut = "ut" + k;
			monto = "monto" + k;
			riam = "riam" + k;
			//alert(formulario.elements[i].name+" "+formulario.elements[i+1].name);
			arr1[cont] = formulario.elements[i].value+",";
			arr2[cont] = formulario.elements[i+1].value+",";
			arr3[cont] = formulario.elements[i+2].value+",";
			arreglo1 = arreglo1 + arr1[cont];
			arreglo2 = arreglo2 + arr2[cont];
			arreglo3 = arreglo3 + arr3[cont];
			cont++;
		}
	}	
	//alert("cadena1: "+arreglo1+" cadena2: "+arreglo2);
	document.getElementById("estaut").value = arreglo1;
	document.getElementById("estamonto").value = arreglo2;
	document.getElementById("estariam").value = arreglo3;
}

function calculo_ajax(){
	getData('ajax/ajax_calculo.php','calculo',            
	'&oculto='+document.getElementById('oculto').value+
	'&id='+document.getElementById('id').value+
	'&txtut='+document.getElementById('txtut').value+ 
	'&txtfactor='+document.getElementById('txtfactor').value+ 
	'&txtislrn='+document.getElementById('txtislrn').value+ 
	'&txtislrm='+document.getElementById('txtislrm').value+ 
	'&txtsustraendo='+document.getElementById('txtsustraendo').value+ 
	'&txtislrj='+document.getElementById('txtislrj').value+ 
	'&estaid='+document.getElementById('estaid').value+
	'&estaut='+document.getElementById('estaut').value+
	'&estamonto='+document.getElementById('estamonto').value+
	'&estariam='+document.getElementById('estariam').value+
	'&usuario='+document.getElementById('hdnusuario').value,
	'var null=nulo;');
	 bloquear(document.frmcalculo);
	 document.frmcalculo.btnEnviar.disabled=true;
}