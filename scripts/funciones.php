<?php  
include("conexion.php");
function ve_date($d)
{
date_default_timezone_set("America/Caracas");
$t = time()-(1800);
return date($d,$t);
}
//FUNCION PARA REALIZAR EL LOGUEO EN EL SISTEMA
function Consultar_Org(){   
	//setea la sesion para organismo     	 
    $login = strtoupper(@$_POST["txtOrga"]);
	$pass = md5(@$_POST["txtCodig"]);     
    $log = "select * from  siscam.usuarios inner join siscam.permisos on (siscam.usuarios.per_id = siscam.permisos.per_id) inner join siscam.acceso on (siscam.usuarios.ac_acceso = siscam.acceso.ac_acceso) where upper(usu_login) = '$login' and usu_passw = '$pass'";					
	/*$log = "select *, torganismo.torga_codig as codigo_orga  from tusuario 
inner join tpermisolo on (tusuario.tperm_codig = tpermisolo.tperm_codig)
inner join tpersona on (tusuario.tpers_cedul = tpersona.tpers_cedul)
left join torganismo on (tusuario.torga_codig = torganismo.torga_codig)
where tusua_passw='$pass' and upper(tusua_login)='$login' "; */
    $query = pg_query($log); //echo $log; 
	if (0 == pg_num_rows($query)){	
	   echo "<script>alert('Error en Login y/o Password');</script>";   
       return false;	      
	} else {	  	
	   $row = pg_fetch_object($query,0);
	   if($row->usu_statu!=1){
	   		echo "<script>alert('El usuario se encuentra deshabilitado');</script>";
	   		return false;
	   }else{
			$_SESSION['usu_cedul'] = $row->usu_cedul;
			$_SESSION['departamento'] = $row->usua_depart;
			$_SESSION['permisologia'] = $row->per_id;
		  	$_SESSION['corre'] = $row->usu_corre;
		  	$_SESSION['login'] = $row->usu_login;
		  	$_SESSION['password'] = $row->usu_passw;
		  	$_SESSION['estatus'] = $row->usu_status;
		  	$_SESSION['crea'] = $row->usu_crea;
		  	$_SESSION['usu_nombre'] = $row->usu_nombre; 
			$_SESSION['permisosid'] = $row->per_id;  
			$_SESSION['permisosdesc'] = $row->per_descr;   
			$_SESSION['peracceso'] = $row->ac_acceso; 
			
			//echo $_SESSION['permisosid'];
			return true;
	   } 
   	}	         
}
 

//FUNCION QUE REGISTRA LA ACTUALIZACION DE CASOS EN LA TABLA BITACORA
function registrobit($cedula, $carta, $control, $fechaa, $horaa, $nombreusu, $status, $var){
	$consulta = "SELECT siscam.pa_bitacora($cedula, $carta,$control,'$fechaa', '$horaa','$nombreusu','$status','$var')";
	//echo $consulta;	
	$con=pg_query($consulta);	
	return $con;	
}//fin funcion

//Cartas avales que no sean emergencias
function registrobit_cartas($usuario, $cedula, $fechaa, $horaa, $nombreusu, $status, $moda, $estudio, $var){
	
	$consulta1 = "select max(aut_cartaaval) as carta from siscam.autorizaciones where siscam.autorizaciones.est_id = $cedula";
	
	//echo $consulta1;
	$con=pg_query($consulta1);
	$row = pg_fetch_object($con,0);
	$carta = $row->carta;
	$consulta2 = "select aut_ncontrol from siscam.autorizaciones where siscam.autorizaciones.aut_cartaaval = 	$carta";
	//echo $consulta2;
	$con2=pg_query($consulta2);
	$rowr = pg_fetch_object($con2,0);
	$control =	$rowr->aut_ncontrol;
	//echo $control;
	
	$consulta3 = "select max(bit_id) as codid from siscam.bitacora where siscam.bitacora.est_id = 	$cedula";
	//echo $consulta2;
	$con3=pg_query($consulta3);
	$rowt = pg_fetch_object($con3,0);
	$codid = $rowt->codid;
	//echo $codid;
	
	$consulta = "SELECT siscam.pa_bitacora_cartas($cedula, $carta,$control,'$fechaa', '$horaa','$nombreusu','$status','$moda','$estudio',$codid,'$var')";
	//echo $consulta;	
	$con=pg_query($consulta);	
	//$nr=pg_num_rows($con);	
}//fin funcion



function insertabit($cedula, $nombre, $apellido, $fechaing, $horaa, $usu_nombre, $tipoced, $cod_id, $observacion, $fechaingr2, $var){	

	$consulta = "SELECT siscam.pa_bitacora_insert($cedula, '$nombre', '$apellido', '$fechaing', '$horaa' , '$usu_nombre', '$tipoced', $cod_id, '$observacion', '$fechaingr2', '$var')";
	
	//echo $consulta;
	$con=pg_query($consulta);
	$nr=pg_num_rows($con);	
}//fin funcion

//INSERTA BITACORA EMERGENCIA
function insertabit_emergencia($usuario, $cedula, $fechaa, $horaa, $nombreusu, $status, $moda, $estudio, $var){	
	$consulta1 = "select max(aut_cartaaval) as carta from siscam.autorizaciones where siscam.autorizaciones.est_id = $cedula";
	
	//echo $consulta1;
	$con=pg_query($consulta1);
	$row = pg_fetch_object($con,0);
	$carta = $row->carta;
	$consulta2 = "select aut_ncontrol from siscam.autorizaciones where siscam.autorizaciones.aut_cartaaval = 	$carta";
	//echo $consulta2;
	$con2=pg_query($consulta2);
	$rowr = pg_fetch_object($con2,0);
	$control =	$rowr->aut_ncontrol;
	//echo $control;

	$consulta = "SELECT siscam.pa_bitacora_insert_emergencia($cedula, $carta,$control,'$fechaa', '$horaa','$nombreusu','$status','$moda','$estudio','$var')";
	
	//echo $consulta;
	$con=pg_query($consulta);
	$nr=pg_num_rows($con);
	
	$var =MOD;	
	
	registro_datos_emergencias($cedula, $carta, $nombreusu,$var);
		
}//fin funcion

//Funcion que actualiza campos nombre y apellido Bitacora
function registro_datos_emergencias($cedula, $carta, $nombreusu, $var){
	
$consulta3 = "select est_pnombre, est_papellido from siscam.estudiantes where siscam.estudiantes.est_id = $cedula";
	$con3=pg_query($consulta3);
	$rowr3 = pg_fetch_object($con3,0);
	$nombre = $rowr3->est_pnombre;
	$apellido = $rowr3->est_papellido;
	
	$consulta = "SELECT siscam.pa_bitacora_mod_emergencia($cedula, '$nombre', '$apellido',$carta, '$nombreusu','$var')";
	
	//echo $consulta;
	$con=pg_query($consulta);
	$nr=pg_num_rows($con);	


}//fin funcion

//FUNCION QUE REGISTRA EN SISTAR
function registrosistar($cedula, $carta, $fechaa, $usuario, $var){
	
	$consulta2 = "select est_pnombre, est_papellido, aut_modalidad from siscam.vst_aut where siscam.vst_aut.aut_cartaaval = $carta";
	//echo $consulta2;
	$con2=pg_query($consulta2);
	$rowr = pg_fetch_object($con2,0);
	$nombre =	$rowr->est_pnombre;
	$apellido =	$rowr->est_papellido;
	$modalidad = $rowr->aut_modalidad;
	//echo $control;
	
	$consulta = "SELECT sistar.func_expediente($cedula, '$nombre', '$apellido', $carta, '$modalidad', 'NO','$fechaa', $usuario, '$var')";
	//echo $consulta;	
	$con=pg_query($consulta);	
	return $consulta;
	//$nr=pg_num_rows($con);	
}//fin funcion

//Registro Movimiento
function registromovsistar($cedula, $carta, $fechaa, $usuario, $var){
	
	$consulta2 = "select id_personal from sistar.personal where sistar.personal.cedula = $usuario";
	//echo $consulta2;
	$con2=pg_query($consulta2);
	$rowr = pg_fetch_object($con2,0);
	$id=	$rowr->id_personal;
	
	$consultanew = "select exped_carta, estat_id from sistar.movimientos where sistar.movimientos.exped_carta =$carta and sistar.movimientos.estat_id =5";
	//echo $consulta2;
	$connew=pg_query($consultanew);
	$rowr2 = pg_fetch_object($connew,0);
	$estatus =	$rowr2->estat_id;
	if(estat_id !=5){
	
	$hora =strftime( "%H:%M", time() ); 
	
	$consulta3 = "SELECT sistar.func_movimientos($carta, '$fechaa', 5, $id, '$hora', '$var')";
	//echo $consulta;	
	$con=pg_query($consulta3);	
	return $consulta3;
	}
	//$nr=pg_num_rows($con);	
}//fin funcion















//FUNCION PARA ACTUALIZAR MISION SUCREe
function insertamsucre($cedula, $tipoced, $msucre, $var2){	

	$consulta = "SELECT siscam.pa_upmsucre($cedula, '$tipoced', '$msucre','$var2')";
	
	//echo consulta;
	$con=pg_query($consulta);
	$nr=pg_num_rows($con);	
}//fin funcion






//FUNCION PARA DAR FORMATO A LA FECHA PARA GUARDARLA EN LA BASE DE DATOS
function CamFormFech($contenido) {
	if($contenido!=null) {
		$fech=explode('-', $contenido,3);
		$fech2=explode(' ', $fech[2],3);
		if(count($fech2) == 2) {
			return $fech2[0]."/".$fech[1]."/".$fech[0]." ".$fech2[1];
		} else {
			return $fech2[0]."/".$fech[1]."/".$fech[0];
		}
	}
}
 
 
//FUNCION PARA TRANSFORMAR LA FECHA CUANDO SE TRAE DE LA BASE DE DATOS 
function CamFormFechBD($fecha) {
	if($fecha!=null) {
		//Recibo la fecha en foprmato A�o/Mes/Dia y separo los numeros de las barras lo convierto en un arreglo
		$fech = explode("/", $fecha, 3);
		//Retorno la Cadena 'A�o'-'Mes'-'Dia'
		return $fech[2]."-".$fech[1]."-".$fech[0];
	}
}


//FUNCION PARA EL MANEJO DE FECHAS
function calculo_periodo($fecha_inicial,$periodo) {
		//Separo dia, mes y a�o por subcadenas de la fecha de inicio						 
		$dia_inicial = (int)substr($fecha_inicial,0,2);
		$mes_inicial = substr($fecha_inicial,3,2);			
		$anio_inicial = substr($fecha_inicial,6);
		
		//Paso el formato de la fecha a timestamp
		$fecha_inicial_stamp = mktime(0,0,0,$mes_inicial,$dia_inicial,$anio_inicial); 
		
		//Calculo en segundos los dias de vigencia
		$periodo_stamp = $periodo * (24*60*60);
		
		//Sumo la fecha final al periodo para resolver cual es la fecha de vencimiento
		$fecha_final_stamp = $fecha_inicial_stamp + $periodo_stamp;
		
		//Calcula la retsa del dia de vencimiento menos la fecha actual para saber cuantos dias faltan para el vencimiento y lo retorno	
		$res = floor((date($fecha_final_stamp - time()))/(24*60*60));		 
		if ($res >0 && $res <= 5)
		{  $res = 1; }
		elseif($res <= 0) 
		{  $res = 0; }		 
		return $res;	
}


//FUNCION CONSULTAR QUERY
function Consultar($string){ 
	$query = pg_query($string);
	if (0 == pg_num_rows($query)){ 
		return false;	
	} else { 	
		return $query;
	}
}
 
 
//CODIGO DE RESPUESTA PARA AJAX EN COMBOLIST DE CIUDADES Y MUNICIPIOS
	$res = $_REQUEST["enviado"];
	$query = Consultar("SELECT * FROM siscam.ciudad ORDER BY ciu_ciudad ASC");
	$total = pg_num_rows($query); 
	for($i=0;$i<$total;$i++) { if($res == pg_result($query, $i, "esta_id")) {  ?>
	<option value="<?= pg_result($query, $i, "mun_id")?>"><?php echo pg_result($query, $i, "ciu_ciudad"); ?></option><? } } 

	$res2 = $_REQUEST["enviado"];
	$query2 = Consultar("SELECT * FROM siscam.municipio ORDER BY mun_municipio ASC");
	$total2 = pg_num_rows($query2);
	for($i=0;$i<$total2;$i++) { if($res2 == pg_result($query2, $i, "mun_id")) {  ?>
	<option value="<?= pg_result($query2, $i, "mun_id")?>"><?= pg_result($query2, $i, "mun_municipio"); ?></option><? } }
	//FIN DEL CODIGO 


//CODIGO DE AJAX DE RESPUESTA PARA CARGA DE DESCRIPCION DE BAREMOS POR A�O
$resul = $_REQUEST["peticion"];
if($resul != 0) {
	$query3 = Consultar("SELECT * FROM siscam.baremo");
	$total3 = pg_num_rows($query3);
	for($i=0;$i<$total3;$i++) {
	  	if($resul == pg_result($query3,$i,"bar_ano")) { ?>
        	<option value="<? echo pg_result($query3,$i,"bar_ano"); ?>"><? echo pg_result($query3,$i,"bar_id"); ?></option><? 
		}
	} 
} //FIN CODIGO AJAX


//FUNCION PARA EL FORMATO DE MONTOS
function formatonum($monto){
   	return number_format($monto,2,",",".");
   }
function formatonum1($monto){
   	return number_format($monto,0," ",".");
}
function unfornum($monto){
	$monto=str_replace('.','',$monto);
	$monto=trim($monto);
	$monto=str_replace(',','.',$monto);
	return $monto;
}


//FUNCION PARA SUBIR IMAGEN
function foto($foto,$fotodir,$fotonom,$accion) {			
//CARGA DE LA IMAGEN, VERIFICA LA EXTENSION Y LUEGO LO GUARDA CON LA CEDULA 
	if (empty($foto)) { 
		$gimagen=$fotodir;
		if($accion=="ELIMINAR")
		{ unlink($gimagen);  }
	} else {			
		$img=$foto;
		$tipoimagen=getimagesize($img);
		if ($tipoimagen[2]==1)
		{ $ext=".gif"; }
			elseif($tipoimagen[2]==2)
		{ $ext=".jpg"; }
			elseif ($tipoimagen[2]==3)
		{ $ext=".png"; }
			else 
		{ $tipoimagen=1; }
		if ($tipoimagen==1)
		{  
			echo '<script> alert("El archivo de la imagen no es valido, escoja otro archivo de tipo gif, jpg o png solamente"); </script>';	
			echo "<script>document.location.href ='?id=universidad';</script>";		
		} else {				
			$dimagen=$fotonom;
			$gimagen=$dimagen.$ext;
			if ($accion!="ELIMINAR") {
				if (move_uploaded_file($img,$gimagen)) {  
					echo '<script> alert("La Foto se ingreso con �xito"); </script>';
				}						
			} else { unlink ($gimagen); }
			
		} 
	} // AQUI TERMINA LA VERIFICACION DE LA CARGA DE LA IMAGEN Y LA INCLUSION DE LA MISMA
	return $gimagen;
}


//FUNCION PARA CALCULAR LA EDAD
function edad($edad){
	list($anio,$mes,$dia) = explode("-",$edad);
	$anio_dif = date("Y") - $anio;
	$mes_dif = date("m") - $mes;
	$dia_dif = date("d") - $dia;
	if ($mes_dif < 0) $anio_dif--;
	if ($mes_dif == 0 and $dia_dif < 0) $anio_dif--;
	return $anio_dif;
}

function obtener_estatus($estatus)
{
				switch ($estatus) 
				{
				case "UMED": $res = 'UNIDAD MEDICA'; break;
				case "AUT" : $res = 'AUTORIZADO'; break;
				case "ATENDIDO" : $res = 'ATENDIDO'; break;
				case "REC" : $res = 'RECIBIDO'; break;
				case "RECPAR" : $res = 'RECIBIDO PARCIAL'; break;
				case "PAGO" : $res = 'PAGO'; break;
				case "PAGOPAR" : $res = 'PAGO PARCIAL'; break;
				case "FIN" : $res = 'FINANZAS'; break;
				case "FINPAR" : $res = 'FINANZAS PARCIAL'; break;
				case "PDO" : $res = 'PAGADO'; break;
				case "PDOPAR" : $res= 'PAGADO PARCIAL'; break;
				case "ATENDIDO" : $res = 'ATENDIDO'; break;
				case "REV" : $res = 'REVISADO'; break;
				case "REG" : $res = 'REGISTRADO'; break;				
				}
		return $res;
						
}	
?>
