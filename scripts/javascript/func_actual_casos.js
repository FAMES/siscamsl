// JavaScript Document
//_______________________________________________________________________________________________________________________________________//
////////////////////////////////////////////////////////+++++ DOCUMENTO: actual_casos2.php +++++///////////////////////////////////////////
//---------------------------------------------------------------------------------------------------------------------------------------//
//FUNCION QUE VERIFICA LA SELECCION DE AL MENOS 1 CARTA Y ORDENA EL ARREGLO CON LAS CARTA(S) SELECCIONADA(S)     
     function chec()
	 {	
		 var count = 0;
		 var cont = 0;
		 //Declaro el array con la cantidad de Cartas Avales por caso (campo valor_oculto)
	 	 var arreglo1 = new Array(document.getElementById("valor_oculto").value);
		 //Recorro el listado de Cartas Avales
	   for(var i = 1; i <= document.getElementById("valor_oculto").value; i++)
	      {
	    		var doc = "chequeo" + i;
				var res = "col" + i;
				//Verifico las Cartas Avales Seleccionada(s)
	   				if(document.getElementById(doc).checked == true)
	   				{ 
					//Lleno un arreglo con las cartas Avales Seleccionadas 
						arreglo1[i] = document.getElementById(doc).value;			 
						count++;
	   				}
		  }
				  	//Lleno 2� arreglo con las Cartas Avales Seleccionadas
					var arreglo2 = new Array(count);
					//Recorro arreglo2 para verificar que las cartas seleccionadas no esten vacias 
						for(var j = 1; j < arreglo1.length; j++)
							{
								//Filtro las cartas distintas a indefinidas 
								if(arreglo1[j] != undefined)
									{	
										arreglo2[cont] = arreglo1[j];
										cont++;
									}
							}
			//Verifico que el arreglo2 no este vacio
			if(arreglo2 == "")
			{
				alert("DEBE SELECCIONAR AL MENOS 1 CARTA");
				return false;
			} else					
				document.getElementById("Cartas_Avales").value = arreglo2;
		   		return true;								
			}
//FUNCION QUE MUESTRA LA SUMA DE LOS MONTOS AUTORIZADOS DE LAS CARTAS AVALES SELECCIONADAS DEL LISTADO DE CARTAS AVALES POR CASO 			
function suma(e) 
	{
	//Recojo el valor de la opcion seleccionada
	var valor = e.value;
	for(var j = 1; j <= document.getElementById("valor_oculto").value; j++)
		{
			var res = "col" + j;
			var doc = "chequeo" + j;
			var resul = Number(document.getElementById("total_cartas").value);
			var bool = Number(document.getElementById(res).innerHTML);
			//Verifico la seleccion para restar o sumar
			if(document.getElementById(doc).value == valor && document.getElementById(doc).checked == true)
				{					
					var resul = resul + bool;				
				}
			else if(document.getElementById(doc).value == valor && document.getElementById(doc).checked == false) 
			{
				var resul = resul - bool;
			}
			document.getElementById("total_cartas").value = resul;
			 //alert(document.getElementById("total_cartas").value);
			document.getElementById("monto_sel").innerHTML = resul + " Bs.";
		}		
	}	
			
//_______________________________________________________________________________________________________________________________________//
////////////////////////////////////////////////////////+++++ FIN DOCUMENTO: actual_casos2.php +++++///////////////////////////////////////////
//---------------------------------------------------------------------------------------------------------------------------------------//
//_______________________________________________________________________________________________________________________________________//
////////////////////////////////////////////////////////+++++ DOCUMENTO: actual_casos3.php +++++///////////////////////////////////////////
//---------------------------------------------------------------------------------------------------------------------------------------//
//FUNCION QUE INICIALIZA LAS FUNCIONES DE ACTUAL_CASOS3
function inicializa_actual_casos()
{	
       verifica_modalidad();
         suma_total();
}
				//FUNCION QUE VALIDA LA M0DALIDAD DE LA CARTA AVAL PARA CARGA DEL FORMULARIO DE FACTURA
					function verifica_modalidad()
					{		
						var mod = document.getElementById('modalidad').value;						
						var estatus = document.getElementById('estatus').value;						
								if(mod != 'ESPECIAL' || estatus == 'PAGOPAR' || estatus == 'REC')
								{ 
								document.getElementById('fila_tipo_doc').style.visibility = 'hidden'; 
								document.getElementById('tipo_doc').value = 'FACTURA'; 
								}										                                                        
					}
		//FUNCION QUE ASIGNA A UN CAMPO OCULTO LOS MONTO DE PATOLOGIAS QUE SE EXONERAN DE IMPUESTO 
			function pat_exoneradas(e)
			{					
				var indice_ajuste = "ajus"+e.id.substr(5);				
				var ajuste_exo = Number(document.getElementById(indice_ajuste).innerHTML);
				var total_exo = Number(document.getElementById('monto_exonerado').innerHTML);
					if(e.checked == true)
					{ 
						total_exo = total_exo + ajuste_exo;
						//alert("exonerado "+total_exo); 
					}
					if(e.checked == false)
					{
						total_exo = total_exo - ajuste_exo;
					}
					document.getElementById('monto_exonerado').innerHTML = total_exo;			
					document.getElementById('monto_no_exonerado').innerHTML = document.getElementById('monto_total').innerHTML - total_exo;                                        
			}

			//Efectua la suma del total de patologias listadas por Carta Aval y Da Formato al total 	
				function suma_total()
				{
					var suma = 0;
					//var res = true;
					var num_cartas = document.getElementById('total_cartas').value;
					for(var i = 0; i < num_cartas; i++)
					{
						var conteo_pat = "conteo_pat"+i; 
						//var indice_montoa = "monto_autorizado"+i;
						//var montoa = document.getElementById(indice_montoa).value;
						for(var j = 1; j < document.getElementById(conteo_pat).innerHTML; j++)
						{
							var ajus = "ajus"+i+j;				
							suma = suma + Number(document.getElementById(ajus).innerHTML);					
						}		
					}
					document.getElementById("monto_total_oculto").innerHTML = FormatoMoneda(suma) + " Bs.";
					document.getElementById("monto_total").innerHTML = suma.toFixed(2);
                    document.getElementById('monto_no_exonerado').innerHTML = suma;                                        
				}
	
				//FUNCION QUE EVALUA QUE EL MONTO AJUSTADO DE CADA CARTA NO SOBREPASE SU MONTO AUTORIZADO
					function evalua_montoa()
					{
						var retorno = true;
						var num_cartas = document.getElementById('total_cartas').value;
						for(var i = 0; i < num_cartas; i++)
						{
							var suma_x_carta = 0;
							var indice_pat = "conteo_pat"+i;
							var indice_monto_aut = "monto_autorizado"+i;
							var num_pat_x_carta = document.getElementById(indice_pat).innerHTML;
							var monto_aut = document.getElementById(indice_monto_aut);
							var carta = document.getElementById('carta'+i).value
								for(var j = 1; j < num_pat_x_carta; j++)
								{
										var indice_ajuste = "ajus"+i+j;
										var indice_sel = ''+i+j;
										var indice = document.getElementById("indice").innerHTML;
										var ajuste = Number(document.getElementById(indice_ajuste).innerHTML);	
										var txtajuste = Number(document.getElementById('txtajuste').value);
									//Verifico el ajuste seleccionado
												if(indice == indice_sel) 
												{
													suma_x_carta = suma_x_carta + txtajuste; 
												}
												else 
												{
													suma_x_carta = suma_x_carta + ajuste; 
												}					
								}				
								if(suma_x_carta > monto_aut.value) 
								{ alert('El Monto de Ajuste no puede superar el Monto Autorizado de la Carta Aval N� ' + carta); 				 
								  retorno = false; }
						}		
						return retorno;
					}

		//Funcion para mostrar y ordenar los datos para la actualizacion de patologias
		//Se activara al dar click al simbolo de edicion del caso correspondiente
			function asigna_patologia(e)
			{
				var num = e.id;	
				//Asignamos el id de patologia correspondiente al baremo que hemos seleccionado en el listado del caso a pagar
				document.getElementById("pat_id_oculto").innerHTML = document.getElementById("pat_id"+num).innerHTML;
				//Asignamos el numero de carta correspondiente al baremo que hemos seleccionado en el listado del caso a pagar
				document.getElementById("carta_oculta").innerHTML = document.getElementById("carta"+num).innerHTML;
				//Asignamos el Codigo de baremo correspondiente al baremo que hemos seleccionado en el listado del caso a pagar
				document.getElementById("bar_id_oculto").innerHTML = document.getElementById("id"+num).innerHTML; 
				//Asignamos el a�o correspondiente al baremo que hemos seleccionado en el listado del caso a pagar
				document.getElementById("txtaniobaremo").innerHTML = document.getElementById("anio"+num).innerHTML;				
				//Asignamos el codigo de patologia correspondiente al baremo que hemos seleccionado en el listado del caso a pagar
				document.getElementById("txtdescbaremo").innerHTML = document.getElementById("desc"+num).innerHTML;
				//Asignamos el monto del baremo correspondiente al baremo que hemos seleccionado en el listado del caso a pagar
				document.getElementById("txtmontobaremo").innerHTML = document.getElementById("bar"+num).innerHTML;	
				//Asignamos el monto a ajustar correspondiente al baremo que hemos seleccionado en el listado del caso a pagar
				document.getElementById("txtajuste").value = document.getElementById("ajus"+num).innerHTML; 		
				//Enviamos el indice actual para la actualizacion del listado de patologias				
				document.getElementById("indice").innerHTML = num;
			}	
		//Todo se Refiere al Area de "Datos de Factura"
		 //Funcion que verifica el Valor del ajuste 
		 function valida_monto(e)
		 {	
				 var bool = true;
				 var monto 			  = 	 Number(document.getElementById("txtfacmonto").value); //Monto de la factura
				 var total_a_pagar    =	 	 Number(document.getElementById("total_a_pagar").innerHTML); //Monto Total de patologias oculto 
				 var ajuste_patologia =  	 Number(document.getElementById("txtajuste").value); //Monto del Ajuste de la patologia Seleccionada
				 var monto_baremo     =	 	 Number(document.getElementById("txtmontobaremo").innerHTML); //Monto del baremo de la patologia seleccionada		
				//Verifico que se haya Seleccionado Alguna Patologia para Ajustar
				if(!ajuste_patologia && e.id == "ajustar") 
				{ 	
					document.getElementById("txtajuste").focus();
					alert("Debe Seleccionar una Patologia");
					bool = false; 
				}
				//Verifico que el Monto del Ajuste de Patologia no supere al Monto Baremo
				if(ajuste_patologia > monto_baremo && e.id == "ajustar")
				{
					document.getElementById("txtajuste").focus();
					alert("El Monto Ajustado es Mayor al Monto del Baremo");
					bool = false;	
				}		
				//Actualizo el Monto del Ajuste donde Corresponda
				bool = evalua_montoa();
				if(bool == true)
				{
					var indice = document.getElementById("indice").innerHTML;	
					document.getElementById("ajus"+indice).innerHTML = document.getElementById("txtajuste").value;	
					suma_total();
                                }
			return bool;
		 }
		
 //FUNCION QUE ASIGNA EL VALOR DEL PORCENTAJE DE RETWNCION CUANDO SE SELECCIONA EN EL CHECK DE APLICAR RETENCION
 /*function asigna_retencion(check)
 {
	var monto_iva = Number(document.getElementById('txtivamonto').value);
	var porc_ret = check.value;
	document.getElementById('hdnret_iva').value = monto_iva * porc_ret/100;	
	alert(document.getElementById('hdnret_iva').value);	
 }*/
 
 //Verifico que no hayan Campos Vacios
 function verifica_campos()
 {	  
 	 var i           = true;
	 var monto_fact  = Number(document.getElementById('txtfacmonto').value);
	 var monto_fact_exo = Number(document.getElementById('txtexomonto').value);
	 var monto_iva = Number(document.getElementById('txtivamonto').value);
	 var monto_total = Number(document.getElementById('monto_total').innerHTML);
	 var modalidad = document.getElementById('modalidad').value;
			 if(!document.getElementById("txtnfactura").value) { i = false; }
			 if(!document.getElementById("txtfechafac").value) { i = false; }
			 if(!document.getElementById("txtfacmonto").value) { i = false; }
			 if(document.getElementById("tipo_doc").value == 0) { alert('Debe Seleccionar Algun TIPO DE DOCUMENTO'); return false; }
	 var monto_pago = monto_fact + monto_fact_exo + monto_iva;
 			
			if(i == false) 
			{ alert("Debe Llenar Todos los Datos de la Factura"); }  
			
			if(monto_iva != 0.00)
			{
				var checkiva1 = document.getElementById('checkiva1');
				var checkiva2 = document.getElementById('checkiva2');
				if(checkiva1.checked == false && checkiva2.checked == false) 
				{ alert("DEBE SELECCIONAR ALGUN TIPO DE RETENCION APLICAR"); i=false; }
				else if(checkiva1.checked == true) { document.getElementById('hdnret_iva').value = monto_iva * checkiva1.value/100; }
				else if(checkiva2.checked == true) { document.getElementById('hdnret_iva').value = monto_iva * checkiva2.value/100; }
			}
			
			if(modalidad != 'AYUDA SOLIDARIA' && modalidad != 'AYUDA SOLIDARIA ESPECIAL')
			{				
				if(monto_pago < monto_total) 
				{
				alert("El Monto de la Factura es inferior al Monto Total de Patologias Codificadas"); 
				document.getElementById('agregar').disabled=false;
				i = false; 
				}
				if(monto_total == 0) 
				{ 
				alert("El Caso no puede tener Monto Total 0"); 
				i = false; 
				}	//Agr. el 22-11-2013
			} else
			{ 				
				document.getElementById('monto_no_exonerado').innerHTML =  monto_fact;						
				document.getElementById('monto_total').innerHTML =  monto_pago;				
			} 
					
	return i;
 }
 
 //FUNCION QUE CARGA EL PRESUPUESTO DE LA CARTA EN MODALIDAD ESPECIAL SI SE SELECCIONA 'PRESUPUESTO' EN EL COMBO 'tipo_doc'
 function carga_pres(e)
 {
	if(document.getElementById('tipo_doc').value == 'PRESUPUESTO')
	{ 
		document.getElementById('txtnfactura').value = document.getElementById('numerop').value;
		document.getElementById('txtnfactura').disabled = true;
		document.getElementById('doc_nombre').innerHTML = 'Presupuesto N� : ';
		document.getElementById('txtfechafac').value = document.getElementById('fechap').value;
		document.getElementById('txtfechafac').disabled = true;
		document.getElementById('doc_fecha').innerHTML = 'Fecha Presupuesto : ';
		document.getElementById('txtfacmonto').value = document.getElementById('montoa').value;	
		document.getElementById('txtfacmonto').disabled = true;
		document.getElementById('doc_monto').innerHTML = 'Monto Autorizado : ';
		document.getElementById('fechaing').style.display = 'none';
	}
	else
	{ 	
		document.getElementById('doc_nombre').innerHTML = 'Factura N� : ';
		document.getElementById('txtnfactura').value = '';
		document.getElementById('txtnfactura').disabled = false;
		document.getElementById('doc_fecha').innerHTML = 'Fecha Factura : ';
		document.getElementById('txtfechafac').value = '';
		document.getElementById('txtfechafac').disabled = false;
		document.getElementById('doc_monto').innerHTML = 'Monto Factura : ';
		document.getElementById('txtfacmonto').disabled = false;
		document.getElementById('txtfacmonto').value = '';		
	} 
 }
 
 function factura_ajax()
 {
 	if(verifica_campos()==true) 
	{	
		getData('ajax/patologias/Bd_factura.php', 'fin_factura', 
				'&cedula='+document.getElementById('est_id').innerHTML+
				'&ncontrol='+document.getElementById('control').innerHTML+
				'&fac_num='+document.getElementById('txtnfactura').value+
				'&fecha='+document.getElementById('txtfechafac').value+
				'&monto='+document.getElementById('monto_total').innerHTML+
				'&monto_bruto='+document.getElementById('txtfacmonto').value+
				'&ncarta='+document.getElementById('oculto_cartaaval').value+
				'&fac_id='+document.getElementById('fac_id').value+
				'&montop='+document.getElementById('montop').value+
				'&montoexo='+document.getElementById('txtexomonto').value+
				'&montonoexo='+document.getElementById('monto_no_exonerado').innerHTML+
				'&modalidad='+document.getElementById('modalidad').value+
				'&accion='+document.getElementById('tipo_doc').value+
				'&nombreUsu='+document.getElementById('usu_nombre').value+
				'&usucedula='+document.getElementById('usu_cedula').value+
				'&monto_iva='+document.getElementById('txtivamonto').value+
				'&re_iva='+document.getElementById('hdnret_iva').value+
				'&fechaing='+document.getElementById('txtfechaing').value
				,'var nulo=null;'); 
		document.getElementById('agregar').disabled=false;
	}
 }
 //_______________________________________________________________________________________________________________________________________//
////////////////////////////////////////////////////////+++++ FIN DOCUMENTO: actual_casos3.php +++++///////////////////////////////////////////
//---------------------------------------------------------------------------------------------------------------------------------------//
