// JavaScript Document
//FUNCION QUE VERIFICA LA CORRECTA CARGA EN BASE DE DATOS PARA LA ALERTA AL USUARIO

function MuestraInfo(estatus)
	{				
		//RESPUESTA AL AGREGAR NUEVOS REGISTROS	
		if(estatus.indexOf('AGREGADO')!=-1)    { estatus = 'AGREGADO'; }
		if(estatus.indexOf('NO_AGREGAR')!=-1)    { estatus = 'NO AGREGADO'; }
		if(estatus.indexOf('ELIMINADO')!=-1)   { estatus = 'ELIMINADO'; }
		if(estatus.indexOf('NO_ELIMINAR')!=-1)   { estatus = 'NO ELIMINADO'; }
		if(estatus.indexOf('MODIFICADO')!=-1)  { estatus = 'MODIFICADO'; }	
		if(estatus.indexOf('NO_MODIFICAR')!=-1)  { estatus = 'NO MODIFICADO'; }
		if(estatus.indexOf('REGISTRADO')!=-1) { estatus = 'REGISTRADO'; }			
		if(estatus.indexOf('NO_ACTUALIZAR')!=-1) { estatus = 'NO ACTUALIZADO'; }
		if(estatus.indexOf('ANULAR')!=-1)     { estatus = 'ANULADO'; }
		if(estatus.indexOf('NO_ANULADO')!=-1)     { estatus = 'NO ANULADO'; }
		if(estatus.indexOf('RECEPCIONE')!=-1){ estatus = 'RECEPCIONADO'; }
		if(estatus.indexOf('ATENDER')!=-1){ estatus = 'ATENDIDO'; }
		if(estatus.indexOf('NO_RECEPCIONAR')!=-1)     { estatus = 'NO RECEPCIONADO'; }
		if(estatus.indexOf('NO_ATENDIDO')!=-1)     { estatus = 'NO ATENDIDO'; }
		if(estatus.indexOf('ELIMINA_PATOLOGIA')!=-1) { estatus = 'ELIMINA_PATOLOGIA'; }
		if(estatus.indexOf('NO_ELIMINA_PATOLOGIA')!=-1) { estatus = 'NO_ELIMINA_PATOLOGIA'; }
		if(estatus.indexOf('PATOLOGIA_ACTUALIZADA')!=-1) { estatus = 'PATOLOGIA_ACTUALIZADA'; }
		if(estatus.indexOf('NO_ACTUA_PAT')!=-1) { estatus = 'PATOLOGIA_NO_ACTUALIZADA'; }
		if(estatus.indexOf('CASO_DEVUELTO')!=-1) { estatus = 'CASO_DEVUELTO'; }
		if(estatus.indexOf('CASO_NO_DEVUELTO')!=-1) { estatus = 'CASO_NO_DEVUELTO'; }
		if(estatus.indexOf('SI_EMBARAZO')!=-1) { estatus = 'SI_EMBARAZO'; }
		if(estatus.indexOf('AYUDA_SOLIDARIA')!=-1) { estatus = 'AYUDA_SOLIDARIA'; }
		if(estatus.indexOf('EXTENSION_AYUDA')!=-1) { estatus = 'EXTENSION_AYUDA'; }
		if(estatus.indexOf('EXT_EXTENSION_AGR')!=-1) { estatus = 'EXT_EXTENSION_AGR'; }
		if(estatus.indexOf('EXT_EXTENSION')!=-1) { estatus = 'EXT_EXTENSION'; }
		if(estatus.indexOf('MATERNIDAD_EXT')!=-1) { estatus = 'MATERNIDAD_EXT'; }		
		if(estatus.indexOf('REVERSADO')!=-1) { estatus = 'REVERSADO'; }
		if(estatus.indexOf('NO_REVERSA')!=-1) { estatus = 'NO_REVERSA'; }
		if(estatus.indexOf('ENVIADO')!=-1) { estatus = 'ENVIADO'; }
		if(estatus.indexOf('NO_ENVIADO')!=-1) { estatus = 'NO_ENVIADO'; }
		
		switch(estatus)
		{			
		case 'AGREGADO':
		//$.blockUI('<span class="titulomen">Informaci&oacute;n Almacenada Satisfactoriamente</span>', {backgroundColor: '#C6C9E1', height: '60px', width: '500px', margin: '-100px 0 0 -193px'});		
		alert("Informacion Almacenada Satisfactoriamente");
setTimeout($.unblockUI, 2000);	
		break;
		
		case 'NO AGREGADO':
		//$.blockUI('<span class="titulomen">La Informaci&oacute;n No Fue Almacenada</span>', {backgroundColor: '#C6C9E1', height: '60px', width: '500px', margin: '-100px 0 0 -193px'});
		alert("Informacion No Almacenada");
setTimeout($.unblockUI, 2000);
		break;

		case 'MODIFICADO':		
		//$.blockUI('<span class="titulomen">La Informaci&oacute;n Fue Modificada Satisfactoriamente</span>', {backgroundColor: '#C6C9E1', height: '60px', width: '500px', margin: '-100px 0 0 -193px'});
		alert("Informacion Modificada Satisfactoriamente");
setTimeout($.unblockUI, 2000);
		break;

		case 'NO MODIFICADO':
		$.blockUI('<span class="titulomen">La Informaci&oacute;n No Fue Modificada Satisfactoriamente</span>', {backgroundColor: '#C6C9E1', height: '60px', width: '500px', margin: '-100px 0 0 -193px'});
setTimeout($.unblockUI, 2000);		
		break;

		case 'ELIMINADO':
		//$.blockUI('<span class="titulomen">La Informaci&oacute;n Fue Eliminada Satisfactoriamente</span>', {backgroundColor: '#C6C9E1', height: '60px', width: '500px', margin: '-100px 0 0 -193px'});
		alert("Informacion Eliminada Satisfactoriamente");
setTimeout($.unblockUI, 2000);		
		break;
				
		case 'NO ELIMINADO':
		$.blockUI('<span class="titulomen">La Informaci&oacute;n No Fue Eliminada Satisfactoriamente</span>', {backgroundColor: '#C6C9E1', height: '60px', width: '500px', margin: '-100px 0 0 -193px'});
setTimeout($.unblockUI, 2000);
		break;
		
		case 'REGISTRADO':
		$.blockUI('<span class="titulomen">El Caso ha sido Registrado Exitosamente</span>', {backgroundColor: '#C6C9E1', height: '60px', width: '500px', margin: '-100px 0 0 -193px'});
setTimeout($.unblockUI, 2000);		
		break;
		
		case 'NO ACTUALIZADO':
		$.blockUI('<span class="titulomen">El Caso no pudo ser Actualizado</span>', {backgroundColor: '#C6C9E1', height: '60px', width: '500px', margin: '-100px 0 0 -193px'});
setTimeout($.unblockUI, 2000);
		break;
		
		case 'ANULADO':
		$.blockUI('<span class="titulomen">El Caso ha sido Anulado Exitosamente</span>', {backgroundColor: '#C6C9E1', height: '60px', width: '500px', margin: '-100px 0 0 -193px'});
setTimeout($.unblockUI, 2000);
		break;
		
		case 'NO ANULADO':
		$.blockUI('<span class="titulomen">El Caso no puede ser Anulado</span>', {backgroundColor: '#C6C9E1', height: '60px', width: '500px', margin: '-100px 0 0 -193px'});
setTimeout($.unblockUI, 2000);
		break;
		
		case 'RECEPCIONADO':
		$.blockUI('<span class="titulomen">El Caso ha sido Recepcionado Exitosamente</span>', {backgroundColor: '#C6C9E1', height: '60px', width: '500px', margin: '-100px 0 0 -193px'});
setTimeout($.unblockUI, 2000);
		break;
		
		case 'NO RECEPCIONADO':
		$.blockUI('<span class="titulomen">El Caso No Pudo Ser Recepcionado</span>', {backgroundColor: '#C6C9E1', height: '60px', width: '500px', margin: '-100px 0 0 -193px'});
setTimeout($.unblockUI, 2000);
		break;
		
		case 'ATENDIDO':
		$.blockUI('<span class="titulomen">El Caso se encuentra en Estatus Atendido</span>', {backgroundColor: '#C6C9E1', height: '60px', width: '500px', margin: '-100px 0 0 -193px'});
setTimeout($.unblockUI, 2000);
		break;
		
		case 'NO ATENDIDO':
		$.blockUI('<span class="titulomen">El Caso No Atendido</span>', {backgroundColor: '#C6C9E1', height: '60px', width: '500px', margin: '-100px 0 0 -193px'});
setTimeout($.unblockUI, 2000);
		break;
		
		case 'ELIMINA_PATOLOGIA':
		$.blockUI('<span class="titulomen">Patologia Eliminada Exitosamente</span>', {backgroundColor: '#C6C9E1', height: '60px', width: '500px', margin: '-100px 0 0 -193px'});
setTimeout($.unblockUI, 2000);
		break;
		
		case 'NO_ELIMINA_PATOLOGIA':
		$.blockUI('<span class="titulomen">La Patologia No Pudo Ser Eliminada</span>', {backgroundColor: '#C6C9E1', height: '60px', width: '500px', margin: '-100px 0 0 -193px'});
setTimeout($.unblockUI, 2000);
		break;
		
		case 'PATOLOGIA_ACTUALIZADA':
		$.blockUI('<span class="titulomen">La Patologia se Ajusto \n Exitosamente</span>', {backgroundColor: '#C6C9E1', height: '60px', width: '500px', margin: '-100px 0 0 -193px'});
setTimeout($.unblockUI, 2000);
		break;
		
		case 'PATOLOGIA_NO_ACTUALIZADA':
		$.blockUI('<span class="titulomen">No Se Ajusto la Patologia</span>', {backgroundColor: '#C6C9E1', height: '60px', width: '500px', margin: '-100px 0 0 -193px'});
setTimeout($.unblockUI, 2000);
		break;
		
		case 'CASO_DEVUELTO':
		$.blockUI('<span class="titulomen">El Caso Fue Devuelto a su Estatus Anterior</span>', {backgroundColor: '#C6C9E1', height: '60px', width: '500px', margin: '-100px 0 0 -193px'});
setTimeout($.unblockUI, 2000);
		break;
		
		
		case 'CASO_NO_DEVUELTO':
		$.blockUI('<span class="titulomen">El Caso No pudo Ser Devuelto a su Estatus Anterior</span>', {backgroundColor: '#C6C9E1', height: '60px', width: '500px', margin: '-100px 0 0 -193px'});
setTimeout($.unblockUI, 2000);
		break;
		
		case 'SI_EMBARAZO':
		alert('LA ESTUDIANTE YA TIENE PATOLOGIA EMBARAZO REGISTRADA');
		//	$.blockUI('<span class="titulomen">La Estudiante ya tiene Embarazo Registrado</span>', {backgroundColor: '#C6C9E1', height: '60px', width: '500px', margin: '-100px 0 0 -193px'});
//setTimeout($.unblockUI, 2000);
		break;
		
		case 'AYUDA_SOLIDARIA':		
		alert('La ampliacion del caso que se desea generar es de modalidad AYUDA SOLIDARIA y no se puede realizar dicha operacion');
		location.href='index2.php?id=carta2&mod=nuevo';
		break;
		
		case 'EXTENSION_AYUDA':		
		alert('No se puede Generar una extension del caso ya que es de modalidad AYUDA SOLIDARIA');
		location.href='index2.php?id=carta2&mod=nuevo';
		break;
		
		case 'EXT_EXTENSION':		
		alert('No se puede Generar una extension del caso ya que tiene una extension generada');
		location.href='index2.php?id=carta2&mod=nuevo';
		break;
		
		case 'EXT_EXTENSION_AGR':		
		alert('Extensión Agregada');
		location.href='index2.php?id=carta2&mod=nuevo';
		break;
		
		case 'MATERNIDAD_EXT':		
		alert('No se puede Generar una extension del caso ya que la patologia es una MATERNIDAD');
		location.href='index2.php?id=carta2&mod=nuevo';
		break;
		
		case 'REVERSADO':		
		alert('CASO REVERSADO EXITOSAMENTE');
		location.href='index2.php?id=reverso';
		break;
		
		case 'NO_REVERSA':		
		alert('EL CASO NO HA SIDO REVERSADO');
		location.href='index2.php?id=reverso';
		break;

		case 'ENVIADO':
		alert('CORREO ENVIADO A DESTINATARIO');
			//$.blockUI('<span class="titulomen">Correo Enviado a Destinatario</span>', {backgroundColor: '#C6C9E1', height: '60px', width: '500px', margin: '-100px 0 0 -193px'});
//setTimeout($.unblockUI, 2000);
		break;

		case 'NO_ENVIADO':
		alert('NO SE PUEDE ENVIAR CORREO POR HOTMAIL A DESTINATARIO');
			//$.blockUI('<span class="titulomen">No se puede enviar correo por Hotmail a Destinatario</span>', {backgroundColor: '#C6C9E1', height: '60px', width: '500px', margin: '-100px 0 0 -193px'});
//setTimeout($.unblockUI, 2000);
		break;

		}
	}		
//CODIGO ESPECIAL PARA SUBIR LOS ARCHIVOS DE FOTOS
	function subeFotos_comi()
		{		
		if(document.getElementById('guardaFoto').value == 'SI')
		{				
			document.getElementById('hdnaccion').value = 'MODIFICARCOMI';
			comi_ajax();						
		}
		}
//FUNCION QUE MANEJA TODAS LAS PETICIONES Y RESPUESTAS DE XMLHTTPREQUEST
function NuevoAjax()
 {     
     var xmlhttp=false;
       try
	   {
          xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	   }
	   catch(e)
	   {
                try
				{
                        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }catch(E){
                        xmlhttp = false;
                }
        }
        if(!xmlhttp && typeof XMLHttpRequest!='undefined')
		{
                xmlhttp = new XMLHttpRequest();
        }
        return xmlhttp;
}


//FUNCION GENERAL AJAX
 function getData(dataSource, divID, parametros,hidDiv)
  {    		
        var contenido, preloader;
        contenido = document.getElementById(divID);				
		preloader = parent.document.getElementById('preloader');				
        //creamos el objeto XMLHttpRequest
        ajax = NuevoAjax(); 
        //peticionamos los datos, le damos la url enviada desde el link
        ajax.open("POST", dataSource,true);
		ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");		
        ajax.onreadystatechange=function()
		{			
                if(ajax.readyState==1)
				{ 					
						//Muestra una imagen para espera de carga						
						preloader.innerHTML = "Cargando....";						
                        //modificamos el estilo de la div, mostrando una imagen de fondo
                        preloader.style.background = "url('../../imagenes/loader.gif') no-repeat right"; 						
                }
				else 
				if(ajax.readyState==4)
				{					
                    if(ajax.status==200)
					{
                                //$('cargando').hide();
								//mostramos los datos dentro de la div								
                                contenido.innerHTML = ajax.responseText;
								//alert("prueba :"+contenido.innerHTML.trim());
								//alert(contenido.innerHTML);
								MuestraInfo(contenido.innerHTML.trim());								
								//alert(ajax.responseText); 
                                preloader.innerHTML = "";
                                preloader.style.background = "";																
								eval(hidDiv);								
                    }
					else if(ajax.status==404)
					{
                                preloader.innerHTML = "La página no existe";
                    }
					else
					{
								//mostramos el posible error
                                preloader.innerHTML = "Error:".ajax.status; 
                    }
                }
        }
        ajax.send(parametros);				
  }



//FUNCION QUE PROCESA DOS PETICIONES CONSECUTIVAS PARA LOS COMBOS DE ESTADO, CIUDAD Y MUNICIPIO  
var peticion_http = null;
var READY_STATE_COMPLETE = 4;
//funciones de manipulacion de datos madiante AJAX 
	    function crea_envio(control2) 
		{
			var valores = document.getElementById(control2).value;
			return "enviado=" + encodeURIComponent(valores)  + "&nocache=" + Math.random(); 	
		}
 		 function asignar_ciudad(control, control2) 
		 {
		    peticion_http = NuevoAjax();
				    if(peticion_http) 
					{
					  peticion_http.onreadystatechange = function() 
						  {
						     if(peticion_http.readyState == READY_STATE_COMPLETE) 
							 {
					     		if(peticion_http.status == 200) 
								{									
								   	document.getElementById(control).innerHTML = peticion_http.responseText;
									codigo_ajax(); 									
									asignar_ciudad('cbomunicipio','cbociudad');									
									document.execCommand('stop');
					    		}
							 }
						  }						  
					 peticion_http.open("POST", "http://srv05/siscam/scripts/funciones.php", true);
					 peticion_http.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
					 var envio = crea_envio(control2);
					 peticion_http.send(envio);
					}						
		 }
		 //FUNCION QUE ASIGNA EL CODIGO DE AREA DE CADA CIUDAD EN EL CAMPO 'COD_AREA' SEGUN LA CIUDAD QUE SE ELIJA
		 function codigo_ajax() 
		 {			
			for(i=0; i < document.getElementById("hdncbocodarea").length; i++) 
			{
				if(document.getElementById("cbociudad").options[document.getElementById("cbociudad").selectedIndex].value == document.getElementById("hdncbocodarea").options[i].value) 
				{					
				  document.getElementById("hdncbocodarea").options[i].selected = true;					  
				  document.getElementById('divcod_area').innerHTML = document.getElementById("hdncbocodarea").options[i].text;			  				  document.getElementById("hdncodigo").value = document.getElementById("hdncbocodarea").options[i].text;			  
	  			}
	 		}			
  		 }
