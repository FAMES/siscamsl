function activa_presupuesto() {
	var modalidad = document.getElementById('Zcbomodalidad').value;	
	if (modalidad=='EMERGENCIA' || modalidad=='ESPECIAL') {
		document.getElementById('presupuesto').style.visibility='visible';
		document.getElementById('Xtxtppto').disabled = false;
		document.getElementById('Xtxtfechappto').disabled = false;
		document.getElementById('Xtxtmontoppto').disabled = false;
	} else {
		document.getElementById('presupuesto').style.visibility='hidden';
	}
}

function inactiva_estado_empresa() {
	var modalidad = document.getElementById('Zcbomodalidad').value;
	
	if (modalidad=='AYUDA SOLIDARIA') {
		document.getElementById('esta_clinicas').value='101';
		document.getElementById('cboclinica').value='G000000000';
		document.getElementById('esta_clinicas').disabled = true;
		document.getElementById('cboclinica').disabled = true;
	} else {
		document.getElementById('esta_clinicas').value='0';
		document.getElementById('cboclinica').value='0';
		document.getElementById('esta_clinicas').disabled = false;
		document.getElementById('cboclinica').disabled = false;
	}
}

function desbloquear_estado_empresa() {
	
		document.getElementById('esta_clinicas').disabled = false;
		document.getElementById('cboclinica').disabled = false;
	
}


//funcion que se utiliza cuando los datos vienen directamente del modulo de 'ESTUDIANTES' ejecutando el ajax que monta el formulario de
// que se encuentra en ajax/ajax_estudiantes.php.
function consulta_cedula()
{
	//Verfica que haya datos en 'txtncontrol' o 'txtcedula'
	if(cartacaso()== true) 
		{
		getData('ajax/ajax_cartaaval.php','mostrar','&txtcedula='+document.getElementById('txtcedula').value+'&cbotipocarta='+document.getElementById('cbotipocarta').value+'&mod=nuevo','var null=nulo;'); 
		} 	
}

//funcion que trae el modulo de 'REGISTRO DE BENEFICIO SOLIDARIO' de manera automatica cuando el usuario viene del modulo de 'ESTUDIANTES'
function form_cartaaval() 
{
	//verifico que el campo 'HDNCEDULA' se cargue con el numero de cedula del estudiante que traigo del modulo 'ESTUDIANTES'
if(document.getElementById('hdnestcedula').value!="")
		{		
		document.getElementById('txtcedula').disabled = false; //activo el campo 'txtcedula'
		document.getElementById('txtcedula').value = document.getElementById('hdnestcedula').value;//Coloco el nro de cedula en 'txtcedula'
		document.getElementById('cbotipocarta').value = 'NUEVA'; //ubico en 'NUEVO' el combo 'tipo carta'
		validar_tipocarta(); //valido el tipo de carta 
		consulta_cedula();	//traigo el formulario por ajax automaticamente	
		}
}

function sel_clinicas(e)
{
	//FUNCION QUE ACTUALIZA LA COLUMNA 'clinica_por_estado' EN 'ajax_cartaaval.php' CON CODIGO PRESENTE EN 'ajax_combo_clinicas' DE LA CARPETA 'ajax'
	var cod_estado = e;
	getData('ajax/ajax_combo_clinicas.php','clinica_por_estado','&COD_ESTADO='+cod_estado,'var nulo=null');
}
function botonBeneficioSolidario(){
	//SI EL MONTO DISPONIBLE DEL ESTUDIANTE ES MAYOR A CERO	
	
	if(document.getElementById('hdnmontod').value>0) 
	{
		botoncarta_ajax(); 
		desbloquear(window.document.frmCartaAval);		
		//SI LA MODALIDAD ES ESPECIAL O NO
		if (document.getElementById('hdnmodalidad').value=='ESPECIAL' || document.getElementById('hdnmodalidad').value=='EMERGENCIA') 
		{		
				if(valida_campos_vacios(window.document.frmCartaAval)== true) 
				{ 
					
					carta_ajax(); 
				} 
				else 
				{ 
				bloquear_secundario(window.document.frmCartaAval); 
				}			
		} 
		else 
		{			
			//SI LA MODALIDAD ES UNA AYUDA SOLIDARIA O NO			
			if (document.getElementById('hdnmodalidad').value=='AYUDA SOLIDARIA' || document.getElementById('hdnmodalidad').value=='AYUDA SOLIDARIA ESPECIAL') 
			{
				desbloquear_estado_empresa();
				//SI ES REEMBOLSO
				document.getElementById('hdnclinica').value='G000000000';
				
				//PARA SABER SI SE MODIFICAR LA AYUDA SOLIDARIA
				if(document.getElementById('oculto').value!='modificar' && document.getElementById('oculto').value!='anular') 
				{ 
					//var fecregmod = document.getElementById('hdnfecregmod').value;
					//if (fecregmod<=0)
					//{
						//alert('No se puede generar una Ayuda Solidaria porque no han transcurrido 365 d�as desde la �ltima Ayuda Solidaria que se genero');
						//bloquear_secundario(window.document.frmCartaAval);
					//}
					//else
					//{
						if(valida_campos_vacios(window.document.frmCartaAval)== true)
						{ carta_ajax(); } 
						else 
						{ bloquear_secundario(window.document.frmCartaAval); }		
					//}
				} 
				else 
				{									
					if(valida_campos_vacios(window.document.frmCartaAval)== true) 
					{  carta_ajax(); } 
					else 
					{ bloquear_secundario(window.document.frmCartaAval); }		
				}
			} 
			else 
			{			
				if(valida_campos_vacios(window.document.frmCartaAval)== true) { carta_ajax(); } 
				else { bloquear_secundario(window.document.frmCartaAval); }			
			}
				
		}	
		
	//SI EL MONTO DISPONIBLE DEL ESTUDIANTE ES MENOR O IGUAL A CERO	
    }
	else 
	{
		//SI EL MONTO ES MENOR A CERO Y LA MODALIDAD ES ESPECIAL
        if (document.getElementById('hdnmodalidad').value=='ESPECIAL' || document.getElementById('hdnmodalidad').value=='EMERGENCIA') 
		{
			botoncarta_ajax(); 
			desbloquear(window.document.frmCartaAval);	
			if (document.getElementById('hdntipocarta').value=='AMPLIACION') 
			{ 
				if(valida_campos_vacios(window.document.frmCartaAval)== true) 
				{ 
					carta_ajax(); 
				} 
				else 
				{ 
					bloquear_secundario(window.document.frmCartaAval); 
				}	
			} 
			else 
			{
				if(validar(window.document.frmCartaAval)== true) 
				{ 
					carta_ajax(); 
				} 
				else 
				{ 
					bloquear_secundario(window.document.frmCartaAval); 
				}
			}
		//SI EL MONTO ES MENOR A CERO Y LA MODALIDAD NO ES ESPECIAL	
		} 
		else 
		{
			alert('No hay monto disponible para generar un Beneficio Solidario. Para procesar la modalidad debe ser especial o emergencia');
		}
		
	}//FIN MONTO MAYOR A CERO O NO
	
}//FIN FUNCION

function botoncarta_ajax(){
	if (document.getElementById('hdntipocarta').value=='NUEVA' && document.getElementById('mod').value!='x')
		{document.frmCartaAval.oculto.value='insertar';}
	if (document.getElementById('hdntipocarta').value=='AMPLIACION' && document.getElementById('mod').value!='x')
	 	{document.frmCartaAval.oculto.value='insertarcontrol';} 
	if (document.getElementById('hdntipocarta').value=='EXTENSION' && document.getElementById('mod').value!='x')
		{document.frmCartaAval.oculto.value='insertarext';}
	if (document.getElementById('mod').value=='x' && document.getElementById('oculto').value!='anular' && document.getElementById('oculto').value!='rec')
		{document.frmCartaAval.oculto.value='modificar';}	
}

function carta_ajax(){	
	getData('ajax/ajax_cartaaval2.php','mostrar2',            
	'&oculto='+document.getElementById('oculto').value+
	'&tipocarta='+document.getElementById('hdntipocarta').value+
	'&cedula='+document.getElementById('hdncedula').value+          
	'&carta='+document.getElementById('hdncarta').value+
	'&control='+document.getElementById('hdncontrol').value+
	'&txtestatus='+document.getElementById('hdnestatus').value+
	'&cbomodalidad='+document.getElementById('hdnmodalidad').value+
	'&cboclinica='+document.getElementById('hdnclinica').value+
	'&txtppto='+document.getElementById('hdnppto').value+
	'&txtfechappto='+document.getElementById('hdnfechappto').value+
	'&txtmontoppto='+document.getElementById('hdnmontoppto').value+
	'&txtfecreg='+document.getElementById('hdnfecreg').value+
	'&txtobservacion='+document.getElementById('hdnobservacion').value+
	'&txtmontoa='+document.getElementById('hdnmontoa').value+
	'&txtporcentajea='+document.getElementById('hdnporcentajea').value+
	'&txtdias='+document.getElementById('hdndiasv').value+
	'&usuario='+document.getElementById('hdnusuario').value+
	'&estudio='+document.getElementById('hdnestudio').value+
	//'&tipoant='+document.getElementById('hdntipoant').value+
	'&nombreusu='+document.getElementById('hdnnombreusu').value,
	'var null=nulo;'); 
	//document.getElementById('txtcedula').value = '';
	bloquear(window.document.frmCartaAval);
	document.frmCartaAval.btnEnviar.disabled=true;
	document.getElementById('btnModificar').disabled=true;
	document.getElementById('btnAnular').disabled=true;
	document.getElementById('btnRec').disabled=true;
}

function inicializa_cartaaval() {	
	validar_tipocarta();	
}

function verifica_cuenta_bancaria()
{
	var banco = document.getElementById('hdnbanco').value;
	var cuentaban = document.getElementById('hdncuentaban').value;
	var modalidad = document.getElementById('hdnmodalidad').value;
	var resultado = true; 
	if(modalidad == 'AYUDA SOLIDARIA' || modalidad == 'AYUDA SOLIDARIA ESPECIAL')
	{
		if(cuentaban == '' || banco == '')
		{
			alert("Debe agregarle cuenta Bancaria al Estudiante");
			resultado = false;
		}
		else
		{
			alert(banco + " " + cuentaban);
			alert("Bien");
		}
	}
	return resultado;
}
//Activacion de los campos de Ncontrol y Cedula
function validar_tipocarta() {
	if(document.getElementById("cbotipocarta").value == "0") {
		document.getElementById("txtncontrol").value = "";
		document.getElementById("txtcedula").value = "";
		document.getElementById("txtncontrol").disabled = true;  
		document.getElementById("txtcedula").disabled = true;
		document.getElementById("btnConsultarControl").disabled = true;
		document.getElementById("btnConsultarCedula").disabled = true;
	}
	if(document.getElementById("cbotipocarta").value == "NUEVA") {
		document.getElementById("txtncontrol").disabled = true;  
		document.getElementById("txtcedula").disabled = false;
		document.getElementById("btnConsultarControl").disabled = true;
		document.getElementById("btnConsultarCedula").disabled = false;
		document.getElementById("txtncontrol").value = "";
	} else if(document.getElementById("cbotipocarta").value == "AMPLIACION" || document.getElementById("cbotipocarta").value == "EXTENSION") {
		document.getElementById("txtcedula").disabled = true;
		document.getElementById("txtncontrol").disabled = false;  
		document.getElementById("btnConsultarCedula").disabled = true;
		document.getElementById("btnConsultarControl").disabled = false;
		document.getElementById("txtcedula").value = "";
	}
} 