// JavaScript Document
function inicializa_clinica()
{
	//funciones que inicializan el formulario de clinicas		
	carga_tipo_rif();	
	carga_banco();	
	//codigo_ajax();
	bloquear_campos_cli();
	asignar_codigo()
}
	function bloquear_campos_cli()
	{		
		if(document.getElementById('hdnmod').value != 'nuevo')
		{
			bloquear(window.document.frmclinicas);	
		}
	}
function carga_tipo_rif()
	{
		for(var i=0; i < document.getElementById("cbotipo_rif").length; i++)
		{
			if(document.getElementById("cbotipo_rif").options[i].value == document.getElementById("oculto_tipo_rif").value)
			{
			   document.getElementById("cbotipo_rif").options[i].selected = true;
			}
		}		
	}
	function validar_rif() 
	{
	   var resul = document.getElementById("txtrif").value.length; //Longitud de caracteres del campo txtrif
		   if(resul < 9) 
		   { 
		  	 alert("Debe incluir todos los Digitos en el campo RIF."); 			 
		   }
		   else if(document.getElementById("cbotipo_rif").options[0].selected == true)
		   { alert("Debe seleccionar un tipo de Rif"); 		   			   
		   }		  
	}
//funcion que valida que el campo txttelefono no tenga menos de 12 digitos
		function validar_cantidad() 
			{
   			var resul = document.getElementById("txttelefono").value.length;
   				if(resul < 7) 
				{
				 alert("Debe incluir todos los Digitos en el campo TELEFONO."); 
   				}
			}				
function carga_banco()
{
	var banco = document.getElementById("banco").value;	
	for(var i = 0; i < document.getElementById("cbobanco").length; i++)
	{		
		if(banco == document.getElementById("cbobanco").options[i].value)
		{		
			document.getElementById("cbobanco").options[i].selected = true;	
		}
	}	
}
function valida_cuentas() {
	   validar_rif();
	   valida_txtcuentaban();  
	   valida_txtcuentadebe();
	   valida_txtcuentahaber();   
	}
//funcion que valida que la longitud del campo txtcuentaban no tenga menos de 20 digitos
		function valida_txtcuentaban()
		{
		if(document.getElementById("txtcuentaban").value.length < 20)
		{
		  alert("Debe incluir todos los Digitos en el campo CUENTA BANCARIA");
		}
		}
//funcion que valida que la longitud del campo txtcuentadebe no tenga menos de 17 digitos
			function valida_txtcuentadebe()
			{
			if(document.getElementById("txtcuentadebe").value.length < 17)
			{
			  alert("Debe incluir todos los Digitos en el campo CUENTA DE ORDEN DEL DEBE");
			}
			}
//funcion que valida que la longitud del campo txtcuentahaber no tenga menos de 17 digitos
				function valida_txtcuentahaber()
				{
				if(document.getElementById("txtcuentahaber").value.length < 17)
				{
				  alert("Debe incluir todos los Digitos en el campo CUENTA DE ORDEN DEL HABER");
				}
				}
//funcion que valida que el campo txtcuentadebe no tenga inconevenientes al borrar con backspace y se inserte el guion
function validar_guiones(e, control) 
{
   	var res = true;
    evt = e ? e : event;
    tcl = (window.Event) ? evt.which : evt.keyCode;
	if(tcl == 8) 
	{
		var res = true; 
		} 
		else 
		{
		 	var guion = "-";
			var lon = document.getElementById(control).value.length;
				if(lon == 1 || lon == 4 || lon == 7 || lon == 10 || lon == 13)
				{
					document.getElementById(control).value = document.getElementById(control).value + guion;
				}
	}
	return res;
}
function envia_clinica()
{	
	getData('ajax/ajax_clinicas.php','res_clinicas',
      '&TIPO_RIF='+document.getElementById('cbotipo_rif').value+
      '&RIF='+document.getElementById('txtrif').value+
	  '&RIF2='+document.getElementById('rif2').value+
      '&CLINICA='+document.getElementById('txtclinica').value+
      '&ESTADO='+document.getElementById('cboestado').value+
      '&MUNICIPIO='+document.getElementById('cbomunicipio').value+
      '&CIUDAD='+document.getElementById('cbociudad').options[document.getElementById('cbociudad').selectedIndex].text+
      '&DIRECCION='+document.getElementById('txtdireccion').value+
      '&CODAREA='+document.getElementById('hdncodigo').value+
      '&TELEFONO='+document.getElementById('txttelefono').value+
	  '&CODCEL='+document.getElementById('cbocodcel').options[document.getElementById('cbocodcel').selectedIndex].text+
      '&CELULAR='+document.getElementById('txtcel').value+
	  '&CORREO='+document.getElementById('txtcorreo').value+
	  '&CONTACTO='+document.getElementById('txtpersona').value+
      '&CONVENIO='+document.getElementById('cboconvenio').value+
      '&CENTROUNI='+document.getElementById('cbocentrouni').value+
      '&BANCO='+document.getElementById('cbobanco').value+
      '&CUENTABAN='+document.getElementById('txtcuentaban').value+
      '&CUENTADEBE='+document.getElementById('txtcuentadebe').value+
      '&CUENTAHABER='+document.getElementById('txtcuentahaber').value+
      '&ESTATUS='+document.getElementById('cboestatus').value+
      '&IMPUESTOISLR='+document.getElementById('cboimpuestoislr').value+
      '&IMPUESTOLTF='+document.getElementById('cboimpuestoltf').value+
      '&RESPSO='+document.getElementById('cborespso').value+
      '&MONTOACUM='+document.getElementById('hdnmontoacum').value+
      '&USU_CEDULA='+document.getElementById('hdnusucedul').value+
      '&USU_NOMBRE='+document.getElementById('hdnusunombre').value+
      '&ACCION='+document.getElementById('hdnaccion').value,	  
      'var nulo=null;');	
}