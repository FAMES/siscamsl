function inicializa_localidad(formulario)
{
	if(!document.getElementById('hdnid').value)
	{ 			
		desbloquear_secundario(formulario); 
	    document.getElementById('btnEnviar').disabled = false; 
	}
}
function est_ajax()
{
	getData('ajax/ajax_estado.php','res',
  		  '&ID_ESTADO='+document.getElementById('hdnid').value+
          '&ESTADO='+document.getElementById('txtestado').value+
		  '&REGION='+document.getElementById('cboregion').value+
		  '&OCULTOESTADO='+document.getElementById('txtestado').value+
          '&CEDULA_USU='+document.getElementById('hdnusucedul').value+
		  '&NOMBRE_USU='+document.getElementById('hdnusunombre').value+
          '&HIDDEN='+document.getElementById('hdnaccion_est').value
          ,'var null=nulo;');	
}

function ciu_ajax()
{
	getData('ajax/ajax_ciudad.php','res',
  		  '&ID_CIUDAD='+document.getElementById('hdnid').value+
		  '&ID_MUNICIPIO='+document.getElementById('cbomunicipio').value+
          '&CIUDAD='+document.getElementById('txtciudad').value+
		  '&ESTADO='+document.getElementById('cboestado').value+
		  '&COD_AREA='+document.getElementById('cod_area').value+
		  '&OCULTOCIUDAD='+document.getElementById('hdncod_ciudad').value+
          '&CEDULA_USU='+document.getElementById('hdnusucedul').value+
		  '&NOMBRE_USU='+document.getElementById('hdnusunombre').value+
          '&HIDDEN='+document.getElementById('hdnaccion_ciu').value
          ,'var null=nulo;');	
}

function mun_ajax()
{		
	getData('ajax/ajax_municipio.php','res',
  		  '&ID_MUNICIPIO='+document.getElementById('hdnid').value+
          '&MUNICIPIO='+document.getElementById('txtmunicipio').value+
		  '&ESTADO='+document.getElementById('cboestado').value+		  		  
          '&CEDULA_USU='+document.getElementById('hdnusucedul').value+
		  '&NOMBRE_USU='+document.getElementById('hdnusunombre').value+
          '&ACCION='+document.getElementById('hdnaccion_mun').value
          ,'var null=nulo;');		
}
function validar_codarea(cadena,e) {	
 var res = true; 	
 	
    evt = e ? e : event; //Asigno a una variable el evento 'e' pasado por parametro
	tcl = (window.Event) ? evt.which : evt.keyCode; //asigno a una variable el codigo de la tecla pulsada
	
	if(cadena.value == '' && tcl == 96 || cadena.value == '' && tcl == 48) 
		 {
		 var res = false; //Asigno false para que no ejecute
		 }		 
		 return res; 
}