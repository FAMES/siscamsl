// JavaScript Document
//FUNCION QUE INICIALIZA LAS FUNCIONES DE 'ESTUDIANTES'
function inicializa_estudiantes()			
			{	
			bloqueo_campos();
			asignar_codigo();
			asignar_sexo();
			asignar_fam_ling();			
			asignar_periodo();
			beneficiario_ajax();
			enlaza_periodo();			
			carga_banco();
			asigna_fecdef();
//EJECUTO LA FUNCION 'PAIS()' AL CARGAR
			pais();
			
			//codigo_ajax();			
//EJECUTO LA FUNCION 'VALIDA_NAC()' AL CARGAR
			valida_nac();				
			   	document.getElementById("cbotdiscapacidad").disabled = true;
			   	document.getElementById("cbodiscapacidad").onchange = cambia_dis;
			}
//FUNCION QUE EJECUTA EL SUBMIT PARA SUBIR LA FOTO DEL ESTUDIANTE
function subeFotos_est()
{
	if(document.getElementById('guardaFoto').value == 'SI')
				{					
					document.getElementById('hdnaccion').value='MODIFICAR';					
					estudiante_ajax();					
				}		
}
//FUNCION QUE VERIFICA SI ES UN NUEVO ESTUDIANTE O UNA MODIFICACION PARA EL BLOQUE DE CAMPOS
function bloqueo_campos()
{		
	if(document.getElementById('hdnced').value != '')
	{ 
		document.getElementById('btnEnviar').disabled = true;
		bloquear(window.document.frmestudiantes); 
		bloquear(window.document.frmFoto);
		
	}
	else
	{ document.getElementById('btnEnviar').disabled = false;}
}


//FUNCION QUE VALIDAD EL NUMERO DE CARACTERES DEL CAMPO 'CEDULA'
	function valida_ced() 
	{
		if(document.getElementById("txtcedula").value.length < 7) 
		{
			alert("Debe Ingresar todos los digitos de la CEDULA");
		}
	}
	//FUNCION PARA ASIGNAR EL VALOR PREDETERMINADO AL COMBO 'NACIONALIDAD'
	function valida_nac() 
  	{		
		var encontrado = false;
		var res = document.getElementById("hdnnac").value;			
			for(var i = 0; i < document.getElementById("cbonacionalidad").length; i++) 
			{
				  if(res == document.getElementById("cbonacionalidad").options[i].value) 
				  {
					document.getElementById("cbonacionalidad").options[i].selected = true;
					var encontrado = true;
				  }	  
		    }
					  if(!encontrado) 
					  {
						document.getElementById("cbopaises").options[0].text = "No Aplica";
					  }
  	}
	//FUNCION PARA PREDETERMINAR EL VALOR DEL COMBO PAIS 
   function pais() 
   {
         var res = document.getElementById("hdnpais").value;		 
              for(var i = 0; i < document.getElementById("cbopaises").length; i++) 
			  {
                  if(res == document.getElementById("cbopaises").options[i].value) 
				  {
	                        document.getElementById("cbopaises").options[i].selected = true;
	              }
              } 
   }
   //FUNCION PARA MANEJAR DINAMICAMENTE EL VALOR DEL COMBO 'PAISES'
	  function des_paises() 
	  {
			   if(document.getElementById("cbonacionalidad").value != 'EXTRANJERO') {
				  document.getElementById("cbopaises").options[0].selected = true;
				  document.getElementById("cbopaises").disabled = true;
				  document.getElementById("cbopaises").options[0].text = "No Aplica";
				  document.getElementById("cbopaises").options[0].value = "1";  				    
			   }
	   else 
			   {
				 document.getElementById("cbopaises").disabled = false;
				 document.getElementById("cbopaises").options[0].text = "=Seleccione="; 	     	 
			   }
	  }  
	  function asignar_sexo() 
		{
 			for(var i = 0; i < document.getElementById("cbosexo").length; i++) 
 			{ 	
     			if(document.getElementById("hdnsexo").value == document.getElementById("cbosexo").options[i].value) 
				{	
    				document.getElementById("cbosexo").options[i].selected = true; 
				}
			}
 		} 
		
	//FUNCION PARA ENLAZAR EL CAMPO DE 'CBOPERIODO' Y 'N_PERIODO' REFERIDOS AL A�O, SEMESTRE O TRIMESTRE DE ESTUDIO DEL ESTUDIANTE
	function enlaza_periodo()
			{				
			var txtperiodo  = document.getElementById('txtperiodo').value;			
			var cboperiodo = document.getElementById('cboperiodo').options[document.getElementById('cboperiodo').selectedIndex].value;			
			document.getElementById('hdnperiodo').value = txtperiodo + " " + cboperiodo;				
			}
	//FUNCION PARA VALIDAR EL NUMERO DE CARACTERES INCLUIDOS EN EL CAMPO 'TELEFONO'
		function valida_telefono() 
		{
  			if(document.getElementById("txttelefono").value.length < 7) 
			{
   			   alert("Debe Ingresar todos los digitos en el campo TELEFONO");
  			}
			
 		}
		//FUNCIONES PARA VALIDAR EL NUMERO DE CARACTERES INCLUIDOS EN EL CAMPO 'CELULAR'
		 function  valida_celular() 
		 { 
  			if(document.getElementById("txtcel").value.length < 7) 
			{
   			   alert("Debe Ingresar todos los digitos en el campo CELULAR");
 		 	}
	     }
		 /*function  valida_celular2()
		 { 
  			if(document.getElementById("txtcel2").value.length < 7) 
			{
   			   alert("Debe Ingresar todos los digitos en el campo CELULAR");
 		 	}
	     }*/
		 		
		 //FUNCION QUE VALIDA LA CARGA PREDETERMINADA DEL CODIGO DE CIUDAD Y CELULAR DEL ESTUDIANTE
  	function asignar_codigo() 
  	{		
  		for(var i = 0; i < document.getElementById("cbocodcel").length; i++) 
  		{
   			if(document.getElementById("hdncel").value == document.getElementById("cbocodcel").options[i].value) 
   			{
	  			document.getElementById("cbocodcel").options[i].selected = true;
			}
  		}
		/*for(var i = 0; i < document.getElementById("cbocodcel2").length; i++)
		{
			if(document.getElementById("hdncel2").value == document.getElementById("cbocodcel2").options[i].value)
			{
				document.getElementById("cbocodcel2").options[i].selected = true;
			}
		}*/

  	}
	//FUNCION QUE VALIDA LA CARGA PREDETERMINADA DEL COMBO DE ETNIAS
	function asignar_fam_ling() 
  	{
		for(var i = 0; i < document.getElementById("cbofam_ling").length; i++) 
  		{			
   			if(document.getElementById("hdnfam_ling").value == document.getElementById("cbofam_ling").options[i].value) 
   			{
	  			document.getElementById("cbofam_ling").options[i].selected = true;
				carga_etnia();				
			}
  		}
  	}
	function carga_etnia()
	{		
		getData('ajax/Estudiantes/bd_estudiantes.php', 'cboetnia', '&hdnaccion=asigna_etnia&grupo='+document.getElementById('cbofam_ling').options[document.getElementById('cbofam_ling').selectedIndex].value,'var nulo=null;');	
	}		

	//FUNCION QUE MANEJA DINAMICAMENTE EL VALOR DE COMBO 'CBODISCAPACIDAD'
			 function cambia_dis() 
			 {
   				if(document.getElementById("cbodiscapacidad").value != "SI") 
				{
				  document.getElementById("cbotdiscapacidad").disabled = true;
				  document.getElementById("cbotdiscapacidad").value = 0;
   				}
   				else 
					if(document.getElementById("cbodiscapacidad").value == "SI") 
    				{ 
						document.getElementById("cbotdiscapacidad").disabled = false; 
					}
			}
	//FUNCION QUE VALIDA EL ENVIO DEL COMBO 'CBOTDISCAPACIDAD'
			function valida_disca() 
			{
  			if(document.getElementById("cbotdiscapacidad").value == '0' && document.getElementById("cbotdiscapacidad").disabled == false) 				{
   					alert("Debe Ingresar una DISCAPACIDAD");
					return false;
		   		}
		   }
	//FUNCION QUE ASIGNA VALOR AL CAMPO cboperiodo SEGUN EL VALOR DE LA TABLA 'ESTUDIANTES'
		   function asignar_periodo()
		   {			        
				for(var i=0; i<document.getElementById('cboperiodo').length; i++)
				{						
					if(document.getElementById('hdntperiodo').value == document.getElementById('cboperiodo').options[i].value)	
					{
						document.getElementById('cboperiodo').options[i].selected = true;	
					}
				}
	   		}
	//FUNCION QUE VALIDA LA FECHA DE NACIMIENTO
		function verifica_fecha_nac()
		{
			var dia = document.getElementById('cbodia_nac').value;
			var mes = document.getElementById('cbomes_nac').value;
			var anio = document.getElementById('cboanio_nac').value;
			var bool = true;
			//Si el a�o es bisiesto
			if(mes == 2 && dia > 28 && anio % 4 != 0) 
			{ alert("Error en Fecha"); document.getElementById('cbodia_nac').focus(); bool = false; }
			else if(mes == 2 && dia > 29) 
			{ alert("Error en Fecha"); document.getElementById('cbodia_nac').focus(); bool = false; }
			//Verifico Resto de meses
			if((mes == 4 || mes == 6 || mes == 9 || mes == 11) && (dia > 30))
			{ alert("Error en Fecha "); document.getElementById('cbodia_nac').focus(); bool = false; }	
			return bool;
		}

		
	
	 //FUNCION QUE EVALUA EL FORMULARIO AL ENVIARLO A GUARDAR O MODIFICAR EN EL BOTON 'GUARDAR'
	 function valida_formulario()
	 {	
	 	 enlaza_periodo();		 		
		 	if(valida_campos_vacios(document.frmestudiantes)== true && verifica_fecha_nac() == true) 
		 	{ 			
		 	desbloquear_secundario(document.frmestudiantes); 			
			estudiante_ajax();			
				if(document.getElementById('fotoest').value != '' || document.getElementById('hdnfotoest').value != '') 
				{					
				document.getElementById('hdnced').value = document.getElementById('Ztxtcedula').value;
				setTimeout('document.frmFoto.submit()', 1000);					
				} 
			} 
			des_paises();		
	 }
	 
	 
	 //FUNCION QUE VALIDA EL CAMPO DE LA CEDULA PARA MODIFICARLO
	 function valida_cedula()
	 {
		 var tipo_cedula = document.getElementById('Zcbotipo').value;
		 var cedula = document.getElementById('Ztxtcedula').value;
		if(cedula != '' && cedula != '')
		{
			estudiante_ajax();			
		}
	 }
	 
///////////////////////////////////FUNCIONES DE LOS BOTONES//////////////////////////////////
function btnmod_est()
{
	document.getElementById('hdnaccion').value='MODIFICAR'; 
	document.getElementById('btnEnviar').disabled = false;
	desbloquear_secundario(window.document.frmestudiantes); 
	desbloquear_secundario(window.document.frmFoto);	
	des_paises();
}

function btnmodced_est()
{
	document.getElementById('hdnaccion').value='MODIFICARCEDU'; 
	document.getElementById("cbopaises").options[0].value = '0';
	desbloquear_principal(window.document.frmestudiantes); 
	document.getElementById('btnEnviar').disabled = false;	
}

function btnEli_est()
{
	if(confirm('�Desea eliminar este registro? Ser\u00E1 eliminado todo el contenido relacionado'))
	{
		document.getElementById('hdnBorraFoto').value='ELIMINAR'; 
		setTimeout('document.frmFoto.submit()', 10);	
		document.getElementById('hdnaccion').value='ELIMINAR';		
		desbloquear(window.document.frmestudiantes); 		
		estudiante_ajax();  		
	}	
}

function envia_datos()
{
	var accion = document.getElementById('hdnaccion').value;
	if(accion == 'MODIFICARCEDU')	
	{
		valida_cedula();	
	}
	else
	{
	
		if(document.getElementById('txtfechadef').value ==''){

			document.getElementById('txtfechadef').value ='1900-01-01';
		}		
		
		

		if((document.getElementById('txtcel2').value =='') || (document.getElementById('txtcel2').value =='Null')){

			document.getElementById('txtcel2').value ='0000000';

	}

		valida_formulario();
	}
}

///////////////////////////////////FIN FUNCIONES DE LOS BOTONES//////////////////////////////////
	//FUNCION AJAX QUE ENVIA LA PETICION A LA BASE DE DATOS PARA AGREGAR,MODIFICAR O ELIMINARr
	function estudiante_ajax()
		   {
			alert(document.getElementById('txtfechadef').value);
						   
			   	getData('ajax/Estudiantes/bd_estudiantes.php','res',           
             	'&CEDULA='+document.getElementById('Ztxtcedula').value+
				'&CEDULA2='+document.getElementById('hdnced').value+
                '&TIPOCED='+document.getElementById('Zcbotipo').value+
				'&TIPOCED2='+document.getElementById('hdntipoced').value+
                '&PNOMBRE='+document.getElementById('txtpnombre').value+
                '&PAPELLIDO='+document.getElementById('txtpapellido').value+
                '&NACIONALIDAD='+document.getElementById('cbonacionalidad').value+
				'&DIA_NACIMIENTO='+document.getElementById('cbodia_nac').value+
				'&MES_NACIMIENTO='+document.getElementById('cbomes_nac').value+
				'&ANIO_NACIMIENTO='+document.getElementById('cboanio_nac').value+
                '&UNIVERSIDAD='+document.getElementById('cbouniversidad').value+
				'&MSUCRE='+document.getElementById('cbomsucre').value+
                '&CARRERA='+document.getElementById('cbocarrera').value+
                '&USU_CEDULA='+document.getElementById('hdnusucedul').value+
                '&DIRECCION='+document.getElementById('txtdireccion').value+
                '&COD_TEL_HABITACION='+document.getElementById('hdncodigo').value+
                '&TEL_HABITACION='+document.getElementById('txttelefono').value+
                '&COD_TEL_CELULAR='+document.getElementById('cbocodcel').value+
                '&TEL_CELULAR='+document.getElementById('txtcel').value+
				'&COD_TEL_CELULAR2='+document.getElementById('cbocodcel2').value+
                '&TEL_CELULAR2='+document.getElementById('txtcel2').value+
                '&ESTADO='+document.getElementById('cboestado').value+
                '&ETNIA='+document.getElementById('cboetnia').value+
                '&DISCAPACIDAD='+document.getElementById('cbodiscapacidad').value+
                '&CIUDAD='+document.getElementById('cbociudad').options[document.getElementById('cbociudad').selectedIndex].text+
                '&MUNICIPIO='+document.getElementById('cbomunicipio').value+
                '&SEXO='+document.getElementById('cbosexo').value+
                '&SNOMBRE='+document.getElementById('txtsnombre').value+
                '&SAPELLIDO='+document.getElementById('txtsapellido').value+
                '&TIPO_DISCAPACIDAD='+document.getElementById('cbotdiscapacidad').value+
                '&EMAIL='+document.getElementById('txtcorreo').value+
                '&FOTOEST='+document.getElementById('fotoest').value+
                '&EDOCIVIL='+document.getElementById('cboedocivil').value+
                '&PAIS='+document.getElementById('cbopaises').value+
                '&MONTO_DISPONIBLE='+document.getElementById('hdnmontod').value+
                '&FAM_LINGUISTICA='+document.getElementById('cbofam_ling').value+                
                '&USU_NOMBRE='+document.getElementById('hdnusunombre').value+
                '&PERIODO='+document.getElementById('hdnperiodo').value+ 
                '&BANCO='+document.getElementById('cbobanco').value+      
                '&CUENTABAN='+document.getElementById('txtcuentaban').value+                                                 
                '&ACCION='+document.getElementById('hdnaccion').value+
		'&FOTOEST='+document.getElementById('hdnrutafoto').value+
		'&FECHA_DEFUNSION='+document.getElementById('txtfechadef').value
                ,'var null=nulo;');
	if(document.getElementById('hdnaccion').value != 'ELIMINAR')
	{
		if(confirm('¿Desea Agregar una Carta Aval a Este Estudiante?'))
		{
	setTimeout("document.location.href='?id=carta2&mod=nuevo&tipo=reg&est_cedula='+document.getElementById('Ztxtcedula').value",1000);
		}
		else
			if(confirm('¿Desea devolver el Caso?'))
			{
		setTimeout("document.location.href='?id=casosdev2&mod=nuevoA'",1000);
			}
	}
}
	
	
		function beneficiario_ajax()
		  {			 
		  getData('ajax/Estudiantes/hist_estudiantes.php','hist_estudiantes',
				  '&cedula='+document.getElementById('hdnced').value				  
				  ,'var nulo=yes;');		  
		  }
