// JavaScript Document
function inicializa_ingreso()
{
	//funciones que inicializan el formulario de clinicas		
	carga_tipo_ced();	
	//carga_banco();	
	//codigo_ajax();
	//bloquear_campos_cli();
	//asignar_codigo()
}
	function bloquear_campos_cli()
	{		
		if(document.getElementById('hdnmod').value != 'nuevo')
		{
			bloquear(window.document.frmclinicas);	
		}
	}
function carga_tipo_ced()
	{
		for(var i=0; i < document.getElementById("cbotipo_ced").length; i++)
		{
			if(document.getElementById("cbotipo_ced").options[i].value == document.getElementById("oculto_tipo_ced").value)
			{
			   document.getElementById("cbotipo_ced").options[i].selected = true;
			}
		}		
	}
	function validar_ced() 
	{
	   var resul = document.getElementById("txtced").value.length; //Longitud de caracteres del campo txtrif
		   if(resul < 8) 
		   { 
		  	 alert("Debe incluir todos los Digitos en el campo CEDULA."); 			 
		   }
		   else if(document.getElementById("cbotipo_ced").options[0].selected == true)
		   { alert("Debe seleccionar un tipo de CEDULA"); 		   			   
		   }		  
	}
//funcion que valida que el campo txttelefono no tenga menos de 12 digitos
		function validar_cantidad() 
			{
   			var resul = document.getElementById("txttelefono").value.length;
   				if(resul < 7) 
				{
				 alert("Debe incluir todos los Digitos en el campo TELEFONO."); 
   				}
			}				
function carga_banco()
{
	var banco = document.getElementById("banco").value;
	for(var i = 0; i < document.getElementById("cbobanco").length; i++)
	{		
		if(banco == document.getElementById("cbobanco").options[i].value)
		{		
			document.getElementById("cbobanco").options[i].selected = true;	
		}
	}	
}
function valida_cuentas() {
	   validar_rif();
	   valida_txtcuentaban();  
	   valida_txtcuentadebe();
	   valida_txtcuentahaber();   
	}
	
function btnmod_estudiante()
{
	document.getElementById('hdnaccion').value='MODIFICAR'; 
	document.getElementById('btnEnviar').disabled = false;
	desbloquear_secundario(window.document.frmclinicas);
	
}

function btnmodced_estudiante()
{
	document.getElementById('hdnaccion').value='MODIFICARCEDU'; 
	desbloquear_principal(window.document.frmclinicas); 
	document.getElementById('btnEnviar').disabled = false;	
}

	
	
//funcion que valida que la longitud del campo txtcuentaban no tenga menos de 20 digitos
		function valida_txtcuentaban()
		{
		if(document.getElementById("txtcuentaban").value.length < 20)
		{
		  alert("Debe incluir todos los Digitos en el campo CUENTA BANCARIA");
		}
		}
//funcion que valida que la longitud del campo txtcuentadebe no tenga menos de 17 digitos
			function valida_txtcuentadebe()
			{
			if(document.getElementById("txtcuentadebe").value.length < 17)
			{
			  alert("Debe incluir todos los Digitos en el campo CUENTA DE ORDEN DEL DEBE");
			}
			}
//funcion que valida que la longitud del campo txtcuentahaber no tenga menos de 17 digitos
				function valida_txtcuentahaber()
				{
				if(document.getElementById("txtcuentahaber").value.length < 17)
				{
				  alert("Debe incluir todos los Digitos en el campo CUENTA DE ORDEN DEL HABER");
				}
				}
//funcion que valida que el campo txtcuentadebe no tenga inconevenientes al borrar con backspace y se inserte el guion
function validar_guiones(e, control, tipo) 
{
   	var res = true;
    evt = e ? e : event;
    tcl = (window.Event) ? evt.which : evt.keyCode;
	if(tcl == 8) 
	{
		var res = true; 
		} 
		else 
		{
		 	var guion = "-";
			var lon = document.getElementById(control).value.length;
			if(tipo == 1)  //Campos txtcuentadebe, txtcuentahaber de formulario CLINICAS
			{			
				if(lon == 1 || lon == 4 || lon == 7 || lon == 10 || lon == 13)
				{
					document.getElementById(control).value = document.getElementById(control).value + guion;
				}
			}
			if(tipo == 2) //Campo codpre de formulario ASEGURADOS
			{				
				if(lon == 2 || lon == 5 || lon == 9 || lon == 12 || lon == 15)
				{
					document.getElementById(control).value = document.getElementById(control).value + guion;
				}	
			}
	}
	return res;
}
function envia_ingreso()
{	
	getData('ajax/ajax_ingreso.php','res_ingreso',
      '&TIPO_CED='+document.getElementById('cbotipo_ced').value+
      '&CED='+document.getElementById('txtced').value+
      '&NOMBRE='+document.getElementById('txtnombre').value+
	  '&APELLIDO='+document.getElementById('txtapellido').value+
	  '&OBSERVACION='+document.getElementById('txtobservacion').value+
	  '&FECHAINGR2='+document.getElementById('txtfechaingrr').value+
      '&USU_CEDULA='+document.getElementById('hdnusucedul').value+
	  '&COD_ID='+document.getElementById('hdnid').value+
      '&USU_NOMBRE='+document.getElementById('hdnusunombre').value+
	  '&USU_DEPARTAMENTO='+document.getElementById('hdnusudepartamento').value+
      '&ACCION='+document.getElementById('hdnaccion').value,	  
      'var nulo=null;');	
}