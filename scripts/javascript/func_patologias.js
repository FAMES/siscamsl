// JavaScript Document
function inicializa_patologia() 
{	 
	
	//INICIALIZO LAS VARIABLES Y EL LISTADO DE PATOLOGIAS DEL CASO
	 getData('ajax/patologias/patoaso.php','resp_patologia','&carta='+document.getElementById('carta').innerHTML+'&control='+document.getElementById('control').innerHTML+'&var=Agregar&inc=inc','var nulo=null;');
	 //ESTA FUNCION SE ENCUENTRA EN EL ARCHIVO UBICADO EN 'script/auto_completa.js'
	 //asignaVariables();	 
}

//Funcion que activa los campos 'codigo baremo' y 'ajuste baremo' al seleccionar un año
		function activa_campos() 
		{
			if(document.getElementById("cbobaremo").selectedIndex != 0)	
			{
				 document.getElementById("input_2").disabled = false; 
				 document.getElementById("txtajuste").disabled = false; 
			}
			else 
			
			{ 
				 document.getElementById("input_2").disabled = true; 
				 document.getElementById("txtajuste").disabled = true;  
			}
		}
		
//Funcion que valida la peticion de 'Descripcion patologia' y 'monto baremo' a la base de datos 	
		function carga_datos_pat() 
		{			
			if(document.getElementById("input_2").value != "") 
			{
		getData('ajax/patologias/patoaso.php','res','&var=Buscar&indice='+document.getElementById("input_2").value+'&ano='+document.getElementById("cbobaremo").options[document.getElementById("cbobaremo").selectedIndex].value+'&PERMISOLOGIA='+document.getElementById('permisologia').innerHTML+'&busca=descripcion&inc=inc',
'var nulo=null;'); 
			}			
		}		

		function verifica_sexo()
		{
			var bool = true;
			if(document.getElementById('sexo').innerHTML == 'MASCULINO' && document.getElementById('input_2').value == 9001)
			{
				alert('EL SEXO DEL ESTUDIANTE ES MASCULINO');
				bool = false;
				//document.execCommand('stop');
				
			}
			return bool;
		}
//Funcion que valida el contenido de la tabla de 'Codificacion de Patologias' para hacer la peticion a la base de datos
		function valida_envio_pat()	
		{			
		var i = true;
		var ajuste = Number(document.getElementById("txtajuste").value);
			if(document.getElementById("input_2").value == "" ) //Verifico campo 'codigo Baremo'  
			{ 
			i = false; 
			}
				if(document.getElementById("descbaremo").innerHTML == "No Existe") //Verifico 'Descripcion Baremo'
				{ 
				i = false; 
				}
					if(document.getElementById("montobaremo").innerHTML == "No Existe") //Verifico 'Monto Baremo'
					{ 
					i = false; 
					}
						if(document.getElementById("txtajuste").value < 1) //Verifico 'Monto Ajuste'
						{ 
						i = false; 
						}
							if(i == false) //Verifico variable i
							{ 
							alert("Debes Cargar los Datos Correctamente"); 
							} 
							else 
							{ 
							i = codigo_ofaes(); //Verifico Codigo Ofaes							
							i = valida_montobar(); //verifico monto de Ajuste valido
							}
							
		return i; 
		}
		
//Funcion que valida que el monto ajustado no supere al monto baremo
	function valida_montobar() 
	{
		var ajuste = document.getElementById('txtajuste').value;
		var montobaremo = Number(document.getElementById("montobaremo").innerHTML);
		var cod_baremo_ofaes = document.getElementById('input_2').value;		
		if(ajuste > montobaremo && cod_baremo_ofaes != 27001)
		{ 
		alert("El Ajuste no puede ser Mayor al Monto Baremo"); 
		return false; 
		} 
		else 
		return true; 
	}	
	
//FUNCION QUE VERIFICA LA EXISTENCIA DE LA PATOLOGIA EN LA BASE DE DATOS

	function valida_exist_pat()	
	{		
	var res = true;	
	var total = 0;
//VERIFICA SI SE HA AGREGADO ALGUN REGISTRO DE PATOLOGIAS AL CASO
		if(document.getElementById("total_pat")) 
		{					
		var total_pat = Number(document.getElementById("total_pat").innerHTML);
			for(var j = 1; j <= total_pat; j++) 
			{
			//SE CREAN 2 INDICES
			var indice = "bar_id" + j; //INDICE PARA RECORRER LOS CODIGOS BAREMOS DE CADA PATOLOGIA
			var ind_ajus = "ajus" + j; //INDICE PARA RECORRER LOS MONTOS DE AJUSTE DE CADA PATOLOGIA			
			//EJECUTO LA SUMA DE LOS AJUSTES DE LAS PATOLOGIAS DEL CASO
			var ajuste = Number(document.getElementById(ind_ajus).innerHTML);
			
			total = total + ajuste;			
				//VERIFICA SI LA PATOLOGIA A AGREGAR YA SE ENCUENTRA EN EL LISTADO DE PATOLOGIAS
				if (document.getElementById("idbaremo").innerHTML == document.getElementById(indice).innerHTML)	
				{
				//alert("Patologia ya Agregada");
				res = true; 
				}
			} 
//COMPARO EL VALOR DEL TOTAL DE LAS PATOLOGIAS AGREGADAS CON EL MONTO DEL FACTURA
//PARA VALIDAR SI SUPERA EL VALOR DEL FACTURA
		var valida_total = total + Number(document.getElementById("txtajuste").value);
		var montofac = Number(document.getElementById("monto_factura").innerHTML);		
			if(valida_total > montofac && montofac != '') 
			{
			alert("El Monto Total de las Patologias Supera al monto de la factura de este caso");
			res = false; 
			}  
		}	
		else 
		{ //SINO SE HA AGREGADO NINGUNA PATOLOGIA VERIFICO DIRECTAMENTE EL VALOR DEL AJUSTE CON EL MONTO DE PRESUPUESTO
		var valida_total = Number(document.getElementById("txtajuste").value);
		var montofac = Number(document.getElementById("monto_factura").innerHTML);		
			if(valida_total > montofac && montofac != '') 
			{
			alert("El Monto Total de las Patologias Supera al monto de la factura de este caso");
			res = false; 
			}  
		}	
	return res; 
	}

//FUNCION QUE VALIDA EL AÑO SELECCIONADO EN EL 'CBOBAREMO' NO SEA DISTINTO AL AÑO DE BAREMO DE PATOLOGIAS DEL CASO
	function valida_anio() 
	{
	var anio_sel = document.getElementById("cbobaremo").options[document.getElementById("cbobaremo").selectedIndex].value;	
//VERIFICA SI SE HA AGREGADO ALGUN REGISTRO DE PATOLOGIAS AL CASO
		if(document.getElementById("total_pat")) 
		{							
		var anio_bar = Number(document.getElementById("anio1").innerHTML);
		var cbobaremo = document.getElementById("cbobaremo");
//COMPARA EL AÑO DEL PRIMER REGISTRO DE PATOLOGIAS DEL CASO CON EL AÑO SELECCIONADO EN 'CBOBAREMO'
			if(anio_bar != anio_sel) 
			{
			alert("No Puede Utilizar Baremo de otro Año");
//RECORRO LAS OPCIONES DE 'CBOBAREMO' Y SELECCIONO EL AÑO DEL BAREMO DE LAS PATOLOGIAS DEL CASO
				for(var k = 0; k < cbobaremo.length; k++) 
				{								
					if(cbobaremo.options[k].value == anio_bar) 
					{
					cbobaremo.options[k].selected = true;
					asignaVariables();
					} 
				} 
			} 
		} 
	}
	

//FUNCION QUE EVALUA CODIGO ORFAES
	function codigo_ofaes()
	{
	 var res = true;
	 var monto_minimo_ofaes = 1000;
	 var monto_maximo_ofaes = 5000;
	 var cod_baremo_ofaes = document.getElementById('input_2').value;
	 var ajuste = Number(document.getElementById('txtajuste').value);
	 var permisologia = document.getElementById('permisologia').innerHTML;
	 	if(cod_baremo_ofaes == 27001 && permisologia != 1)
		{
			if(ajuste < monto_minimo_ofaes || ajuste > monto_maximo_ofaes)
			{
			alert('El Monto ORFAES debe estar entre ' + monto_minimo_ofaes + ' Bs o ' + monto_maximo_ofaes + ' Bs \n \n       Se Requiere Permisologia de Coordinador');
			document.execCommand('stop');
			}
		}
	return res;	
	}
	//Funcion que se complementa con la funcion baremo_excedente_disponible()
				function casos_baremo_excedente(montod,ajuste,total,dif)
				{					
					var bool = true;	
					var disponible_actual = montod - total;
					//1º Caso 
						if(montod < ajuste && !total)
						{ 
						    alert("El Monto del Ajuste supera la Disponibilidad del Estudiante"); 
							bool = false;
						}
					//2º Caso
						if(disponible_actual > 0 && dif == 0)
						{
							alert("\t\tEl Estudiante Tiene " + disponible_actual + " Bs. Disponibles \n No puede Utilizar Baremo 27002 si existe Disponibilidad");
							bool = false;
						} else if(disponible_actual != 0 && dif == 0)
								{									
									alert("\tEl Estudiante Tiene " + montod + " Bs. Disponibles \n No puede Utilizar Baremo 27002 si existe Disponibilidad");
									bool = false;	
								}
					//3º Caso 	 
						if(total) 
						{	
							var total_calculo =  total + ajuste;	
								if(montod == 0)
								{
									alert("El Estudiante No tiene Monto Disponibilidad"); 
									bool = false;
								}
								else if(total_calculo > montod && dif != 0) 
									{ 								
										alert("El Estudiante solo tiene disponibilidad de : "+ disponible_actual + " Bs."); 
										bool = false; 									
									}							
						}											
						return bool;
				}
	function baremo_excedente_disponible()
	{
		//Datos de Condicion
		var permisologia = document.getElementById('permisologia').innerHTML;
		var peracceso = document.getElementById('hdnper_acceso').value;
		var anio_baremo = document.getElementById('cbobaremo').value;	
		//Valores Comparativos
		var ajuste = parseFloat(document.getElementById('txtajuste').value);
		var montod = parseFloat(document.getElementById('montod').innerHTML);
		var total = parseFloat(document.getElementById('total').innerHTML);		
		//Variables de Ejecucion		
		var bool = true;
		/*if(codigo_baremo != 27002)
		{
			//FUNCION QUE VERIFICA QUE EL MONTO DE DISPONIBILIDAD NO SEA EXCEDIDO
			bool = casos_baremo_excedente(montod,ajuste,total,1);		
		}*/
		//alert(permisologia);
			/*if(permisologia == 6 && peracceso == 1 && anio_baremo == 90)
			{
				//alert(" \t   No puede utilizar Baremo " + codigo_baremo +"\n\t Se Necesita Codigo de Coordinador");
				bool = true;
			}
			else 
			{ 
				bool = casos_baremo_excedente(montod,ajuste,total,1); 
			}*/					
		return bool;
	}
	
	function valida_cod_Anticonceptivo()
	{
		var bool = true;
		if(document.getElementById('bar_id1'))
		{	
			var total_pat = Number(document.getElementById('total_pat').innerHTML);
			if(document.getElementById('bar_id1').innerHTML == '13013' && document.getElementById('centro_uni').value == 'SI')
			{
				alert('No Se Pueden Agregar Mas Patologias');
				bool = false;
			} else if(document.getElementById('input_2').value == '13013' && document.getElementById('centro_uni').value == 'SI')
			{
				alert('La Patologia de Anticonceptivos No Se Puede Agregar');
				bool = false;
			}		
		}
		return bool;
	}
	
	function valida_monto_pres()
	{
		var montop = Number(document.getElementById('montop').innerHTML);
		var ajuste = document.getElementById('txtajuste').value;
		var result = true;
		var total_ajuste = 0;
		
		if(document.getElementById('modalidad').innerHTML == 'EMERGENCIA')
		{
			if(!document.getElementById('total_pat'))
			{
				if(document.getElementById('txtajuste').value > montop)
				{ 
					alert("Monto de Patologia es superior a Monto del presupuesto"); 
					result = false;
				}
			} 
			else 
			{				
			var total_pat = Number(document.getElementById('total_pat').innerHTML);
			
				for(var i = 1; i <= total_pat; i++)
				{
					var ind_ajus = "ajus" + i; 
					var ajuste = Number(document.getElementById(ind_ajus).innerHTML);
					var monto_actual = Number(document.getElementById('txtajuste').value);
					var total_ajuste = total_ajuste + ajuste;					
				}
				var total = total_ajuste + monto_actual;
				var total2 = parseFloat(Math.round(total*100)/100);
				if(total2 > montop)
				{					
					alert("El Monto de las patologias Supera al Monto del Presupuesto del Caso");
					result = false;
				}
			}
		}
		return result;
	}
//////////////////////////////////////FUNCIONES AJAX////////////////////////////////////
	function carga_patologia()
	{	
		//Este Codigo va en el condicional de la siguiente Linea "&& valida_exist_pat() == true"
		if(valida_envio_pat() == true && valida_exist_pat() == true 
														&& valida_cod_Anticonceptivo() == true && valida_monto_pres() == true)
		{			
			var bool = baremo_excedente_disponible();			
			 if(bool == true)
			 {				 
    		getData('ajax/patologias/patoaso.php','resp_patologia', 
    				'&var='+document.getElementById('btnAgregar').value+
    				'&cedula='+document.getElementById('cedula').innerHTML+
					'&carta='+document.getElementById('carta').innerHTML+ 
					'&control='+document.getElementById('control').innerHTML+					
					'&aniobaremo='+document.getElementById('cbobaremo').value+
					'&idbaremo='+document.getElementById('idbaremo').innerHTML+
					'&descbaremo='+document.getElementById('descbaremo').innerHTML+ 
					'&montobaremo='+document.getElementById('montobaremo').innerHTML+
					'&ajuste='+document.getElementById('txtajuste').value+
					'&inc=inc&cedulaLog='+document.getElementById('usu_cedula').value+
					'&nombreUsu='+document.getElementById('usu_nombre').value,
					'var nulo=null;');
			 }
		}
	}
	function autoriza_caso()
	{	
		var montod = parseFloat(document.getElementById('montod').innerHTML);
		
		var monto_total = 0; //parseFloat(document.getElementById('total').innerHTML);
		var monto_disp = montod - monto_total;							
	    var porcentaje = 100;
		if(monto_disp < 0) { monto_disp = 0; }	
		
			if(document.getElementById('total_pat')) 
			{ 
				//if(document.getElementById('hdnresumen').value !=''){
	
					getData('ajax/patologias/patoaso.php','fin_patologia', 
				  			'&var=Actualiza&cedula='+document.getElementById('cedula').innerHTML+
				  			'&carta='+document.getElementById('carta').innerHTML+
							'&diagnostico='+document.getElementById('txtdiagnostico').value+
							'&observaciones='+document.getElementById('txtobservaciones').value+
				  			'&control='+document.getElementById('control').innerHTML+
							'&centro_uni='+document.getElementById('centro_uni').value+
							'&rif_clinica='+document.getElementById('hdnrif').value+
							'&cod_patologia_eval='+document.getElementById('bar_id1').innerHTML+
				  			'&mod='+document.getElementById('modalidad').innerHTML+
				  			'&inc=inc&porcentaje='+porcentaje+
							'&cedulaLog='+document.getElementById('usu_cedula').value+
				  			'&nombreUsu='+document.getElementById('usu_nombre').value+
							'&montod='+monto_disp,
				  			'var nulo=null;');				
						document.getElementById('btnImprimir').disabled = false; 

				//}
				//else
				//{
					//alert('Debe realizar el Resumen Medico para Registrar la Patologia');
				
				//}
		} 
		else 
			if(!document.getElementById('total_pat'))
				{ alert('No Hay Patologias Agregadas'); }
	}
