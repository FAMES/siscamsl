// JavaScript Document
//Funcion que verifica la carga del formulario si viene con data o no
	function carga_clinicas()
	{
		if(document.getElementById('hdnres_id').value != '')
		{			
			for(var i = 1; i < 4; i++)
			{	
				var index_cli = 'clinica'+i;
				var index_clirif = 'nom_centro'+i;
				var index_pro = 'proveedor'+i;				
				var index_prorif = 'nom_material'+i;
				for(var j = 1; j < document.getElementById(index_cli).length; j++)
				{					
					if(document.getElementById(index_cli).options[j].value == document.getElementById(index_clirif).value)
					{
						document.getElementById(index_cli).options[j].selected=true;
					}
				}
				for(var j = 1; j < document.getElementById(index_pro).length; j++)
				{					
					if(document.getElementById(index_pro).options[j].value == document.getElementById(index_prorif).value)
					{
						document.getElementById(index_pro).options[j].selected=true;
					}
				}		
			}
		}
		else
		{
			desbloquear(document.formresumen);
			document.getElementById('btnGuardar').disabled = false;	
			document.getElementById('hdnaccion').value = 'INSERTAR';
		}
	}	
	
	//Funcion que verifica el correcto envio de la data a la impresion o a guardar en la base de datos
		function valida_envio_res()
		{
			var ver_doc_centro = true;	
			var ver_doc_material = false;	
			var enc = false;
			var enc_mat = false;
			var item_ver = 0;
			//Verifica que se haya marcado alguna modalidad de Atencion
			var trat_med = document.getElementById('trat_med_quiru');
			var emer = document.getElementById('emergencias');
			var pago_centro = document.getElementById('centros_salud');
			var ayuda = document.getElementById('ayuda_solidaria');
					if(trat_med.checked == false && emer.checked == false && pago_centro.checked == false && ayuda.checked == false)
					{ 
						alert('Debe Seleccionar una Modalidad de Atencion'); 
						return false; 
					}
			//Verifico que se haya seleccionado algun tipo de documento en 'CENTRO DE SALUD' o 'MATERIAL DE SINTESIS'
			var pres_centro = document.getElementById('centro_presupuesto');
			var fact_centro = document.getElementById('centro_factura');
			var pres_material = document.getElementById('material_presupuesto');
			var fact_material = document.getElementById('material_factura');			
					if(pres_centro.checked == false && fact_centro.checked == false)
					{ 
						//alert('Debe Seleccionar un documento'); 
						ver_doc_centro = true; 
						//return false; 
					}
					if(pres_material.checked == true || fact_material.checked == true)
					{ 
						ver_doc_material = true; 
					}					
			//Verifico que se haya codificado patologias al caso
			if(document.getElementById('patologias').innerHTML == '')
			{
				alert("DEBE CODIFICAR AL MENOS 1 PATOLOGIA A ESTE CASO");				
				return false;
			}		
			//Verifico que se haya Seleccionado Algun Item de Presupuesto/Factura en 'CENTRO DE SALUD'
					/*if(ver_doc_centro == true) 
					{
						for(var i = 1; i <= 4; i++)
						{
							var sel_centro = "sel_centro" + i;
							var item_sel = document.getElementById(sel_centro).checked;						
							if(item_sel == true) 
							{ 
								var item_ver = i; 
								enc = true; 
							}
						}
							if(enc!=true) 
							{ 
								alert('Debe Seleccionar un Presupuesto'); 
								return false; 
							}
					}*/
			//Verifico que se haya Seleccionado Algun Item de Presupuesto/Factura en 'MATERIAL DE SINTESIS'
					if(ver_doc_material == true)
					{
						for(var j = 1; j <= 4; j++)	
						{
							var sel_material = "sel_material"+j;							
							item_sel_mat = document.getElementById(sel_material).checked;
							if(item_sel_mat == true) 
							{ 
								var item_ver_mat = j; 
								var enc_mat = true; 
							}							
						}
							if(enc_mat!=true) 
							{ 
								alert('Debe Seleccionar algun Proveedor');  
								return false; 
							}
						//Verifico que el proveedor correspondiente al Item seleccionado sea valida en 'MATERIAL DE SINTESIS'
			var item_pro = "proveedor" + item_ver_mat;
			var proveedor = document.getElementById(item_pro);
							if(proveedor.value == "") 
							{ 
								alert('Debe Seleccionar algun Proveedor'); 
								proveedor.focus(); 
								return false; 
							}
							else 
							{ 
								document.getElementById('nom_material').value = proveedor.value; 
							}
							
			//Verifico el Monto deL Proveedor Seleccionado en MATERIAL DE SINTESIS''
			var item_monto_material = "monto_material"+item_ver_mat;
			var monto_material = document.getElementById(item_monto_material);
			 				if(monto_material.value == "" || monto_material.value == 0) 
							{ 
								alert('Debes Colocar un Monto'); 
								monto_material.focus(); 
								return false; 
							}	 		 	    
							else 
							{ 
								document.getElementById('monto_material').value = monto_material.value; 
							}
					}
					
					
			//Verifico la Clinica correspondiente al Item seleccionado sea valida en 'CENTRO DE SALUD'
			/*var item_cli = "clinica" + item_ver;
			var clinica = document.getElementById(item_cli);
					if(clinica.value == "") 
					{ 
						alert('Debe Seleccionar alguna Clinica'); 
						clinica.focus(); 
						return false; 
					}
					else 
					{ 
						document.getElementById('nom_centro').value = clinica.value; 
					}
			//Verifico el Monto de la Clinica Seleccionada en 'CENTRO DE SALUD'
			var item_monto_centro = "monto_centro"+item_ver;
			var monto_centro = document.getElementById(item_monto_centro);
			 	   if(monto_centro.value == "" || monto_centro.value == 0) 
				   { 
				   	    alert('Debes Colocar un Monto'); 
						monto_centro.focus(); 
						return false; 
				   } 
			 	   else 
				   { 
				   		document.getElementById('monto_centro').value = monto_centro.value; 
			       } */					  
		}
		
		//Verifica la seleccion de presupuestos o facturas en la 'Descripcion de Presupuesto/factura'
		function verifica_doc()
		{
			var centro_ppto = document.getElementById("centro_presupuesto").checked;
			var centro_fact = document.getElementById("centro_factura").checked;
			var material_ppto = document.getElementById("material_presupuesto").checked;
			var material_fact = document.getElementById("material_factura").checked;
			//Verificamos  en Centro de salud
			if(centro_ppto == true)
			{
				//document.getElementById("sel_centro1").disabled = false;
				//document.getElementById("clinica1").disabled = false;
				//document.getElementById("monto_centro1").disabled = false;	
				document.getElementById("centro2").style.visibility = "visible";
				document.getElementById("centro3").style.visibility = "visible";
				//document.getElementById("centro4").style.visibility = "visible";
			}
			else if(centro_fact == true)
			{		
				//document.getElementById("sel_centro1").disabled = false;
				//document.getElementById("clinica1").disabled = false;
				//document.getElementById("monto_centro1").disabled = false;
				//document.getElementById("sel_centro1").checked = true;
				actualiza_total(); 
				document.getElementById("centro2").style.visibility = "hidden";
				document.getElementById("centro3").style.visibility = "hidden";
				//document.getElementById("centro4").style.visibility = "hidden";				
			}
			//Verificamos  en Material de Sintesis
			if(material_ppto == true)
			{
				//document.getElementById("sel_material1").disabled = false;
				//document.getElementById("proveedor1").disabled = false;
				//document.getElementById("monto_material1").disabled = false;
				document.getElementById("material2").style.visibility = "visible";
				document.getElementById("material3").style.visibility = "visible";
				//document.getElementById("material4").style.visibility = "visible";
			}
			else if(material_fact == true) 
			{	
				//document.getElementById("sel_material1").disabled = false;
				//document.getElementById("proveedor1").disabled = false;
				//document.getElementById("monto_material1").disabled = false;
				document.getElementById("sel_material1").checked = true;
				actualiza_total(); 
				document.getElementById("material2").style.visibility = "hidden";
				document.getElementById("material3").style.visibility = "hidden";
				//document.getElementById("material4").style.visibility = "hidden";				
			}			
		}
		
		//funcion que calcula los subtotales de Centro de Salud y Material de Sintesis y el Total de Ambas
		function actualiza_total()
		{				
				//Inicializo las variables
				var total_centro = 0;
				var total_material = 0;
				var total = 0;
				document.getElementById("total").innerHTML = 0;
				
			for(var i = 1; i < 4; i++)
			{
				//Inicializo los Indices 
				var sel_centro = "sel_centro"+i;
				var monto_centro = "monto_centro"+i;
				var sel_material = "sel_material"+i;
				var monto_material = "monto_material"+i;
				//Verifica el listado de Centro  de Salud
				
				if(document.getElementById(sel_centro).checked == true)
				{
					document.getElementById('hdnsel_centro').value = i;
					total_centro = Number(document.getElementById(monto_centro).value);	
					document.getElementById('subTotal_centro').value = total_centro;
				}	
				//Verifica el listado de Materia de Sintesis
				if(document.getElementById(sel_material).checked == true)
				{
					document.getElementById('hdnsel_material').value = i;
					total_material = Number(document.getElementById(monto_material).value);
					document.getElementById('subTotal_material').value = total_material;
				}
				//Asigno los totales a los campos respectivos
				total = total_centro + total_material;
				document.getElementById("total").innerHTML = FormatoMoneda(total) + " Bs.";
				document.getElementById("total_proc").value = total;
			}			
		}	
		
	//Funcion que verifica que modalidad esta actualmente seleccionada para añadirla a una cadena para enviar a Base de Datos
		function verifica_mod()
		{			
 			var trat_med = document.getElementById('trat_med_quiru');
			var emer = document.getElementById('emergencias');
			var pago_centro = document.getElementById('centros_salud');
			var ayuda = document.getElementById('ayuda_solidaria');
			var cadena = "";
			//Verifico que modalidades estan seleccionadas y las encadeno en una variable
			if(trat_med.checked == true) { cadena = trat_med.name+"/"; } else { cadena = "/"; } 
			if(pago_centro.checked == true) { cadena = cadena + pago_centro.name+"/"; } else { cadena = cadena + "/"; }
			if(emer.checked == true) { cadena = cadena + emer.name+"/"; } else { cadena = cadena + "/"; }
			if(ayuda.checked == true) { cadena = cadena + ayuda.name+"/"; }  else { cadena = cadena + "/"; }
			//Guardo la cadena en un campo oculto para enviarlo a Base de Datos
			document.getElementById('hdnmod').value = cadena;			
		}
//funcion que verifica la seleccion de documentos en 'CENTRO DE SALUD Y MATERIAL DE SISNTESIS' 
		function verifica_desc()
		{
			var centro_fact = document.getElementById('centro_factura');
			var centro_pres = document.getElementById('centro_presupuesto');
			var material_fact = document.getElementById('material_factura');
			var material_pres = document.getElementById('material_presupuesto');
			if(centro_fact.checked == true) { document.getElementById('doc_centro').value = 'FACTURA'; }
			else if(centro_pres.checked == true) { document.getElementById('doc_centro').value = 'PRESUPUESTO'; }
			if(material_fact.checked == true) { document.getElementById('doc_material').value = 'FACTURA'; }
			else if(material_pres.checked == true) { document.getElementById('doc_material').value = 'PRESUPUESTO';}			
		}
		
//////////////////////////////FUNCIONES DE LOS BOTONES////////////////////////////////////////////
	function btnImprimir_res()
	{		
		if(valida_envio_res() != false) 
           { 
           document.getElementById('hdnconsulta').value = 'boton_Modificar'; 		 
		   document.formresumen.action = 'reports/report_resumen.php'; 
		   document.getElementById('formresumen').submit(); 
		   }		  
	}
	
	function btnEliminar_res()
	{
				
		if(confirm('Esta Seguro de Eliminar el Registrollllllllll'))  
		{
			document.getElementById('hdnaccion').value = 'ELIMINAR';
			
			getData('ajax/patologias/resumen.php', 'res',  
					'&ACCION='+document.getElementById('hdnaccion').value+'&CARTA='+document.getElementById('carta').innerHTML,
					'var nulo=null;'); 
		}
	}
	
	function btnModificar_res()
	{
		desbloquear_secundario(document.formresumen); 	
		document.getElementById('btnGuardar').disabled = false;
		document.getElementById('hdnaccion').value = 'MODIFICAR';
		document.getElementById('hdnconsulta').value = 'boton_Modificar'; 
		//document.getElementById('btnImprimir').disabled = false; 
		document.getElementById('btnEliminar').disabled = true;
		
		verifica_desc();
		
	}
	
	function btnGuardar_res()
	{		
		if(valida_envio_res() != false) 
		{ 
			getData('ajax/patologias/resumen.php','res',
					'&ACCION='+document.getElementById('hdnaccion').value+
					'&NCONTROL='+document.getElementById('control').innerHTML+
					'&CARTA='+document.getElementById('carta').innerHTML+
					'&CEDULA='+document.getElementById('cedula').innerHTML+
					'&RESUMEN='+document.getElementById('txtresumen').value+
					'&MOD='+document.getElementById('hdnmod').value+
					'&SEL_CENTRO='+document.getElementById('hdnsel_centro').value+
					'&DOC_CENTRO='+document.getElementById('doc_centro').value+
					'&NOM_CENTRO='+document.getElementById('clinica1').value+
					'&MONTO_CENTRO='+document.getElementById('monto_centro1').value+
					'&NOM_CENTRO2='+document.getElementById('clinica2').value+
					'&MONTO_CENTRO2='+document.getElementById('monto_centro2').value+
					'&NOM_CENTRO3='+document.getElementById('clinica3').value+
					'&MONTO_CENTRO3='+document.getElementById('monto_centro3').value+
					'&SEL_MATERIAL='+document.getElementById('hdnsel_material').value+
					'&DOC_MATERIAL='+document.getElementById('doc_material').value+
					'&NOM_MATERIAL='+document.getElementById('proveedor1').value+
					'&MONTO_MATERIAL='+document.getElementById('monto_material1').value+
					'&NOM_MATERIAL2='+document.getElementById('proveedor2').value+
					'&MONTO_MATERIAL2='+document.getElementById('monto_material2').value+
					'&NOM_MATERIAL3='+document.getElementById('proveedor3').value+
					'&MONTO_MATERIAL3='+document.getElementById('monto_material3').value+
					'&MONTO_TOTAL='+document.getElementById('total_proc').value+
					'&USU_CEDULA='+document.getElementById('hdnusucedul').value,					
					'var nulo=null;');
		} 	
	}	
//////////////////////////////FIN DE FUNCIONES DE LOS BOTONES/////////////////////////////////////
