//FUNCION QUE SE ACTIVA CUANDO SELECCIONA CHECKBOX DE LA LISTA DE BENEFICIOS SOLIDARIOS DE LA RELACION
function seleccion_relacion(valor)
{
	var indice = 'monto'+valor;	 //MONTO DE BENEFICIO SOLIDARIO SELECCIONADO
	var cantidad = document.getElementById('cantidad').value - 1; //TOTAL DE LISTA MENOS 1, DEBIDO A ULTIMO LOOP
	var arr_relacion_sel = new Array(); //ARREGLO DE BENEFICIOS SOLIDARIO(S) SELECCIONADO(S)
	var arr_relacion_nosel = new Array(); //ARREGLO DE BENEFICIOS SOLIDARIO(S) NO SELECCIONADO(S)
	var cont = 0;
	var cont2 = 0;
	
	//RECORRO TODOS LOS REGISTROS DE LA LISTA DE BENEFICIOS SOLIDARIOS
	for(var i = 1; i <= cantidad; i++)
	{
		var indice_i = 'monto'+i;  //MONTO DE BENEFICIO SOLIDARIO
		var rel_i = 'sel_relacion'+i; //CHECKBOX DE LA LISTA DE BENEFICIO SOLIDARIO	
		var carta_i = 'carta'+i; //NUMERO DE BENEFICIO SOLIDARIO
		
		//VERIFICA QUE BENEFICIO  SOLIDARIO ESTA SELECCIONADO O NO PARA REGISTRAR EN ARREGLOS
		if(document.getElementById(rel_i).checked == true)
		{					
			arr_relacion_sel[cont] = document.getElementById(carta_i).innerHTML;
			cont++;
		}
		else 
		{
			arr_relacion_nosel[cont2] = document.getElementById(carta_i).innerHTML;
			cont2++;			
		}
	}
	//COLOCO LOS ARREGLOS EN CAMPOS HTML PARA SU PASE POR AJAX.
	document.getElementById('hdn_relacion_sel').value = arr_relacion_sel;
	document.getElementById('hdn_relacion_nosel').value = arr_relacion_nosel; 
}


//FUNCION QUE VERIFICA QUE SE SELECCIONE AL MENOS UN CASO PARA PODER PROCESAR
function valida_relacion()
{
	var total_sel = document.getElementById('cantidad').value; //CANTIDAD DE REGISTROS DE LA RELACION
	var resultado = false;	//VALOR BOOLEANO DE RETORNO	
	
	//RECORRO LA LISTA DE BENEFICIOS SOLIDARIOS
	for(var i = 1; i < total_sel; i++)
	{
		var sel = 'sel_relacion'+i; //INDICE DEL CHECKBOX
		if(document.getElementById(sel).checked == true)
		{
			resultado = true;			
		}
	}	
	return resultado;
}

//FUNCION QUE PERMITE LA IMPRESION DE LA RELACION SIEMPRE Y CUANDO QUEDEN CASOS ATADOS A LA RELACION
function reimpresion_relacion()
{
	var total_sel = document.getElementById('cantidad').value; //REGISTROS DE LA RELACION
	var resultado = true; //VALOR BOOLEANO PARA RETORNO
	var contador = 1;
	
	//RECORRO TODOS LOS REGISTROS DE LA RELACION
	for(var i = 1; i < total_sel; i++)
	{
		var sel = 'sel_relacion'+i;
		if(document.getElementById(sel).checked == true) //VERIFICO LOS QUE ESTAN SELECCIONADOS PARA CONTARLOS
		{
			contador++; 
			if(contador == total_sel) //COMPARO SI LOS SELECCIONADOS SON IGUALES AL TOTAL DE REGISTROS
			{ resultado = false; }	//DEVUELVO FALSE PORQ LA RELACION ESTA SEIENDO REVERSADA TOTALMENTE		
		}
	}
	return resultado;
}


//FUNCION QUE EJECUTA EL AJAX PARA EL REGISTRO EN LA BASE DE DATOS
function ajax_reverso_relacion()
{	
	if(valida_relacion()==true)
	{
		if(confirm('�Seguro de Reversar El Beneficio Solidario Seleccionado?'))
		{
    	getData('ajax/ajax_reverso_relacion2.php','reverso',            
				'&relacion='+document.getElementById('hdnrelacion').value+
				'&relsel='+document.getElementById('hdn_relacion_sel').value+
				'&relnosel='+document.getElementById('hdn_relacion_nosel').value+
				'&rif='+document.getElementById('clinica').value+
				'&usuario='+document.getElementById('hdnusucedula').value+
				'&nombreusu='+document.getElementById('hdnusunombre').value+
				'&opcion='+document.getElementById('hdnopcion').value,
				'var null=nulo;');	 
			if(reimpresion_relacion()==true) //VERIFICO SI LA RELACION TODAVIA TIENE BENEFICIO(S) SOLIDARIO(S)
			{
				if(confirm('�Desea Imprimir la Relacion?')) 
				{				
				setTimeout('document.frmrelacion.submit();', 1000);			
				}			
			setTimeout("document.location.href = '?id=reverso_relacion2&cod='+document.getElementById('hdnrelacion').value;", 1000);				
			}
			else
			{
			alert('Se Ha Reversado Exitosamente la Relacion');
			setTimeout("document.location.href = '?id=reverso_relacion'", 1000);					
			}
		}
	}
	else
	{
	alert("Debe seleccionar al menos un Caso");			
	}
}