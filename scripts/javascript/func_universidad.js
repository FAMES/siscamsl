// JavaScript Document	
//CODIGO ESPECIAL PARA SUBIR LOS ARCHIVOS DE FOTOS
	function subeFotos_comi()
		{		
		if(document.getElementById('guardaFoto').value == 'SI')
		{				
			document.getElementById('hdnaccion').value = 'MODIFICARCOMI';
			comi_ajax();						
		}
		}
//FUNCION QUE BLOQUEA LOS CAMPOS DEPENDIENDO LA MODALIDAD 'NUEVO' O 'EDICION'
function bloquear_campos_univ()
{ 
	if(document.getElementById('hdnmod').value!='nuevo')
	{
	document.getElementById('hdnaccion').value = 'AGREGARCOMI';	
	bloquear(window.document.frmuniversidad);
	bloquear(window.document.frmcomision);
	}
	else
	{		
		document.getElementById('hdnaccion').value = 'INSERTAR';
		document.getElementById('hdnacciontodo').value = 'INSERTARTODO';
		document.getElementById('btnEnviarTodo').disabled = false;
	}	
} 
//FUNCION QUE VERIFICA LOS DIGITOS DEL TELEFONO O CELULAR
function valida_telefonos(control) 
	{
		var cont = document.getElementById(control).value.length;
			if(cont < 7) 
			{
				alert("Debe Ingresar todos los Digitos del Telefono"); 
			}
	}
//FUNCION QUE VERIFICA LOS DIGITOS DE LA CEDULA
function cedula(control) 
	{
	 var cont = document.getElementById(control).value.length;
		 if(cont < 7)
		 { alert("Debe Ingresar todos los digitos de la cedula"); }
	}

function des_esp()
{
	if(document.getElementById("cboservmed").value != 'SI') {
		//document.getElementById("cbopaises").options[0].selected = true;
		document.getElementById("txtespecialidades").disabled = true;
		document.getElementById("txtespecialidades").value = "No Aplica";
		//document.getElementById("cbopaises").options[0].value = "1";
	}
	else
	{
		document.getElementById("txtespecialidades").disabled = false;
		//document.getElementById("cbopaises").options[0].text = "=Seleccione=";
	}

}
//FUNCION QUE VERIFICA LA ESTRUCTURA DEL CORREO
function valida_correo(control) 
			{
				var valor = document.getElementById(control).value;
					if(document.getElementById(control).value != "") 
					{
						if( !(/\w{1,}[@][\w\-]{1,}([.]([\w\-]{1,})){1,3}$/.test(valor)) ) 
						{
							alert("Debe Ingresar un correo valido");
						}
					}
					else 
					{
						alert("Debe Ingresar un correo valido");
					}
		    }
			
//FUNCIONES PARA VALIDAR CADA BOTON
//BOTON MODIFICAR DE LA UNIVERSIDAD
function btnModificar_univ()
{
	document.getElementById('hdnaccion').value ='MODIFICAR'; 
	desbloquear_secundario(window.document.frmuniversidad);		
}
//BOTON MODIFICAR TODO
function btnModificar_Todo()
{
	document.getElementById('btnEnviarComi').style.display = 'none'; 
	document.getElementById('btnEnviarTodo').style.display = 'BLOCK'; 
	document.getElementById('btnEnviarTodo').disabled = false; 
	desbloquear_secundario(window.document.frmuniversidad); 
	desbloquear(window.document.frmcomision); 
	document.getElementById('hdnacciontodo').value='MODIFICARTODO';	
	
}
//BOTON MODIFICAR DE LA COMISION DE SALUD
function btnModificar_Comi()
{	
		//alert(document.getElementById('hdnidc').value);		
		if(document.getElementById('hdnidc').value != '')
		{						
				
		//document.getElementById('btnEnviarTodo').style.display = 'NONE'; 
		document.getElementById('btnEnviarComi').disabled = false; 
		document.getElementById('hdnaccion').value='MODIFICARCOMI'; 
		desbloquear(window.document.frmcomision);		
		}
		else
		{
						
			//document.getElementById('btnEnviarTodo').style.display = 'NONE'; 
			document.getElementById('btnEnviarComi').disabled = false; 
			desbloquear(window.document.frmcomision);
			document.getElementById('hdnaccion').value='AGREGARCOMI'; 
		}		
}
//-------------------------------FUNCIONES AJAX----------------------------------//
//FUNCION AJAX SOLO PARA INSERTAR UNIVERSIDADES
function uni_ajax()
{
	getData('ajax/ajax_universidad.php','res',
  		  '&NOMBRE_UNI='+document.getElementById('txtnombre').value+
          '&ESTADO='+document.getElementById('cboestado').value+
          '&CEDULA_USU='+document.getElementById('hdnusucedul').value+
          '&DIRECCION='+document.getElementById('txtdireccion').value+
		  '&MSUCRE='+document.getElementById('cbocodsucre').value+
		  '&ESTATUS='+document.getElementById('cboestatus').value+
          '&COD_AREA='+document.getElementById('hdncodigo').value+
          '&TELEFONO='+document.getElementById('txttelefono').value+
		  '&COD_CEL='+document.getElementById('cbocodcel').value+
		  '&MSUCRE='+document.getElementById('cbocodsucre').value+
          '&CELULAR='+document.getElementById('txtcel').value+
          '&CIUDAD='+document.getElementById('cbociudad').options[document.getElementById('cbociudad').selectedIndex].text+
          '&MUNICIPIO='+document.getElementById('cbomunicipio').value+
          '&NOMBRE_USU='+document.getElementById('hdnusunombre').value+
		  '&ID_UNI='+document.getElementById('hdnid').value+
		  '&SERV_MED='+document.getElementById('cboservmed').value+
		  '&ESPECIALIDADES='+document.getElementById('txtespecialidades').value+
		  '&CENTROS_PUB='+document.getElementById('txtcentrosaludpub').value+
		  '&MATRICULA='+document.getElementById('txtmatricula').value+
		  '&ACCION='+document.getElementById('hdnaccion').value
          ,'var null=nulo;');	
}

//FUNCION AJAX SOLO PARA INSERTAR COMISIONES
function comi_ajax()
{
	getData('ajax/ajax_universidad.php','res',
		'&ID_UNI='+document.getElementById('hdnid').value+
		'&ID_COMI='+document.getElementById('hdnidc').value+
		'&CEDULA_USU='+document.getElementById('hdnusucedul').value+
		'&NOMBRE_USU='+document.getElementById('hdnusunombre').value+
		'&NOMBRE_JEFB='+document.getElementById('txtnombrejefb').value+
		'&CEDULA_JEFB='+document.getElementById('txtcedulajefb').value+
		'&PROFESIONJEFB='+document.getElementById('txtprofesionjefb').value+
		'&TLFHABJEFB='+document.getElementById('txttlfhabjefb').value+
		'&TLFCELJEFB='+document.getElementById('txttlfceljefb').value+
		'&CORREOJEFB='+document.getElementById('txtcorreojefb').value+
		'&FOTOJEFB='+document.getElementById('hdnfotojefb').value+		
        '&NOMBRECOR='+document.getElementById('txtnombrecor').value+
        '&CEDULACOR='+document.getElementById('txtcedulacor').value+ 
        '&PROFESIONCOR='+document.getElementById('txtprofesioncor').value+
        '&TLFHABCOR='+document.getElementById('txttlfhabcor').value+
        '&TLFCELCOR='+document.getElementById('txttlfcelcor').value+
        '&CORREOCOR='+document.getElementById('txtcorreocor').value+
		'&FOTOCOR='+document.getElementById('hdnfotocor').value+
        '&NOMBREASIS='+document.getElementById('txtnombreasis').value+
        '&CEDULAASIS='+document.getElementById('txtcedulaasis').value+ 
        '&PROFESIONASIS='+document.getElementById('txtprofesionasis').value+
        '&TLFHABASIS='+document.getElementById('txttlfhabasis').value+
        '&TLFCELASIS='+document.getElementById('txttlfcelasis').value+
        '&CORREOASIS='+document.getElementById('txtcorreoasis').value+
		'&FOTOASIS='+document.getElementById('hdnfotoasis').value+
        '&NOMBREREPE='+document.getElementById('txtnombrerepe').value+
        '&CEDULAREPE='+document.getElementById('txtcedularepe').value+ 
        '&PROFESIONREPE='+document.getElementById('txtprofesionrepe').value+
        '&TLFHABREPE='+document.getElementById('txttlfhabrepe').value+
        '&TLFCELREPE='+document.getElementById('txttlfcelrepe').value+
        '&CORREOREPE='+document.getElementById('txtcorreorepe').value+
		'&FOTOREPE='+document.getElementById('hdnfotorepe').value+		
		'&ACCION='+document.getElementById('hdnaccion').value,		   
        'var null=nulo;');		
}

//FUNCION AJAX PARA INSERTAR UNIVERSIDADES Y COMISIONES DE SALUD
function comiuniv_ajax()
{	
	getData('ajax/ajax_universidad.php','res',
		'&ID_UNI='+document.getElementById('hdnid').value+
		'&ID_COMI='+document.getElementById('hdnidc').value+
		'&CEDULA_USU='+document.getElementById('hdnusucedul').value+
		'&NOMBRE_USU='+document.getElementById('hdnusunombre').value+
		'&NOMBRE_UNI='+document.getElementById('txtnombre').value+
        '&ESTADO='+document.getElementById('cboestado').value+
		'&DIRECCION='+document.getElementById('txtdireccion').value+
		'&MSUCRE='+document.getElementById('cbocodsucre').value+
		'&ESTATUS='+document.getElementById('cboestatus').value+
        '&COD_AREA='+document.getElementById('hdncodigo').value+
        '&TELEFONO='+document.getElementById('txttelefono').value+
		'&COD_CEL='+document.getElementById('cbocodcel').value+
        '&CELULAR='+document.getElementById('txtcel').value+
        '&CIUDAD='+document.getElementById('cbociudad').options[document.getElementById('cbociudad').selectedIndex].text+
        '&MUNICIPIO='+document.getElementById('cbomunicipio').value+
		'&NOMBRE_JEFB='+document.getElementById('txtnombrejefb').value+
		'&CEDULA_JEFB='+document.getElementById('txtcedulajefb').value+
		'&PROFESIONJEFB='+document.getElementById('txtprofesionjefb').value+
		'&TLFHABJEFB='+document.getElementById('txttlfhabjefb').value+
		'&TLFCELJEFB='+document.getElementById('txttlfceljefb').value+
		'&CORREOJEFB='+document.getElementById('txtcorreojefb').value+
		'&FOTOJEFB='+document.getElementById('hdnfotojefb').value+		
		'&NOMBRECOR='+document.getElementById('txtnombrecor').value+
        '&CEDULACOR='+document.getElementById('txtcedulacor').value+ 
        '&PROFESIONCOR='+document.getElementById('txtprofesioncor').value+
        '&TLFHABCOR='+document.getElementById('txttlfhabcor').value+
        '&TLFCELCOR='+document.getElementById('txttlfcelcor').value+
        '&CORREOCOR='+document.getElementById('txtcorreocor').value+
		'&FOTOCOR='+document.getElementById('hdnfotocor').value+
        '&NOMBREASIS='+document.getElementById('txtnombreasis').value+
        '&CEDULAASIS='+document.getElementById('txtcedulaasis').value+ 
        '&PROFESIONASIS='+document.getElementById('txtprofesionasis').value+
        '&TLFHABASIS='+document.getElementById('txttlfhabasis').value+
        '&TLFCELASIS='+document.getElementById('txttlfcelasis').value+
        '&CORREOASIS='+document.getElementById('txtcorreoasis').value+
		'&FOTOASIS='+document.getElementById('hdnfotoasis').value+
        '&NOMBREREPE='+document.getElementById('txtnombrerepe').value+
        '&CEDULAREPE='+document.getElementById('txtcedularepe').value+ 
        '&PROFESIONREPE='+document.getElementById('txtprofesionrepe').value+
        '&TLFHABREPE='+document.getElementById('txttlfhabrepe').value+
        '&TLFCELREPE='+document.getElementById('txttlfcelrepe').value+
        '&CORREOREPE='+document.getElementById('txtcorreorepe').value+
		'&FOTOREPE='+document.getElementById('hdnfotorepe').value+
		'&SERV_MED='+document.getElementById('cboservmed').value+
		'&ESPECIALIDADES='+document.getElementById('txtespecialidades').value+
		'&CENTROS_PUB='+document.getElementById('txtcentrosaludpub').value+
		'&MATRICULA='+document.getElementById('txtmatricula').value+
		'&ACCION='+document.getElementById('hdnacciontodo').value,		   
        'var null=nulo;');	
}
