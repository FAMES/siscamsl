function inicializa_usuarios()
{
	if (document.getElementById('hdnmod').value=='nuevo') 
		{		
			desbloquear(document.frmusuarios);	
			document.frmusuarios.btnEnviar.disabled=false;
		}		
}

function btn_modusu()
{ 
	document.frmusuarios.oculto.value='modificar';
	desbloquear_secundario(document.frmusuarios);	
}

function btn_modusupass()
{
	document.frmusuarios.oculto.value='modificarP';
	desbloquear_principal(document.frmusuarios);
	document.frmusuarios.btnEnviar.disabled=false;		
}

function btn_eliusu()
{
	if(confirm('�Desea eliminar el Usuario? Ser� eliminado todo el contenido relacionado?'))
		{	
			document.frmusuarios.oculto.value='eliminar'; 
			desbloquear(document.frmusuarios);
			ajax_usuarios();
		}	
}

function ajax_usuarios()
{
	if(validar(document.frmusuarios)==true)
	{ 			 
      		getData('ajax/ajax_usuario.php','usuario',
            '&cedula='+document.getElementById('txtcedula').value+
            '&nombre='+document.getElementById('txtnombre').value+
            '&dpto='+document.getElementById('txtdptto').value+
            '&perid='+document.getElementById('cboper').value+
            '&login='+document.getElementById('Zloginu').value+
            '&passw='+document.getElementById('Zpassw').value+
            '&estatus='+document.getElementById('cboestatus').value+
            '&mod='+document.getElementById('hdnmod').value+
            '&usuario='+document.getElementById('hdnusuario').value+
            '&acceso='+document.getElementById('cboacceso').value+
            '&oculto='+document.getElementById('oculto').value,
			'var null=nulo;'); 
	}	
}